﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>
struct Dictionary_2_t1855157670;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3175182372.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23907470188.h"
#include "Stores_UnityEngine_Purchasing_AppStore379104228.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1689433262_gshared (Enumerator_t3175182372 * __this, Dictionary_2_t1855157670 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1689433262(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3175182372 *, Dictionary_2_t1855157670 *, const MethodInfo*))Enumerator__ctor_m1689433262_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m18154257_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18154257(__this, method) ((  Il2CppObject * (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18154257_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3295310809_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3295310809(__this, method) ((  void (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3295310809_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3230011756_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3230011756(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3230011756_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2643368291_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2643368291(__this, method) ((  Il2CppObject * (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2643368291_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3348204795_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3348204795(__this, method) ((  Il2CppObject * (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3348204795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3968011081_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3968011081(__this, method) ((  bool (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_MoveNext_m3968011081_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3907470188  Enumerator_get_Current_m3550708493_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3550708493(__this, method) ((  KeyValuePair_2_t3907470188  (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_get_Current_m3550708493_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m739060958_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m739060958(__this, method) ((  int32_t (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_get_CurrentKey_m739060958_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3291390238_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3291390238(__this, method) ((  Il2CppObject * (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_get_CurrentValue_m3291390238_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m944422548_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_Reset_m944422548(__this, method) ((  void (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_Reset_m944422548_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m47465931_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m47465931(__this, method) ((  void (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_VerifyState_m47465931_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m866906857_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m866906857(__this, method) ((  void (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_VerifyCurrent_m866906857_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1941330462_gshared (Enumerator_t3175182372 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1941330462(__this, method) ((  void (*) (Enumerator_t3175182372 *, const MethodInfo*))Enumerator_Dispose_m1941330462_gshared)(__this, method)
