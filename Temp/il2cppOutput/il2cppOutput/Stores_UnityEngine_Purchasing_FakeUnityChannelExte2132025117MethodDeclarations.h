﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.FakeUnityChannelExtensions
struct FakeUnityChannelExtensions_t2132025117;
// System.String
struct String_t;
// System.Action`3<System.Boolean,System.String,System.String>
struct Action_3_t3864773326;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.FakeUnityChannelExtensions::ValidateReceipt(System.String,System.Action`3<System.Boolean,System.String,System.String>)
extern "C"  void FakeUnityChannelExtensions_ValidateReceipt_m3893525072 (FakeUnityChannelExtensions_t2132025117 * __this, String_t* ___transactionId0, Action_3_t3864773326 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.FakeUnityChannelExtensions::GetLastPurchaseError()
extern "C"  String_t* FakeUnityChannelExtensions_GetLastPurchaseError_m387143437 (FakeUnityChannelExtensions_t2132025117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.FakeUnityChannelExtensions::.ctor()
extern "C"  void FakeUnityChannelExtensions__ctor_m1536633499 (FakeUnityChannelExtensions_t2132025117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
