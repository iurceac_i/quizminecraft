﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.NativeStoreProvider
struct NativeStoreProvider_t4107132677;
// UnityEngine.Purchasing.INativeStore
struct INativeStore_t3203646079;
// UnityEngine.Purchasing.IUnityCallback
struct IUnityCallback_t1155931721;
// UnityEngine.Purchasing.Extension.IPurchasingBinder
struct IPurchasingBinder_t2054654539;
// Uniject.IUtil
struct IUtil_t2188430191;
// UnityEngine.Purchasing.INativeAppleStore
struct INativeAppleStore_t2240226449;
// UnityEngine.Purchasing.INativeTizenStore
struct INativeTizenStore_t513596045;
// UnityEngine.Purchasing.INativeFacebookStore
struct INativeFacebookStore_t3367400535;

#include "codegen/il2cpp-codegen.h"
#include "Stores_UnityEngine_Purchasing_AppStore379104228.h"

// UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.NativeStoreProvider::GetAndroidStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.AppStore,UnityEngine.Purchasing.Extension.IPurchasingBinder,Uniject.IUtil)
extern "C"  Il2CppObject * NativeStoreProvider_GetAndroidStore_m3063670543 (NativeStoreProvider_t4107132677 * __this, Il2CppObject * ___callback0, int32_t ___store1, Il2CppObject * ___binder2, Il2CppObject * ___util3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.NativeStoreProvider::GetAndroidStoreHelper(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.AppStore,UnityEngine.Purchasing.Extension.IPurchasingBinder,Uniject.IUtil)
extern "C"  Il2CppObject * NativeStoreProvider_GetAndroidStoreHelper_m2509293545 (NativeStoreProvider_t4107132677 * __this, Il2CppObject * ___callback0, int32_t ___store1, Il2CppObject * ___binder2, Il2CppObject * ___util3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.INativeAppleStore UnityEngine.Purchasing.NativeStoreProvider::GetStorekit(UnityEngine.Purchasing.IUnityCallback)
extern "C"  Il2CppObject * NativeStoreProvider_GetStorekit_m3187446631 (NativeStoreProvider_t4107132677 * __this, Il2CppObject * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.INativeTizenStore UnityEngine.Purchasing.NativeStoreProvider::GetTizenStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.Extension.IPurchasingBinder)
extern "C"  Il2CppObject * NativeStoreProvider_GetTizenStore_m1486348258 (NativeStoreProvider_t4107132677 * __this, Il2CppObject * ___callback0, Il2CppObject * ___binder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.INativeFacebookStore UnityEngine.Purchasing.NativeStoreProvider::GetFacebookStore()
extern "C"  Il2CppObject * NativeStoreProvider_GetFacebookStore_m1327404357 (NativeStoreProvider_t4107132677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.NativeStoreProvider::.ctor()
extern "C"  void NativeStoreProvider__ctor_m1686917727 (NativeStoreProvider_t4107132677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
