﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Action`3<System.Int32[],System.Byte[],System.Int32>
struct Action_3_t676603244;
// System.Action`3<System.Single[],System.Byte[],System.Int32>
struct Action_3_t2127361240;
// System.Action`3<UnityEngine.Vector2[],System.Byte[],System.Int32>
struct Action_3_t3961902885;
// System.Action`3<UnityEngine.Vector3[],System.Byte[],System.Int32>
struct Action_3_t3958575688;
// System.Action`3<UnityEngine.Quaternion[],System.Byte[],System.Int32>
struct Action_3_t1730450702;
// System.Action`3<UnityEngine.Color[],System.Byte[],System.Int32>
struct Action_3_t2179559061;
// System.Action`2<System.Collections.Generic.List`1<System.Int32>,System.Byte[]>
struct Action_2_t2760079778;
// System.Action`2<System.Collections.Generic.List`1<System.Single>,System.Byte[]>
struct Action_2_t306807534;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Byte[]>
struct Action_2_t415804163;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Byte[]>
struct Action_2_t901991902;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Quaternion>,System.Byte[]>
struct Action_2_t1584067604;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Color>,System.Byte[]>
struct Action_2_t402030579;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerPrefsX
struct  PlayerPrefsX_t1687815431  : public Il2CppObject
{
public:

public:
};

struct PlayerPrefsX_t1687815431_StaticFields
{
public:
	// System.Int32 PlayerPrefsX::endianDiff1
	int32_t ___endianDiff1_0;
	// System.Int32 PlayerPrefsX::endianDiff2
	int32_t ___endianDiff2_1;
	// System.Int32 PlayerPrefsX::idx
	int32_t ___idx_2;
	// System.Byte[] PlayerPrefsX::byteBlock
	ByteU5BU5D_t3397334013* ___byteBlock_3;
	// System.Action`3<System.Int32[],System.Byte[],System.Int32> PlayerPrefsX::<>f__mg$cache0
	Action_3_t676603244 * ___U3CU3Ef__mgU24cache0_4;
	// System.Action`3<System.Single[],System.Byte[],System.Int32> PlayerPrefsX::<>f__mg$cache1
	Action_3_t2127361240 * ___U3CU3Ef__mgU24cache1_5;
	// System.Action`3<UnityEngine.Vector2[],System.Byte[],System.Int32> PlayerPrefsX::<>f__mg$cache2
	Action_3_t3961902885 * ___U3CU3Ef__mgU24cache2_6;
	// System.Action`3<UnityEngine.Vector3[],System.Byte[],System.Int32> PlayerPrefsX::<>f__mg$cache3
	Action_3_t3958575688 * ___U3CU3Ef__mgU24cache3_7;
	// System.Action`3<UnityEngine.Quaternion[],System.Byte[],System.Int32> PlayerPrefsX::<>f__mg$cache4
	Action_3_t1730450702 * ___U3CU3Ef__mgU24cache4_8;
	// System.Action`3<UnityEngine.Color[],System.Byte[],System.Int32> PlayerPrefsX::<>f__mg$cache5
	Action_3_t2179559061 * ___U3CU3Ef__mgU24cache5_9;
	// System.Action`2<System.Collections.Generic.List`1<System.Int32>,System.Byte[]> PlayerPrefsX::<>f__mg$cache6
	Action_2_t2760079778 * ___U3CU3Ef__mgU24cache6_10;
	// System.Action`2<System.Collections.Generic.List`1<System.Single>,System.Byte[]> PlayerPrefsX::<>f__mg$cache7
	Action_2_t306807534 * ___U3CU3Ef__mgU24cache7_11;
	// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Byte[]> PlayerPrefsX::<>f__mg$cache8
	Action_2_t415804163 * ___U3CU3Ef__mgU24cache8_12;
	// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Byte[]> PlayerPrefsX::<>f__mg$cache9
	Action_2_t901991902 * ___U3CU3Ef__mgU24cache9_13;
	// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Quaternion>,System.Byte[]> PlayerPrefsX::<>f__mg$cacheA
	Action_2_t1584067604 * ___U3CU3Ef__mgU24cacheA_14;
	// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Color>,System.Byte[]> PlayerPrefsX::<>f__mg$cacheB
	Action_2_t402030579 * ___U3CU3Ef__mgU24cacheB_15;

public:
	inline static int32_t get_offset_of_endianDiff1_0() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___endianDiff1_0)); }
	inline int32_t get_endianDiff1_0() const { return ___endianDiff1_0; }
	inline int32_t* get_address_of_endianDiff1_0() { return &___endianDiff1_0; }
	inline void set_endianDiff1_0(int32_t value)
	{
		___endianDiff1_0 = value;
	}

	inline static int32_t get_offset_of_endianDiff2_1() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___endianDiff2_1)); }
	inline int32_t get_endianDiff2_1() const { return ___endianDiff2_1; }
	inline int32_t* get_address_of_endianDiff2_1() { return &___endianDiff2_1; }
	inline void set_endianDiff2_1(int32_t value)
	{
		___endianDiff2_1 = value;
	}

	inline static int32_t get_offset_of_idx_2() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___idx_2)); }
	inline int32_t get_idx_2() const { return ___idx_2; }
	inline int32_t* get_address_of_idx_2() { return &___idx_2; }
	inline void set_idx_2(int32_t value)
	{
		___idx_2 = value;
	}

	inline static int32_t get_offset_of_byteBlock_3() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___byteBlock_3)); }
	inline ByteU5BU5D_t3397334013* get_byteBlock_3() const { return ___byteBlock_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_byteBlock_3() { return &___byteBlock_3; }
	inline void set_byteBlock_3(ByteU5BU5D_t3397334013* value)
	{
		___byteBlock_3 = value;
		Il2CppCodeGenWriteBarrier(&___byteBlock_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_4() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cache0_4)); }
	inline Action_3_t676603244 * get_U3CU3Ef__mgU24cache0_4() const { return ___U3CU3Ef__mgU24cache0_4; }
	inline Action_3_t676603244 ** get_address_of_U3CU3Ef__mgU24cache0_4() { return &___U3CU3Ef__mgU24cache0_4; }
	inline void set_U3CU3Ef__mgU24cache0_4(Action_3_t676603244 * value)
	{
		___U3CU3Ef__mgU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_5() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cache1_5)); }
	inline Action_3_t2127361240 * get_U3CU3Ef__mgU24cache1_5() const { return ___U3CU3Ef__mgU24cache1_5; }
	inline Action_3_t2127361240 ** get_address_of_U3CU3Ef__mgU24cache1_5() { return &___U3CU3Ef__mgU24cache1_5; }
	inline void set_U3CU3Ef__mgU24cache1_5(Action_3_t2127361240 * value)
	{
		___U3CU3Ef__mgU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_6() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cache2_6)); }
	inline Action_3_t3961902885 * get_U3CU3Ef__mgU24cache2_6() const { return ___U3CU3Ef__mgU24cache2_6; }
	inline Action_3_t3961902885 ** get_address_of_U3CU3Ef__mgU24cache2_6() { return &___U3CU3Ef__mgU24cache2_6; }
	inline void set_U3CU3Ef__mgU24cache2_6(Action_3_t3961902885 * value)
	{
		___U3CU3Ef__mgU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache2_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_7() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cache3_7)); }
	inline Action_3_t3958575688 * get_U3CU3Ef__mgU24cache3_7() const { return ___U3CU3Ef__mgU24cache3_7; }
	inline Action_3_t3958575688 ** get_address_of_U3CU3Ef__mgU24cache3_7() { return &___U3CU3Ef__mgU24cache3_7; }
	inline void set_U3CU3Ef__mgU24cache3_7(Action_3_t3958575688 * value)
	{
		___U3CU3Ef__mgU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache3_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_8() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cache4_8)); }
	inline Action_3_t1730450702 * get_U3CU3Ef__mgU24cache4_8() const { return ___U3CU3Ef__mgU24cache4_8; }
	inline Action_3_t1730450702 ** get_address_of_U3CU3Ef__mgU24cache4_8() { return &___U3CU3Ef__mgU24cache4_8; }
	inline void set_U3CU3Ef__mgU24cache4_8(Action_3_t1730450702 * value)
	{
		___U3CU3Ef__mgU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache4_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_9() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cache5_9)); }
	inline Action_3_t2179559061 * get_U3CU3Ef__mgU24cache5_9() const { return ___U3CU3Ef__mgU24cache5_9; }
	inline Action_3_t2179559061 ** get_address_of_U3CU3Ef__mgU24cache5_9() { return &___U3CU3Ef__mgU24cache5_9; }
	inline void set_U3CU3Ef__mgU24cache5_9(Action_3_t2179559061 * value)
	{
		___U3CU3Ef__mgU24cache5_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache5_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_10() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cache6_10)); }
	inline Action_2_t2760079778 * get_U3CU3Ef__mgU24cache6_10() const { return ___U3CU3Ef__mgU24cache6_10; }
	inline Action_2_t2760079778 ** get_address_of_U3CU3Ef__mgU24cache6_10() { return &___U3CU3Ef__mgU24cache6_10; }
	inline void set_U3CU3Ef__mgU24cache6_10(Action_2_t2760079778 * value)
	{
		___U3CU3Ef__mgU24cache6_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache6_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_11() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cache7_11)); }
	inline Action_2_t306807534 * get_U3CU3Ef__mgU24cache7_11() const { return ___U3CU3Ef__mgU24cache7_11; }
	inline Action_2_t306807534 ** get_address_of_U3CU3Ef__mgU24cache7_11() { return &___U3CU3Ef__mgU24cache7_11; }
	inline void set_U3CU3Ef__mgU24cache7_11(Action_2_t306807534 * value)
	{
		___U3CU3Ef__mgU24cache7_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache7_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_12() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cache8_12)); }
	inline Action_2_t415804163 * get_U3CU3Ef__mgU24cache8_12() const { return ___U3CU3Ef__mgU24cache8_12; }
	inline Action_2_t415804163 ** get_address_of_U3CU3Ef__mgU24cache8_12() { return &___U3CU3Ef__mgU24cache8_12; }
	inline void set_U3CU3Ef__mgU24cache8_12(Action_2_t415804163 * value)
	{
		___U3CU3Ef__mgU24cache8_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache8_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_13() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cache9_13)); }
	inline Action_2_t901991902 * get_U3CU3Ef__mgU24cache9_13() const { return ___U3CU3Ef__mgU24cache9_13; }
	inline Action_2_t901991902 ** get_address_of_U3CU3Ef__mgU24cache9_13() { return &___U3CU3Ef__mgU24cache9_13; }
	inline void set_U3CU3Ef__mgU24cache9_13(Action_2_t901991902 * value)
	{
		___U3CU3Ef__mgU24cache9_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache9_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_14() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cacheA_14)); }
	inline Action_2_t1584067604 * get_U3CU3Ef__mgU24cacheA_14() const { return ___U3CU3Ef__mgU24cacheA_14; }
	inline Action_2_t1584067604 ** get_address_of_U3CU3Ef__mgU24cacheA_14() { return &___U3CU3Ef__mgU24cacheA_14; }
	inline void set_U3CU3Ef__mgU24cacheA_14(Action_2_t1584067604 * value)
	{
		___U3CU3Ef__mgU24cacheA_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cacheA_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_15() { return static_cast<int32_t>(offsetof(PlayerPrefsX_t1687815431_StaticFields, ___U3CU3Ef__mgU24cacheB_15)); }
	inline Action_2_t402030579 * get_U3CU3Ef__mgU24cacheB_15() const { return ___U3CU3Ef__mgU24cacheB_15; }
	inline Action_2_t402030579 ** get_address_of_U3CU3Ef__mgU24cacheB_15() { return &___U3CU3Ef__mgU24cacheB_15; }
	inline void set_U3CU3Ef__mgU24cacheB_15(Action_2_t402030579 * value)
	{
		___U3CU3Ef__mgU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cacheB_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
