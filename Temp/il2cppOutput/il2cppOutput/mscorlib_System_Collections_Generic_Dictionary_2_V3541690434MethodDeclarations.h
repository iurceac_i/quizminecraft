﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>
struct Dictionary_2_t1855157670;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3541690434.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2227412704_gshared (Enumerator_t3541690434 * __this, Dictionary_2_t1855157670 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2227412704(__this, ___host0, method) ((  void (*) (Enumerator_t3541690434 *, Dictionary_2_t1855157670 *, const MethodInfo*))Enumerator__ctor_m2227412704_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m761869775_gshared (Enumerator_t3541690434 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m761869775(__this, method) ((  Il2CppObject * (*) (Enumerator_t3541690434 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m761869775_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m945893211_gshared (Enumerator_t3541690434 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m945893211(__this, method) ((  void (*) (Enumerator_t3541690434 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m945893211_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2832689260_gshared (Enumerator_t3541690434 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2832689260(__this, method) ((  void (*) (Enumerator_t3541690434 *, const MethodInfo*))Enumerator_Dispose_m2832689260_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2540624527_gshared (Enumerator_t3541690434 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2540624527(__this, method) ((  bool (*) (Enumerator_t3541690434 *, const MethodInfo*))Enumerator_MoveNext_m2540624527_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1796106213_gshared (Enumerator_t3541690434 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1796106213(__this, method) ((  Il2CppObject * (*) (Enumerator_t3541690434 *, const MethodInfo*))Enumerator_get_Current_m1796106213_gshared)(__this, method)
