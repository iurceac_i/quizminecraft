﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// QuestionController/UpdateListener
struct UpdateListener_t3189012468;
// System.String
struct String_t;
// Levels[]
struct LevelsU5BU5D_t2050899114;
// AssemblyCSharp.Purchase
struct Purchase_t1644043085;
// SoundController
struct SoundController_t1686593041;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestionController
struct  QuestionController_t445239244  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text QuestionController::question
	Text_t356221433 * ___question_2;
	// UnityEngine.UI.Text QuestionController::answer_0
	Text_t356221433 * ___answer_0_3;
	// UnityEngine.UI.Text QuestionController::answer_1
	Text_t356221433 * ___answer_1_4;
	// UnityEngine.UI.Text QuestionController::answer_2
	Text_t356221433 * ___answer_2_5;
	// UnityEngine.UI.Text QuestionController::answer_3
	Text_t356221433 * ___answer_3_6;
	// UnityEngine.UI.Button QuestionController::btn_1
	Button_t2872111280 * ___btn_1_7;
	// UnityEngine.UI.Button QuestionController::btn_2
	Button_t2872111280 * ___btn_2_8;
	// UnityEngine.UI.Button QuestionController::btn_3
	Button_t2872111280 * ___btn_3_9;
	// UnityEngine.UI.Button QuestionController::btn_4
	Button_t2872111280 * ___btn_4_10;
	// UnityEngine.GameObject QuestionController::star1
	GameObject_t1756533147 * ___star1_11;
	// UnityEngine.GameObject QuestionController::star2
	GameObject_t1756533147 * ___star2_12;
	// UnityEngine.GameObject QuestionController::star3
	GameObject_t1756533147 * ___star3_13;
	// QuestionController/UpdateListener QuestionController::callBack
	Il2CppObject * ___callBack_14;
	// System.String QuestionController::correctAns
	String_t* ___correctAns_15;
	// UnityEngine.GameObject QuestionController::buyPanel
	GameObject_t1756533147 * ___buyPanel_16;
	// UnityEngine.GameObject QuestionController::GamePanel
	GameObject_t1756533147 * ___GamePanel_17;
	// UnityEngine.GameObject QuestionController::twoButtons
	GameObject_t1756533147 * ___twoButtons_18;
	// UnityEngine.GameObject QuestionController::singleButtonPanel
	GameObject_t1756533147 * ___singleButtonPanel_19;
	// UnityEngine.GameObject QuestionController::finalAlertPanel
	GameObject_t1756533147 * ___finalAlertPanel_20;
	// UnityEngine.GameObject QuestionController::levelPanel
	GameObject_t1756533147 * ___levelPanel_21;
	// UnityEngine.GameObject QuestionController::notEnought
	GameObject_t1756533147 * ___notEnought_22;
	// UnityEngine.UI.Text QuestionController::numb_of_compl_mission
	Text_t356221433 * ___numb_of_compl_mission_23;
	// System.Int32 QuestionController::numQuest
	int32_t ___numQuest_24;
	// Levels[] QuestionController::levelGlobal
	LevelsU5BU5D_t2050899114* ___levelGlobal_25;
	// System.Int32 QuestionController::positionLevel
	int32_t ___positionLevel_26;
	// System.Int32 QuestionController::correctAnswer
	int32_t ___correctAnswer_27;
	// AssemblyCSharp.Purchase QuestionController::purch
	Purchase_t1644043085 * ___purch_28;
	// SoundController QuestionController::soundControll
	SoundController_t1686593041 * ___soundControll_29;

public:
	inline static int32_t get_offset_of_question_2() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___question_2)); }
	inline Text_t356221433 * get_question_2() const { return ___question_2; }
	inline Text_t356221433 ** get_address_of_question_2() { return &___question_2; }
	inline void set_question_2(Text_t356221433 * value)
	{
		___question_2 = value;
		Il2CppCodeGenWriteBarrier(&___question_2, value);
	}

	inline static int32_t get_offset_of_answer_0_3() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___answer_0_3)); }
	inline Text_t356221433 * get_answer_0_3() const { return ___answer_0_3; }
	inline Text_t356221433 ** get_address_of_answer_0_3() { return &___answer_0_3; }
	inline void set_answer_0_3(Text_t356221433 * value)
	{
		___answer_0_3 = value;
		Il2CppCodeGenWriteBarrier(&___answer_0_3, value);
	}

	inline static int32_t get_offset_of_answer_1_4() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___answer_1_4)); }
	inline Text_t356221433 * get_answer_1_4() const { return ___answer_1_4; }
	inline Text_t356221433 ** get_address_of_answer_1_4() { return &___answer_1_4; }
	inline void set_answer_1_4(Text_t356221433 * value)
	{
		___answer_1_4 = value;
		Il2CppCodeGenWriteBarrier(&___answer_1_4, value);
	}

	inline static int32_t get_offset_of_answer_2_5() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___answer_2_5)); }
	inline Text_t356221433 * get_answer_2_5() const { return ___answer_2_5; }
	inline Text_t356221433 ** get_address_of_answer_2_5() { return &___answer_2_5; }
	inline void set_answer_2_5(Text_t356221433 * value)
	{
		___answer_2_5 = value;
		Il2CppCodeGenWriteBarrier(&___answer_2_5, value);
	}

	inline static int32_t get_offset_of_answer_3_6() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___answer_3_6)); }
	inline Text_t356221433 * get_answer_3_6() const { return ___answer_3_6; }
	inline Text_t356221433 ** get_address_of_answer_3_6() { return &___answer_3_6; }
	inline void set_answer_3_6(Text_t356221433 * value)
	{
		___answer_3_6 = value;
		Il2CppCodeGenWriteBarrier(&___answer_3_6, value);
	}

	inline static int32_t get_offset_of_btn_1_7() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___btn_1_7)); }
	inline Button_t2872111280 * get_btn_1_7() const { return ___btn_1_7; }
	inline Button_t2872111280 ** get_address_of_btn_1_7() { return &___btn_1_7; }
	inline void set_btn_1_7(Button_t2872111280 * value)
	{
		___btn_1_7 = value;
		Il2CppCodeGenWriteBarrier(&___btn_1_7, value);
	}

	inline static int32_t get_offset_of_btn_2_8() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___btn_2_8)); }
	inline Button_t2872111280 * get_btn_2_8() const { return ___btn_2_8; }
	inline Button_t2872111280 ** get_address_of_btn_2_8() { return &___btn_2_8; }
	inline void set_btn_2_8(Button_t2872111280 * value)
	{
		___btn_2_8 = value;
		Il2CppCodeGenWriteBarrier(&___btn_2_8, value);
	}

	inline static int32_t get_offset_of_btn_3_9() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___btn_3_9)); }
	inline Button_t2872111280 * get_btn_3_9() const { return ___btn_3_9; }
	inline Button_t2872111280 ** get_address_of_btn_3_9() { return &___btn_3_9; }
	inline void set_btn_3_9(Button_t2872111280 * value)
	{
		___btn_3_9 = value;
		Il2CppCodeGenWriteBarrier(&___btn_3_9, value);
	}

	inline static int32_t get_offset_of_btn_4_10() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___btn_4_10)); }
	inline Button_t2872111280 * get_btn_4_10() const { return ___btn_4_10; }
	inline Button_t2872111280 ** get_address_of_btn_4_10() { return &___btn_4_10; }
	inline void set_btn_4_10(Button_t2872111280 * value)
	{
		___btn_4_10 = value;
		Il2CppCodeGenWriteBarrier(&___btn_4_10, value);
	}

	inline static int32_t get_offset_of_star1_11() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___star1_11)); }
	inline GameObject_t1756533147 * get_star1_11() const { return ___star1_11; }
	inline GameObject_t1756533147 ** get_address_of_star1_11() { return &___star1_11; }
	inline void set_star1_11(GameObject_t1756533147 * value)
	{
		___star1_11 = value;
		Il2CppCodeGenWriteBarrier(&___star1_11, value);
	}

	inline static int32_t get_offset_of_star2_12() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___star2_12)); }
	inline GameObject_t1756533147 * get_star2_12() const { return ___star2_12; }
	inline GameObject_t1756533147 ** get_address_of_star2_12() { return &___star2_12; }
	inline void set_star2_12(GameObject_t1756533147 * value)
	{
		___star2_12 = value;
		Il2CppCodeGenWriteBarrier(&___star2_12, value);
	}

	inline static int32_t get_offset_of_star3_13() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___star3_13)); }
	inline GameObject_t1756533147 * get_star3_13() const { return ___star3_13; }
	inline GameObject_t1756533147 ** get_address_of_star3_13() { return &___star3_13; }
	inline void set_star3_13(GameObject_t1756533147 * value)
	{
		___star3_13 = value;
		Il2CppCodeGenWriteBarrier(&___star3_13, value);
	}

	inline static int32_t get_offset_of_callBack_14() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___callBack_14)); }
	inline Il2CppObject * get_callBack_14() const { return ___callBack_14; }
	inline Il2CppObject ** get_address_of_callBack_14() { return &___callBack_14; }
	inline void set_callBack_14(Il2CppObject * value)
	{
		___callBack_14 = value;
		Il2CppCodeGenWriteBarrier(&___callBack_14, value);
	}

	inline static int32_t get_offset_of_correctAns_15() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___correctAns_15)); }
	inline String_t* get_correctAns_15() const { return ___correctAns_15; }
	inline String_t** get_address_of_correctAns_15() { return &___correctAns_15; }
	inline void set_correctAns_15(String_t* value)
	{
		___correctAns_15 = value;
		Il2CppCodeGenWriteBarrier(&___correctAns_15, value);
	}

	inline static int32_t get_offset_of_buyPanel_16() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___buyPanel_16)); }
	inline GameObject_t1756533147 * get_buyPanel_16() const { return ___buyPanel_16; }
	inline GameObject_t1756533147 ** get_address_of_buyPanel_16() { return &___buyPanel_16; }
	inline void set_buyPanel_16(GameObject_t1756533147 * value)
	{
		___buyPanel_16 = value;
		Il2CppCodeGenWriteBarrier(&___buyPanel_16, value);
	}

	inline static int32_t get_offset_of_GamePanel_17() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___GamePanel_17)); }
	inline GameObject_t1756533147 * get_GamePanel_17() const { return ___GamePanel_17; }
	inline GameObject_t1756533147 ** get_address_of_GamePanel_17() { return &___GamePanel_17; }
	inline void set_GamePanel_17(GameObject_t1756533147 * value)
	{
		___GamePanel_17 = value;
		Il2CppCodeGenWriteBarrier(&___GamePanel_17, value);
	}

	inline static int32_t get_offset_of_twoButtons_18() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___twoButtons_18)); }
	inline GameObject_t1756533147 * get_twoButtons_18() const { return ___twoButtons_18; }
	inline GameObject_t1756533147 ** get_address_of_twoButtons_18() { return &___twoButtons_18; }
	inline void set_twoButtons_18(GameObject_t1756533147 * value)
	{
		___twoButtons_18 = value;
		Il2CppCodeGenWriteBarrier(&___twoButtons_18, value);
	}

	inline static int32_t get_offset_of_singleButtonPanel_19() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___singleButtonPanel_19)); }
	inline GameObject_t1756533147 * get_singleButtonPanel_19() const { return ___singleButtonPanel_19; }
	inline GameObject_t1756533147 ** get_address_of_singleButtonPanel_19() { return &___singleButtonPanel_19; }
	inline void set_singleButtonPanel_19(GameObject_t1756533147 * value)
	{
		___singleButtonPanel_19 = value;
		Il2CppCodeGenWriteBarrier(&___singleButtonPanel_19, value);
	}

	inline static int32_t get_offset_of_finalAlertPanel_20() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___finalAlertPanel_20)); }
	inline GameObject_t1756533147 * get_finalAlertPanel_20() const { return ___finalAlertPanel_20; }
	inline GameObject_t1756533147 ** get_address_of_finalAlertPanel_20() { return &___finalAlertPanel_20; }
	inline void set_finalAlertPanel_20(GameObject_t1756533147 * value)
	{
		___finalAlertPanel_20 = value;
		Il2CppCodeGenWriteBarrier(&___finalAlertPanel_20, value);
	}

	inline static int32_t get_offset_of_levelPanel_21() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___levelPanel_21)); }
	inline GameObject_t1756533147 * get_levelPanel_21() const { return ___levelPanel_21; }
	inline GameObject_t1756533147 ** get_address_of_levelPanel_21() { return &___levelPanel_21; }
	inline void set_levelPanel_21(GameObject_t1756533147 * value)
	{
		___levelPanel_21 = value;
		Il2CppCodeGenWriteBarrier(&___levelPanel_21, value);
	}

	inline static int32_t get_offset_of_notEnought_22() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___notEnought_22)); }
	inline GameObject_t1756533147 * get_notEnought_22() const { return ___notEnought_22; }
	inline GameObject_t1756533147 ** get_address_of_notEnought_22() { return &___notEnought_22; }
	inline void set_notEnought_22(GameObject_t1756533147 * value)
	{
		___notEnought_22 = value;
		Il2CppCodeGenWriteBarrier(&___notEnought_22, value);
	}

	inline static int32_t get_offset_of_numb_of_compl_mission_23() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___numb_of_compl_mission_23)); }
	inline Text_t356221433 * get_numb_of_compl_mission_23() const { return ___numb_of_compl_mission_23; }
	inline Text_t356221433 ** get_address_of_numb_of_compl_mission_23() { return &___numb_of_compl_mission_23; }
	inline void set_numb_of_compl_mission_23(Text_t356221433 * value)
	{
		___numb_of_compl_mission_23 = value;
		Il2CppCodeGenWriteBarrier(&___numb_of_compl_mission_23, value);
	}

	inline static int32_t get_offset_of_numQuest_24() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___numQuest_24)); }
	inline int32_t get_numQuest_24() const { return ___numQuest_24; }
	inline int32_t* get_address_of_numQuest_24() { return &___numQuest_24; }
	inline void set_numQuest_24(int32_t value)
	{
		___numQuest_24 = value;
	}

	inline static int32_t get_offset_of_levelGlobal_25() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___levelGlobal_25)); }
	inline LevelsU5BU5D_t2050899114* get_levelGlobal_25() const { return ___levelGlobal_25; }
	inline LevelsU5BU5D_t2050899114** get_address_of_levelGlobal_25() { return &___levelGlobal_25; }
	inline void set_levelGlobal_25(LevelsU5BU5D_t2050899114* value)
	{
		___levelGlobal_25 = value;
		Il2CppCodeGenWriteBarrier(&___levelGlobal_25, value);
	}

	inline static int32_t get_offset_of_positionLevel_26() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___positionLevel_26)); }
	inline int32_t get_positionLevel_26() const { return ___positionLevel_26; }
	inline int32_t* get_address_of_positionLevel_26() { return &___positionLevel_26; }
	inline void set_positionLevel_26(int32_t value)
	{
		___positionLevel_26 = value;
	}

	inline static int32_t get_offset_of_correctAnswer_27() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___correctAnswer_27)); }
	inline int32_t get_correctAnswer_27() const { return ___correctAnswer_27; }
	inline int32_t* get_address_of_correctAnswer_27() { return &___correctAnswer_27; }
	inline void set_correctAnswer_27(int32_t value)
	{
		___correctAnswer_27 = value;
	}

	inline static int32_t get_offset_of_purch_28() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___purch_28)); }
	inline Purchase_t1644043085 * get_purch_28() const { return ___purch_28; }
	inline Purchase_t1644043085 ** get_address_of_purch_28() { return &___purch_28; }
	inline void set_purch_28(Purchase_t1644043085 * value)
	{
		___purch_28 = value;
		Il2CppCodeGenWriteBarrier(&___purch_28, value);
	}

	inline static int32_t get_offset_of_soundControll_29() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___soundControll_29)); }
	inline SoundController_t1686593041 * get_soundControll_29() const { return ___soundControll_29; }
	inline SoundController_t1686593041 ** get_address_of_soundControll_29() { return &___soundControll_29; }
	inline void set_soundControll_29(SoundController_t1686593041 * value)
	{
		___soundControll_29 = value;
		Il2CppCodeGenWriteBarrier(&___soundControll_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
