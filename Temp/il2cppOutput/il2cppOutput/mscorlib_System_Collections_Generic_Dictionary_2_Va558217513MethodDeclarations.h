﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>
struct ValueCollection_t558217513;
// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>
struct Dictionary_2_t1855157670;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3541690434.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m265178557_gshared (ValueCollection_t558217513 * __this, Dictionary_2_t1855157670 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m265178557(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t558217513 *, Dictionary_2_t1855157670 *, const MethodInfo*))ValueCollection__ctor_m265178557_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3521129867_gshared (ValueCollection_t558217513 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3521129867(__this, ___item0, method) ((  void (*) (ValueCollection_t558217513 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3521129867_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4117182274_gshared (ValueCollection_t558217513 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4117182274(__this, method) ((  void (*) (ValueCollection_t558217513 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4117182274_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3510268949_gshared (ValueCollection_t558217513 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3510268949(__this, ___item0, method) ((  bool (*) (ValueCollection_t558217513 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3510268949_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1408463800_gshared (ValueCollection_t558217513 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1408463800(__this, ___item0, method) ((  bool (*) (ValueCollection_t558217513 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1408463800_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2310517290_gshared (ValueCollection_t558217513 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2310517290(__this, method) ((  Il2CppObject* (*) (ValueCollection_t558217513 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2310517290_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2627478578_gshared (ValueCollection_t558217513 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2627478578(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t558217513 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2627478578_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m204113011_gshared (ValueCollection_t558217513 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m204113011(__this, method) ((  Il2CppObject * (*) (ValueCollection_t558217513 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m204113011_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m184767916_gshared (ValueCollection_t558217513 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m184767916(__this, method) ((  bool (*) (ValueCollection_t558217513 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m184767916_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4268585782_gshared (ValueCollection_t558217513 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4268585782(__this, method) ((  bool (*) (ValueCollection_t558217513 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4268585782_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m4134424594_gshared (ValueCollection_t558217513 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m4134424594(__this, method) ((  Il2CppObject * (*) (ValueCollection_t558217513 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m4134424594_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m552790214_gshared (ValueCollection_t558217513 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m552790214(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t558217513 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m552790214_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3541690434  ValueCollection_GetEnumerator_m827968417_gshared (ValueCollection_t558217513 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m827968417(__this, method) ((  Enumerator_t3541690434  (*) (ValueCollection_t558217513 *, const MethodInfo*))ValueCollection_GetEnumerator_m827968417_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m2125382038_gshared (ValueCollection_t558217513 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m2125382038(__this, method) ((  int32_t (*) (ValueCollection_t558217513 *, const MethodInfo*))ValueCollection_get_Count_m2125382038_gshared)(__this, method)
