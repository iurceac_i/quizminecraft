﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Adapter
struct Adapter_t814751345;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Adapter/<setAdapter>c__AnonStorey0
struct  U3CsetAdapterU3Ec__AnonStorey0_t1050088512  : public Il2CppObject
{
public:
	// UnityEngine.GameObject Adapter/<setAdapter>c__AnonStorey0::finalObj
	GameObject_t1756533147 * ___finalObj_0;
	// Adapter Adapter/<setAdapter>c__AnonStorey0::$this
	Adapter_t814751345 * ___U24this_1;

public:
	inline static int32_t get_offset_of_finalObj_0() { return static_cast<int32_t>(offsetof(U3CsetAdapterU3Ec__AnonStorey0_t1050088512, ___finalObj_0)); }
	inline GameObject_t1756533147 * get_finalObj_0() const { return ___finalObj_0; }
	inline GameObject_t1756533147 ** get_address_of_finalObj_0() { return &___finalObj_0; }
	inline void set_finalObj_0(GameObject_t1756533147 * value)
	{
		___finalObj_0 = value;
		Il2CppCodeGenWriteBarrier(&___finalObj_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CsetAdapterU3Ec__AnonStorey0_t1050088512, ___U24this_1)); }
	inline Adapter_t814751345 * get_U24this_1() const { return ___U24this_1; }
	inline Adapter_t814751345 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Adapter_t814751345 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
