﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key43688145MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2143110649(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3678426379 *, Dictionary_2_t1194928608 *, const MethodInfo*))KeyCollection__ctor_m590569093_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3818283535(__this, ___item0, method) ((  void (*) (KeyCollection_t3678426379 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1156272491_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m462799980(__this, method) ((  void (*) (KeyCollection_t3678426379 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3197456066_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3032088841(__this, ___item0, method) ((  bool (*) (KeyCollection_t3678426379 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1547453893_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1219366748(__this, ___item0, method) ((  bool (*) (KeyCollection_t3678426379 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m111470242_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m450978934(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3678426379 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2808543900_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1402169598(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3678426379 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3488596196_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3689274031(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3678426379 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1291148883_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3432019442(__this, method) ((  bool (*) (KeyCollection_t3678426379 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m917965996_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1452501594(__this, method) ((  bool (*) (KeyCollection_t3678426379 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1234865824_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1357013390(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3678426379 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1518127764_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3469349440(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3678426379 *, AppStoreU5BU5D_t3188282381*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m36123398_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1980690149(__this, method) ((  Enumerator_t3884432046  (*) (KeyCollection_t3678426379 *, const MethodInfo*))KeyCollection_GetEnumerator_m3405000929_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.String>::get_Count()
#define KeyCollection_get_Count_m2440747330(__this, method) ((  int32_t (*) (KeyCollection_t3678426379 *, const MethodInfo*))KeyCollection_get_Count_m522341384_gshared)(__this, method)
