﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QuestionController
struct QuestionController_t445239244;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Levels[]
struct LevelsU5BU5D_t2050899114;
// Question
struct Question_t2927948840;
// QuestionController/UpdateListener
struct UpdateListener_t3189012468;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_Question2927948840.h"

// System.Void QuestionController::.ctor()
extern "C"  void QuestionController__ctor_m3917901329 (QuestionController_t445239244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::VerifyQuestion(UnityEngine.GameObject)
extern "C"  void QuestionController_VerifyQuestion_m145838332 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::SetLevels(Levels[],System.Int32)
extern "C"  void QuestionController_SetLevels_m2193348496 (QuestionController_t445239244 * __this, LevelsU5BU5D_t2050899114* ___level0, int32_t ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::SetQuestion(Question)
extern "C"  void QuestionController_SetQuestion_m3640094679 (QuestionController_t445239244 * __this, Question_t2927948840 * ___quest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::CounterQuestion(UnityEngine.GameObject)
extern "C"  void QuestionController_CounterQuestion_m3694873063 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::NextLevel()
extern "C"  void QuestionController_NextLevel_m2486282466 (QuestionController_t445239244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::RetryLevel(UnityEngine.GameObject)
extern "C"  void QuestionController_RetryLevel_m2188939663 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::BackButton(UnityEngine.GameObject)
extern "C"  void QuestionController_BackButton_m2933698246 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::ClosePopup(UnityEngine.GameObject)
extern "C"  void QuestionController_ClosePopup_m4255358609 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::GoHome(UnityEngine.GameObject)
extern "C"  void QuestionController_GoHome_m7599600 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::registerListener(QuestionController/UpdateListener)
extern "C"  void QuestionController_registerListener_m712129072 (QuestionController_t445239244 * __this, Il2CppObject * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::RefreshQuestionController()
extern "C"  void QuestionController_RefreshQuestionController_m1860514236 (QuestionController_t445239244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
