﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stats
struct Stats_t967880071;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"

// System.Void Stats::.ctor()
extern "C"  void Stats__ctor_m3441957910 (Stats_t967880071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::StoreMenu()
extern "C"  void Stats_StoreMenu_m3465421044 (Stats_t967880071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::activeAlert(UnityEngine.GameObject)
extern "C"  void Stats_activeAlert_m406229684 (Stats_t967880071 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::CloseAlert(UnityEngine.GameObject)
extern "C"  void Stats_CloseAlert_m3751967524 (Stats_t967880071 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::BackButtStats(UnityEngine.GameObject)
extern "C"  void Stats_BackButtStats_m1146948631 (Stats_t967880071 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::DisablePanel(UnityEngine.GameObject)
extern "C"  void Stats_DisablePanel_m3629909344 (Stats_t967880071 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::LanscapeEnable()
extern "C"  void Stats_LanscapeEnable_m3462121364 (Stats_t967880071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::PortretEnable()
extern "C"  void Stats_PortretEnable_m2884510581 (Stats_t967880071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::onBackButt()
extern "C"  void Stats_onBackButt_m2446812067 (Stats_t967880071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::offBackButt()
extern "C"  void Stats_offBackButt_m4112918333 (Stats_t967880071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::ActiveMyStatsSong()
extern "C"  void Stats_ActiveMyStatsSong_m751971532 (Stats_t967880071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Stats::LoadImageStats(System.String,UnityEngine.UI.Image)
extern "C"  Il2CppObject * Stats_LoadImageStats_m535395134 (Stats_t967880071 * __this, String_t* ___path0, Image_t2042527209 * ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
