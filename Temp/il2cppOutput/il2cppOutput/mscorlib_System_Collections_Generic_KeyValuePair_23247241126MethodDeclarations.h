﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23907470188MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.String>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1422337144(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3247241126 *, int32_t, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m3254048546_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m3317615498(__this, method) ((  int32_t (*) (KeyValuePair_2_t3247241126 *, const MethodInfo*))KeyValuePair_2_get_Key_m158792468_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3752772015(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3247241126 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3630760627_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m2790420618(__this, method) ((  String_t* (*) (KeyValuePair_2_t3247241126 *, const MethodInfo*))KeyValuePair_2_get_Value_m2242053076_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2007016767(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3247241126 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m1791557123_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.String>::ToString()
#define KeyValuePair_2_ToString_m4098422321(__this, method) ((  String_t* (*) (KeyValuePair_2_t3247241126 *, const MethodInfo*))KeyValuePair_2_ToString_m435779533_gshared)(__this, method)
