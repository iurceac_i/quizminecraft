﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CU3872126098.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CR1220668507.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CS2291090002.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CRe536115202.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CV1085247229.h"
#include "Stores_UnityEngine_Purchasing_PayMethod1558319955.h"
#include "Stores_UnityEngine_Purchasing_GooglePlayAndroidJav4038061283.h"
#include "Stores_UnityEngine_Purchasing_FakeGooglePlayConfigu737012266.h"
#include "Stores_UnityEngine_Purchasing_FakeSamsungAppsExten1522853249.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsMode2214306743.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsStoreExte3441062041.h"
#include "Stores_UnityEngine_Purchasing_FakeUnityChannelConf4230617165.h"
#include "Stores_UnityEngine_Purchasing_FakeUnityChannelExte2132025117.h"
#include "Stores_UnityEngine_Purchasing_XiaomiPriceTiers2494450221.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelBindings2880355556.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelBindings1107960729.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelImpl1327714682.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelImpl_U3CU3Ec505099.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelImpl_U3C2729388454.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelPurchase1964142665.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl1301617341.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3CU32170469347.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3CU33474431165.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleConfiguatio4052738437.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleExtensions4039399289.h"
#include "Stores_UnityEngine_Purchasing_AppStore379104228.h"
#include "Stores_UnityEngine_Purchasing_FakeMicrosoftExtensi3351923809.h"
#include "Stores_UnityEngine_Purchasing_WinRTStore36043095.h"
#include "Stores_UnityEngine_Purchasing_WinRTStore_U3CU3Ec4236842726.h"
#include "Stores_UnityEngine_Purchasing_TizenStoreImpl274247241.h"
#include "Stores_UnityEngine_Purchasing_FakeTizenStoreConfig3055550456.h"
#include "Stores_UnityEngine_Purchasing_FacebookStoreImpl1362794587.h"
#include "Stores_UnityEngine_Purchasing_FacebookStoreImpl_U3C294810501.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887.h"
#include "Stores_UnityEngine_Purchasing_FakeStore3882981564.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_DialogType1733969544.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CU3Ec__D1664791861.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CU3Ec__D2968753776.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore3684252124.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_DialogRe2092195449.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_Lifecycl1057582876.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_U3CU3Ec3471345421.h"
#include "Stores_UnityEngine_Purchasing_AsyncWebUtil1370427196.h"
#include "Stores_UnityEngine_Purchasing_AsyncWebUtil_U3CDoInvo21677283.h"
#include "Stores_UnityEngine_Purchasing_AsyncWebUtil_U3CProc1496112696.h"
#include "Stores_UnityEngine_Purchasing_CloudCatalogImpl569898932.h"
#include "Stores_UnityEngine_Purchasing_CloudCatalogImpl_U3C2096495139.h"
#include "Stores_UnityEngine_Purchasing_CloudCatalogImpl_U3C4238608033.h"
#include "Stores_UnityEngine_Purchasing_JSONStore1890359403.h"
#include "Stores_UnityEngine_Purchasing_NativeStoreProvider4107132677.h"
#include "Stores_UnityEngine_Purchasing_Price1853024949.h"
#include "Stores_UnityEngine_Purchasing_StoreID471452324.h"
#include "Stores_UnityEngine_Purchasing_TranslationLocale3543162129.h"
#include "Stores_UnityEngine_Purchasing_LocalizedProductDesc1525635964.h"
#include "Stores_UnityEngine_Purchasing_LocalizedProductDesc3617267505.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogPayout96590568.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogPayout3881497528.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogItem977711995.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogItem_U3197607536.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalog2667590766.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogImpl2093457986.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMod107230755.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo1310600573.h"
#include "Stores_UnityEngine_Purchasing_StoreConfiguration2466794143.h"
#include "Stores_UnityEngine_Purchasing_UnifiedReceipt2654419430.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil166323129.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil_U830721369.h"
#include "Stores_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Stores_U3CPrivateImplementationDetailsU3E___Static1542897494.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasing53875121.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2749524914.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi1696213691.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2756307745.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3077837360.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasin593747732.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi4018783659.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (U3CU3Ec__DisplayClass18_0_t3872126098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[3] = 
{
	U3CU3Ec__DisplayClass18_0_t3872126098::get_offset_of_purchaseSucceed_0(),
	U3CU3Ec__DisplayClass18_0_t3872126098::get_offset_of_purchaseFailed_1(),
	U3CU3Ec__DisplayClass18_0_t3872126098::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (U3CRequestAuthCodeU3Ed__22_t1220668507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1901[13] = 
{
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CU3E1__state_0(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CU3E2__current_1(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_wf_2(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_productID_3(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_transactionId_4(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_succeed_5(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_failed_6(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CU3E4__this_7(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CwU3E5__1_8(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CauthCodeResultU3E5__2_9(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CauthCodeValuesU3E5__3_10(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CauthCodeU3E5__4_11(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CpaymentURLU3E5__5_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (U3CStartPurchasePollingU3Ed__23_t2291090002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[21] = 
{
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CU3E1__state_0(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CU3E2__current_1(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_authGlobal_2(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_transactionId_3(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_purchaseSucceed_4(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_purchaseFailed_5(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CU3E4__this_6(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CorderSuccessU3E5__1_7(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CsignstrU3E5__2_8(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CsignU3E5__3_9(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CparamU3E5__4_10(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CurlU3E5__5_11(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CpollingstrU3E5__6_12(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CjsonPollingObjectsU3E5__7_13(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CcodeU3E5__8_14(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CpollingValuesU3E5__9_15(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CtradeSeqU3E5__10_16(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CtradeStateU3E5__11_17(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CproductIdU3E5__12_18(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CMsgU3E5__13_19(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CreceiptU3E5__14_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (U3CRestoreTransactionIDProcessU3Ed__45_t536115202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1903[20] = 
{
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CU3E1__state_0(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CU3E2__current_1(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_result_2(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CU3E4__this_3(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CcustomIDU3E5__1_4(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CwfU3E5__2_5(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CnowU3E5__3_6(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CendDateU3E5__4_7(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CupperWeekU3E5__5_8(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CstartDateU3E5__6_9(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CsignU3E5__7_10(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CwU3E5__8_11(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CrestoreObjectsU3E5__9_12(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CcodeU3E5__10_13(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CrestoreValuesU3E5__11_14(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CU3Es__12_15(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CrestoreObjectElemU3E5__13_16(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CproductIdU3E5__14_17(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CtradeSeqU3E5__15_18(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CreceiptU3E5__16_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (U3CValidateReceiptProcessU3Ed__47_t1085247229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[13] = 
{
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CU3E1__state_0(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CU3E2__current_1(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_transactionId_2(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_receipt_3(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_result_4(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CU3E4__this_5(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CtempJsonU3E5__1_6(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CwfU3E5__2_7(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CsignU3E5__3_8(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CwU3E5__4_9(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CjsonObjectsU3E5__5_10(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CcodeU3E5__6_11(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CmsgU3E5__7_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (PayMethod_t1558319955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (GooglePlayAndroidJavaStore_t4038061283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (FakeGooglePlayConfiguration_t737012266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (FakeSamsungAppsExtensions_t1522853249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (SamsungAppsMode_t2214306743)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1913[4] = 
{
	SamsungAppsMode_t2214306743::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (SamsungAppsStoreExtensions_t3441062041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[2] = 
{
	SamsungAppsStoreExtensions_t3441062041::get_offset_of_m_RestoreCallback_1(),
	SamsungAppsStoreExtensions_t3441062041::get_offset_of_m_Java_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (FakeUnityChannelConfiguration_t4230617165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[1] = 
{
	FakeUnityChannelConfiguration_t4230617165::get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (FakeUnityChannelExtensions_t2132025117), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (XiaomiPriceTiers_t2494450221), -1, sizeof(XiaomiPriceTiers_t2494450221_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1920[1] = 
{
	XiaomiPriceTiers_t2494450221_StaticFields::get_offset_of_XiaomiPriceTierPrices_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (UnityChannelBindings_t2880355556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[4] = 
{
	UnityChannelBindings_t2880355556::get_offset_of_m_PurchaseCallback_0(),
	UnityChannelBindings_t2880355556::get_offset_of_m_PurchaseGuid_1(),
	UnityChannelBindings_t2880355556::get_offset_of_m_ValidateCallbacks_2(),
	UnityChannelBindings_t2880355556::get_offset_of_m_PurchaseConfirmCallbacks_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (U3CU3Ec__DisplayClass16_0_t1107960729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[2] = 
{
	U3CU3Ec__DisplayClass16_0_t1107960729::get_offset_of_transactionId_0(),
	U3CU3Ec__DisplayClass16_0_t1107960729::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (UnityChannelImpl_t1327714682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[3] = 
{
	UnityChannelImpl_t1327714682::get_offset_of_m_Bindings_2(),
	UnityChannelImpl_t1327714682::get_offset_of_m_LastPurchaseError_3(),
	UnityChannelImpl_t1327714682::get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (U3CU3Ec__DisplayClass7_0_t505099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[2] = 
{
	U3CU3Ec__DisplayClass7_0_t505099::get_offset_of_product_0(),
	U3CU3Ec__DisplayClass7_0_t505099::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (U3CU3Ec__DisplayClass7_1_t2729388454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[3] = 
{
	U3CU3Ec__DisplayClass7_1_t2729388454::get_offset_of_dic_0(),
	U3CU3Ec__DisplayClass7_1_t2729388454::get_offset_of_transactionId_1(),
	U3CU3Ec__DisplayClass7_1_t2729388454::get_offset_of_CSU24U3CU3E8__locals1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (UnityChannelPurchaseReceipt_t1964142665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[3] = 
{
	UnityChannelPurchaseReceipt_t1964142665::get_offset_of_storeSpecificId_0(),
	UnityChannelPurchaseReceipt_t1964142665::get_offset_of_transactionId_1(),
	UnityChannelPurchaseReceipt_t1964142665::get_offset_of_orderQueryToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (AppleStoreImpl_t1301617341), -1, sizeof(AppleStoreImpl_t1301617341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1927[7] = 
{
	AppleStoreImpl_t1301617341::get_offset_of_m_DeferredCallback_2(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RefreshReceiptError_3(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RefreshReceiptSuccess_4(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RestoreCallback_5(),
	AppleStoreImpl_t1301617341::get_offset_of_m_Native_6(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_util_7(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_instance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (U3CU3Ec__DisplayClass19_0_t2170469347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[1] = 
{
	U3CU3Ec__DisplayClass19_0_t2170469347::get_offset_of_productDescription_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (U3CU3Ec__DisplayClass28_0_t3474431165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1929[4] = 
{
	U3CU3Ec__DisplayClass28_0_t3474431165::get_offset_of_subject_0(),
	U3CU3Ec__DisplayClass28_0_t3474431165::get_offset_of_payload_1(),
	U3CU3Ec__DisplayClass28_0_t3474431165::get_offset_of_receipt_2(),
	U3CU3Ec__DisplayClass28_0_t3474431165::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (FakeAppleConfiguation_t4052738437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (FakeAppleExtensions_t4039399289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (AppStore_t379104228)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1934[13] = 
{
	AppStore_t379104228::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (FakeMicrosoftExtensions_t3351923809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (WinRTStore_t36043095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1938[5] = 
{
	WinRTStore_t36043095::get_offset_of_win8_0(),
	WinRTStore_t36043095::get_offset_of_callback_1(),
	WinRTStore_t36043095::get_offset_of_util_2(),
	WinRTStore_t36043095::get_offset_of_logger_3(),
	WinRTStore_t36043095::get_offset_of_m_CanReceivePurchases_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (U3CU3Ec_t4236842726), -1, sizeof(U3CU3Ec_t4236842726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1939[3] = 
{
	U3CU3Ec_t4236842726_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4236842726_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
	U3CU3Ec_t4236842726_StaticFields::get_offset_of_U3CU3E9__8_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (TizenStoreImpl_t274247241), -1, sizeof(TizenStoreImpl_t274247241_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1941[2] = 
{
	TizenStoreImpl_t274247241_StaticFields::get_offset_of_instance_2(),
	TizenStoreImpl_t274247241::get_offset_of_m_Native_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (FakeTizenStoreConfiguration_t3055550456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (FacebookStoreImpl_t1362794587), -1, sizeof(FacebookStoreImpl_t1362794587_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1943[3] = 
{
	FacebookStoreImpl_t1362794587::get_offset_of_m_Native_2(),
	FacebookStoreImpl_t1362794587_StaticFields::get_offset_of_util_3(),
	FacebookStoreImpl_t1362794587_StaticFields::get_offset_of_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (U3CU3Ec__DisplayClass6_0_t294810501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[4] = 
{
	U3CU3Ec__DisplayClass6_0_t294810501::get_offset_of_subject_0(),
	U3CU3Ec__DisplayClass6_0_t294810501::get_offset_of_payload_1(),
	U3CU3Ec__DisplayClass6_0_t294810501::get_offset_of_receipt_2(),
	U3CU3Ec__DisplayClass6_0_t294810501::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (FakeStoreUIMode_t2321492887)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1945[4] = 
{
	FakeStoreUIMode_t2321492887::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (FakeStore_t3882981564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[5] = 
{
	FakeStore_t3882981564::get_offset_of_m_Biller_0(),
	FakeStore_t3882981564::get_offset_of_m_PurchasedProducts_1(),
	FakeStore_t3882981564::get_offset_of_purchaseCalled_2(),
	FakeStore_t3882981564::get_offset_of_U3CunavailableProductIdU3Ek__BackingField_3(),
	FakeStore_t3882981564::get_offset_of_UIMode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (DialogType_t1733969544)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1947[3] = 
{
	DialogType_t1733969544::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (U3CU3Ec__DisplayClass12_0_t1664791861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1948[2] = 
{
	U3CU3Ec__DisplayClass12_0_t1664791861::get_offset_of_products_0(),
	U3CU3Ec__DisplayClass12_0_t1664791861::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (U3CU3Ec__DisplayClass13_0_t2968753776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1949[2] = 
{
	U3CU3Ec__DisplayClass13_0_t2968753776::get_offset_of_product_0(),
	U3CU3Ec__DisplayClass13_0_t2968753776::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (UIFakeStore_t3684252124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[6] = 
{
	UIFakeStore_t3684252124::get_offset_of_m_CurrentDialog_5(),
	UIFakeStore_t3684252124::get_offset_of_m_LastSelectedDropdownIndex_6(),
	UIFakeStore_t3684252124::get_offset_of_UIFakeStoreCanvasPrefab_7(),
	UIFakeStore_t3684252124::get_offset_of_m_Canvas_8(),
	UIFakeStore_t3684252124::get_offset_of_m_EventSystem_9(),
	UIFakeStore_t3684252124::get_offset_of_m_ParentGameObjectPath_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (DialogRequest_t2092195449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[5] = 
{
	DialogRequest_t2092195449::get_offset_of_QueryText_0(),
	DialogRequest_t2092195449::get_offset_of_OkayButtonText_1(),
	DialogRequest_t2092195449::get_offset_of_CancelButtonText_2(),
	DialogRequest_t2092195449::get_offset_of_Options_3(),
	DialogRequest_t2092195449::get_offset_of_Callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (LifecycleNotifier_t1057582876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[1] = 
{
	LifecycleNotifier_t1057582876::get_offset_of_OnDestroyCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (U3CU3Ec_t3471345421), -1, sizeof(U3CU3Ec_t3471345421_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1955[2] = 
{
	U3CU3Ec_t3471345421_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3471345421_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (AsyncWebUtil_t1370427196), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (U3CDoInvokeU3Ed__2_t21677283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[5] = 
{
	U3CDoInvokeU3Ed__2_t21677283::get_offset_of_U3CU3E1__state_0(),
	U3CDoInvokeU3Ed__2_t21677283::get_offset_of_U3CU3E2__current_1(),
	U3CDoInvokeU3Ed__2_t21677283::get_offset_of_a_2(),
	U3CDoInvokeU3Ed__2_t21677283::get_offset_of_delayInSeconds_3(),
	U3CDoInvokeU3Ed__2_t21677283::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (U3CProcessU3Ed__3_t1496112696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[6] = 
{
	U3CProcessU3Ed__3_t1496112696::get_offset_of_U3CU3E1__state_0(),
	U3CProcessU3Ed__3_t1496112696::get_offset_of_U3CU3E2__current_1(),
	U3CProcessU3Ed__3_t1496112696::get_offset_of_request_2(),
	U3CProcessU3Ed__3_t1496112696::get_offset_of_responseHandler_3(),
	U3CProcessU3Ed__3_t1496112696::get_offset_of_errorHandler_4(),
	U3CProcessU3Ed__3_t1496112696::get_offset_of_U3CU3E4__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (CloudCatalogImpl_t569898932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[7] = 
{
	CloudCatalogImpl_t569898932::get_offset_of_m_AsyncUtil_0(),
	CloudCatalogImpl_t569898932::get_offset_of_m_CacheFileName_1(),
	CloudCatalogImpl_t569898932::get_offset_of_m_Logger_2(),
	CloudCatalogImpl_t569898932::get_offset_of_m_CatalogURL_3(),
	CloudCatalogImpl_t569898932::get_offset_of_m_StoreName_4(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (U3CU3Ec__DisplayClass10_0_t2096495139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[4] = 
{
	U3CU3Ec__DisplayClass10_0_t2096495139::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass10_0_t2096495139::get_offset_of_delayInSeconds_1(),
	U3CU3Ec__DisplayClass10_0_t2096495139::get_offset_of_U3CU3E4__this_2(),
	U3CU3Ec__DisplayClass10_0_t2096495139::get_offset_of_U3CU3E9__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (U3CU3Ec_t4238608033), -1, sizeof(U3CU3Ec_t4238608033_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1961[3] = 
{
	U3CU3Ec_t4238608033_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4238608033_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
	U3CU3Ec_t4238608033_StaticFields::get_offset_of_U3CU3E9__12_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (JSONStore_t1890359403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[2] = 
{
	JSONStore_t1890359403::get_offset_of_unity_0(),
	JSONStore_t1890359403::get_offset_of_store_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (NativeStoreProvider_t4107132677), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (Price_t1853024949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1966[3] = 
{
	Price_t1853024949::get_offset_of_value_0(),
	Price_t1853024949::get_offset_of_data_1(),
	Price_t1853024949::get_offset_of_num_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (StoreID_t471452324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[2] = 
{
	StoreID_t471452324::get_offset_of_store_0(),
	StoreID_t471452324::get_offset_of_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (TranslationLocale_t3543162129)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1968[33] = 
{
	TranslationLocale_t3543162129::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (LocalizedProductDescription_t1525635964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1969[3] = 
{
	LocalizedProductDescription_t1525635964::get_offset_of_googleLocale_0(),
	LocalizedProductDescription_t1525635964::get_offset_of_title_1(),
	LocalizedProductDescription_t1525635964::get_offset_of_description_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (U3CU3Ec_t3617267505), -1, sizeof(U3CU3Ec_t3617267505_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1970[2] = 
{
	U3CU3Ec_t3617267505_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3617267505_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (ProductCatalogPayout_t96590568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1971[3] = 
{
	ProductCatalogPayout_t96590568::get_offset_of_t_0(),
	ProductCatalogPayout_t96590568::get_offset_of_st_1(),
	ProductCatalogPayout_t96590568::get_offset_of_d_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (ProductCatalogPayoutType_t3881497528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1972[5] = 
{
	ProductCatalogPayoutType_t3881497528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (ProductCatalogItem_t977711995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1973[9] = 
{
	ProductCatalogItem_t977711995::get_offset_of_id_0(),
	ProductCatalogItem_t977711995::get_offset_of_type_1(),
	ProductCatalogItem_t977711995::get_offset_of_storeIDs_2(),
	ProductCatalogItem_t977711995::get_offset_of_defaultDescription_3(),
	ProductCatalogItem_t977711995::get_offset_of_applePriceTier_4(),
	ProductCatalogItem_t977711995::get_offset_of_xiaomiPriceTier_5(),
	ProductCatalogItem_t977711995::get_offset_of_googlePrice_6(),
	ProductCatalogItem_t977711995::get_offset_of_descriptions_7(),
	ProductCatalogItem_t977711995::get_offset_of_payouts_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (U3CU3Ec__DisplayClass21_0_t3197607536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1974[1] = 
{
	U3CU3Ec__DisplayClass21_0_t3197607536::get_offset_of_locale_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (ProductCatalog_t2667590766), -1, sizeof(ProductCatalog_t2667590766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1975[2] = 
{
	ProductCatalog_t2667590766_StaticFields::get_offset_of_instance_0(),
	ProductCatalog_t2667590766::get_offset_of_products_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (ProductCatalogImpl_t2093457986), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (StandardPurchasingModule_t4003664591), -1, sizeof(StandardPurchasingModule_t4003664591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1978[13] = 
{
	StandardPurchasingModule_t4003664591::get_offset_of_m_AppStorePlatform_1(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_NativeStoreProvider_2(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_RuntimePlatform_3(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_Util_4(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_Logger_5(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_UseCloudCatalog_6(),
	StandardPurchasingModule_t4003664591_StaticFields::get_offset_of_ModuleInstance_7(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_StoreInstance_8(),
	StandardPurchasingModule_t4003664591_StaticFields::get_offset_of_AndroidStoreNameMap_9(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_CloudCatalog_10(),
	StandardPurchasingModule_t4003664591::get_offset_of_U3CuseFakeStoreUIModeU3Ek__BackingField_11(),
	StandardPurchasingModule_t4003664591::get_offset_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_12(),
	StandardPurchasingModule_t4003664591::get_offset_of_windowsStore_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (StoreInstance_t107230755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[2] = 
{
	StoreInstance_t107230755::get_offset_of_U3CstoreNameU3Ek__BackingField_0(),
	StoreInstance_t107230755::get_offset_of_U3CinstanceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (MicrosoftConfiguration_t1310600573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[2] = 
{
	MicrosoftConfiguration_t1310600573::get_offset_of_useMock_0(),
	MicrosoftConfiguration_t1310600573::get_offset_of_module_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (StoreConfiguration_t2466794143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[1] = 
{
	StoreConfiguration_t2466794143::get_offset_of_U3CandroidStoreU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (UnifiedReceipt_t2654419430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1982[1] = 
{
	UnifiedReceipt_t2654419430::get_offset_of_Payload_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (UnityUtil_t166323129), -1, sizeof(UnityUtil_t166323129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1983[4] = 
{
	UnityUtil_t166323129_StaticFields::get_offset_of_s_Callbacks_2(),
	UnityUtil_t166323129_StaticFields::get_offset_of_s_CallbacksPending_3(),
	UnityUtil_t166323129_StaticFields::get_offset_of_s_PcControlledPlatforms_4(),
	UnityUtil_t166323129::get_offset_of_pauseListeners_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (U3CDelayedCoroutineU3Ed__29_t830721369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1984[5] = 
{
	U3CDelayedCoroutineU3Ed__29_t830721369::get_offset_of_U3CU3E1__state_0(),
	U3CDelayedCoroutineU3Ed__29_t830721369::get_offset_of_U3CU3E2__current_1(),
	U3CDelayedCoroutineU3Ed__29_t830721369::get_offset_of_coroutine_2(),
	U3CDelayedCoroutineU3Ed__29_t830721369::get_offset_of_delay_3(),
	U3CDelayedCoroutineU3Ed__29_t830721369::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1985[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (__StaticArrayInitTypeSizeU3D368_t1542897494)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D368_t1542897494 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (U3CModuleU3E_t3783534230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1989[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (U3CModuleU3E_t3783534231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (AppleTangle_t53875121), -1, sizeof(AppleTangle_t53875121_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1993[4] = 
{
	AppleTangle_t53875121_StaticFields::get_offset_of_data_0(),
	AppleTangle_t53875121_StaticFields::get_offset_of_order_1(),
	AppleTangle_t53875121_StaticFields::get_offset_of_key_2(),
	AppleTangle_t53875121_StaticFields::get_offset_of_IsPopulated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (GooglePlayTangle_t2749524914), -1, sizeof(GooglePlayTangle_t2749524914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1994[4] = 
{
	GooglePlayTangle_t2749524914_StaticFields::get_offset_of_data_0(),
	GooglePlayTangle_t2749524914_StaticFields::get_offset_of_order_1(),
	GooglePlayTangle_t2749524914_StaticFields::get_offset_of_key_2(),
	GooglePlayTangle_t2749524914_StaticFields::get_offset_of_IsPopulated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (UnityChannelTangle_t1696213691), -1, sizeof(UnityChannelTangle_t1696213691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1995[4] = 
{
	UnityChannelTangle_t1696213691_StaticFields::get_offset_of_data_0(),
	UnityChannelTangle_t1696213691_StaticFields::get_offset_of_order_1(),
	UnityChannelTangle_t1696213691_StaticFields::get_offset_of_key_2(),
	UnityChannelTangle_t1696213691_StaticFields::get_offset_of_IsPopulated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (DemoInventory_t2756307745), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (IAPButton_t3077837360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1997[8] = 
{
	IAPButton_t3077837360::get_offset_of_productId_2(),
	IAPButton_t3077837360::get_offset_of_buttonType_3(),
	IAPButton_t3077837360::get_offset_of_consumePurchase_4(),
	IAPButton_t3077837360::get_offset_of_onPurchaseComplete_5(),
	IAPButton_t3077837360::get_offset_of_onPurchaseFailed_6(),
	IAPButton_t3077837360::get_offset_of_titleText_7(),
	IAPButton_t3077837360::get_offset_of_descriptionText_8(),
	IAPButton_t3077837360::get_offset_of_priceText_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (ButtonType_t593747732)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1998[3] = 
{
	ButtonType_t593747732::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (OnPurchaseCompletedEvent_t4018783659), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
