﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent
struct OnPurchaseFailedEvent_t3694879381;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent::.ctor()
extern "C"  void OnPurchaseFailedEvent__ctor_m3253910082 (OnPurchaseFailedEvent_t3694879381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
