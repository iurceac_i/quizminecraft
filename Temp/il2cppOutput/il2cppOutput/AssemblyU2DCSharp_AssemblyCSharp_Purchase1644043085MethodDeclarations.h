﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AssemblyCSharp.Purchase
struct Purchase_t1644043085;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.String
struct String_t;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_t547992434;
// UnityEngine.Purchasing.Product
struct Product_t1203687971;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"

// System.Void AssemblyCSharp.Purchase::.ctor()
extern "C"  void Purchase__ctor_m123144463 (Purchase_t1644043085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::Start()
extern "C"  void Purchase_Start_m402233827 (Purchase_t1644043085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::readPurchaseJson()
extern "C"  void Purchase_readPurchaseJson_m1913674994 (Purchase_t1644043085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::InitializePurchasing()
extern "C"  void Purchase_InitializePurchasing_m1496585717 (Purchase_t1644043085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssemblyCSharp.Purchase::IsInitialized()
extern "C"  bool Purchase_IsInitialized_m2743679681 (Purchase_t1644043085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::BuyConsumable()
extern "C"  void Purchase_BuyConsumable_m2197570716 (Purchase_t1644043085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::BuyNonConsumable()
extern "C"  void Purchase_BuyNonConsumable_m2013515493 (Purchase_t1644043085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::BuySubscription(UnityEngine.GameObject)
extern "C"  void Purchase_BuySubscription_m937582440 (Purchase_t1644043085 * __this, GameObject_t1756533147 * ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::BlockInteractableButton(UnityEngine.UI.Button)
extern "C"  void Purchase_BlockInteractableButton_m2639323695 (Purchase_t1644043085 * __this, Button_t2872111280 * ___btn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::BuyProductID(System.String)
extern "C"  void Purchase_BuyProductID_m4244666769 (Purchase_t1644043085 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::RestorePurchases(UnityEngine.GameObject)
extern "C"  void Purchase_RestorePurchases_m2342185403 (Purchase_t1644043085 * __this, GameObject_t1756533147 * ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern "C"  void Purchase_OnInitialized_m973436933 (Purchase_t1644043085 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern "C"  void Purchase_OnInitializeFailed_m2837146904 (Purchase_t1644043085 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.PurchaseProcessingResult AssemblyCSharp.Purchase::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern "C"  int32_t Purchase_ProcessPurchase_m2859853793 (Purchase_t1644043085 * __this, PurchaseEventArgs_t547992434 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern "C"  void Purchase_OnPurchaseFailed_m645576138 (Purchase_t1644043085 * __this, Product_t1203687971 * ___product0, int32_t ___failureReason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> AssemblyCSharp.Purchase::validate(System.String,System.String)
extern "C"  Nullable_1_t3251239280  Purchase_validate_m3617248933 (Purchase_t1644043085 * __this, String_t* ___receiptStr0, String_t* ___myProduct1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AssemblyCSharp.Purchase::verifyOnStart()
extern "C"  bool Purchase_verifyOnStart_m1690669461 (Purchase_t1644043085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::.cctor()
extern "C"  void Purchase__cctor_m3135322714 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AssemblyCSharp.Purchase::<RestorePurchases>m__0(System.Boolean)
extern "C"  void Purchase_U3CRestorePurchasesU3Em__0_m3309735971 (Purchase_t1644043085 * __this, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
