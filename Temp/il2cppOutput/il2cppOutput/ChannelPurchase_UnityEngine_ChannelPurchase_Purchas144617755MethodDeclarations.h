﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ChannelPurchase.PurchaseForwardCallback
struct PurchaseForwardCallback_t144617755;
// UnityEngine.ChannelPurchase.IPurchaseListener
struct IPurchaseListener_t2367674166;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.ChannelPurchase.PurchaseForwardCallback::.ctor(UnityEngine.ChannelPurchase.IPurchaseListener)
extern "C"  void PurchaseForwardCallback__ctor_m859758458 (PurchaseForwardCallback_t144617755 * __this, Il2CppObject * ___purchaseListener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
