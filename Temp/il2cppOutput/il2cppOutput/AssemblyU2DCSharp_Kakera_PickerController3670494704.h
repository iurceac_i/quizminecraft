﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Kakera.Unimgpicker
struct Unimgpicker_t2332148304;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kakera.PickerController
struct  PickerController_t3670494704  : public MonoBehaviour_t1158329972
{
public:
	// Kakera.Unimgpicker Kakera.PickerController::imagePicker
	Unimgpicker_t2332148304 * ___imagePicker_2;
	// UnityEngine.UI.Image Kakera.PickerController::imageRenderer
	Image_t2042527209 * ___imageRenderer_3;
	// UnityEngine.UI.Image Kakera.PickerController::imagePortretRenderer
	Image_t2042527209 * ___imagePortretRenderer_4;

public:
	inline static int32_t get_offset_of_imagePicker_2() { return static_cast<int32_t>(offsetof(PickerController_t3670494704, ___imagePicker_2)); }
	inline Unimgpicker_t2332148304 * get_imagePicker_2() const { return ___imagePicker_2; }
	inline Unimgpicker_t2332148304 ** get_address_of_imagePicker_2() { return &___imagePicker_2; }
	inline void set_imagePicker_2(Unimgpicker_t2332148304 * value)
	{
		___imagePicker_2 = value;
		Il2CppCodeGenWriteBarrier(&___imagePicker_2, value);
	}

	inline static int32_t get_offset_of_imageRenderer_3() { return static_cast<int32_t>(offsetof(PickerController_t3670494704, ___imageRenderer_3)); }
	inline Image_t2042527209 * get_imageRenderer_3() const { return ___imageRenderer_3; }
	inline Image_t2042527209 ** get_address_of_imageRenderer_3() { return &___imageRenderer_3; }
	inline void set_imageRenderer_3(Image_t2042527209 * value)
	{
		___imageRenderer_3 = value;
		Il2CppCodeGenWriteBarrier(&___imageRenderer_3, value);
	}

	inline static int32_t get_offset_of_imagePortretRenderer_4() { return static_cast<int32_t>(offsetof(PickerController_t3670494704, ___imagePortretRenderer_4)); }
	inline Image_t2042527209 * get_imagePortretRenderer_4() const { return ___imagePortretRenderer_4; }
	inline Image_t2042527209 ** get_address_of_imagePortretRenderer_4() { return &___imagePortretRenderer_4; }
	inline void set_imagePortretRenderer_4(Image_t2042527209 * value)
	{
		___imagePortretRenderer_4 = value;
		Il2CppCodeGenWriteBarrier(&___imagePortretRenderer_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
