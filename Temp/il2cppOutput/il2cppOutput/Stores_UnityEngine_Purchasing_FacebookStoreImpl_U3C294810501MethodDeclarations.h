﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t294810501;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass6_0__ctor_m1271070588 (U3CU3Ec__DisplayClass6_0_t294810501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.FacebookStoreImpl/<>c__DisplayClass6_0::<MessageCallback>b__0()
extern "C"  void U3CU3Ec__DisplayClass6_0_U3CMessageCallbackU3Eb__0_m1444268142 (U3CU3Ec__DisplayClass6_0_t294810501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
