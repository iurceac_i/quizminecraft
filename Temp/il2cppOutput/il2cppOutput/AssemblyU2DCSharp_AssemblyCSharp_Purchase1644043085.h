﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// System.String
struct String_t;
// Main
struct Main_t2809994845;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssemblyCSharp.Purchase
struct  Purchase_t1644043085  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject AssemblyCSharp.Purchase::alertPurchase
	GameObject_t1756533147 * ___alertPurchase_3;
	// UnityEngine.GameObject AssemblyCSharp.Purchase::gamePanel
	GameObject_t1756533147 * ___gamePanel_4;
	// Main AssemblyCSharp.Purchase::itemsContent
	Main_t2809994845 * ___itemsContent_10;

public:
	inline static int32_t get_offset_of_alertPurchase_3() { return static_cast<int32_t>(offsetof(Purchase_t1644043085, ___alertPurchase_3)); }
	inline GameObject_t1756533147 * get_alertPurchase_3() const { return ___alertPurchase_3; }
	inline GameObject_t1756533147 ** get_address_of_alertPurchase_3() { return &___alertPurchase_3; }
	inline void set_alertPurchase_3(GameObject_t1756533147 * value)
	{
		___alertPurchase_3 = value;
		Il2CppCodeGenWriteBarrier(&___alertPurchase_3, value);
	}

	inline static int32_t get_offset_of_gamePanel_4() { return static_cast<int32_t>(offsetof(Purchase_t1644043085, ___gamePanel_4)); }
	inline GameObject_t1756533147 * get_gamePanel_4() const { return ___gamePanel_4; }
	inline GameObject_t1756533147 ** get_address_of_gamePanel_4() { return &___gamePanel_4; }
	inline void set_gamePanel_4(GameObject_t1756533147 * value)
	{
		___gamePanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___gamePanel_4, value);
	}

	inline static int32_t get_offset_of_itemsContent_10() { return static_cast<int32_t>(offsetof(Purchase_t1644043085, ___itemsContent_10)); }
	inline Main_t2809994845 * get_itemsContent_10() const { return ___itemsContent_10; }
	inline Main_t2809994845 ** get_address_of_itemsContent_10() { return &___itemsContent_10; }
	inline void set_itemsContent_10(Main_t2809994845 * value)
	{
		___itemsContent_10 = value;
		Il2CppCodeGenWriteBarrier(&___itemsContent_10, value);
	}
};

struct Purchase_t1644043085_StaticFields
{
public:
	// System.Boolean AssemblyCSharp.Purchase::isPurchased
	bool ___isPurchased_2;
	// UnityEngine.Purchasing.IStoreController AssemblyCSharp.Purchase::m_StoreController
	Il2CppObject * ___m_StoreController_5;
	// UnityEngine.Purchasing.IExtensionProvider AssemblyCSharp.Purchase::m_StoreExtensionProvider
	Il2CppObject * ___m_StoreExtensionProvider_6;
	// System.String AssemblyCSharp.Purchase::kProductIDConsumable
	String_t* ___kProductIDConsumable_7;
	// System.String AssemblyCSharp.Purchase::kProductIDNonConsumable
	String_t* ___kProductIDNonConsumable_8;
	// System.String AssemblyCSharp.Purchase::kProductIDSubscription
	String_t* ___kProductIDSubscription_9;

public:
	inline static int32_t get_offset_of_isPurchased_2() { return static_cast<int32_t>(offsetof(Purchase_t1644043085_StaticFields, ___isPurchased_2)); }
	inline bool get_isPurchased_2() const { return ___isPurchased_2; }
	inline bool* get_address_of_isPurchased_2() { return &___isPurchased_2; }
	inline void set_isPurchased_2(bool value)
	{
		___isPurchased_2 = value;
	}

	inline static int32_t get_offset_of_m_StoreController_5() { return static_cast<int32_t>(offsetof(Purchase_t1644043085_StaticFields, ___m_StoreController_5)); }
	inline Il2CppObject * get_m_StoreController_5() const { return ___m_StoreController_5; }
	inline Il2CppObject ** get_address_of_m_StoreController_5() { return &___m_StoreController_5; }
	inline void set_m_StoreController_5(Il2CppObject * value)
	{
		___m_StoreController_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreController_5, value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_6() { return static_cast<int32_t>(offsetof(Purchase_t1644043085_StaticFields, ___m_StoreExtensionProvider_6)); }
	inline Il2CppObject * get_m_StoreExtensionProvider_6() const { return ___m_StoreExtensionProvider_6; }
	inline Il2CppObject ** get_address_of_m_StoreExtensionProvider_6() { return &___m_StoreExtensionProvider_6; }
	inline void set_m_StoreExtensionProvider_6(Il2CppObject * value)
	{
		___m_StoreExtensionProvider_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreExtensionProvider_6, value);
	}

	inline static int32_t get_offset_of_kProductIDConsumable_7() { return static_cast<int32_t>(offsetof(Purchase_t1644043085_StaticFields, ___kProductIDConsumable_7)); }
	inline String_t* get_kProductIDConsumable_7() const { return ___kProductIDConsumable_7; }
	inline String_t** get_address_of_kProductIDConsumable_7() { return &___kProductIDConsumable_7; }
	inline void set_kProductIDConsumable_7(String_t* value)
	{
		___kProductIDConsumable_7 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDConsumable_7, value);
	}

	inline static int32_t get_offset_of_kProductIDNonConsumable_8() { return static_cast<int32_t>(offsetof(Purchase_t1644043085_StaticFields, ___kProductIDNonConsumable_8)); }
	inline String_t* get_kProductIDNonConsumable_8() const { return ___kProductIDNonConsumable_8; }
	inline String_t** get_address_of_kProductIDNonConsumable_8() { return &___kProductIDNonConsumable_8; }
	inline void set_kProductIDNonConsumable_8(String_t* value)
	{
		___kProductIDNonConsumable_8 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDNonConsumable_8, value);
	}

	inline static int32_t get_offset_of_kProductIDSubscription_9() { return static_cast<int32_t>(offsetof(Purchase_t1644043085_StaticFields, ___kProductIDSubscription_9)); }
	inline String_t* get_kProductIDSubscription_9() const { return ___kProductIDSubscription_9; }
	inline String_t** get_address_of_kProductIDSubscription_9() { return &___kProductIDSubscription_9; }
	inline void set_kProductIDSubscription_9(String_t* value)
	{
		___kProductIDSubscription_9 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDSubscription_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
