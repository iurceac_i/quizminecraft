﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Adapter
struct Adapter_t814751345;
// Levels[]
struct LevelsU5BU5D_t2050899114;
// QuestionController
struct QuestionController_t445239244;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelController
struct  LevelController_t2717835266  : public MonoBehaviour_t1158329972
{
public:
	// Adapter LevelController::adapter
	Adapter_t814751345 * ___adapter_2;
	// Levels[] LevelController::levels
	LevelsU5BU5D_t2050899114* ___levels_3;
	// QuestionController LevelController::questController
	QuestionController_t445239244 * ___questController_4;
	// UnityEngine.GameObject LevelController::gamePanel
	GameObject_t1756533147 * ___gamePanel_5;
	// UnityEngine.GameObject LevelController::buyPanel
	GameObject_t1756533147 * ___buyPanel_6;

public:
	inline static int32_t get_offset_of_adapter_2() { return static_cast<int32_t>(offsetof(LevelController_t2717835266, ___adapter_2)); }
	inline Adapter_t814751345 * get_adapter_2() const { return ___adapter_2; }
	inline Adapter_t814751345 ** get_address_of_adapter_2() { return &___adapter_2; }
	inline void set_adapter_2(Adapter_t814751345 * value)
	{
		___adapter_2 = value;
		Il2CppCodeGenWriteBarrier(&___adapter_2, value);
	}

	inline static int32_t get_offset_of_levels_3() { return static_cast<int32_t>(offsetof(LevelController_t2717835266, ___levels_3)); }
	inline LevelsU5BU5D_t2050899114* get_levels_3() const { return ___levels_3; }
	inline LevelsU5BU5D_t2050899114** get_address_of_levels_3() { return &___levels_3; }
	inline void set_levels_3(LevelsU5BU5D_t2050899114* value)
	{
		___levels_3 = value;
		Il2CppCodeGenWriteBarrier(&___levels_3, value);
	}

	inline static int32_t get_offset_of_questController_4() { return static_cast<int32_t>(offsetof(LevelController_t2717835266, ___questController_4)); }
	inline QuestionController_t445239244 * get_questController_4() const { return ___questController_4; }
	inline QuestionController_t445239244 ** get_address_of_questController_4() { return &___questController_4; }
	inline void set_questController_4(QuestionController_t445239244 * value)
	{
		___questController_4 = value;
		Il2CppCodeGenWriteBarrier(&___questController_4, value);
	}

	inline static int32_t get_offset_of_gamePanel_5() { return static_cast<int32_t>(offsetof(LevelController_t2717835266, ___gamePanel_5)); }
	inline GameObject_t1756533147 * get_gamePanel_5() const { return ___gamePanel_5; }
	inline GameObject_t1756533147 ** get_address_of_gamePanel_5() { return &___gamePanel_5; }
	inline void set_gamePanel_5(GameObject_t1756533147 * value)
	{
		___gamePanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___gamePanel_5, value);
	}

	inline static int32_t get_offset_of_buyPanel_6() { return static_cast<int32_t>(offsetof(LevelController_t2717835266, ___buyPanel_6)); }
	inline GameObject_t1756533147 * get_buyPanel_6() const { return ___buyPanel_6; }
	inline GameObject_t1756533147 ** get_address_of_buyPanel_6() { return &___buyPanel_6; }
	inline void set_buyPanel_6(GameObject_t1756533147 * value)
	{
		___buyPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___buyPanel_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
