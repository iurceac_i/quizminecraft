﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.Store.AppInfo
struct AppInfo_t2080248435;
// System.String
struct String_t;
// UnityEngine.Store.LoginForwardCallback
struct LoginForwardCallback_t1105422505;
// UnityEngine.Store.ILoginListener
struct ILoginListener_t3164042774;
// UnityEngine.Store.MainThreadDispatcher
struct MainThreadDispatcher_t513574034;
// System.Action
struct Action_t3226471752;
// System.Object
struct Il2CppObject;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4251328308;
// UnityEngine.Store.UserInfo
struct UserInfo_t741955747;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityStore_U3CModuleU3E3783534214.h"
#include "UnityStore_U3CModuleU3E3783534214MethodDeclarations.h"
#include "UnityStore_UnityEngine_Store_AppInfo2080248435.h"
#include "UnityStore_UnityEngine_Store_AppInfo2080248435MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityStore_UnityEngine_Store_LoginForwardCallback1105422505.h"
#include "UnityStore_UnityEngine_Store_LoginForwardCallback1105422505MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaProxy4274989947MethodDeclarations.h"
#include "UnityStore_UnityEngine_Store_MainThreadDispatcher513574034.h"
#include "UnityStore_UnityEngine_Store_MainThreadDispatcher513574034MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2595592884MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2595592884.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "System_Core_System_Action3226471752MethodDeclarations.h"
#include "System.Core_ArrayTypes.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityStore_UnityEngine_Store_StoreService3339196318.h"
#include "UnityStore_UnityEngine_Store_StoreService3339196318MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityStore_UnityEngine_Store_UserInfo741955747.h"
#include "UnityStore_UnityEngine_Store_UserInfo741955747MethodDeclarations.h"

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3535407496_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3535407496(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3535407496_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Store.MainThreadDispatcher>()
#define GameObject_AddComponent_TisMainThreadDispatcher_t513574034_m3133984902(__this, method) ((  MainThreadDispatcher_t513574034 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3535407496_gshared)(__this, method)
// !!0 UnityEngine.AndroidJavaObject::GetStatic<System.Object>(System.String)
extern "C"  Il2CppObject * AndroidJavaObject_GetStatic_TisIl2CppObject_m569049653_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, const MethodInfo* method);
#define AndroidJavaObject_GetStatic_TisIl2CppObject_m569049653(__this, p0, method) ((  Il2CppObject * (*) (AndroidJavaObject_t4251328308 *, String_t*, const MethodInfo*))AndroidJavaObject_GetStatic_TisIl2CppObject_m569049653_gshared)(__this, p0, method)
// !!0 UnityEngine.AndroidJavaObject::GetStatic<UnityEngine.AndroidJavaObject>(System.String)
#define AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m974310956(__this, p0, method) ((  AndroidJavaObject_t4251328308 * (*) (AndroidJavaObject_t4251328308 *, String_t*, const MethodInfo*))AndroidJavaObject_GetStatic_TisIl2CppObject_m569049653_gshared)(__this, p0, method)
// System.Void UnityEngine.AndroidJavaObject::Set<System.Object>(System.String,!!0)
extern "C"  void AndroidJavaObject_Set_TisIl2CppObject_m1125093376_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, Il2CppObject * p1, const MethodInfo* method);
#define AndroidJavaObject_Set_TisIl2CppObject_m1125093376(__this, p0, p1, method) ((  void (*) (AndroidJavaObject_t4251328308 *, String_t*, Il2CppObject *, const MethodInfo*))AndroidJavaObject_Set_TisIl2CppObject_m1125093376_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.AndroidJavaObject::Set<System.String>(System.String,!!0)
#define AndroidJavaObject_Set_TisString_t_m551399930(__this, p0, p1, method) ((  void (*) (AndroidJavaObject_t4251328308 *, String_t*, String_t*, const MethodInfo*))AndroidJavaObject_Set_TisIl2CppObject_m1125093376_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.AndroidJavaObject::Set<System.Boolean>(System.String,!!0)
extern "C"  void AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, bool p1, const MethodInfo* method);
#define AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435(__this, p0, p1, method) ((  void (*) (AndroidJavaObject_t4251328308 *, String_t*, bool, const MethodInfo*))AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435_gshared)(__this, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Store.AppInfo::.ctor()
extern "C"  void AppInfo__ctor_m2870035846 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Store.AppInfo::get_appId()
extern "C"  String_t* AppInfo_get_appId_m3632567790 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CappIdU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Store.AppInfo::set_appId(System.String)
extern "C"  void AppInfo_set_appId_m1112756485 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CappIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String UnityEngine.Store.AppInfo::get_appKey()
extern "C"  String_t* AppInfo_get_appKey_m3678981840 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CappKeyU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Store.AppInfo::set_appKey(System.String)
extern "C"  void AppInfo_set_appKey_m3344021451 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CappKeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UnityEngine.Store.AppInfo::get_clientId()
extern "C"  String_t* AppInfo_get_clientId_m1686889884 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CclientIdU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Store.AppInfo::set_clientId(System.String)
extern "C"  void AppInfo_set_clientId_m1298558813 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CclientIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String UnityEngine.Store.AppInfo::get_clientKey()
extern "C"  String_t* AppInfo_get_clientKey_m385900370 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CclientKeyU3Ek__BackingField_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Store.AppInfo::set_clientKey(System.String)
extern "C"  void AppInfo_set_clientKey_m1161742527 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CclientKeyU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Store.AppInfo::get_debug()
extern "C"  bool AppInfo_get_debug_m3181976920 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_U3CdebugU3Ek__BackingField_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Store.AppInfo::set_debug(System.Boolean)
extern "C"  void AppInfo_set_debug_m4042117453 (AppInfo_t2080248435 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CdebugU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void UnityEngine.Store.LoginForwardCallback::.ctor(UnityEngine.Store.ILoginListener)
extern Il2CppCodeGenString* _stringLiteral772087215;
extern const uint32_t LoginForwardCallback__ctor_m1842789250_MetadataUsageId;
extern "C"  void LoginForwardCallback__ctor_m1842789250 (LoginForwardCallback_t1105422505 * __this, Il2CppObject * ___loginListener0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoginForwardCallback__ctor_m1842789250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaProxy__ctor_m4016180768(__this, _stringLiteral772087215, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___loginListener0;
		__this->set_loginListener_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Store.MainThreadDispatcher::.ctor()
extern "C"  void MainThreadDispatcher__ctor_m292601619 (MainThreadDispatcher_t513574034 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Store.MainThreadDispatcher::RunOnMainThread(System.Action)
extern Il2CppClass* MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m253778810_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_RunOnMainThread_m3425248423_MetadataUsageId;
extern "C"  void MainThreadDispatcher_RunOnMainThread_m3425248423 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___runnable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_RunOnMainThread_m3425248423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		List_1_t2595592884 * L_0 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		List_1_t2595592884 * L_2 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
		Action_t3226471752 * L_3 = ___runnable0;
		NullCheck(L_2);
		List_1_Add_m253778810(L_2, L_3, /*hidden argument*/List_1_Add_m253778810_MethodInfo_var);
		il2cpp_codegen_memory_barrier();
		((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->set_s_CallbacksPending_4(1);
		IL2CPP_LEAVE(0x2E, FINALLY_0027);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0027;
	}

FINALLY_0027:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(39)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(39)
	{
		IL2CPP_JUMP_TBL(0x2E, IL_002e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_002e:
	{
		return;
	}
}
// System.Void UnityEngine.Store.MainThreadDispatcher::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t MainThreadDispatcher_Start_m3725736351_MetadataUsageId;
extern "C"  void MainThreadDispatcher_Start_m3725736351 (MainThreadDispatcher_t513574034 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_Start_m3725736351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Store.MainThreadDispatcher::Update()
extern Il2CppClass* MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var;
extern Il2CppClass* ActionU5BU5D_t87223449_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1567401164_MethodInfo_var;
extern const MethodInfo* List_1_CopyTo_m3662704345_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m2016173969_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_Update_m546764710_MetadataUsageId;
extern "C"  void MainThreadDispatcher_Update_m546764710 (MainThreadDispatcher_t513574034 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_Update_m546764710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ActionU5BU5D_t87223449* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Action_t3226471752 * V_2 = NULL;
	ActionU5BU5D_t87223449* V_3 = NULL;
	int32_t V_4 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		bool L_0 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_CallbacksPending_4();
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_0095;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		List_1_t2595592884 * L_1 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
		V_1 = L_1;
		Il2CppObject * L_2 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
			List_1_t2595592884 * L_3 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
			NullCheck(L_3);
			int32_t L_4 = List_1_get_Count_m1567401164(L_3, /*hidden argument*/List_1_get_Count_m1567401164_MethodInfo_var);
			if (L_4)
			{
				goto IL_0033;
			}
		}

IL_002e:
		{
			IL2CPP_LEAVE(0x95, FINALLY_0066);
		}

IL_0033:
		{
			IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
			List_1_t2595592884 * L_5 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
			NullCheck(L_5);
			int32_t L_6 = List_1_get_Count_m1567401164(L_5, /*hidden argument*/List_1_get_Count_m1567401164_MethodInfo_var);
			V_0 = ((ActionU5BU5D_t87223449*)SZArrayNew(ActionU5BU5D_t87223449_il2cpp_TypeInfo_var, (uint32_t)L_6));
			List_1_t2595592884 * L_7 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
			ActionU5BU5D_t87223449* L_8 = V_0;
			NullCheck(L_7);
			List_1_CopyTo_m3662704345(L_7, L_8, /*hidden argument*/List_1_CopyTo_m3662704345_MethodInfo_var);
			List_1_t2595592884 * L_9 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
			NullCheck(L_9);
			List_1_Clear_m2016173969(L_9, /*hidden argument*/List_1_Clear_m2016173969_MethodInfo_var);
			il2cpp_codegen_memory_barrier();
			((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->set_s_CallbacksPending_4(0);
			IL2CPP_LEAVE(0x6D, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		Il2CppObject * L_10 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006d:
	{
		ActionU5BU5D_t87223449* L_11 = V_0;
		V_3 = L_11;
		V_4 = 0;
		goto IL_008b;
	}

IL_0078:
	{
		ActionU5BU5D_t87223449* L_12 = V_3;
		int32_t L_13 = V_4;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Action_t3226471752 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_2 = L_15;
		Action_t3226471752 * L_16 = V_2;
		NullCheck(L_16);
		Action_Invoke_m3801112262(L_16, /*hidden argument*/NULL);
		int32_t L_17 = V_4;
		V_4 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_008b:
	{
		int32_t L_18 = V_4;
		ActionU5BU5D_t87223449* L_19 = V_3;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0078;
		}
	}

IL_0095:
	{
		return;
	}
}
// System.Void UnityEngine.Store.MainThreadDispatcher::.cctor()
extern Il2CppClass* MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2595592884_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3444459430_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral334013814;
extern const uint32_t MainThreadDispatcher__cctor_m2497885682_MetadataUsageId;
extern "C"  void MainThreadDispatcher__cctor_m2497885682 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher__cctor_m2497885682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->set_OBJECT_NAME_2(_stringLiteral334013814);
		List_1_t2595592884 * L_0 = (List_1_t2595592884 *)il2cpp_codegen_object_new(List_1_t2595592884_il2cpp_TypeInfo_var);
		List_1__ctor_m3444459430(L_0, /*hidden argument*/List_1__ctor_m3444459430_MethodInfo_var);
		((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->set_s_Callbacks_3(L_0);
		return;
	}
}
// System.Void UnityEngine.Store.StoreService::Initialize(UnityEngine.Store.AppInfo,UnityEngine.Store.ILoginListener)
extern Il2CppClass* MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var;
extern Il2CppClass* LoginForwardCallback_t1105422505_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaObject_t4251328308_il2cpp_TypeInfo_var;
extern Il2CppClass* StoreService_t3339196318_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMainThreadDispatcher_t513574034_m3133984902_MethodInfo_var;
extern const MethodInfo* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m974310956_MethodInfo_var;
extern const MethodInfo* AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var;
extern const MethodInfo* AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3075639932;
extern Il2CppCodeGenString* _stringLiteral2584981958;
extern Il2CppCodeGenString* _stringLiteral766701006;
extern Il2CppCodeGenString* _stringLiteral526454908;
extern Il2CppCodeGenString* _stringLiteral1195878932;
extern Il2CppCodeGenString* _stringLiteral1643072148;
extern Il2CppCodeGenString* _stringLiteral1396753435;
extern Il2CppCodeGenString* _stringLiteral882420973;
extern Il2CppCodeGenString* _stringLiteral2126705824;
extern const uint32_t StoreService_Initialize_m2334922442_MetadataUsageId;
extern "C"  void StoreService_Initialize_m2334922442 (Il2CppObject * __this /* static, unused */, AppInfo_t2080248435 * ___appInfo0, Il2CppObject * ___listener1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService_Initialize_m2334922442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	AndroidJavaClass_t2973420583 * V_1 = NULL;
	AndroidJavaObject_t4251328308 * V_2 = NULL;
	LoginForwardCallback_t1105422505 * V_3 = NULL;
	AndroidJavaObject_t4251328308 * V_4 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		String_t* L_0 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_OBJECT_NAME_2();
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		String_t* L_3 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_OBJECT_NAME_2();
		GameObject_t1756533147 * L_4 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_4, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t1756533147 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		Object_set_hideFlags_m2204253440(L_6, 3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = V_0;
		NullCheck(L_7);
		GameObject_AddComponent_TisMainThreadDispatcher_t513574034_m3133984902(L_7, /*hidden argument*/GameObject_AddComponent_TisMainThreadDispatcher_t513574034_m3133984902_MethodInfo_var);
	}

IL_0037:
	{
		AndroidJavaClass_t2973420583 * L_8 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_8, _stringLiteral3075639932, /*hidden argument*/NULL);
		V_1 = L_8;
		AndroidJavaClass_t2973420583 * L_9 = V_1;
		NullCheck(L_9);
		AndroidJavaObject_t4251328308 * L_10 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m974310956(L_9, _stringLiteral2584981958, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m974310956_MethodInfo_var);
		V_2 = L_10;
		Il2CppObject * L_11 = ___listener1;
		LoginForwardCallback_t1105422505 * L_12 = (LoginForwardCallback_t1105422505 *)il2cpp_codegen_object_new(LoginForwardCallback_t1105422505_il2cpp_TypeInfo_var);
		LoginForwardCallback__ctor_m1842789250(L_12, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		AndroidJavaObject_t4251328308 * L_13 = (AndroidJavaObject_t4251328308 *)il2cpp_codegen_object_new(AndroidJavaObject_t4251328308_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m1076535321(L_13, _stringLiteral766701006, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_4 = L_13;
		AndroidJavaObject_t4251328308 * L_14 = V_4;
		AppInfo_t2080248435 * L_15 = ___appInfo0;
		NullCheck(L_15);
		String_t* L_16 = AppInfo_get_appId_m3632567790(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		AndroidJavaObject_Set_TisString_t_m551399930(L_14, _stringLiteral526454908, L_16, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var);
		AndroidJavaObject_t4251328308 * L_17 = V_4;
		AppInfo_t2080248435 * L_18 = ___appInfo0;
		NullCheck(L_18);
		String_t* L_19 = AppInfo_get_appKey_m3678981840(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		AndroidJavaObject_Set_TisString_t_m551399930(L_17, _stringLiteral1195878932, L_19, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var);
		AndroidJavaObject_t4251328308 * L_20 = V_4;
		AppInfo_t2080248435 * L_21 = ___appInfo0;
		NullCheck(L_21);
		String_t* L_22 = AppInfo_get_clientId_m1686889884(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		AndroidJavaObject_Set_TisString_t_m551399930(L_20, _stringLiteral1643072148, L_22, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var);
		AndroidJavaObject_t4251328308 * L_23 = V_4;
		AppInfo_t2080248435 * L_24 = ___appInfo0;
		NullCheck(L_24);
		String_t* L_25 = AppInfo_get_clientKey_m385900370(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		AndroidJavaObject_Set_TisString_t_m551399930(L_23, _stringLiteral1396753435, L_25, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var);
		AndroidJavaObject_t4251328308 * L_26 = V_4;
		AppInfo_t2080248435 * L_27 = ___appInfo0;
		NullCheck(L_27);
		bool L_28 = AppInfo_get_debug_m3181976920(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435(L_26, _stringLiteral882420973, L_28, /*hidden argument*/AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(StoreService_t3339196318_il2cpp_TypeInfo_var);
		AndroidJavaClass_t2973420583 * L_29 = ((StoreService_t3339196318_StaticFields*)StoreService_t3339196318_il2cpp_TypeInfo_var->static_fields)->get_serviceClass_0();
		ObjectU5BU5D_t3614634134* L_30 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3));
		AndroidJavaObject_t4251328308 * L_31 = V_2;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_31);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_31);
		ObjectU5BU5D_t3614634134* L_32 = L_30;
		AndroidJavaObject_t4251328308 * L_33 = V_4;
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_33);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_33);
		ObjectU5BU5D_t3614634134* L_34 = L_32;
		LoginForwardCallback_t1105422505 * L_35 = V_3;
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, L_35);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_35);
		NullCheck(L_29);
		AndroidJavaObject_CallStatic_m1227537731(L_29, _stringLiteral2126705824, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Store.StoreService::Login(UnityEngine.Store.ILoginListener)
extern Il2CppClass* LoginForwardCallback_t1105422505_il2cpp_TypeInfo_var;
extern Il2CppClass* StoreService_t3339196318_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4283626287;
extern const uint32_t StoreService_Login_m2535849060_MetadataUsageId;
extern "C"  void StoreService_Login_m2535849060 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___listener0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService_Login_m2535849060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LoginForwardCallback_t1105422505 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___listener0;
		LoginForwardCallback_t1105422505 * L_1 = (LoginForwardCallback_t1105422505 *)il2cpp_codegen_object_new(LoginForwardCallback_t1105422505_il2cpp_TypeInfo_var);
		LoginForwardCallback__ctor_m1842789250(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(StoreService_t3339196318_il2cpp_TypeInfo_var);
		AndroidJavaClass_t2973420583 * L_2 = ((StoreService_t3339196318_StaticFields*)StoreService_t3339196318_il2cpp_TypeInfo_var->static_fields)->get_serviceClass_0();
		ObjectU5BU5D_t3614634134* L_3 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		LoginForwardCallback_t1105422505 * L_4 = V_0;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck(L_2);
		AndroidJavaObject_CallStatic_m1227537731(L_2, _stringLiteral4283626287, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Store.StoreService::.cctor()
extern Il2CppClass* AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var;
extern Il2CppClass* StoreService_t3339196318_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2225618755;
extern const uint32_t StoreService__cctor_m2623941302_MetadataUsageId;
extern "C"  void StoreService__cctor_m2623941302 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService__cctor_m2623941302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaClass_t2973420583 * L_0 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_0, _stringLiteral2225618755, /*hidden argument*/NULL);
		((StoreService_t3339196318_StaticFields*)StoreService_t3339196318_il2cpp_TypeInfo_var->static_fields)->set_serviceClass_0(L_0);
		return;
	}
}
// System.String UnityEngine.Store.UserInfo::get_channel()
extern "C"  String_t* UserInfo_get_channel_m1234361509 (UserInfo_t741955747 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CchannelU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Store.UserInfo::get_userId()
extern "C"  String_t* UserInfo_get_userId_m3160010982 (UserInfo_t741955747 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CuserIdU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Store.UserInfo::get_userLoginToken()
extern "C"  String_t* UserInfo_get_userLoginToken_m706751899 (UserInfo_t741955747 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CuserLoginTokenU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
