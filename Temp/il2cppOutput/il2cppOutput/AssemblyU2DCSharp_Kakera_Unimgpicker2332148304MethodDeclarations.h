﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Kakera.Unimgpicker
struct Unimgpicker_t2332148304;
// Kakera.Unimgpicker/ImageDelegate
struct ImageDelegate_t3548206662;
// Kakera.Unimgpicker/ErrorDelegate
struct ErrorDelegate_t402150177;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker_ImageDelegate3548206662.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker_ErrorDelegate402150177.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Kakera.Unimgpicker::.ctor()
extern "C"  void Unimgpicker__ctor_m4196344082 (Unimgpicker_t2332148304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Unimgpicker::add_Completed(Kakera.Unimgpicker/ImageDelegate)
extern "C"  void Unimgpicker_add_Completed_m169132615 (Unimgpicker_t2332148304 * __this, ImageDelegate_t3548206662 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Unimgpicker::remove_Completed(Kakera.Unimgpicker/ImageDelegate)
extern "C"  void Unimgpicker_remove_Completed_m1384417240 (Unimgpicker_t2332148304 * __this, ImageDelegate_t3548206662 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Unimgpicker::add_Failed(Kakera.Unimgpicker/ErrorDelegate)
extern "C"  void Unimgpicker_add_Failed_m613040790 (Unimgpicker_t2332148304 * __this, ErrorDelegate_t402150177 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Unimgpicker::remove_Failed(Kakera.Unimgpicker/ErrorDelegate)
extern "C"  void Unimgpicker_remove_Failed_m1445021837 (Unimgpicker_t2332148304 * __this, ErrorDelegate_t402150177 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Unimgpicker::Show(System.String,System.String,System.Int32)
extern "C"  void Unimgpicker_Show_m3309216668 (Unimgpicker_t2332148304 * __this, String_t* ___title0, String_t* ___outputFileName1, int32_t ___maxSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Unimgpicker::OnComplete(System.String)
extern "C"  void Unimgpicker_OnComplete_m1188031576 (Unimgpicker_t2332148304 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Unimgpicker::OnFailure(System.String)
extern "C"  void Unimgpicker_OnFailure_m2180883323 (Unimgpicker_t2332148304 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
