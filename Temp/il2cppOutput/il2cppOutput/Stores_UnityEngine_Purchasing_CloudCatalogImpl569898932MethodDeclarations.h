﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.CloudCatalogImpl
struct CloudCatalogImpl_t569898932;
// System.String
struct String_t;
// UnityEngine.Purchasing.IAsyncWebUtil
struct IAsyncWebUtil_t364059421;
// UnityEngine.ILogger
struct ILogger_t1425954571;
// System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>
struct Action_1_t77735504;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>
struct HashSet_1_t275936122;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// UnityEngine.Purchasing.CloudCatalogImpl UnityEngine.Purchasing.CloudCatalogImpl::CreateInstance(System.String)
extern "C"  CloudCatalogImpl_t569898932 * CloudCatalogImpl_CreateInstance_m2226279203 (Il2CppObject * __this /* static, unused */, String_t* ___storeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.CloudCatalogImpl::.ctor(UnityEngine.Purchasing.IAsyncWebUtil,System.String,UnityEngine.ILogger,System.String,System.String)
extern "C"  void CloudCatalogImpl__ctor_m1640096356 (CloudCatalogImpl_t569898932 * __this, Il2CppObject * ___util0, String_t* ___cacheFile1, Il2CppObject * ___logger2, String_t* ___catalogURL3, String_t* ___storeName4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.CloudCatalogImpl::FetchProducts(System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>)
extern "C"  void CloudCatalogImpl_FetchProducts_m2003249609 (CloudCatalogImpl_t569898932 * __this, Action_1_t77735504 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.CloudCatalogImpl::FetchProducts(System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>,System.Int32)
extern "C"  void CloudCatalogImpl_FetchProducts_m1108461544 (CloudCatalogImpl_t569898932 * __this, Action_1_t77735504 * ___callback0, int32_t ___delayInSeconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.CloudCatalogImpl::ParseProductsFromJSON(System.String,System.String)
extern "C"  HashSet_1_t275936122 * CloudCatalogImpl_ParseProductsFromJSON_m531940327 (Il2CppObject * __this /* static, unused */, String_t* ___json0, String_t* ___storeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.CloudCatalogImpl::CamelCaseToSnakeCase(System.String)
extern "C"  String_t* CloudCatalogImpl_CamelCaseToSnakeCase_m184670320 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.CloudCatalogImpl::TryPersistCatalog(System.String)
extern "C"  void CloudCatalogImpl_TryPersistCatalog_m3079513122 (CloudCatalogImpl_t569898932 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.CloudCatalogImpl::TryLoadCachedCatalog()
extern "C"  HashSet_1_t275936122 * CloudCatalogImpl_TryLoadCachedCatalog_m3662621688 (CloudCatalogImpl_t569898932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
