﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Handler/ExecuteWithButton
struct ExecuteWithButton_t3718107720;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Handler/<WaitForSeconds>c__Iterator3
struct  U3CWaitForSecondsU3Ec__Iterator3_t1286869610  : public Il2CppObject
{
public:
	// System.Single Handler/<WaitForSeconds>c__Iterator3::time
	float ___time_0;
	// Handler/ExecuteWithButton Handler/<WaitForSeconds>c__Iterator3::method
	ExecuteWithButton_t3718107720 * ___method_1;
	// UnityEngine.UI.Button Handler/<WaitForSeconds>c__Iterator3::btn
	Button_t2872111280 * ___btn_2;
	// System.Object Handler/<WaitForSeconds>c__Iterator3::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean Handler/<WaitForSeconds>c__Iterator3::$disposing
	bool ___U24disposing_4;
	// System.Int32 Handler/<WaitForSeconds>c__Iterator3::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator3_t1286869610, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator3_t1286869610, ___method_1)); }
	inline ExecuteWithButton_t3718107720 * get_method_1() const { return ___method_1; }
	inline ExecuteWithButton_t3718107720 ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(ExecuteWithButton_t3718107720 * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier(&___method_1, value);
	}

	inline static int32_t get_offset_of_btn_2() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator3_t1286869610, ___btn_2)); }
	inline Button_t2872111280 * get_btn_2() const { return ___btn_2; }
	inline Button_t2872111280 ** get_address_of_btn_2() { return &___btn_2; }
	inline void set_btn_2(Button_t2872111280 * value)
	{
		___btn_2 = value;
		Il2CppCodeGenWriteBarrier(&___btn_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator3_t1286869610, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator3_t1286869610, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator3_t1286869610, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
