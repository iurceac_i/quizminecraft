﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Handler/<WaitForSeconds>c__Iterator0
struct U3CWaitForSecondsU3Ec__Iterator0_t4015752965;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Handler/<WaitForSeconds>c__Iterator0::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0__ctor_m1894433998 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Handler/<WaitForSeconds>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator0_MoveNext_m2095135530 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Handler/<WaitForSeconds>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2981468158 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Handler/<WaitForSeconds>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2172460022 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/<WaitForSeconds>c__Iterator0::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Dispose_m46014511 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/<WaitForSeconds>c__Iterator0::Reset()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Reset_m3335393237 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
