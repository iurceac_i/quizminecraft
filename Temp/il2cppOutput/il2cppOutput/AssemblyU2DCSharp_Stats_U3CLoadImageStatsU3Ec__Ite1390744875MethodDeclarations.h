﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stats/<LoadImageStats>c__Iterator0
struct U3CLoadImageStatsU3Ec__Iterator0_t1390744875;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Stats/<LoadImageStats>c__Iterator0::.ctor()
extern "C"  void U3CLoadImageStatsU3Ec__Iterator0__ctor_m3820255872 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Stats/<LoadImageStats>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadImageStatsU3Ec__Iterator0_MoveNext_m3115053136 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Stats/<LoadImageStats>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadImageStatsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1270063688 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Stats/<LoadImageStats>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadImageStatsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m16369936 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats/<LoadImageStats>c__Iterator0::Dispose()
extern "C"  void U3CLoadImageStatsU3Ec__Iterator0_Dispose_m2104620369 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats/<LoadImageStats>c__Iterator0::Reset()
extern "C"  void U3CLoadImageStatsU3Ec__Iterator0_Reset_m318265383 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
