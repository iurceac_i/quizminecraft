﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Purchasing.MiniJSON.Json/Parser
struct Parser_t2551496676;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// UnityEngine.Purchasing.MiniJSON.Json/Serializer
struct Serializer_t645677759;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.IList
struct IList_t3321498491;
// UnityEngine.Purchasing.UnityPurchasingCallback
struct UnityPurchasingCallback_t2635187846;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Purchasing_Common_U3CModuleU3E3783534214.h"
#include "Purchasing_Common_U3CModuleU3E3783534214MethodDeclarations.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJson838727235.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJson838727235MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_J581258312MethodDeclarations.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_J581258312.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_2551496676MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_J645677759MethodDeclarations.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_2551496676.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Char3454481338MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader1480123486MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader1480123486.h"
#include "mscorlib_System_IO_TextReader1561828458MethodDeclarations.h"
#include "mscorlib_System_IO_TextReader1561828458.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261MethodDeclarations.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_3093089510.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524MethodDeclarations.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "mscorlib_System_Double4078015681MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_Globalization_NumberStyles3408984435.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_3093089510MethodDeclarations.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_J645677759.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Decimal724701077.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_Mi53552535.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_Mi53552535MethodDeclarations.h"
#include "Purchasing_Common_UnityEngine_Purchasing_UnityPurc2635187846.h"
#include "Purchasing_Common_UnityEngine_Purchasing_UnityPurc2635187846MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String UnityEngine.Purchasing.MiniJson::JsonEncode(System.Object)
extern "C"  String_t* MiniJson_JsonEncode_m3219535883 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = ___json0;
		String_t* L_1 = Json_Serialize_m2966712493(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Object UnityEngine.Purchasing.MiniJson::JsonDecode(System.String)
extern "C"  Il2CppObject * MiniJson_JsonDecode_m947751129 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		String_t* L_0 = ___json0;
		Il2CppObject * L_1 = Json_Deserialize_m1979311554(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// System.Object UnityEngine.Purchasing.MiniJSON.Json::Deserialize(System.String)
extern "C"  Il2CppObject * Json_Deserialize_m1979311554 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	{
		String_t* L_0 = ___json0;
		V_0 = (bool)((((Il2CppObject*)(String_t*)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		V_1 = NULL;
		goto IL_0017;
	}

IL_000e:
	{
		String_t* L_2 = ___json0;
		Il2CppObject * L_3 = Parser_Parse_m2389868300(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		Il2CppObject * L_4 = V_1;
		return L_4;
	}
}
// System.String UnityEngine.Purchasing.MiniJSON.Json::Serialize(System.Object)
extern "C"  String_t* Json_Serialize_m2966712493 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		String_t* L_1 = Serializer_Serialize_m1537877676(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Purchasing.MiniJSON.Json/Parser::IsWordBreak(System.Char)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1102494086;
extern const uint32_t Parser_IsWordBreak_m1816689754_MetadataUsageId;
extern "C"  bool Parser_IsWordBreak_m1816689754 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_IsWordBreak_m1816689754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Il2CppChar L_0 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsWhiteSpace_m1507160293(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppChar L_2 = ___c0;
		NullCheck(_stringLiteral1102494086);
		int32_t L_3 = String_IndexOf_m2358239236(_stringLiteral1102494086, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001d;
	}

IL_001c:
	{
		G_B3_0 = 1;
	}

IL_001d:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0020;
	}

IL_0020:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Purchasing.MiniJSON.Json/Parser::.ctor(System.String)
extern Il2CppClass* StringReader_t1480123486_il2cpp_TypeInfo_var;
extern const uint32_t Parser__ctor_m615402772_MetadataUsageId;
extern "C"  void Parser__ctor_m615402772 (Parser_t2551496676 * __this, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser__ctor_m615402772_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___jsonString0;
		StringReader_t1480123486 * L_1 = (StringReader_t1480123486 *)il2cpp_codegen_object_new(StringReader_t1480123486_il2cpp_TypeInfo_var);
		StringReader__ctor_m643998729(L_1, L_0, /*hidden argument*/NULL);
		__this->set_json_0(L_1);
		return;
	}
}
// System.Object UnityEngine.Purchasing.MiniJSON.Json/Parser::Parse(System.String)
extern Il2CppClass* Parser_t2551496676_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Parser_Parse_m2389868300_MetadataUsageId;
extern "C"  Il2CppObject * Parser_Parse_m2389868300 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_Parse_m2389868300_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Parser_t2551496676 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___jsonString0;
		Parser_t2551496676 * L_1 = (Parser_t2551496676 *)il2cpp_codegen_object_new(Parser_t2551496676_il2cpp_TypeInfo_var);
		Parser__ctor_m615402772(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		Parser_t2551496676 * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_3 = Parser_ParseValue_m1337096923(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_LEAVE(0x1D, FINALLY_0012);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0012;
	}

FINALLY_0012:
	{ // begin finally (depth: 1)
		{
			Parser_t2551496676 * L_4 = V_0;
			if (!L_4)
			{
				goto IL_001c;
			}
		}

IL_0015:
		{
			Parser_t2551496676 * L_5 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_5);
		}

IL_001c:
		{
			IL2CPP_END_FINALLY(18)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(18)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_001d:
	{
		Il2CppObject * L_6 = V_1;
		return L_6;
	}
}
// System.Void UnityEngine.Purchasing.MiniJSON.Json/Parser::Dispose()
extern "C"  void Parser_Dispose_m1469870323 (Parser_t2551496676 * __this, const MethodInfo* method)
{
	{
		StringReader_t1480123486 * L_0 = __this->get_json_0();
		NullCheck(L_0);
		TextReader_Dispose_m4077464570(L_0, /*hidden argument*/NULL);
		__this->set_json_0((StringReader_t1480123486 *)NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MiniJSON.Json/Parser::ParseObject()
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3188644741_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m4132139590_MethodInfo_var;
extern const uint32_t Parser_ParseObject_m2734739896_MetadataUsageId;
extern "C"  Dictionary_2_t309261261 * Parser_ParseObject_m2734739896 (Parser_t2551496676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseObject_m2734739896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	Dictionary_2_t309261261 * V_3 = NULL;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	{
		Dictionary_2_t309261261 * L_0 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3188644741(L_0, /*hidden argument*/Dictionary_2__ctor_m3188644741_MethodInfo_var);
		V_0 = L_0;
		StringReader_t1480123486 * L_1 = __this->get_json_0();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Read() */, L_1);
		goto IL_0082;
	}

IL_0015:
	{
		int32_t L_2 = Parser_get_NextToken_m955932932(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		goto IL_0022;
	}

IL_0022:
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_0028;
	}

IL_0028:
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)6)))
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0038;
	}

IL_002e:
	{
		V_3 = (Dictionary_2_t309261261 *)NULL;
		goto IL_0087;
	}

IL_0032:
	{
		goto IL_0082;
	}

IL_0034:
	{
		Dictionary_2_t309261261 * L_6 = V_0;
		V_3 = L_6;
		goto IL_0087;
	}

IL_0038:
	{
		String_t* L_7 = Parser_ParseString_m1852532745(__this, /*hidden argument*/NULL);
		V_2 = L_7;
		String_t* L_8 = V_2;
		V_4 = (bool)((((Il2CppObject*)(String_t*)L_8) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_9 = V_4;
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		V_3 = (Dictionary_2_t309261261 *)NULL;
		goto IL_0087;
	}

IL_004e:
	{
		int32_t L_10 = Parser_get_NextToken_m955932932(__this, /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)((((int32_t)L_10) == ((int32_t)5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_11 = V_5;
		if (!L_11)
		{
			goto IL_0065;
		}
	}
	{
		V_3 = (Dictionary_2_t309261261 *)NULL;
		goto IL_0087;
	}

IL_0065:
	{
		StringReader_t1480123486 * L_12 = __this->get_json_0();
		NullCheck(L_12);
		VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Read() */, L_12);
		Dictionary_2_t309261261 * L_13 = V_0;
		String_t* L_14 = V_2;
		Il2CppObject * L_15 = Parser_ParseValue_m1337096923(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Dictionary_2_set_Item_m4132139590(L_13, L_14, L_15, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
		goto IL_0081;
	}

IL_0081:
	{
	}

IL_0082:
	{
		V_6 = (bool)1;
		goto IL_0015;
	}

IL_0087:
	{
		Dictionary_2_t309261261 * L_16 = V_3;
		return L_16;
	}
}
// System.Collections.Generic.List`1<System.Object> UnityEngine.Purchasing.MiniJSON.Json/Parser::ParseArray()
extern Il2CppClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m365405030_MethodInfo_var;
extern const MethodInfo* List_1_Add_m567051994_MethodInfo_var;
extern const uint32_t Parser_ParseArray_m3001801163_MetadataUsageId;
extern "C"  List_1_t2058570427 * Parser_ParseArray_m3001801163 (Parser_t2551496676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseArray_m3001801163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2058570427 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Il2CppObject * V_4 = NULL;
	List_1_t2058570427 * V_5 = NULL;
	bool V_6 = false;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)il2cpp_codegen_object_new(List_1_t2058570427_il2cpp_TypeInfo_var);
		List_1__ctor_m365405030(L_0, /*hidden argument*/List_1__ctor_m365405030_MethodInfo_var);
		V_0 = L_0;
		StringReader_t1480123486 * L_1 = __this->get_json_0();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Read() */, L_1);
		V_1 = (bool)1;
		goto IL_0052;
	}

IL_0017:
	{
		int32_t L_2 = Parser_get_NextToken_m955932932(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_2;
		V_3 = L_3;
		int32_t L_4 = V_3;
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0026;
	}

IL_0026:
	{
		int32_t L_5 = V_3;
		if ((((int32_t)L_5) == ((int32_t)4)))
		{
			goto IL_0039;
		}
	}
	{
		goto IL_002c;
	}

IL_002c:
	{
		int32_t L_6 = V_3;
		if ((((int32_t)L_6) == ((int32_t)6)))
		{
			goto IL_0037;
		}
	}
	{
		goto IL_003d;
	}

IL_0032:
	{
		V_5 = (List_1_t2058570427 *)NULL;
		goto IL_005e;
	}

IL_0037:
	{
		goto IL_0052;
	}

IL_0039:
	{
		V_1 = (bool)0;
		goto IL_0051;
	}

IL_003d:
	{
		int32_t L_7 = V_2;
		Il2CppObject * L_8 = Parser_ParseByToken_m3249989542(__this, L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		List_1_t2058570427 * L_9 = V_0;
		Il2CppObject * L_10 = V_4;
		NullCheck(L_9);
		List_1_Add_m567051994(L_9, L_10, /*hidden argument*/List_1_Add_m567051994_MethodInfo_var);
		goto IL_0051;
	}

IL_0051:
	{
	}

IL_0052:
	{
		bool L_11 = V_1;
		V_6 = L_11;
		bool L_12 = V_6;
		if (L_12)
		{
			goto IL_0017;
		}
	}
	{
		List_1_t2058570427 * L_13 = V_0;
		V_5 = L_13;
		goto IL_005e;
	}

IL_005e:
	{
		List_1_t2058570427 * L_14 = V_5;
		return L_14;
	}
}
// System.Object UnityEngine.Purchasing.MiniJSON.Json/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m1337096923 (Parser_t2551496676 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		int32_t L_0 = Parser_get_NextToken_m955932932(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		Il2CppObject * L_2 = Parser_ParseByToken_m3249989542(__this, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		Il2CppObject * L_3 = V_1;
		return L_3;
	}
}
// System.Object UnityEngine.Purchasing.MiniJSON.Json/Parser::ParseByToken(UnityEngine.Purchasing.MiniJSON.Json/Parser/TOKEN)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseByToken_m3249989542_MetadataUsageId;
extern "C"  Il2CppObject * Parser_ParseByToken_m3249989542 (Parser_t2551496676 * __this, int32_t ___token0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseByToken_m3249989542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		int32_t L_0 = ___token0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0073;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0054;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0073;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_0073;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_0073;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_0039;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 7)
		{
			goto IL_0042;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 8)
		{
			goto IL_005d;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 9)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 10)
		{
			goto IL_006f;
		}
	}
	{
		goto IL_0073;
	}

IL_0039:
	{
		String_t* L_2 = Parser_ParseString_m1852532745(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0077;
	}

IL_0042:
	{
		Il2CppObject * L_3 = Parser_ParseNumber_m604222115(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0077;
	}

IL_004b:
	{
		Dictionary_2_t309261261 * L_4 = Parser_ParseObject_m2734739896(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0077;
	}

IL_0054:
	{
		List_1_t2058570427 * L_5 = Parser_ParseArray_m3001801163(__this, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0077;
	}

IL_005d:
	{
		bool L_6 = ((bool)1);
		Il2CppObject * L_7 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_6);
		V_1 = L_7;
		goto IL_0077;
	}

IL_0066:
	{
		bool L_8 = ((bool)0);
		Il2CppObject * L_9 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_8);
		V_1 = L_9;
		goto IL_0077;
	}

IL_006f:
	{
		V_1 = NULL;
		goto IL_0077;
	}

IL_0073:
	{
		V_1 = NULL;
		goto IL_0077;
	}

IL_0077:
	{
		Il2CppObject * L_10 = V_1;
		return L_10;
	}
}
// System.String UnityEngine.Purchasing.MiniJSON.Json/Parser::ParseString()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseString_m1852532745_MetadataUsageId;
extern "C"  String_t* Parser_ParseString_m1852532745 (Parser_t2551496676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseString_m1852532745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	bool V_2 = false;
	bool V_3 = false;
	Il2CppChar V_4 = 0x0;
	bool V_5 = false;
	Il2CppChar V_6 = 0x0;
	CharU5BU5D_t1328083999* V_7 = NULL;
	int32_t V_8 = 0;
	bool V_9 = false;
	bool V_10 = false;
	String_t* V_11 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringReader_t1480123486 * L_1 = __this->get_json_0();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Read() */, L_1);
		V_2 = (bool)1;
		goto IL_0170;
	}

IL_001a:
	{
		StringReader_t1480123486 * L_2 = __this->get_json_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.IO.TextReader::Peek() */, L_2);
		V_3 = (bool)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0);
		bool L_4 = V_3;
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_017a;
	}

IL_0035:
	{
		Il2CppChar L_5 = Parser_get_NextChar_m3536737078(__this, /*hidden argument*/NULL);
		V_1 = L_5;
		Il2CppChar L_6 = V_1;
		V_4 = L_6;
		Il2CppChar L_7 = V_4;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)34))))
		{
			goto IL_0052;
		}
	}
	{
		goto IL_0047;
	}

IL_0047:
	{
		Il2CppChar L_8 = V_4;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)92))))
		{
			goto IL_0059;
		}
	}
	{
		goto IL_0165;
	}

IL_0052:
	{
		V_2 = (bool)0;
		goto IL_016f;
	}

IL_0059:
	{
		StringReader_t1480123486 * L_9 = __this->get_json_0();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.IO.TextReader::Peek() */, L_9);
		V_5 = (bool)((((int32_t)L_10) == ((int32_t)(-1)))? 1 : 0);
		bool L_11 = V_5;
		if (!L_11)
		{
			goto IL_0075;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_016f;
	}

IL_0075:
	{
		Il2CppChar L_12 = Parser_get_NextChar_m3536737078(__this, /*hidden argument*/NULL);
		V_1 = L_12;
		Il2CppChar L_13 = V_1;
		V_6 = L_13;
		Il2CppChar L_14 = V_6;
		if ((!(((uint32_t)L_14) <= ((uint32_t)((int32_t)92)))))
		{
			goto IL_00a0;
		}
	}
	{
		Il2CppChar L_15 = V_6;
		if ((((int32_t)L_15) == ((int32_t)((int32_t)34))))
		{
			goto IL_00e0;
		}
	}
	{
		goto IL_008d;
	}

IL_008d:
	{
		Il2CppChar L_16 = V_6;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)47))))
		{
			goto IL_00e0;
		}
	}
	{
		goto IL_0095;
	}

IL_0095:
	{
		Il2CppChar L_17 = V_6;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)92))))
		{
			goto IL_00e0;
		}
	}
	{
		goto IL_0163;
	}

IL_00a0:
	{
		Il2CppChar L_18 = V_6;
		if ((!(((uint32_t)L_18) <= ((uint32_t)((int32_t)102)))))
		{
			goto IL_00b9;
		}
	}
	{
		Il2CppChar L_19 = V_6;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)98))))
		{
			goto IL_00ea;
		}
	}
	{
		goto IL_00ae;
	}

IL_00ae:
	{
		Il2CppChar L_20 = V_6;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)102))))
		{
			goto IL_00f4;
		}
	}
	{
		goto IL_0163;
	}

IL_00b9:
	{
		Il2CppChar L_21 = V_6;
		if ((((int32_t)L_21) == ((int32_t)((int32_t)110))))
		{
			goto IL_00ff;
		}
	}
	{
		goto IL_00c1;
	}

IL_00c1:
	{
		Il2CppChar L_22 = V_6;
		if (((int32_t)((int32_t)L_22-(int32_t)((int32_t)114))) == 0)
		{
			goto IL_010a;
		}
		if (((int32_t)((int32_t)L_22-(int32_t)((int32_t)114))) == 1)
		{
			goto IL_0163;
		}
		if (((int32_t)((int32_t)L_22-(int32_t)((int32_t)114))) == 2)
		{
			goto IL_0115;
		}
		if (((int32_t)((int32_t)L_22-(int32_t)((int32_t)114))) == 3)
		{
			goto IL_0120;
		}
	}
	{
		goto IL_0163;
	}

IL_00e0:
	{
		StringBuilder_t1221177846 * L_23 = V_0;
		Il2CppChar L_24 = V_1;
		NullCheck(L_23);
		StringBuilder_Append_m3618697540(L_23, L_24, /*hidden argument*/NULL);
		goto IL_0163;
	}

IL_00ea:
	{
		StringBuilder_t1221177846 * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m3618697540(L_25, 8, /*hidden argument*/NULL);
		goto IL_0163;
	}

IL_00f4:
	{
		StringBuilder_t1221177846 * L_26 = V_0;
		NullCheck(L_26);
		StringBuilder_Append_m3618697540(L_26, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_0163;
	}

IL_00ff:
	{
		StringBuilder_t1221177846 * L_27 = V_0;
		NullCheck(L_27);
		StringBuilder_Append_m3618697540(L_27, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_0163;
	}

IL_010a:
	{
		StringBuilder_t1221177846 * L_28 = V_0;
		NullCheck(L_28);
		StringBuilder_Append_m3618697540(L_28, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_0163;
	}

IL_0115:
	{
		StringBuilder_t1221177846 * L_29 = V_0;
		NullCheck(L_29);
		StringBuilder_Append_m3618697540(L_29, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0163;
	}

IL_0120:
	{
		V_7 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)4));
		V_8 = 0;
		goto IL_0140;
	}

IL_012d:
	{
		CharU5BU5D_t1328083999* L_30 = V_7;
		int32_t L_31 = V_8;
		Il2CppChar L_32 = Parser_get_NextChar_m3536737078(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(L_31), (Il2CppChar)L_32);
		int32_t L_33 = V_8;
		V_8 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_0140:
	{
		int32_t L_34 = V_8;
		V_9 = (bool)((((int32_t)L_34) < ((int32_t)4))? 1 : 0);
		bool L_35 = V_9;
		if (L_35)
		{
			goto IL_012d;
		}
	}
	{
		StringBuilder_t1221177846 * L_36 = V_0;
		CharU5BU5D_t1328083999* L_37 = V_7;
		String_t* L_38 = String_CreateString_m3818307083(NULL, L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_39 = Convert_ToInt32_m3262696010(NULL /*static, unused*/, L_38, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_36);
		StringBuilder_Append_m3618697540(L_36, (((int32_t)((uint16_t)L_39))), /*hidden argument*/NULL);
		goto IL_0163;
	}

IL_0163:
	{
		goto IL_016f;
	}

IL_0165:
	{
		StringBuilder_t1221177846 * L_40 = V_0;
		Il2CppChar L_41 = V_1;
		NullCheck(L_40);
		StringBuilder_Append_m3618697540(L_40, L_41, /*hidden argument*/NULL);
		goto IL_016f;
	}

IL_016f:
	{
	}

IL_0170:
	{
		bool L_42 = V_2;
		V_10 = L_42;
		bool L_43 = V_10;
		if (L_43)
		{
			goto IL_001a;
		}
	}

IL_017a:
	{
		StringBuilder_t1221177846 * L_44 = V_0;
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_44);
		V_11 = L_45;
		goto IL_0184;
	}

IL_0184:
	{
		String_t* L_46 = V_11;
		return L_46;
	}
}
// System.Object UnityEngine.Purchasing.MiniJSON.Json/Parser::ParseNumber()
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseNumber_m604222115_MetadataUsageId;
extern "C"  Il2CppObject * Parser_ParseNumber_m604222115 (Parser_t2551496676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseNumber_m604222115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	double V_1 = 0.0;
	bool V_2 = false;
	int64_t V_3 = 0;
	Il2CppObject * V_4 = NULL;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = Parser_get_NextWord_m3438888761(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = String_IndexOf_m2358239236(L_1, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = String_IndexOf_m2358239236(L_3, ((int32_t)101), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = String_IndexOf_m2358239236(L_5, ((int32_t)69), /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B4_0 = 0;
	}

IL_002c:
	{
		V_2 = (bool)G_B4_0;
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_9 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int64_TryParse_m3093198325(NULL /*static, unused*/, L_8, ((int32_t)511), L_9, (&V_3), /*hidden argument*/NULL);
		int64_t L_10 = V_3;
		int64_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_11);
		V_4 = L_12;
		goto IL_006b;
	}

IL_004e:
	{
		String_t* L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_14 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		Double_TryParse_m815528105(NULL /*static, unused*/, L_13, ((int32_t)511), L_14, (&V_1), /*hidden argument*/NULL);
		double L_15 = V_1;
		double L_16 = L_15;
		Il2CppObject * L_17 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_16);
		V_4 = L_17;
		goto IL_006b;
	}

IL_006b:
	{
		Il2CppObject * L_18 = V_4;
		return L_18;
	}
}
// System.Void UnityEngine.Purchasing.MiniJSON.Json/Parser::EatWhitespace()
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t Parser_EatWhitespace_m137903075_MetadataUsageId;
extern "C"  void Parser_EatWhitespace_m137903075 (Parser_t2551496676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_EatWhitespace_m137903075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		goto IL_0026;
	}

IL_0003:
	{
		StringReader_t1480123486 * L_0 = __this->get_json_0();
		NullCheck(L_0);
		VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Read() */, L_0);
		StringReader_t1480123486 * L_1 = __this->get_json_0();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.IO.TextReader::Peek() */, L_1);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)(-1)))? 1 : 0);
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0035;
	}

IL_0025:
	{
	}

IL_0026:
	{
		Il2CppChar L_4 = Parser_get_PeekChar_m1056554944(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_5 = Char_IsWhiteSpace_m1507160293(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		bool L_6 = V_1;
		if (L_6)
		{
			goto IL_0003;
		}
	}

IL_0035:
	{
		return;
	}
}
// System.Char UnityEngine.Purchasing.MiniJSON.Json/Parser::get_PeekChar()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_PeekChar_m1056554944_MetadataUsageId;
extern "C"  Il2CppChar Parser_get_PeekChar_m1056554944 (Parser_t2551496676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_PeekChar_m1056554944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	{
		StringReader_t1480123486 * L_0 = __this->get_json_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.IO.TextReader::Peek() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppChar L_2 = Convert_ToChar_m3827339132(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppChar L_3 = V_0;
		return L_3;
	}
}
// System.Char UnityEngine.Purchasing.MiniJSON.Json/Parser::get_NextChar()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_NextChar_m3536737078_MetadataUsageId;
extern "C"  Il2CppChar Parser_get_NextChar_m3536737078 (Parser_t2551496676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextChar_m3536737078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	{
		StringReader_t1480123486 * L_0 = __this->get_json_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Read() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppChar L_2 = Convert_ToChar_m3827339132(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppChar L_3 = V_0;
		return L_3;
	}
}
// System.String UnityEngine.Purchasing.MiniJSON.Json/Parser::get_NextWord()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_NextWord_m3438888761_MetadataUsageId;
extern "C"  String_t* Parser_get_NextWord_m3438888761 (Parser_t2551496676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextWord_m3438888761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	String_t* V_3 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_002d;
	}

IL_0009:
	{
		StringBuilder_t1221177846 * L_1 = V_0;
		Il2CppChar L_2 = Parser_get_NextChar_m3536737078(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m3618697540(L_1, L_2, /*hidden argument*/NULL);
		StringReader_t1480123486 * L_3 = __this->get_json_0();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.IO.TextReader::Peek() */, L_3);
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0);
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_003f;
	}

IL_002c:
	{
	}

IL_002d:
	{
		Il2CppChar L_6 = Parser_get_PeekChar_m1056554944(__this, /*hidden argument*/NULL);
		bool L_7 = Parser_IsWordBreak_m1816689754(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		bool L_8 = V_2;
		if (L_8)
		{
			goto IL_0009;
		}
	}

IL_003f:
	{
		StringBuilder_t1221177846 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		V_3 = L_10;
		goto IL_0048;
	}

IL_0048:
	{
		String_t* L_11 = V_3;
		return L_11;
	}
}
// UnityEngine.Purchasing.MiniJSON.Json/Parser/TOKEN UnityEngine.Purchasing.MiniJSON.Json/Parser::get_NextToken()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern const uint32_t Parser_get_NextToken_m955932932_MetadataUsageId;
extern "C"  int32_t Parser_get_NextToken_m955932932 (Parser_t2551496676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextToken_m955932932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	String_t* V_3 = NULL;
	{
		Parser_EatWhitespace_m137903075(__this, /*hidden argument*/NULL);
		StringReader_t1480123486 * L_0 = __this->get_json_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.IO.TextReader::Peek() */, L_0);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)(-1)))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		V_1 = 0;
		goto IL_0143;
	}

IL_0022:
	{
		Il2CppChar L_3 = Parser_get_PeekChar_m1056554944(__this, /*hidden argument*/NULL);
		V_2 = L_3;
		Il2CppChar L_4 = V_2;
		if ((!(((uint32_t)L_4) <= ((uint32_t)((int32_t)91)))))
		{
			goto IL_00a4;
		}
	}
	{
		Il2CppChar L_5 = V_2;
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 0)
		{
			goto IL_00f4;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 1)
		{
			goto IL_0100;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 2)
		{
			goto IL_0100;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 3)
		{
			goto IL_0100;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 4)
		{
			goto IL_0100;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 5)
		{
			goto IL_0100;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 6)
		{
			goto IL_0100;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 7)
		{
			goto IL_0100;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 8)
		{
			goto IL_0100;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 9)
		{
			goto IL_0100;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 10)
		{
			goto IL_00e4;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 11)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 12)
		{
			goto IL_0100;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 13)
		{
			goto IL_0100;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 14)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 15)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 16)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 17)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 18)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 19)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 20)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 21)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 22)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 23)
		{
			goto IL_00fc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)34))) == 24)
		{
			goto IL_00f8;
		}
	}
	{
		goto IL_009d;
	}

IL_009d:
	{
		Il2CppChar L_6 = V_2;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)91))))
		{
			goto IL_00d0;
		}
	}
	{
		goto IL_0100;
	}

IL_00a4:
	{
		Il2CppChar L_7 = V_2;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)93))))
		{
			goto IL_00d4;
		}
	}
	{
		goto IL_00ab;
	}

IL_00ab:
	{
		Il2CppChar L_8 = V_2;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)123))))
		{
			goto IL_00b9;
		}
	}
	{
		goto IL_00b2;
	}

IL_00b2:
	{
		Il2CppChar L_9 = V_2;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)125))))
		{
			goto IL_00c0;
		}
	}
	{
		goto IL_0100;
	}

IL_00b9:
	{
		V_1 = 1;
		goto IL_0143;
	}

IL_00c0:
	{
		StringReader_t1480123486 * L_10 = __this->get_json_0();
		NullCheck(L_10);
		VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Read() */, L_10);
		V_1 = 2;
		goto IL_0143;
	}

IL_00d0:
	{
		V_1 = 3;
		goto IL_0143;
	}

IL_00d4:
	{
		StringReader_t1480123486 * L_11 = __this->get_json_0();
		NullCheck(L_11);
		VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Read() */, L_11);
		V_1 = 4;
		goto IL_0143;
	}

IL_00e4:
	{
		StringReader_t1480123486 * L_12 = __this->get_json_0();
		NullCheck(L_12);
		VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.TextReader::Read() */, L_12);
		V_1 = 6;
		goto IL_0143;
	}

IL_00f4:
	{
		V_1 = 7;
		goto IL_0143;
	}

IL_00f8:
	{
		V_1 = 5;
		goto IL_0143;
	}

IL_00fc:
	{
		V_1 = 8;
		goto IL_0143;
	}

IL_0100:
	{
		String_t* L_13 = Parser_get_NextWord_m3438888761(__this, /*hidden argument*/NULL);
		V_3 = L_13;
		String_t* L_14 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_14, _stringLiteral2609877245, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0130;
		}
	}
	{
		String_t* L_16 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_16, _stringLiteral3323263070, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0135;
		}
	}
	{
		String_t* L_18 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_18, _stringLiteral1743624307, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_013a;
		}
	}
	{
		goto IL_013f;
	}

IL_0130:
	{
		V_1 = ((int32_t)10);
		goto IL_0143;
	}

IL_0135:
	{
		V_1 = ((int32_t)9);
		goto IL_0143;
	}

IL_013a:
	{
		V_1 = ((int32_t)11);
		goto IL_0143;
	}

IL_013f:
	{
		V_1 = 0;
		goto IL_0143;
	}

IL_0143:
	{
		int32_t L_20 = V_1;
		return L_20;
	}
}
// System.Void UnityEngine.Purchasing.MiniJSON.Json/Serializer::.ctor()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern const uint32_t Serializer__ctor_m4057455507_MetadataUsageId;
extern "C"  void Serializer__ctor_m4057455507 (Serializer_t645677759 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer__ctor_m4057455507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		__this->set_builder_0(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.MiniJSON.Json/Serializer::Serialize(System.Object)
extern Il2CppClass* Serializer_t645677759_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_Serialize_m1537877676_MetadataUsageId;
extern "C"  String_t* Serializer_Serialize_m1537877676 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_Serialize_m1537877676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Serializer_t645677759 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		Serializer_t645677759 * L_0 = (Serializer_t645677759 *)il2cpp_codegen_object_new(Serializer_t645677759_il2cpp_TypeInfo_var);
		Serializer__ctor_m4057455507(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Serializer_t645677759 * L_1 = V_0;
		Il2CppObject * L_2 = ___obj0;
		NullCheck(L_1);
		Serializer_SerializeValue_m56800584(L_1, L_2, /*hidden argument*/NULL);
		Serializer_t645677759 * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_t1221177846 * L_4 = L_3->get_builder_0();
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		V_1 = L_5;
		goto IL_001d;
	}

IL_001d:
	{
		String_t* L_6 = V_1;
		return L_6;
	}
}
// System.Void UnityEngine.Purchasing.MiniJSON.Json/Serializer::SerializeValue(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern const uint32_t Serializer_SerializeValue_m56800584_MetadataUsageId;
extern "C"  void Serializer_SerializeValue_m56800584 (Serializer_t645677759 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeValue_m56800584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	String_t* V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	StringBuilder_t1221177846 * G_B7_0 = NULL;
	StringBuilder_t1221177846 * G_B6_0 = NULL;
	String_t* G_B8_0 = NULL;
	StringBuilder_t1221177846 * G_B8_1 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		V_3 = (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_3;
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		StringBuilder_t1221177846 * L_2 = __this->get_builder_0();
		NullCheck(L_2);
		StringBuilder_Append_m3636508479(L_2, _stringLiteral1743624307, /*hidden argument*/NULL);
		goto IL_00de;
	}

IL_0021:
	{
		Il2CppObject * L_3 = ___value0;
		String_t* L_4 = ((String_t*)IsInstSealed(L_3, String_t_il2cpp_TypeInfo_var));
		V_2 = L_4;
		V_4 = (bool)((!(((Il2CppObject*)(String_t*)L_4) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_5 = V_4;
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		String_t* L_6 = V_2;
		Serializer_SerializeString_m1215437050(__this, L_6, /*hidden argument*/NULL);
		goto IL_00de;
	}

IL_0041:
	{
		Il2CppObject * L_7 = ___value0;
		V_5 = (bool)((!(((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)IsInstSealed(L_7, Boolean_t3825574718_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_8 = V_5;
		if (!L_8)
		{
			goto IL_0074;
		}
	}
	{
		StringBuilder_t1221177846 * L_9 = __this->get_builder_0();
		Il2CppObject * L_10 = ___value0;
		G_B6_0 = L_9;
		if (((*(bool*)((bool*)UnBox (L_10, Boolean_t3825574718_il2cpp_TypeInfo_var)))))
		{
			G_B7_0 = L_9;
			goto IL_0066;
		}
	}
	{
		G_B8_0 = _stringLiteral2609877245;
		G_B8_1 = G_B6_0;
		goto IL_006b;
	}

IL_0066:
	{
		G_B8_0 = _stringLiteral3323263070;
		G_B8_1 = G_B7_0;
	}

IL_006b:
	{
		NullCheck(G_B8_1);
		StringBuilder_Append_m3636508479(G_B8_1, G_B8_0, /*hidden argument*/NULL);
		goto IL_00de;
	}

IL_0074:
	{
		Il2CppObject * L_11 = ___value0;
		Il2CppObject * L_12 = ((Il2CppObject *)IsInst(L_11, IList_t3321498491_il2cpp_TypeInfo_var));
		V_0 = L_12;
		V_6 = (bool)((!(((Il2CppObject*)(Il2CppObject *)L_12) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_13 = V_6;
		if (!L_13)
		{
			goto IL_0091;
		}
	}
	{
		Il2CppObject * L_14 = V_0;
		Serializer_SerializeArray_m706447411(__this, L_14, /*hidden argument*/NULL);
		goto IL_00de;
	}

IL_0091:
	{
		Il2CppObject * L_15 = ___value0;
		Il2CppObject * L_16 = ((Il2CppObject *)IsInst(L_15, IDictionary_t596158605_il2cpp_TypeInfo_var));
		V_1 = L_16;
		V_7 = (bool)((!(((Il2CppObject*)(Il2CppObject *)L_16) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_17 = V_7;
		if (!L_17)
		{
			goto IL_00ae;
		}
	}
	{
		Il2CppObject * L_18 = V_1;
		Serializer_SerializeObject_m3083208701(__this, L_18, /*hidden argument*/NULL);
		goto IL_00de;
	}

IL_00ae:
	{
		Il2CppObject * L_19 = ___value0;
		V_8 = (bool)((!(((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)IsInstSealed(L_19, Char_t3454481338_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_20 = V_8;
		if (!L_20)
		{
			goto IL_00d4;
		}
	}
	{
		Il2CppObject * L_21 = ___value0;
		String_t* L_22 = String_CreateString_m2556700934(NULL, ((*(Il2CppChar*)((Il2CppChar*)UnBox (L_21, Char_t3454481338_il2cpp_TypeInfo_var)))), 1, /*hidden argument*/NULL);
		Serializer_SerializeString_m1215437050(__this, L_22, /*hidden argument*/NULL);
		goto IL_00de;
	}

IL_00d4:
	{
		Il2CppObject * L_23 = ___value0;
		Serializer_SerializeOther_m2564855511(__this, L_23, /*hidden argument*/NULL);
	}

IL_00de:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.MiniJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeObject_m3083208701_MetadataUsageId;
extern "C"  void Serializer_SerializeObject_m3083208701 (Serializer_t645677759 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeObject_m3083208701_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		StringBuilder_t1221177846 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m3618697540(L_0, ((int32_t)123), /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_2);
		V_1 = L_3;
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006c;
		}

IL_0020:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck(L_4);
			Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_4);
			V_2 = L_5;
			bool L_6 = V_0;
			V_3 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
			bool L_7 = V_3;
			if (!L_7)
			{
				goto IL_0040;
			}
		}

IL_0030:
		{
			StringBuilder_t1221177846 * L_8 = __this->get_builder_0();
			NullCheck(L_8);
			StringBuilder_Append_m3618697540(L_8, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_0040:
		{
			Il2CppObject * L_9 = V_2;
			NullCheck(L_9);
			String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
			Serializer_SerializeString_m1215437050(__this, L_10, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_11 = __this->get_builder_0();
			NullCheck(L_11);
			StringBuilder_Append_m3618697540(L_11, ((int32_t)58), /*hidden argument*/NULL);
			Il2CppObject * L_12 = ___obj0;
			Il2CppObject * L_13 = V_2;
			NullCheck(L_12);
			Il2CppObject * L_14 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_12, L_13);
			Serializer_SerializeValue_m56800584(__this, L_14, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_006c:
		{
			Il2CppObject * L_15 = V_1;
			NullCheck(L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0020;
			}
		}

IL_0074:
		{
			IL2CPP_LEAVE(0x8B, FINALLY_0076);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_17 = V_1;
			V_4 = ((Il2CppObject *)IsInst(L_17, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_18 = V_4;
			if (!L_18)
			{
				goto IL_008a;
			}
		}

IL_0082:
		{
			Il2CppObject * L_19 = V_4;
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_19);
		}

IL_008a:
		{
			IL2CPP_END_FINALLY(118)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x8B, IL_008b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008b:
	{
		StringBuilder_t1221177846 * L_20 = __this->get_builder_0();
		NullCheck(L_20);
		StringBuilder_Append_m3618697540(L_20, ((int32_t)125), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.MiniJSON.Json/Serializer::SerializeArray(System.Collections.IList)
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeArray_m706447411_MetadataUsageId;
extern "C"  void Serializer_SerializeArray_m706447411 (Serializer_t645677759 * __this, Il2CppObject * ___anArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeArray_m706447411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1221177846 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m3618697540(L_0, ((int32_t)91), /*hidden argument*/NULL);
		V_0 = (bool)1;
		Il2CppObject * L_1 = ___anArray0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0046;
		}

IL_001b:
		{
			Il2CppObject * L_3 = V_1;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_3);
			V_2 = L_4;
			bool L_5 = V_0;
			V_3 = (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
			bool L_6 = V_3;
			if (!L_6)
			{
				goto IL_003b;
			}
		}

IL_002b:
		{
			StringBuilder_t1221177846 * L_7 = __this->get_builder_0();
			NullCheck(L_7);
			StringBuilder_Append_m3618697540(L_7, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_003b:
		{
			Il2CppObject * L_8 = V_2;
			Serializer_SerializeValue_m56800584(__this, L_8, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_0046:
		{
			Il2CppObject * L_9 = V_1;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_001b;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_11 = V_1;
			V_4 = ((Il2CppObject *)IsInst(L_11, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_12 = V_4;
			if (!L_12)
			{
				goto IL_0064;
			}
		}

IL_005c:
		{
			Il2CppObject * L_13 = V_4;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_13);
		}

IL_0064:
		{
			IL2CPP_END_FINALLY(80)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x65, IL_0065)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0065:
	{
		StringBuilder_t1221177846 * L_14 = __this->get_builder_0();
		NullCheck(L_14);
		StringBuilder_Append_m3618697540(L_14, ((int32_t)93), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.MiniJSON.Json/Serializer::SerializeString(System.String)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3943473468;
extern Il2CppCodeGenString* _stringLiteral2088416310;
extern Il2CppCodeGenString* _stringLiteral1093630588;
extern Il2CppCodeGenString* _stringLiteral3419229416;
extern Il2CppCodeGenString* _stringLiteral3062999056;
extern Il2CppCodeGenString* _stringLiteral381169868;
extern Il2CppCodeGenString* _stringLiteral3869568110;
extern Il2CppCodeGenString* _stringLiteral2303484169;
extern Il2CppCodeGenString* _stringLiteral2424443666;
extern const uint32_t Serializer_SerializeString_m1215437050_MetadataUsageId;
extern "C"  void Serializer_SerializeString_m1215437050 (Serializer_t645677759 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeString_m1215437050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t1328083999* V_0 = NULL;
	CharU5BU5D_t1328083999* V_1 = NULL;
	int32_t V_2 = 0;
	Il2CppChar V_3 = 0x0;
	Il2CppChar V_4 = 0x0;
	int32_t V_5 = 0;
	bool V_6 = false;
	int32_t G_B17_0 = 0;
	{
		StringBuilder_t1221177846 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m3618697540(L_0, ((int32_t)34), /*hidden argument*/NULL);
		String_t* L_1 = ___str0;
		NullCheck(L_1);
		CharU5BU5D_t1328083999* L_2 = String_ToCharArray_m870309954(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		CharU5BU5D_t1328083999* L_3 = V_0;
		V_1 = L_3;
		V_2 = 0;
		goto IL_0155;
	}

IL_0020:
	{
		CharU5BU5D_t1328083999* L_4 = V_1;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Il2CppChar L_8 = V_3;
		V_4 = L_8;
		Il2CppChar L_9 = V_4;
		if (((int32_t)((int32_t)L_9-(int32_t)8)) == 0)
		{
			goto IL_008a;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)8)) == 1)
		{
			goto IL_00df;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)8)) == 2)
		{
			goto IL_00b6;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)8)) == 3)
		{
			goto IL_00f2;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)8)) == 4)
		{
			goto IL_00a0;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)8)) == 5)
		{
			goto IL_00cc;
		}
	}
	{
		goto IL_004b;
	}

IL_004b:
	{
		Il2CppChar L_10 = V_4;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)34))))
		{
			goto IL_005e;
		}
	}
	{
		goto IL_0053;
	}

IL_0053:
	{
		Il2CppChar L_11 = V_4;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)92))))
		{
			goto IL_0074;
		}
	}
	{
		goto IL_00f2;
	}

IL_005e:
	{
		StringBuilder_t1221177846 * L_12 = __this->get_builder_0();
		NullCheck(L_12);
		StringBuilder_Append_m3636508479(L_12, _stringLiteral3943473468, /*hidden argument*/NULL);
		goto IL_0150;
	}

IL_0074:
	{
		StringBuilder_t1221177846 * L_13 = __this->get_builder_0();
		NullCheck(L_13);
		StringBuilder_Append_m3636508479(L_13, _stringLiteral2088416310, /*hidden argument*/NULL);
		goto IL_0150;
	}

IL_008a:
	{
		StringBuilder_t1221177846 * L_14 = __this->get_builder_0();
		NullCheck(L_14);
		StringBuilder_Append_m3636508479(L_14, _stringLiteral1093630588, /*hidden argument*/NULL);
		goto IL_0150;
	}

IL_00a0:
	{
		StringBuilder_t1221177846 * L_15 = __this->get_builder_0();
		NullCheck(L_15);
		StringBuilder_Append_m3636508479(L_15, _stringLiteral3419229416, /*hidden argument*/NULL);
		goto IL_0150;
	}

IL_00b6:
	{
		StringBuilder_t1221177846 * L_16 = __this->get_builder_0();
		NullCheck(L_16);
		StringBuilder_Append_m3636508479(L_16, _stringLiteral3062999056, /*hidden argument*/NULL);
		goto IL_0150;
	}

IL_00cc:
	{
		StringBuilder_t1221177846 * L_17 = __this->get_builder_0();
		NullCheck(L_17);
		StringBuilder_Append_m3636508479(L_17, _stringLiteral381169868, /*hidden argument*/NULL);
		goto IL_0150;
	}

IL_00df:
	{
		StringBuilder_t1221177846 * L_18 = __this->get_builder_0();
		NullCheck(L_18);
		StringBuilder_Append_m3636508479(L_18, _stringLiteral3869568110, /*hidden argument*/NULL);
		goto IL_0150;
	}

IL_00f2:
	{
		Il2CppChar L_19 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_20 = Convert_ToInt32_m3683486440(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_5 = L_20;
		int32_t L_21 = V_5;
		if ((((int32_t)L_21) < ((int32_t)((int32_t)32))))
		{
			goto IL_010b;
		}
	}
	{
		int32_t L_22 = V_5;
		G_B17_0 = ((((int32_t)((((int32_t)L_22) > ((int32_t)((int32_t)126)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_010c;
	}

IL_010b:
	{
		G_B17_0 = 0;
	}

IL_010c:
	{
		V_6 = (bool)G_B17_0;
		bool L_23 = V_6;
		if (!L_23)
		{
			goto IL_0123;
		}
	}
	{
		StringBuilder_t1221177846 * L_24 = __this->get_builder_0();
		Il2CppChar L_25 = V_3;
		NullCheck(L_24);
		StringBuilder_Append_m3618697540(L_24, L_25, /*hidden argument*/NULL);
		goto IL_014e;
	}

IL_0123:
	{
		StringBuilder_t1221177846 * L_26 = __this->get_builder_0();
		NullCheck(L_26);
		StringBuilder_Append_m3636508479(L_26, _stringLiteral2303484169, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_27 = __this->get_builder_0();
		String_t* L_28 = Int32_ToString_m1064459878((&V_5), _stringLiteral2424443666, /*hidden argument*/NULL);
		NullCheck(L_27);
		StringBuilder_Append_m3636508479(L_27, L_28, /*hidden argument*/NULL);
	}

IL_014e:
	{
		goto IL_0150;
	}

IL_0150:
	{
		int32_t L_29 = V_2;
		V_2 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_0155:
	{
		int32_t L_30 = V_2;
		CharU5BU5D_t1328083999* L_31 = V_1;
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))))))
		{
			goto IL_0020;
		}
	}
	{
		StringBuilder_t1221177846 * L_32 = __this->get_builder_0();
		NullCheck(L_32);
		StringBuilder_Append_m3618697540(L_32, ((int32_t)34), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.MiniJSON.Json/Serializer::SerializeOther(System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t454417549_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029424;
extern const uint32_t Serializer_SerializeOther_m2564855511_MetadataUsageId;
extern "C"  void Serializer_SerializeOther_m2564855511 (Serializer_t645677759 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeOther_m2564855511_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	bool V_2 = false;
	bool V_3 = false;
	double V_4 = 0.0;
	int32_t G_B11_0 = 0;
	int32_t G_B16_0 = 0;
	{
		Il2CppObject * L_0 = ___value0;
		V_0 = (bool)((!(((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)IsInstSealed(L_0, Single_t2076509932_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		StringBuilder_t1221177846 * L_2 = __this->get_builder_0();
		Il2CppObject * L_3 = ___value0;
		V_1 = ((*(float*)((float*)UnBox (L_3, Single_t2076509932_il2cpp_TypeInfo_var))));
		String_t* L_4 = Single_ToString_m2359963436((&V_1), _stringLiteral372029424, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_Append_m3636508479(L_2, L_4, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_0034:
	{
		Il2CppObject * L_5 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_5, Int32_t2071877448_il2cpp_TypeInfo_var)))
		{
			goto IL_0077;
		}
	}
	{
		Il2CppObject * L_6 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_6, UInt32_t2149682021_il2cpp_TypeInfo_var)))
		{
			goto IL_0077;
		}
	}
	{
		Il2CppObject * L_7 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_7, Int64_t909078037_il2cpp_TypeInfo_var)))
		{
			goto IL_0077;
		}
	}
	{
		Il2CppObject * L_8 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_8, SByte_t454417549_il2cpp_TypeInfo_var)))
		{
			goto IL_0077;
		}
	}
	{
		Il2CppObject * L_9 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_9, Byte_t3683104436_il2cpp_TypeInfo_var)))
		{
			goto IL_0077;
		}
	}
	{
		Il2CppObject * L_10 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_10, Int16_t4041245914_il2cpp_TypeInfo_var)))
		{
			goto IL_0077;
		}
	}
	{
		Il2CppObject * L_11 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_11, UInt16_t986882611_il2cpp_TypeInfo_var)))
		{
			goto IL_0077;
		}
	}
	{
		Il2CppObject * L_12 = ___value0;
		G_B11_0 = ((!(((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)IsInstSealed(L_12, UInt64_t2909196914_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		goto IL_0078;
	}

IL_0077:
	{
		G_B11_0 = 1;
	}

IL_0078:
	{
		V_2 = (bool)G_B11_0;
		bool L_13 = V_2;
		if (!L_13)
		{
			goto IL_008d;
		}
	}
	{
		StringBuilder_t1221177846 * L_14 = __this->get_builder_0();
		Il2CppObject * L_15 = ___value0;
		NullCheck(L_14);
		StringBuilder_Append_m3541816491(L_14, L_15, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_008d:
	{
		Il2CppObject * L_16 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_16, Double_t4078015681_il2cpp_TypeInfo_var)))
		{
			goto IL_00a0;
		}
	}
	{
		Il2CppObject * L_17 = ___value0;
		G_B16_0 = ((!(((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)IsInstSealed(L_17, Decimal_t724701077_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		goto IL_00a1;
	}

IL_00a0:
	{
		G_B16_0 = 1;
	}

IL_00a1:
	{
		V_3 = (bool)G_B16_0;
		bool L_18 = V_3;
		if (!L_18)
		{
			goto IL_00c9;
		}
	}
	{
		StringBuilder_t1221177846 * L_19 = __this->get_builder_0();
		Il2CppObject * L_20 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_21 = Convert_ToDouble_m3751930225(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		String_t* L_22 = Double_ToString_m2210043919((&V_4), _stringLiteral372029424, /*hidden argument*/NULL);
		NullCheck(L_19);
		StringBuilder_Append_m3636508479(L_19, L_22, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00c9:
	{
		Il2CppObject * L_23 = ___value0;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_23);
		Serializer_SerializeString_m1215437050(__this, L_24, /*hidden argument*/NULL);
	}

IL_00d8:
	{
		return;
	}
}
// System.String UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::GetString(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String,System.String)
extern const MethodInfo* Dictionary_2_ContainsKey_m1533770720_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m464793699_MethodInfo_var;
extern const uint32_t MiniJsonExtensions_GetString_m2316350864_MetadataUsageId;
extern "C"  String_t* MiniJsonExtensions_GetString_m2316350864 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, String_t* ___defaultValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_GetString_m2316350864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	{
		Dictionary_2_t309261261 * L_0 = ___dic0;
		String_t* L_1 = ___key1;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1533770720(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1533770720_MethodInfo_var);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		Dictionary_2_t309261261 * L_4 = ___dic0;
		String_t* L_5 = ___key1;
		NullCheck(L_4);
		Il2CppObject * L_6 = Dictionary_2_get_Item_m464793699(L_4, L_5, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		V_1 = L_7;
		goto IL_001f;
	}

IL_001b:
	{
		String_t* L_8 = ___defaultValue2;
		V_1 = L_8;
		goto IL_001f;
	}

IL_001f:
	{
		String_t* L_9 = V_1;
		return L_9;
	}
}
// System.String UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  String_t* MiniJsonExtensions_toJson_m2405339524 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___obj0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		Dictionary_2_t309261261 * L_0 = ___obj0;
		String_t* L_1 = MiniJson_JsonEncode_m3219535883(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::HashtableFromJson(System.String)
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern const uint32_t MiniJsonExtensions_HashtableFromJson_m374830805_MetadataUsageId;
extern "C"  Dictionary_2_t309261261 * MiniJsonExtensions_HashtableFromJson_m374830805 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MiniJsonExtensions_HashtableFromJson_m374830805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	{
		String_t* L_0 = ___json0;
		Il2CppObject * L_1 = MiniJson_JsonDecode_m947751129(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Dictionary_2_t309261261 *)IsInstClass(L_1, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		goto IL_000f;
	}

IL_000f:
	{
		Dictionary_2_t309261261 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Purchasing.UnityPurchasingCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityPurchasingCallback__ctor_m905612093 (UnityPurchasingCallback_t2635187846 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Purchasing.UnityPurchasingCallback::Invoke(System.String,System.String,System.String,System.String)
extern "C"  void UnityPurchasingCallback_Invoke_m3954350125 (UnityPurchasingCallback_t2635187846 * __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityPurchasingCallback_Invoke_m3954350125((UnityPurchasingCallback_t2635187846 *)__this->get_prev_9(),___subject0, ___payload1, ___receipt2, ___transactionId3, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___subject0, ___payload1, ___receipt2, ___transactionId3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___subject0, ___payload1, ___receipt2, ___transactionId3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___subject0, ___payload1, ___receipt2, ___transactionId3,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_UnityPurchasingCallback_t2635187846 (UnityPurchasingCallback_t2635187846 * __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___subject0' to native representation
	char* ____subject0_marshaled = NULL;
	____subject0_marshaled = il2cpp_codegen_marshal_string(___subject0);

	// Marshaling of parameter '___payload1' to native representation
	char* ____payload1_marshaled = NULL;
	____payload1_marshaled = il2cpp_codegen_marshal_string(___payload1);

	// Marshaling of parameter '___receipt2' to native representation
	char* ____receipt2_marshaled = NULL;
	____receipt2_marshaled = il2cpp_codegen_marshal_string(___receipt2);

	// Marshaling of parameter '___transactionId3' to native representation
	char* ____transactionId3_marshaled = NULL;
	____transactionId3_marshaled = il2cpp_codegen_marshal_string(___transactionId3);

	// Native function invocation
	il2cppPInvokeFunc(____subject0_marshaled, ____payload1_marshaled, ____receipt2_marshaled, ____transactionId3_marshaled);

	// Marshaling cleanup of parameter '___subject0' native representation
	il2cpp_codegen_marshal_free(____subject0_marshaled);
	____subject0_marshaled = NULL;

	// Marshaling cleanup of parameter '___payload1' native representation
	il2cpp_codegen_marshal_free(____payload1_marshaled);
	____payload1_marshaled = NULL;

	// Marshaling cleanup of parameter '___receipt2' native representation
	il2cpp_codegen_marshal_free(____receipt2_marshaled);
	____receipt2_marshaled = NULL;

	// Marshaling cleanup of parameter '___transactionId3' native representation
	il2cpp_codegen_marshal_free(____transactionId3_marshaled);
	____transactionId3_marshaled = NULL;

}
// System.IAsyncResult UnityEngine.Purchasing.UnityPurchasingCallback::BeginInvoke(System.String,System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityPurchasingCallback_BeginInvoke_m3821805568 (UnityPurchasingCallback_t2635187846 * __this, String_t* ___subject0, String_t* ___payload1, String_t* ___receipt2, String_t* ___transactionId3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___subject0;
	__d_args[1] = ___payload1;
	__d_args[2] = ___receipt2;
	__d_args[3] = ___transactionId3;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void UnityEngine.Purchasing.UnityPurchasingCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnityPurchasingCallback_EndInvoke_m1856131231 (UnityPurchasingCallback_t2635187846 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
