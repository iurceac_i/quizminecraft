﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundController
struct SoundController_t1686593041;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void SoundController::.ctor()
extern "C"  void SoundController__ctor_m1375591970 (SoundController_t1686593041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundController::Start()
extern "C"  void SoundController_Start_m2439355714 (SoundController_t1686593041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundController::Update()
extern "C"  void SoundController_Update_m1015243901 (SoundController_t1686593041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundController::OnSound()
extern "C"  void SoundController_OnSound_m4103739782 (SoundController_t1686593041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundController::OffSound()
extern "C"  void SoundController_OffSound_m3094588068 (SoundController_t1686593041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundController::FailMission()
extern "C"  void SoundController_FailMission_m1630072376 (SoundController_t1686593041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundController::CloseWindow()
extern "C"  void SoundController_CloseWindow_m1597160790 (SoundController_t1686593041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundController::OpenWindow()
extern "C"  void SoundController_OpenWindow_m1923503136 (SoundController_t1686593041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundController::SuccesMission()
extern "C"  void SoundController_SuccesMission_m3861769026 (SoundController_t1686593041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundController::soundSettings()
extern "C"  void SoundController_soundSettings_m608092896 (SoundController_t1686593041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundController::DisableEffectSound(UnityEngine.GameObject)
extern "C"  void SoundController_DisableEffectSound_m2674684258 (SoundController_t1686593041 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundController::DisableBackgroundSound(UnityEngine.GameObject)
extern "C"  void SoundController_DisableBackgroundSound_m178036229 (SoundController_t1686593041 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
