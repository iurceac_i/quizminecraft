﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerPrefsX
struct PlayerPrefsX_t1687815431;
// System.String
struct String_t;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t1854387467;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Quaternion>
struct List_1_t3399195050;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1389513207;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void PlayerPrefsX::.ctor()
extern "C"  void PlayerPrefsX__ctor_m743890102 (PlayerPrefsX_t1687815431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetBool(System.String,System.Boolean)
extern "C"  bool PlayerPrefsX_SetBool_m2483950367 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::GetBool(System.String)
extern "C"  bool PlayerPrefsX_GetBool_m1433478398 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::GetBool(System.String,System.Boolean)
extern "C"  bool PlayerPrefsX_GetBool_m563309883 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PlayerPrefsX::GetLong(System.String,System.Int64)
extern "C"  int64_t PlayerPrefsX_GetLong_m431868007 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int64_t ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PlayerPrefsX::GetLong(System.String)
extern "C"  int64_t PlayerPrefsX_GetLong_m3735091293 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::SplitLong(System.Int64,System.Int32&,System.Int32&)
extern "C"  void PlayerPrefsX_SplitLong_m2682948856 (Il2CppObject * __this /* static, unused */, int64_t ___input0, int32_t* ___lowBits1, int32_t* ___highBits2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::SetLong(System.String,System.Int64)
extern "C"  void PlayerPrefsX_SetLong_m1312103776 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetVector2(System.String,UnityEngine.Vector2)
extern "C"  bool PlayerPrefsX_SetVector2_m1470654325 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2_t2243707579  ___vector1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PlayerPrefsX::GetVector2(System.String)
extern "C"  Vector2_t2243707579  PlayerPrefsX_GetVector2_m3651951690 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PlayerPrefsX::GetVector2(System.String,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  PlayerPrefsX_GetVector2_m2771394184 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2_t2243707579  ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetVector3(System.String,UnityEngine.Vector3)
extern "C"  bool PlayerPrefsX_SetVector3_m378186261 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3_t2243707580  ___vector1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PlayerPrefsX::GetVector3(System.String)
extern "C"  Vector3_t2243707580  PlayerPrefsX_GetVector3_m2471918672 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PlayerPrefsX::GetVector3(System.String,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  PlayerPrefsX_GetVector3_m707181159 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3_t2243707580  ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetQuaternion(System.String,UnityEngine.Quaternion)
extern "C"  bool PlayerPrefsX_SetQuaternion_m1970153377 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Quaternion_t4030073918  ___vector1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion PlayerPrefsX::GetQuaternion(System.String)
extern "C"  Quaternion_t4030073918  PlayerPrefsX_GetQuaternion_m887856706 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion PlayerPrefsX::GetQuaternion(System.String,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  PlayerPrefsX_GetQuaternion_m2630933209 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Quaternion_t4030073918  ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetColor(System.String,UnityEngine.Color)
extern "C"  bool PlayerPrefsX_SetColor_m3641687733 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Color_t2020392075  ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayerPrefsX::GetColor(System.String)
extern "C"  Color_t2020392075  PlayerPrefsX_GetColor_m2868168762 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayerPrefsX::GetColor(System.String,UnityEngine.Color)
extern "C"  Color_t2020392075  PlayerPrefsX_GetColor_m3306356312 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Color_t2020392075  ___defaultValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetBoolArray(System.String,System.Boolean[])
extern "C"  bool PlayerPrefsX_SetBoolArray_m2699961658 (Il2CppObject * __this /* static, unused */, String_t* ___key0, BooleanU5BU5D_t3568034315* ___boolArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean[] PlayerPrefsX::GetBoolArray(System.String)
extern "C"  BooleanU5BU5D_t3568034315* PlayerPrefsX_GetBoolArray_m2134041115 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean[] PlayerPrefsX::GetBoolArray(System.String,System.Boolean,System.Int32)
extern "C"  BooleanU5BU5D_t3568034315* PlayerPrefsX_GetBoolArray_m806442737 (Il2CppObject * __this /* static, unused */, String_t* ___key0, bool ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetStringArray(System.String,System.String[])
extern "C"  bool PlayerPrefsX_SetStringArray_m3559579766 (Il2CppObject * __this /* static, unused */, String_t* ___key0, StringU5BU5D_t1642385972* ___stringArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] PlayerPrefsX::GetStringArray(System.String)
extern "C"  StringU5BU5D_t1642385972* PlayerPrefsX_GetStringArray_m1821490265 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] PlayerPrefsX::GetStringArray(System.String,System.String,System.Int32)
extern "C"  StringU5BU5D_t1642385972* PlayerPrefsX_GetStringArray_m335614902 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetIntArray(System.String,System.Int32[])
extern "C"  bool PlayerPrefsX_SetIntArray_m4032022271 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Int32U5BU5D_t3030399641* ___intArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetFloatArray(System.String,System.Single[])
extern "C"  bool PlayerPrefsX_SetFloatArray_m1305280912 (Il2CppObject * __this /* static, unused */, String_t* ___key0, SingleU5BU5D_t577127397* ___floatArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetVector2Array(System.String,UnityEngine.Vector2[])
extern "C"  bool PlayerPrefsX_SetVector2Array_m2335509664 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2U5BU5D_t686124026* ___vector2Array1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetVector3Array(System.String,UnityEngine.Vector3[])
extern "C"  bool PlayerPrefsX_SetVector3Array_m2015741178 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3U5BU5D_t1172311765* ___vector3Array1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetQuaternionArray(System.String,UnityEngine.Quaternion[])
extern "C"  bool PlayerPrefsX_SetQuaternionArray_m3670241050 (Il2CppObject * __this /* static, unused */, String_t* ___key0, QuaternionU5BU5D_t1854387467* ___quaternionArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetColorArray(System.String,UnityEngine.Color[])
extern "C"  bool PlayerPrefsX_SetColorArray_m848705280 (Il2CppObject * __this /* static, unused */, String_t* ___key0, ColorU5BU5D_t672350442* ___colorArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertFromInt(System.Int32[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromInt_m961274613 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertFromFloat(System.Single[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromFloat_m2802631456 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertFromVector2(UnityEngine.Vector2[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromVector2_m3403065726 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t686124026* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertFromVector3(UnityEngine.Vector3[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromVector3_m1055003390 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertFromQuaternion(UnityEngine.Quaternion[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromQuaternion_m3332436508 (Il2CppObject * __this /* static, unused */, QuaternionU5BU5D_t1854387467* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertFromColor(UnityEngine.Color[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromColor_m1007003518 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] PlayerPrefsX::GetIntArray(System.String)
extern "C"  Int32U5BU5D_t3030399641* PlayerPrefsX_GetIntArray_m1386818360 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] PlayerPrefsX::GetIntArray(System.String,System.Int32,System.Int32)
extern "C"  Int32U5BU5D_t3030399641* PlayerPrefsX_GetIntArray_m1834526262 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] PlayerPrefsX::GetFloatArray(System.String)
extern "C"  SingleU5BU5D_t577127397* PlayerPrefsX_GetFloatArray_m1814773467 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] PlayerPrefsX::GetFloatArray(System.String,System.Single,System.Int32)
extern "C"  SingleU5BU5D_t577127397* PlayerPrefsX_GetFloatArray_m2057507575 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] PlayerPrefsX::GetVector2Array(System.String)
extern "C"  Vector2U5BU5D_t686124026* PlayerPrefsX_GetVector2Array_m2652766529 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] PlayerPrefsX::GetVector2Array(System.String,UnityEngine.Vector2,System.Int32)
extern "C"  Vector2U5BU5D_t686124026* PlayerPrefsX_GetVector2Array_m2920103712 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2_t2243707579  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] PlayerPrefsX::GetVector3Array(System.String)
extern "C"  Vector3U5BU5D_t1172311765* PlayerPrefsX_GetVector3Array_m3968718013 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] PlayerPrefsX::GetVector3Array(System.String,UnityEngine.Vector3,System.Int32)
extern "C"  Vector3U5BU5D_t1172311765* PlayerPrefsX_GetVector3Array_m2590474277 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3_t2243707580  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion[] PlayerPrefsX::GetQuaternionArray(System.String)
extern "C"  QuaternionU5BU5D_t1854387467* PlayerPrefsX_GetQuaternionArray_m3914704109 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion[] PlayerPrefsX::GetQuaternionArray(System.String,UnityEngine.Quaternion,System.Int32)
extern "C"  QuaternionU5BU5D_t1854387467* PlayerPrefsX_GetQuaternionArray_m3503864975 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Quaternion_t4030073918  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] PlayerPrefsX::GetColorArray(System.String)
extern "C"  ColorU5BU5D_t672350442* PlayerPrefsX_GetColorArray_m366961213 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] PlayerPrefsX::GetColorArray(System.String,UnityEngine.Color,System.Int32)
extern "C"  ColorU5BU5D_t672350442* PlayerPrefsX_GetColorArray_m1825582668 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Color_t2020392075  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertToInt(System.Collections.Generic.List`1<System.Int32>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToInt_m644963337 (Il2CppObject * __this /* static, unused */, List_1_t1440998580 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertToFloat(System.Collections.Generic.List`1<System.Single>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToFloat_m2617188600 (Il2CppObject * __this /* static, unused */, List_1_t1445631064 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertToVector2(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToVector2_m3795235550 (Il2CppObject * __this /* static, unused */, List_1_t1612828711 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertToVector3(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToVector3_m3000137214 (Il2CppObject * __this /* static, unused */, List_1_t1612828712 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertToQuaternion(System.Collections.Generic.List`1<UnityEngine.Quaternion>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToQuaternion_m4252035516 (Il2CppObject * __this /* static, unused */, List_1_t3399195050 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertToColor(System.Collections.Generic.List`1<UnityEngine.Color>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToColor_m3889997982 (Il2CppObject * __this /* static, unused */, List_1_t1389513207 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ShowArrayType(System.String)
extern "C"  void PlayerPrefsX_ShowArrayType_m1550763734 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::Initialize()
extern "C"  void PlayerPrefsX_Initialize_m3123284730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SaveBytes(System.String,System.Byte[])
extern "C"  bool PlayerPrefsX_SaveBytes_m2961073563 (Il2CppObject * __this /* static, unused */, String_t* ___key0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertFloatToBytes(System.Single,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertFloatToBytes_m3413650209 (Il2CppObject * __this /* static, unused */, float ___f0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PlayerPrefsX::ConvertBytesToFloat(System.Byte[])
extern "C"  float PlayerPrefsX_ConvertBytesToFloat_m1424229334 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertInt32ToBytes(System.Int32,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertInt32ToBytes_m990830831 (Il2CppObject * __this /* static, unused */, int32_t ___i0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayerPrefsX::ConvertBytesToInt32(System.Byte[])
extern "C"  int32_t PlayerPrefsX_ConvertBytesToInt32_m1019863976 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertTo4Bytes(System.Byte[])
extern "C"  void PlayerPrefsX_ConvertTo4Bytes_m3604966018 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertFrom4Bytes(System.Byte[])
extern "C"  void PlayerPrefsX_ConvertFrom4Bytes_m9415465 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
