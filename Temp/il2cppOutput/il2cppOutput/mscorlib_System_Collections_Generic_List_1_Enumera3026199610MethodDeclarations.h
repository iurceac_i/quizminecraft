﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.SignerInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m144689098(__this, ___l0, method) ((  void (*) (Enumerator_t3026199610 *, List_1_t3491469936 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.SignerInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1229508648(__this, method) ((  void (*) (Enumerator_t3026199610 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.SignerInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2805209230(__this, method) ((  Il2CppObject * (*) (Enumerator_t3026199610 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.SignerInfo>::Dispose()
#define Enumerator_Dispose_m3075839395(__this, method) ((  void (*) (Enumerator_t3026199610 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.SignerInfo>::VerifyState()
#define Enumerator_VerifyState_m3476644664(__this, method) ((  void (*) (Enumerator_t3026199610 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.SignerInfo>::MoveNext()
#define Enumerator_MoveNext_m3882097823(__this, method) ((  bool (*) (Enumerator_t3026199610 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.SignerInfo>::get_Current()
#define Enumerator_get_Current_m3179888355(__this, method) ((  SignerInfo_t4122348804 * (*) (Enumerator_t3026199610 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
