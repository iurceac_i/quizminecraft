﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Kakera.PickerUnsupported
struct PickerUnsupported_t1738158813;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Kakera.PickerUnsupported::.ctor()
extern "C"  void PickerUnsupported__ctor_m1506088397 (PickerUnsupported_t1738158813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.PickerUnsupported::Show(System.String,System.String,System.Int32)
extern "C"  void PickerUnsupported_Show_m1565248927 (PickerUnsupported_t1738158813 * __this, String_t* ___title0, String_t* ___outputFileName1, int32_t ___maxSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
