﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Handler/<WaitForSeconds>c__Iterator2
struct U3CWaitForSecondsU3Ec__Iterator2_t2852953551;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Handler/<WaitForSeconds>c__Iterator2::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator2__ctor_m3934226712 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Handler/<WaitForSeconds>c__Iterator2::MoveNext()
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator2_MoveNext_m226710572 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Handler/<WaitForSeconds>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m585009908 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Handler/<WaitForSeconds>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3960121836 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/<WaitForSeconds>c__Iterator2::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator2_Dispose_m179864869 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/<WaitForSeconds>c__Iterator2::Reset()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator2_Reset_m3747011487 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
