﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Handler/ExecuteWithGameObjcet
struct ExecuteWithGameObjcet_t1191175011;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Handler/ExecuteWithGameObjcet::.ctor(System.Object,System.IntPtr)
extern "C"  void ExecuteWithGameObjcet__ctor_m342781680 (ExecuteWithGameObjcet_t1191175011 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/ExecuteWithGameObjcet::Invoke(UnityEngine.GameObject)
extern "C"  void ExecuteWithGameObjcet_Invoke_m2016891188 (ExecuteWithGameObjcet_t1191175011 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Handler/ExecuteWithGameObjcet::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExecuteWithGameObjcet_BeginInvoke_m3662776725 (ExecuteWithGameObjcet_t1191175011 * __this, GameObject_t1756533147 * ___go0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/ExecuteWithGameObjcet::EndInvoke(System.IAsyncResult)
extern "C"  void ExecuteWithGameObjcet_EndInvoke_m1737415974 (ExecuteWithGameObjcet_t1191175011 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
