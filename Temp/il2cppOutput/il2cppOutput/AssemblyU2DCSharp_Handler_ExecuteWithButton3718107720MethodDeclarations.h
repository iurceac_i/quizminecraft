﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Handler/ExecuteWithButton
struct ExecuteWithButton_t3718107720;
// System.Object
struct Il2CppObject;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Handler/ExecuteWithButton::.ctor(System.Object,System.IntPtr)
extern "C"  void ExecuteWithButton__ctor_m1880605207 (ExecuteWithButton_t3718107720 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/ExecuteWithButton::Invoke(UnityEngine.UI.Button)
extern "C"  void ExecuteWithButton_Invoke_m2824180992 (ExecuteWithButton_t3718107720 * __this, Button_t2872111280 * ___btns0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Handler/ExecuteWithButton::BeginInvoke(UnityEngine.UI.Button,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExecuteWithButton_BeginInvoke_m2797783651 (ExecuteWithButton_t3718107720 * __this, Button_t2872111280 * ___btns0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/ExecuteWithButton::EndInvoke(System.IAsyncResult)
extern "C"  void ExecuteWithButton_EndInvoke_m1258134841 (ExecuteWithButton_t3718107720 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
