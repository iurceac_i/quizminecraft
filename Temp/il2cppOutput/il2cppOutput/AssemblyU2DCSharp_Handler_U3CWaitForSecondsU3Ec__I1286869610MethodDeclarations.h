﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Handler/<WaitForSeconds>c__Iterator3
struct U3CWaitForSecondsU3Ec__Iterator3_t1286869610;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Handler/<WaitForSeconds>c__Iterator3::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator3__ctor_m2125104883 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Handler/<WaitForSeconds>c__Iterator3::MoveNext()
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator3_MoveNext_m3308615789 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Handler/<WaitForSeconds>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3184991801 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Handler/<WaitForSeconds>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3737969969 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/<WaitForSeconds>c__Iterator3::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator3_Dispose_m1067827552 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/<WaitForSeconds>c__Iterator3::Reset()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator3_Reset_m160027770 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
