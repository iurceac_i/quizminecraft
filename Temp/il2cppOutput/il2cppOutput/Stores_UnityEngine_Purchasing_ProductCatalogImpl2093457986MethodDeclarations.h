﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.ProductCatalogImpl
struct ProductCatalogImpl_t2093457986;
// UnityEngine.Purchasing.ProductCatalog
struct ProductCatalog_t2667590766;

#include "codegen/il2cpp-codegen.h"

// UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.ProductCatalogImpl::LoadDefaultCatalog()
extern "C"  ProductCatalog_t2667590766 * ProductCatalogImpl_LoadDefaultCatalog_m3876298388 (ProductCatalogImpl_t2093457986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.ProductCatalogImpl::.ctor()
extern "C"  void ProductCatalogImpl__ctor_m1140423388 (ProductCatalogImpl_t2093457986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
