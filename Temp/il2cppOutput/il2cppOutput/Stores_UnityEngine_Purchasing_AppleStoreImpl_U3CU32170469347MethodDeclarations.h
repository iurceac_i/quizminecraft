﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass19_0
struct U3CU3Ec__DisplayClass19_0_t2170469347;
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt
struct AppleInAppPurchaseReceipt_t3271698749;

#include "codegen/il2cpp-codegen.h"
#include "Security_UnityEngine_Purchasing_Security_AppleInAp3271698749.h"

// System.Void UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass19_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass19_0__ctor_m2944503998 (U3CU3Ec__DisplayClass19_0_t2170469347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass19_0::<OnProductsRetrieved>b__0(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt)
extern "C"  bool U3CU3Ec__DisplayClass19_0_U3COnProductsRetrievedU3Eb__0_m3776727603 (U3CU3Ec__DisplayClass19_0_t2170469347 * __this, AppleInAppPurchaseReceipt_t3271698749 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
