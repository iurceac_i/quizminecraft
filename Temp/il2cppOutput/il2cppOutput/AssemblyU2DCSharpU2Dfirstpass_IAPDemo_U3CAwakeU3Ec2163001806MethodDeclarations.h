﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IAPDemo/<Awake>c__AnonStorey0
struct U3CAwakeU3Ec__AnonStorey0_t2163001806;

#include "codegen/il2cpp-codegen.h"

// System.Void IAPDemo/<Awake>c__AnonStorey0::.ctor()
extern "C"  void U3CAwakeU3Ec__AnonStorey0__ctor_m1436498841 (U3CAwakeU3Ec__AnonStorey0_t2163001806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo/<Awake>c__AnonStorey0::<>m__0()
extern "C"  void U3CAwakeU3Ec__AnonStorey0_U3CU3Em__0_m3852349590 (U3CAwakeU3Ec__AnonStorey0_t2163001806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo/<Awake>c__AnonStorey0::<>m__1()
extern "C"  void U3CAwakeU3Ec__AnonStorey0_U3CU3Em__1_m3852349495 (U3CAwakeU3Ec__AnonStorey0_t2163001806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
