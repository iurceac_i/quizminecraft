﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.UnityChannelPurchaseReceipt
struct UnityChannelPurchaseReceipt_t1964142665;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.UnityChannelPurchaseReceipt::.ctor()
extern "C"  void UnityChannelPurchaseReceipt__ctor_m420745419 (UnityChannelPurchaseReceipt_t1964142665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
