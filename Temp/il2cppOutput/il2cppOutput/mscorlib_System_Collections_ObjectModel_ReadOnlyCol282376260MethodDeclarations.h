﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m4007673057(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3671019970_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1095765615(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, ProductCatalogPayout_t96590568 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2989589458_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1982807355(__this, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m454937302_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1177247956(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, int32_t, ProductCatalogPayout_t96590568 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4272763307_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1883224298(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t282376260 *, ProductCatalogPayout_t96590568 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3199809075_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3356081512(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m962041751_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2934120286(__this, ___index0, method) ((  ProductCatalogPayout_t96590568 * (*) (ReadOnlyCollection_1_t282376260 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m70085287_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1315696371(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, int32_t, ProductCatalogPayout_t96590568 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1547026160_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3538759999(__this, method) ((  bool (*) (ReadOnlyCollection_1_t282376260 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4041967064_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2968307290(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3664791405_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m304229083(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t282376260 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m531171980_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1334359902(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t282376260 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3780136817_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m593457924(__this, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3983677501_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2420993432(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t282376260 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1990607517_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3104185904(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t282376260 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m606942423_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3634788667(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m691705570_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m284288699(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3182494192_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2534517905(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m572840272_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m878208894(__this, method) ((  bool (*) (ReadOnlyCollection_1_t282376260 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2871048729_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2417686074(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t282376260 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m769863805_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2143188339(__this, method) ((  bool (*) (ReadOnlyCollection_1_t282376260 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m942145650_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1880712762(__this, method) ((  bool (*) (ReadOnlyCollection_1_t282376260 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1367736517_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1210159683(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t282376260 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3336878134_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3293535452(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1799572719_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::Contains(T)
#define ReadOnlyCollection_1_Contains_m3088652349(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t282376260 *, ProductCatalogPayout_t96590568 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m1227826160_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1470895739(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t282376260 *, ProductCatalogPayoutU5BU5D_t3219985529*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m4257276542_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m2867862410(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t282376260 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1627519329_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m3376185567(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t282376260 *, ProductCatalogPayout_t96590568 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1981423404_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::get_Count()
#define ReadOnlyCollection_1_get_Count_m2130597502(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t282376260 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2562379905_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductCatalogPayout>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m747810138(__this, ___index0, method) ((  ProductCatalogPayout_t96590568 * (*) (ReadOnlyCollection_1_t282376260 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m191392387_gshared)(__this, ___index0, method)
