﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3175182372MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1419251432(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2514953310 *, Dictionary_2_t1194928608 *, const MethodInfo*))Enumerator__ctor_m1689433262_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3057457277(__this, method) ((  Il2CppObject * (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18154257_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3479327461(__this, method) ((  void (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3295310809_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1280200214(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3230011756_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3211285103(__this, method) ((  Il2CppObject * (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2643368291_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1905779039(__this, method) ((  Il2CppObject * (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3348204795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::MoveNext()
#define Enumerator_MoveNext_m1652681005(__this, method) ((  bool (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_MoveNext_m3968011081_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::get_Current()
#define Enumerator_get_Current_m2911828817(__this, method) ((  KeyValuePair_2_t3247241126  (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_get_Current_m3550708493_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2675227144(__this, method) ((  int32_t (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_get_CurrentKey_m739060958_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1807246472(__this, method) ((  String_t* (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_get_CurrentValue_m3291390238_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::Reset()
#define Enumerator_Reset_m1041763150(__this, method) ((  void (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_Reset_m944422548_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::VerifyState()
#define Enumerator_VerifyState_m2717508367(__this, method) ((  void (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_VerifyState_m47465931_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1997450709(__this, method) ((  void (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_VerifyCurrent_m866906857_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Purchasing.AppStore,System.String>::Dispose()
#define Enumerator_Dispose_m3530773912(__this, method) ((  void (*) (Enumerator_t2514953310 *, const MethodInfo*))Enumerator_Dispose_m1941330462_gshared)(__this, method)
