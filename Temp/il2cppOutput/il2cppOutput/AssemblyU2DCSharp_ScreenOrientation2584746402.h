﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenOrientation
struct  ScreenOrientation_t2584746402  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ScreenOrientation::landscapeStatsPanel
	GameObject_t1756533147 * ___landscapeStatsPanel_2;
	// UnityEngine.GameObject ScreenOrientation::PortraitStatsPanel
	GameObject_t1756533147 * ___PortraitStatsPanel_3;

public:
	inline static int32_t get_offset_of_landscapeStatsPanel_2() { return static_cast<int32_t>(offsetof(ScreenOrientation_t2584746402, ___landscapeStatsPanel_2)); }
	inline GameObject_t1756533147 * get_landscapeStatsPanel_2() const { return ___landscapeStatsPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_landscapeStatsPanel_2() { return &___landscapeStatsPanel_2; }
	inline void set_landscapeStatsPanel_2(GameObject_t1756533147 * value)
	{
		___landscapeStatsPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___landscapeStatsPanel_2, value);
	}

	inline static int32_t get_offset_of_PortraitStatsPanel_3() { return static_cast<int32_t>(offsetof(ScreenOrientation_t2584746402, ___PortraitStatsPanel_3)); }
	inline GameObject_t1756533147 * get_PortraitStatsPanel_3() const { return ___PortraitStatsPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_PortraitStatsPanel_3() { return &___PortraitStatsPanel_3; }
	inline void set_PortraitStatsPanel_3(GameObject_t1756533147 * value)
	{
		___PortraitStatsPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___PortraitStatsPanel_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
