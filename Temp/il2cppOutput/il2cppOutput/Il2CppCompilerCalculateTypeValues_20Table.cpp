﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2813769101.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasin911589174.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2142434692.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo823941185.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannelP594865545.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannelP855507751.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannel2253884029.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_U3CAwakeU3Ec2163001806.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_U3CInitUIU3Ec996539604.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3789552708.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3384326983.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3694879381.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1500295812.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_Levels748035019.h"
#include "AssemblyU2DCSharp_Main2809994845.h"
#include "AssemblyU2DCSharp_Question2927948840.h"
#include "AssemblyU2DCSharp_Root2702590648.h"
#include "AssemblyU2DCSharp_ButtonAnimation1426385366.h"
#include "AssemblyU2DCSharp_LevelController2717835266.h"
#include "AssemblyU2DCSharp_MainGame3800664731.h"
#include "AssemblyU2DCSharp_Adapter814751345.h"
#include "AssemblyU2DCSharp_Adapter_U3CsetAdapterU3Ec__AnonS1050088512.h"
#include "AssemblyU2DCSharp_AssemblyCSharp_Purchase1644043085.h"
#include "AssemblyU2DCSharp_QuestionController445239244.h"
#include "AssemblyU2DCSharp_ScreenOrientation2584746402.h"
#include "AssemblyU2DCSharp_SoundController1686593041.h"
#include "AssemblyU2DCSharp_Stats967880071.h"
#include "AssemblyU2DCSharp_Stats_U3CLoadImageStatsU3Ec__Ite1390744875.h"
#include "AssemblyU2DCSharp_FPSDisplay3952194245.h"
#include "AssemblyU2DCSharp_Handler2658839040.h"
#include "AssemblyU2DCSharp_Handler_Execute1389284496.h"
#include "AssemblyU2DCSharp_Handler_ExecuteWithGameObjcet1191175011.h"
#include "AssemblyU2DCSharp_Handler_ExecuteWithButton3718107720.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I4015752965.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I2449669024.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I2852953551.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I1286869610.h"
#include "AssemblyU2DCSharp_PlayerPrefsX1687815431.h"
#include "AssemblyU2DCSharp_PlayerPrefsX_ArrayType77146353.h"
#include "AssemblyU2DCSharp_Util4006552276.h"
#include "AssemblyU2DCSharp_readHTML503794377.h"
#include "AssemblyU2DCSharp_Kakera_PickerController3670494704.h"
#include "AssemblyU2DCSharp_Kakera_PickerController_U3CLoadI2450652310.h"
#include "AssemblyU2DCSharp_Kakera_Rotator3287300421.h"
#include "AssemblyU2DCSharp_Kakera_PickerUnsupported1738158813.h"
#include "AssemblyU2DCSharp_Kakera_PickeriOS3652150543.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker2332148304.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker_ImageDelegate3548206662.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker_ErrorDelegate402150177.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (OnPurchaseFailedEvent_t2813769101), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (IAPButtonStoreManager_t911589174), -1, sizeof(IAPButtonStoreManager_t911589174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2001[6] = 
{
	IAPButtonStoreManager_t911589174_StaticFields::get_offset_of_instance_0(),
	IAPButtonStoreManager_t911589174::get_offset_of_catalog_1(),
	IAPButtonStoreManager_t911589174::get_offset_of_activeButtons_2(),
	IAPButtonStoreManager_t911589174::get_offset_of_m_Listener_3(),
	IAPButtonStoreManager_t911589174::get_offset_of_controller_4(),
	IAPButtonStoreManager_t911589174::get_offset_of_extensions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (IAPConfigurationHelper_t2142434692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (IAPDemo_t823941185), -1, sizeof(IAPDemo_t823941185_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2003[19] = 
{
	IAPDemo_t823941185::get_offset_of_m_Controller_2(),
	IAPDemo_t823941185::get_offset_of_m_AppleExtensions_3(),
	IAPDemo_t823941185::get_offset_of_m_MoolahExtensions_4(),
	IAPDemo_t823941185::get_offset_of_m_SamsungExtensions_5(),
	IAPDemo_t823941185::get_offset_of_m_MicrosoftExtensions_6(),
	IAPDemo_t823941185::get_offset_of_m_UnityChannelExtensions_7(),
	IAPDemo_t823941185::get_offset_of_m_IsGooglePlayStoreSelected_8(),
	IAPDemo_t823941185::get_offset_of_m_IsCloudMoolahStoreSelected_9(),
	IAPDemo_t823941185::get_offset_of_m_IsUnityChannelSelected_10(),
	IAPDemo_t823941185::get_offset_of_m_LastTransationID_11(),
	IAPDemo_t823941185::get_offset_of_m_LastReceipt_12(),
	IAPDemo_t823941185::get_offset_of_m_CloudMoolahUserName_13(),
	IAPDemo_t823941185::get_offset_of_m_IsLoggedIn_14(),
	IAPDemo_t823941185::get_offset_of_unityChannelLoginHandler_15(),
	IAPDemo_t823941185::get_offset_of_m_FetchReceiptPayloadOnPurchase_16(),
	IAPDemo_t823941185::get_offset_of_m_SelectedItemIndex_17(),
	IAPDemo_t823941185::get_offset_of_m_PurchaseInProgress_18(),
	IAPDemo_t823941185::get_offset_of_m_InteractableSelectable_19(),
	IAPDemo_t823941185_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (UnityChannelPurchaseError_t594865545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[2] = 
{
	UnityChannelPurchaseError_t594865545::get_offset_of_error_0(),
	UnityChannelPurchaseError_t594865545::get_offset_of_purchaseInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (UnityChannelPurchaseInfo_t855507751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[3] = 
{
	UnityChannelPurchaseInfo_t855507751::get_offset_of_productCode_0(),
	UnityChannelPurchaseInfo_t855507751::get_offset_of_gameOrderId_1(),
	UnityChannelPurchaseInfo_t855507751::get_offset_of_orderQueryToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (UnityChannelLoginHandler_t2253884029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[4] = 
{
	UnityChannelLoginHandler_t2253884029::get_offset_of_initializeSucceededAction_0(),
	UnityChannelLoginHandler_t2253884029::get_offset_of_initializeFailedAction_1(),
	UnityChannelLoginHandler_t2253884029::get_offset_of_loginSucceededAction_2(),
	UnityChannelLoginHandler_t2253884029::get_offset_of_loginFailedAction_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (U3CAwakeU3Ec__AnonStorey0_t2163001806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[3] = 
{
	U3CAwakeU3Ec__AnonStorey0_t2163001806::get_offset_of_builder_0(),
	U3CAwakeU3Ec__AnonStorey0_t2163001806::get_offset_of_initializeUnityIap_1(),
	U3CAwakeU3Ec__AnonStorey0_t2163001806::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (U3CInitUIU3Ec__AnonStorey1_t996539604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[1] = 
{
	U3CInitUIU3Ec__AnonStorey1_t996539604::get_offset_of_txId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (IAPListener_t3789552708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[4] = 
{
	IAPListener_t3789552708::get_offset_of_consumePurchase_2(),
	IAPListener_t3789552708::get_offset_of_dontDestroyOnLoad_3(),
	IAPListener_t3789552708::get_offset_of_onPurchaseComplete_4(),
	IAPListener_t3789552708::get_offset_of_onPurchaseFailed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (OnPurchaseCompletedEvent_t3384326983), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (OnPurchaseFailedEvent_t3694879381), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305144), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2012[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (U24ArrayTypeU3D244_t1500295812)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D244_t1500295812 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (U3CModuleU3E_t3783534232), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (Levels_t748035019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[4] = 
{
	Levels_t748035019::get_offset_of_nrLevel_0(),
	Levels_t748035019::get_offset_of_corectAnswers_1(),
	Levels_t748035019::get_offset_of_quests_2(),
	Levels_t748035019::get_offset_of_isLocked_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (Main_t2809994845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[4] = 
{
	Main_t2809994845::get_offset_of_product_id_0(),
	Main_t2809994845::get_offset_of_store_id_1(),
	Main_t2809994845::get_offset_of_game_title_2(),
	Main_t2809994845::get_offset_of_game_description_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (Question_t2927948840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[3] = 
{
	Question_t2927948840::get_offset_of_answer_0(),
	Question_t2927948840::get_offset_of_choices_1(),
	Question_t2927948840::get_offset_of_question_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (Root_t2702590648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[1] = 
{
	Root_t2702590648::get_offset_of_questions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (ButtonAnimation_t1426385366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (LevelController_t2717835266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[5] = 
{
	LevelController_t2717835266::get_offset_of_adapter_2(),
	LevelController_t2717835266::get_offset_of_levels_3(),
	LevelController_t2717835266::get_offset_of_questController_4(),
	LevelController_t2717835266::get_offset_of_gamePanel_5(),
	LevelController_t2717835266::get_offset_of_buyPanel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (MainGame_t3800664731), -1, sizeof(MainGame_t3800664731_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2021[18] = 
{
	MainGame_t3800664731_StaticFields::get_offset_of_questions_2(),
	MainGame_t3800664731::get_offset_of_LegasyInfo_3(),
	MainGame_t3800664731::get_offset_of_levelPanel_4(),
	MainGame_t3800664731::get_offset_of_play_btn_5(),
	MainGame_t3800664731::get_offset_of_store_btn_6(),
	MainGame_t3800664731::get_offset_of_stats_btn_7(),
	MainGame_t3800664731::get_offset_of_back_1_8(),
	MainGame_t3800664731::get_offset_of_back_2_9(),
	MainGame_t3800664731::get_offset_of_back_3_10(),
	MainGame_t3800664731::get_offset_of_back_4_11(),
	MainGame_t3800664731::get_offset_of_titleGame_12(),
	MainGame_t3800664731::get_offset_of_gameDescription_13(),
	MainGame_t3800664731::get_offset_of_valueDescription_14(),
	MainGame_t3800664731::get_offset_of_valueTitle_15(),
	MainGame_t3800664731::get_offset_of_isSubscribed_16(),
	MainGame_t3800664731_StaticFields::get_offset_of_verfiSubscribe_17(),
	MainGame_t3800664731::get_offset_of_soundController_18(),
	MainGame_t3800664731::get_offset_of_itemsContent_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (Adapter_t814751345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[3] = 
{
	Adapter_t814751345::get_offset_of_gameObjects_2(),
	Adapter_t814751345::get_offset_of_isPurchase_3(),
	Adapter_t814751345::get_offset_of_adapterCallBack_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (U3CsetAdapterU3Ec__AnonStorey0_t1050088512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[2] = 
{
	U3CsetAdapterU3Ec__AnonStorey0_t1050088512::get_offset_of_finalObj_0(),
	U3CsetAdapterU3Ec__AnonStorey0_t1050088512::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (Purchase_t1644043085), -1, sizeof(Purchase_t1644043085_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2025[9] = 
{
	Purchase_t1644043085_StaticFields::get_offset_of_isPurchased_2(),
	Purchase_t1644043085::get_offset_of_alertPurchase_3(),
	Purchase_t1644043085::get_offset_of_gamePanel_4(),
	Purchase_t1644043085_StaticFields::get_offset_of_m_StoreController_5(),
	Purchase_t1644043085_StaticFields::get_offset_of_m_StoreExtensionProvider_6(),
	Purchase_t1644043085_StaticFields::get_offset_of_kProductIDConsumable_7(),
	Purchase_t1644043085_StaticFields::get_offset_of_kProductIDNonConsumable_8(),
	Purchase_t1644043085_StaticFields::get_offset_of_kProductIDSubscription_9(),
	Purchase_t1644043085::get_offset_of_itemsContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (QuestionController_t445239244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[28] = 
{
	QuestionController_t445239244::get_offset_of_question_2(),
	QuestionController_t445239244::get_offset_of_answer_0_3(),
	QuestionController_t445239244::get_offset_of_answer_1_4(),
	QuestionController_t445239244::get_offset_of_answer_2_5(),
	QuestionController_t445239244::get_offset_of_answer_3_6(),
	QuestionController_t445239244::get_offset_of_btn_1_7(),
	QuestionController_t445239244::get_offset_of_btn_2_8(),
	QuestionController_t445239244::get_offset_of_btn_3_9(),
	QuestionController_t445239244::get_offset_of_btn_4_10(),
	QuestionController_t445239244::get_offset_of_star1_11(),
	QuestionController_t445239244::get_offset_of_star2_12(),
	QuestionController_t445239244::get_offset_of_star3_13(),
	QuestionController_t445239244::get_offset_of_callBack_14(),
	QuestionController_t445239244::get_offset_of_correctAns_15(),
	QuestionController_t445239244::get_offset_of_buyPanel_16(),
	QuestionController_t445239244::get_offset_of_GamePanel_17(),
	QuestionController_t445239244::get_offset_of_twoButtons_18(),
	QuestionController_t445239244::get_offset_of_singleButtonPanel_19(),
	QuestionController_t445239244::get_offset_of_finalAlertPanel_20(),
	QuestionController_t445239244::get_offset_of_levelPanel_21(),
	QuestionController_t445239244::get_offset_of_notEnought_22(),
	QuestionController_t445239244::get_offset_of_numb_of_compl_mission_23(),
	QuestionController_t445239244::get_offset_of_numQuest_24(),
	QuestionController_t445239244::get_offset_of_levelGlobal_25(),
	QuestionController_t445239244::get_offset_of_positionLevel_26(),
	QuestionController_t445239244::get_offset_of_correctAnswer_27(),
	QuestionController_t445239244::get_offset_of_purch_28(),
	QuestionController_t445239244::get_offset_of_soundControll_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (ScreenOrientation_t2584746402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[2] = 
{
	ScreenOrientation_t2584746402::get_offset_of_landscapeStatsPanel_2(),
	ScreenOrientation_t2584746402::get_offset_of_PortraitStatsPanel_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (SoundController_t1686593041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[11] = 
{
	SoundController_t1686593041::get_offset_of_correctSound_2(),
	SoundController_t1686593041::get_offset_of_incorrectSound_3(),
	SoundController_t1686593041::get_offset_of_closeWindow_4(),
	SoundController_t1686593041::get_offset_of_failMission_5(),
	SoundController_t1686593041::get_offset_of_menuSong_6(),
	SoundController_t1686593041::get_offset_of_succesMission_7(),
	SoundController_t1686593041::get_offset_of_settingsMenu_8(),
	SoundController_t1686593041::get_offset_of_soundControll_9(),
	SoundController_t1686593041::get_offset_of_backgroundSound_10(),
	SoundController_t1686593041::get_offset_of_onButton_11(),
	SoundController_t1686593041::get_offset_of_offButton_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (Stats_t967880071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[18] = 
{
	Stats_t967880071::get_offset_of_statsPanel_2(),
	Stats_t967880071::get_offset_of_backButton_3(),
	Stats_t967880071::get_offset_of_totalLikes_4(),
	Stats_t967880071::get_offset_of_totalFollowers_5(),
	Stats_t967880071::get_offset_of_totalLikesPortret_6(),
	Stats_t967880071::get_offset_of_totalFollowersPortret_7(),
	Stats_t967880071::get_offset_of_alertFailPanel_8(),
	Stats_t967880071::get_offset_of_alertCorrectPanel_9(),
	Stats_t967880071::get_offset_of_landscapeStatsPanel_10(),
	Stats_t967880071::get_offset_of_PortraitStatsPanel_11(),
	Stats_t967880071::get_offset_of_soundController_12(),
	Stats_t967880071::get_offset_of_mainGame_13(),
	Stats_t967880071::get_offset_of_btn_back_1_14(),
	Stats_t967880071::get_offset_of_btn_back_2_15(),
	Stats_t967880071::get_offset_of_pathImageStats_16(),
	Stats_t967880071::get_offset_of_imageStats_17(),
	Stats_t967880071::get_offset_of_imagePortraitStats_18(),
	Stats_t967880071::get_offset_of_defaultImage_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (U3CLoadImageStatsU3Ec__Iterator0_t1390744875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[8] = 
{
	U3CLoadImageStatsU3Ec__Iterator0_t1390744875::get_offset_of_path_0(),
	U3CLoadImageStatsU3Ec__Iterator0_t1390744875::get_offset_of_U3CurlU3E__0_1(),
	U3CLoadImageStatsU3Ec__Iterator0_t1390744875::get_offset_of_U3CwwwU3E__1_2(),
	U3CLoadImageStatsU3Ec__Iterator0_t1390744875::get_offset_of_U3CtextureU3E__2_3(),
	U3CLoadImageStatsU3Ec__Iterator0_t1390744875::get_offset_of_output_4(),
	U3CLoadImageStatsU3Ec__Iterator0_t1390744875::get_offset_of_U24current_5(),
	U3CLoadImageStatsU3Ec__Iterator0_t1390744875::get_offset_of_U24disposing_6(),
	U3CLoadImageStatsU3Ec__Iterator0_t1390744875::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (FPSDisplay_t3952194245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[1] = 
{
	FPSDisplay_t3952194245::get_offset_of_deltaTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (Handler_t2658839040), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (Execute_t1389284496), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (ExecuteWithGameObjcet_t1191175011), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (ExecuteWithButton_t3718107720), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (U3CWaitForSecondsU3Ec__Iterator0_t4015752965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[5] = 
{
	U3CWaitForSecondsU3Ec__Iterator0_t4015752965::get_offset_of_time_0(),
	U3CWaitForSecondsU3Ec__Iterator0_t4015752965::get_offset_of_isValable_1(),
	U3CWaitForSecondsU3Ec__Iterator0_t4015752965::get_offset_of_U24current_2(),
	U3CWaitForSecondsU3Ec__Iterator0_t4015752965::get_offset_of_U24disposing_3(),
	U3CWaitForSecondsU3Ec__Iterator0_t4015752965::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (U3CWaitForSecondsU3Ec__Iterator1_t2449669024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[5] = 
{
	U3CWaitForSecondsU3Ec__Iterator1_t2449669024::get_offset_of_time_0(),
	U3CWaitForSecondsU3Ec__Iterator1_t2449669024::get_offset_of_method_1(),
	U3CWaitForSecondsU3Ec__Iterator1_t2449669024::get_offset_of_U24current_2(),
	U3CWaitForSecondsU3Ec__Iterator1_t2449669024::get_offset_of_U24disposing_3(),
	U3CWaitForSecondsU3Ec__Iterator1_t2449669024::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (U3CWaitForSecondsU3Ec__Iterator2_t2852953551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2039[6] = 
{
	U3CWaitForSecondsU3Ec__Iterator2_t2852953551::get_offset_of_time_0(),
	U3CWaitForSecondsU3Ec__Iterator2_t2852953551::get_offset_of_method_1(),
	U3CWaitForSecondsU3Ec__Iterator2_t2852953551::get_offset_of_GO_2(),
	U3CWaitForSecondsU3Ec__Iterator2_t2852953551::get_offset_of_U24current_3(),
	U3CWaitForSecondsU3Ec__Iterator2_t2852953551::get_offset_of_U24disposing_4(),
	U3CWaitForSecondsU3Ec__Iterator2_t2852953551::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (U3CWaitForSecondsU3Ec__Iterator3_t1286869610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[6] = 
{
	U3CWaitForSecondsU3Ec__Iterator3_t1286869610::get_offset_of_time_0(),
	U3CWaitForSecondsU3Ec__Iterator3_t1286869610::get_offset_of_method_1(),
	U3CWaitForSecondsU3Ec__Iterator3_t1286869610::get_offset_of_btn_2(),
	U3CWaitForSecondsU3Ec__Iterator3_t1286869610::get_offset_of_U24current_3(),
	U3CWaitForSecondsU3Ec__Iterator3_t1286869610::get_offset_of_U24disposing_4(),
	U3CWaitForSecondsU3Ec__Iterator3_t1286869610::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (PlayerPrefsX_t1687815431), -1, sizeof(PlayerPrefsX_t1687815431_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2041[16] = 
{
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_endianDiff1_0(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_endianDiff2_1(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_idx_2(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_byteBlock_3(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_6(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_7(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_8(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_9(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_10(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_11(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_12(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_13(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_14(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (ArrayType_t77146353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2042[9] = 
{
	ArrayType_t77146353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (Util_t4006552276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (readHTML_t503794377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[14] = 
{
	readHTML_t503794377::get_offset_of_contentURL_2(),
	readHTML_t503794377::get_offset_of_backButton_3(),
	readHTML_t503794377::get_offset_of_path_terms_4(),
	readHTML_t503794377::get_offset_of_path_subscription_5(),
	readHTML_t503794377::get_offset_of_path_privacy_6(),
	readHTML_t503794377::get_offset_of_anim_7(),
	readHTML_t503794377::get_offset_of_readTextFileSubscription_8(),
	readHTML_t503794377::get_offset_of_readTextFileTerms_9(),
	readHTML_t503794377::get_offset_of_readTextFilePrivacy_10(),
	readHTML_t503794377::get_offset_of_SubscriptionBtn_11(),
	readHTML_t503794377::get_offset_of_privacyBtn_12(),
	readHTML_t503794377::get_offset_of_termsBtn_13(),
	readHTML_t503794377::get_offset_of_btn_1_14(),
	readHTML_t503794377::get_offset_of_soundControll_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (PickerController_t3670494704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[3] = 
{
	PickerController_t3670494704::get_offset_of_imagePicker_2(),
	PickerController_t3670494704::get_offset_of_imageRenderer_3(),
	PickerController_t3670494704::get_offset_of_imagePortretRenderer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (U3CLoadImageU3Ec__Iterator0_t2450652310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[8] = 
{
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_path_0(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U3CurlU3E__0_1(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U3CwwwU3E__1_2(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U3CtextureU3E__2_3(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_output_4(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U24current_5(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U24disposing_6(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (Rotator_t3287300421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[1] = 
{
	Rotator_t3287300421::get_offset_of_rotationVector_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (PickerUnsupported_t1738158813), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (PickeriOS_t3652150543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (Unimgpicker_t2332148304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2051[3] = 
{
	Unimgpicker_t2332148304::get_offset_of_Completed_2(),
	Unimgpicker_t2332148304::get_offset_of_Failed_3(),
	Unimgpicker_t2332148304::get_offset_of_picker_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (ImageDelegate_t3548206662), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (ErrorDelegate_t402150177), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
