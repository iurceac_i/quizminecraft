﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Question[]
struct QuestionU5BU5D_t1028533817;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// SoundController
struct SoundController_t1686593041;
// Main
struct Main_t2809994845;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainGame
struct  MainGame_t3800664731  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MainGame::LegasyInfo
	GameObject_t1756533147 * ___LegasyInfo_3;
	// UnityEngine.GameObject MainGame::levelPanel
	GameObject_t1756533147 * ___levelPanel_4;
	// UnityEngine.UI.Button MainGame::play_btn
	Button_t2872111280 * ___play_btn_5;
	// UnityEngine.UI.Button MainGame::store_btn
	Button_t2872111280 * ___store_btn_6;
	// UnityEngine.UI.Button MainGame::stats_btn
	Button_t2872111280 * ___stats_btn_7;
	// UnityEngine.UI.Button MainGame::back_1
	Button_t2872111280 * ___back_1_8;
	// UnityEngine.UI.Button MainGame::back_2
	Button_t2872111280 * ___back_2_9;
	// UnityEngine.UI.Button MainGame::back_3
	Button_t2872111280 * ___back_3_10;
	// UnityEngine.UI.Button MainGame::back_4
	Button_t2872111280 * ___back_4_11;
	// UnityEngine.UI.Text MainGame::titleGame
	Text_t356221433 * ___titleGame_12;
	// UnityEngine.UI.Text MainGame::gameDescription
	Text_t356221433 * ___gameDescription_13;
	// System.String MainGame::valueDescription
	String_t* ___valueDescription_14;
	// System.String MainGame::valueTitle
	String_t* ___valueTitle_15;
	// System.String MainGame::isSubscribed
	String_t* ___isSubscribed_16;
	// SoundController MainGame::soundController
	SoundController_t1686593041 * ___soundController_18;
	// Main MainGame::itemsContent
	Main_t2809994845 * ___itemsContent_19;

public:
	inline static int32_t get_offset_of_LegasyInfo_3() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___LegasyInfo_3)); }
	inline GameObject_t1756533147 * get_LegasyInfo_3() const { return ___LegasyInfo_3; }
	inline GameObject_t1756533147 ** get_address_of_LegasyInfo_3() { return &___LegasyInfo_3; }
	inline void set_LegasyInfo_3(GameObject_t1756533147 * value)
	{
		___LegasyInfo_3 = value;
		Il2CppCodeGenWriteBarrier(&___LegasyInfo_3, value);
	}

	inline static int32_t get_offset_of_levelPanel_4() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___levelPanel_4)); }
	inline GameObject_t1756533147 * get_levelPanel_4() const { return ___levelPanel_4; }
	inline GameObject_t1756533147 ** get_address_of_levelPanel_4() { return &___levelPanel_4; }
	inline void set_levelPanel_4(GameObject_t1756533147 * value)
	{
		___levelPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___levelPanel_4, value);
	}

	inline static int32_t get_offset_of_play_btn_5() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___play_btn_5)); }
	inline Button_t2872111280 * get_play_btn_5() const { return ___play_btn_5; }
	inline Button_t2872111280 ** get_address_of_play_btn_5() { return &___play_btn_5; }
	inline void set_play_btn_5(Button_t2872111280 * value)
	{
		___play_btn_5 = value;
		Il2CppCodeGenWriteBarrier(&___play_btn_5, value);
	}

	inline static int32_t get_offset_of_store_btn_6() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___store_btn_6)); }
	inline Button_t2872111280 * get_store_btn_6() const { return ___store_btn_6; }
	inline Button_t2872111280 ** get_address_of_store_btn_6() { return &___store_btn_6; }
	inline void set_store_btn_6(Button_t2872111280 * value)
	{
		___store_btn_6 = value;
		Il2CppCodeGenWriteBarrier(&___store_btn_6, value);
	}

	inline static int32_t get_offset_of_stats_btn_7() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___stats_btn_7)); }
	inline Button_t2872111280 * get_stats_btn_7() const { return ___stats_btn_7; }
	inline Button_t2872111280 ** get_address_of_stats_btn_7() { return &___stats_btn_7; }
	inline void set_stats_btn_7(Button_t2872111280 * value)
	{
		___stats_btn_7 = value;
		Il2CppCodeGenWriteBarrier(&___stats_btn_7, value);
	}

	inline static int32_t get_offset_of_back_1_8() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___back_1_8)); }
	inline Button_t2872111280 * get_back_1_8() const { return ___back_1_8; }
	inline Button_t2872111280 ** get_address_of_back_1_8() { return &___back_1_8; }
	inline void set_back_1_8(Button_t2872111280 * value)
	{
		___back_1_8 = value;
		Il2CppCodeGenWriteBarrier(&___back_1_8, value);
	}

	inline static int32_t get_offset_of_back_2_9() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___back_2_9)); }
	inline Button_t2872111280 * get_back_2_9() const { return ___back_2_9; }
	inline Button_t2872111280 ** get_address_of_back_2_9() { return &___back_2_9; }
	inline void set_back_2_9(Button_t2872111280 * value)
	{
		___back_2_9 = value;
		Il2CppCodeGenWriteBarrier(&___back_2_9, value);
	}

	inline static int32_t get_offset_of_back_3_10() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___back_3_10)); }
	inline Button_t2872111280 * get_back_3_10() const { return ___back_3_10; }
	inline Button_t2872111280 ** get_address_of_back_3_10() { return &___back_3_10; }
	inline void set_back_3_10(Button_t2872111280 * value)
	{
		___back_3_10 = value;
		Il2CppCodeGenWriteBarrier(&___back_3_10, value);
	}

	inline static int32_t get_offset_of_back_4_11() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___back_4_11)); }
	inline Button_t2872111280 * get_back_4_11() const { return ___back_4_11; }
	inline Button_t2872111280 ** get_address_of_back_4_11() { return &___back_4_11; }
	inline void set_back_4_11(Button_t2872111280 * value)
	{
		___back_4_11 = value;
		Il2CppCodeGenWriteBarrier(&___back_4_11, value);
	}

	inline static int32_t get_offset_of_titleGame_12() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___titleGame_12)); }
	inline Text_t356221433 * get_titleGame_12() const { return ___titleGame_12; }
	inline Text_t356221433 ** get_address_of_titleGame_12() { return &___titleGame_12; }
	inline void set_titleGame_12(Text_t356221433 * value)
	{
		___titleGame_12 = value;
		Il2CppCodeGenWriteBarrier(&___titleGame_12, value);
	}

	inline static int32_t get_offset_of_gameDescription_13() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___gameDescription_13)); }
	inline Text_t356221433 * get_gameDescription_13() const { return ___gameDescription_13; }
	inline Text_t356221433 ** get_address_of_gameDescription_13() { return &___gameDescription_13; }
	inline void set_gameDescription_13(Text_t356221433 * value)
	{
		___gameDescription_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameDescription_13, value);
	}

	inline static int32_t get_offset_of_valueDescription_14() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___valueDescription_14)); }
	inline String_t* get_valueDescription_14() const { return ___valueDescription_14; }
	inline String_t** get_address_of_valueDescription_14() { return &___valueDescription_14; }
	inline void set_valueDescription_14(String_t* value)
	{
		___valueDescription_14 = value;
		Il2CppCodeGenWriteBarrier(&___valueDescription_14, value);
	}

	inline static int32_t get_offset_of_valueTitle_15() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___valueTitle_15)); }
	inline String_t* get_valueTitle_15() const { return ___valueTitle_15; }
	inline String_t** get_address_of_valueTitle_15() { return &___valueTitle_15; }
	inline void set_valueTitle_15(String_t* value)
	{
		___valueTitle_15 = value;
		Il2CppCodeGenWriteBarrier(&___valueTitle_15, value);
	}

	inline static int32_t get_offset_of_isSubscribed_16() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___isSubscribed_16)); }
	inline String_t* get_isSubscribed_16() const { return ___isSubscribed_16; }
	inline String_t** get_address_of_isSubscribed_16() { return &___isSubscribed_16; }
	inline void set_isSubscribed_16(String_t* value)
	{
		___isSubscribed_16 = value;
		Il2CppCodeGenWriteBarrier(&___isSubscribed_16, value);
	}

	inline static int32_t get_offset_of_soundController_18() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___soundController_18)); }
	inline SoundController_t1686593041 * get_soundController_18() const { return ___soundController_18; }
	inline SoundController_t1686593041 ** get_address_of_soundController_18() { return &___soundController_18; }
	inline void set_soundController_18(SoundController_t1686593041 * value)
	{
		___soundController_18 = value;
		Il2CppCodeGenWriteBarrier(&___soundController_18, value);
	}

	inline static int32_t get_offset_of_itemsContent_19() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___itemsContent_19)); }
	inline Main_t2809994845 * get_itemsContent_19() const { return ___itemsContent_19; }
	inline Main_t2809994845 ** get_address_of_itemsContent_19() { return &___itemsContent_19; }
	inline void set_itemsContent_19(Main_t2809994845 * value)
	{
		___itemsContent_19 = value;
		Il2CppCodeGenWriteBarrier(&___itemsContent_19, value);
	}
};

struct MainGame_t3800664731_StaticFields
{
public:
	// Question[] MainGame::questions
	QuestionU5BU5D_t1028533817* ___questions_2;
	// System.Boolean MainGame::verfiSubscribe
	bool ___verfiSubscribe_17;

public:
	inline static int32_t get_offset_of_questions_2() { return static_cast<int32_t>(offsetof(MainGame_t3800664731_StaticFields, ___questions_2)); }
	inline QuestionU5BU5D_t1028533817* get_questions_2() const { return ___questions_2; }
	inline QuestionU5BU5D_t1028533817** get_address_of_questions_2() { return &___questions_2; }
	inline void set_questions_2(QuestionU5BU5D_t1028533817* value)
	{
		___questions_2 = value;
		Il2CppCodeGenWriteBarrier(&___questions_2, value);
	}

	inline static int32_t get_offset_of_verfiSubscribe_17() { return static_cast<int32_t>(offsetof(MainGame_t3800664731_StaticFields, ___verfiSubscribe_17)); }
	inline bool get_verfiSubscribe_17() const { return ___verfiSubscribe_17; }
	inline bool* get_address_of_verfiSubscribe_17() { return &___verfiSubscribe_17; }
	inline void set_verfiSubscribe_17(bool value)
	{
		___verfiSubscribe_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
