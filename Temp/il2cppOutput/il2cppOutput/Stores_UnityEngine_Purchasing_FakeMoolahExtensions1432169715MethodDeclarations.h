﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.FakeMoolahExtensions
struct FakeMoolahExtensions_t1432169715;
// System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>
struct Action_1_t2289103034;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.FakeMoolahExtensions::RestoreTransactionID(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>)
extern "C"  void FakeMoolahExtensions_RestoreTransactionID_m3634146525 (FakeMoolahExtensions_t1432169715 * __this, Action_1_t2289103034 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.FakeMoolahExtensions::.ctor()
extern "C"  void FakeMoolahExtensions__ctor_m813897259 (FakeMoolahExtensions_t1432169715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
