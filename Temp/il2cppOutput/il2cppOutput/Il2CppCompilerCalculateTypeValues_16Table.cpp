﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityStore_UnityEngine_Store_LoginForwardCallback1105422505.h"
#include "UnityStore_UnityEngine_Store_UserInfo741955747.h"
#include "Apple_U3CModuleU3E3783534214.h"
#include "Apple_UnityEngine_Purchasing_iOSStoreBindings2633471826.h"
#include "Apple_UnityEngine_Purchasing_OSXStoreBindings116576999.h"
#include "ChannelPurchase_U3CModuleU3E3783534214.h"
#include "ChannelPurchase_UnityEngine_ChannelPurchase_Purchas659182236.h"
#include "ChannelPurchase_UnityEngine_ChannelPurchase_Purchas144617755.h"
#include "FacebookStore_U3CModuleU3E3783534214.h"
#include "FacebookStore_UnityEngine_Purchasing_FacebookStore1025727499.h"
#include "Security_U3CModuleU3E3783534214.h"
#include "Security_UnityEngine_Purchasing_Security_Distingui1881593989.h"
#include "Security_UnityEngine_Purchasing_Security_X509Cert481809278.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidX51630759105.h"
#include "Security_UnityEngine_Purchasing_Security_PKCS71974940522.h"
#include "Security_UnityEngine_Purchasing_Security_SignerInf4122348804.h"
#include "Security_UnityEngine_Purchasing_Security_IAPSecuri3038093501.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidSig488933488.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidPK4123278833.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidTi3933748955.h"
#include "Security_UnityEngine_Purchasing_Security_Unsupport2780725255.h"
#include "Security_UnityEngine_Purchasing_Security_RSAKey446464277.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidRS1674954323.h"
#include "Security_UnityEngine_Purchasing_Security_AppleVali3837389912.h"
#include "Security_UnityEngine_Purchasing_Security_AppleRecei732159403.h"
#include "Security_UnityEngine_Purchasing_Security_AppleRece3991411794.h"
#include "Security_UnityEngine_Purchasing_Security_AppleInAp3271698749.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato2878230988.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscator330213567.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Parser914015216.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Util2059476207.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Oid113668572.h"
#include "Security_LipingShare_LCLib_Asn1Processor_RelativeOi880150712.h"
#include "Security_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Security_U3CPrivateImplementationDetailsU3E___Stat2544559954.h"
#include "Tizen_U3CModuleU3E3783534214.h"
#include "Tizen_UnityEngine_Purchasing_UnityNativePurchasing3230812225.h"
#include "Tizen_UnityEngine_Purchasing_TizenStoreBindings1951392817.h"
#include "UnityEngine_Purchasing_U3CModuleU3E3783534214.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Anal3513180421.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Async423752048.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Asyn2099263868.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Asyn3541402849.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Clou3988464631.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Clou2402319400.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_IDs3808979560.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod2754455291.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte1607114611.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc3525211160.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch728606867.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Stor2597962341.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Transa45391254.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unit4076614841.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unit3301441281.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unity768337690.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unit1673686536.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte4102635892.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte2787096497.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte3318267523.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (LoginForwardCallback_t1105422505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1600[1] = 
{
	LoginForwardCallback_t1105422505::get_offset_of_loginListener_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (UserInfo_t741955747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[3] = 
{
	UserInfo_t741955747::get_offset_of_U3CchannelU3Ek__BackingField_0(),
	UserInfo_t741955747::get_offset_of_U3CuserIdU3Ek__BackingField_1(),
	UserInfo_t741955747::get_offset_of_U3CuserLoginTokenU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (iOSStoreBindings_t2633471826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (OSXStoreBindings_t116576999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (PurchaseService_t659182236), -1, sizeof(PurchaseService_t659182236_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1607[1] = 
{
	PurchaseService_t659182236_StaticFields::get_offset_of_serviceClass_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (PurchaseForwardCallback_t144617755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1609[1] = 
{
	PurchaseForwardCallback_t144617755::get_offset_of_purchaseListener_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (FacebookStoreBindings_t1025727499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (DistinguishedName_t1881593989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1614[7] = 
{
	DistinguishedName_t1881593989::get_offset_of_U3CCountryU3Ek__BackingField_0(),
	DistinguishedName_t1881593989::get_offset_of_U3COrganizationU3Ek__BackingField_1(),
	DistinguishedName_t1881593989::get_offset_of_U3COrganizationalUnitU3Ek__BackingField_2(),
	DistinguishedName_t1881593989::get_offset_of_U3CDnqU3Ek__BackingField_3(),
	DistinguishedName_t1881593989::get_offset_of_U3CStateU3Ek__BackingField_4(),
	DistinguishedName_t1881593989::get_offset_of_U3CCommonNameU3Ek__BackingField_5(),
	DistinguishedName_t1881593989::get_offset_of_U3CSerialNumberU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (X509Cert_t481809278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1615[10] = 
{
	X509Cert_t481809278::get_offset_of_U3CSerialNumberU3Ek__BackingField_0(),
	X509Cert_t481809278::get_offset_of_U3CValidAfterU3Ek__BackingField_1(),
	X509Cert_t481809278::get_offset_of_U3CValidBeforeU3Ek__BackingField_2(),
	X509Cert_t481809278::get_offset_of_U3CPubKeyU3Ek__BackingField_3(),
	X509Cert_t481809278::get_offset_of_U3CSelfSignedU3Ek__BackingField_4(),
	X509Cert_t481809278::get_offset_of_U3CSubjectU3Ek__BackingField_5(),
	X509Cert_t481809278::get_offset_of_U3CIssuerU3Ek__BackingField_6(),
	X509Cert_t481809278::get_offset_of_TbsCertificate_7(),
	X509Cert_t481809278::get_offset_of_U3CSignatureU3Ek__BackingField_8(),
	X509Cert_t481809278::get_offset_of_rawTBSCertificate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (InvalidX509Data_t1630759105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (PKCS7_t1974940522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1617[5] = 
{
	PKCS7_t1974940522::get_offset_of_root_0(),
	PKCS7_t1974940522::get_offset_of_U3CdataU3Ek__BackingField_1(),
	PKCS7_t1974940522::get_offset_of_U3CsinfosU3Ek__BackingField_2(),
	PKCS7_t1974940522::get_offset_of_U3CcertChainU3Ek__BackingField_3(),
	PKCS7_t1974940522::get_offset_of_validStructure_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (SignerInfo_t4122348804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1618[3] = 
{
	SignerInfo_t4122348804::get_offset_of_U3CVersionU3Ek__BackingField_0(),
	SignerInfo_t4122348804::get_offset_of_U3CIssuerSerialNumberU3Ek__BackingField_1(),
	SignerInfo_t4122348804::get_offset_of_U3CEncryptedDigestU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (IAPSecurityException_t3038093501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (InvalidSignatureException_t488933488), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (InvalidPKCS7Data_t4123278833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (InvalidTimeFormat_t3933748955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (UnsupportedSignerInfoVersion_t2780725255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (RSAKey_t446464277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1624[1] = 
{
	RSAKey_t446464277::get_offset_of_U3CrsaU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (InvalidRSAData_t1674954323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (AppleValidator_t3837389912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1626[2] = 
{
	AppleValidator_t3837389912::get_offset_of_cert_0(),
	AppleValidator_t3837389912::get_offset_of_parser_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (AppleReceiptParser_t732159403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (AppleReceipt_t3991411794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[7] = 
{
	AppleReceipt_t3991411794::get_offset_of_U3CbundleIDU3Ek__BackingField_0(),
	AppleReceipt_t3991411794::get_offset_of_U3CappVersionU3Ek__BackingField_1(),
	AppleReceipt_t3991411794::get_offset_of_U3CopaqueU3Ek__BackingField_2(),
	AppleReceipt_t3991411794::get_offset_of_U3ChashU3Ek__BackingField_3(),
	AppleReceipt_t3991411794::get_offset_of_U3CoriginalApplicationVersionU3Ek__BackingField_4(),
	AppleReceipt_t3991411794::get_offset_of_U3CreceiptCreationDateU3Ek__BackingField_5(),
	AppleReceipt_t3991411794::get_offset_of_inAppPurchaseReceipts_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (AppleInAppPurchaseReceipt_t3271698749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[8] = 
{
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CquantityU3Ek__BackingField_0(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CproductIDU3Ek__BackingField_1(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CtransactionIDU3Ek__BackingField_2(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CoriginalTransactionIdentifierU3Ek__BackingField_3(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CpurchaseDateU3Ek__BackingField_4(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CoriginalPurchaseDateU3Ek__BackingField_5(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CsubscriptionExpirationDateU3Ek__BackingField_6(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CcancellationDateU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (Obfuscator_t2878230988), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (U3CU3Ec__DisplayClass1_0_t330213567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1631[1] = 
{
	U3CU3Ec__DisplayClass1_0_t330213567::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (Asn1Node_t1770761751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1634[13] = 
{
	Asn1Node_t1770761751::get_offset_of_tag_0(),
	Asn1Node_t1770761751::get_offset_of_dataOffset_1(),
	Asn1Node_t1770761751::get_offset_of_dataLength_2(),
	Asn1Node_t1770761751::get_offset_of_lengthFieldBytes_3(),
	Asn1Node_t1770761751::get_offset_of_data_4(),
	Asn1Node_t1770761751::get_offset_of_childNodeList_5(),
	Asn1Node_t1770761751::get_offset_of_unusedBits_6(),
	Asn1Node_t1770761751::get_offset_of_deepness_7(),
	Asn1Node_t1770761751::get_offset_of_path_8(),
	Asn1Node_t1770761751::get_offset_of_parentNode_9(),
	Asn1Node_t1770761751::get_offset_of_requireRecalculatePar_10(),
	Asn1Node_t1770761751::get_offset_of_isIndefiniteLength_11(),
	Asn1Node_t1770761751::get_offset_of_parseEncapsulatedData_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (Asn1Parser_t914015216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1635[2] = 
{
	Asn1Parser_t914015216::get_offset_of_rawData_0(),
	Asn1Parser_t914015216::get_offset_of_rootNode_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (Asn1Util_t2059476207), -1, sizeof(Asn1Util_t2059476207_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1636[1] = 
{
	Asn1Util_t2059476207_StaticFields::get_offset_of_hexDigits_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (Oid_t113668572), -1, sizeof(Oid_t113668572_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1637[1] = 
{
	Oid_t113668572_StaticFields::get_offset_of_oidDictionary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (RelativeOid_t880150712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1639[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (__StaticArrayInitTypeSizeU3D32_t2544559954)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D32_t2544559954 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (UnityNativePurchasingCallback_t3230812225), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (TizenStoreBindings_t1951392817), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (AnalyticsReporter_t3513180421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1646[1] = 
{
	AnalyticsReporter_t3513180421::get_offset_of_m_Analytics_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (AsyncUtil_t423752048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (U3CDoInvokeU3Ec__Iterator0_t2099263868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1648[5] = 
{
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_delayInSeconds_0(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_a_1(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U24current_2(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U24disposing_3(),
	U3CDoInvokeU3Ec__Iterator0_t2099263868::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (U3CProcessU3Ec__Iterator1_t3541402849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1649[6] = 
{
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_request_0(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_errorHandler_1(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_responseHandler_2(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U24current_3(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U24disposing_4(),
	U3CProcessU3Ec__Iterator1_t3541402849::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (CloudCatalogManager_t3988464631), -1, sizeof(CloudCatalogManager_t3988464631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1650[7] = 
{
	CloudCatalogManager_t3988464631::get_offset_of_m_AsyncUtil_0(),
	CloudCatalogManager_t3988464631::get_offset_of_m_CacheFileName_1(),
	CloudCatalogManager_t3988464631::get_offset_of_m_Logger_2(),
	CloudCatalogManager_t3988464631::get_offset_of_m_CatalogURL_3(),
	CloudCatalogManager_t3988464631::get_offset_of_m_StoreName_4(),
	CloudCatalogManager_t3988464631_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	CloudCatalogManager_t3988464631_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (U3CFetchProductsU3Ec__AnonStorey0_t2402319400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1651[3] = 
{
	U3CFetchProductsU3Ec__AnonStorey0_t2402319400::get_offset_of_callback_0(),
	U3CFetchProductsU3Ec__AnonStorey0_t2402319400::get_offset_of_delayInSeconds_1(),
	U3CFetchProductsU3Ec__AnonStorey0_t2402319400::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (IDs_t3808979560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1652[1] = 
{
	IDs_t3808979560::get_offset_of_m_Dic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (ConfigurationBuilder_t1298400415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[3] = 
{
	ConfigurationBuilder_t1298400415::get_offset_of_m_Factory_0(),
	ConfigurationBuilder_t1298400415::get_offset_of_m_Products_1(),
	ConfigurationBuilder_t1298400415::get_offset_of_U3CuseCloudCatalogU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (InitializationFailureReason_t2954032642)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1657[4] = 
{
	InitializationFailureReason_t2954032642::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (Product_t1203687971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1661[5] = 
{
	Product_t1203687971::get_offset_of_U3CdefinitionU3Ek__BackingField_0(),
	Product_t1203687971::get_offset_of_U3CmetadataU3Ek__BackingField_1(),
	Product_t1203687971::get_offset_of_U3CavailableToPurchaseU3Ek__BackingField_2(),
	Product_t1203687971::get_offset_of_U3CtransactionIDU3Ek__BackingField_3(),
	Product_t1203687971::get_offset_of_U3CreceiptU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (ProductCollection_t3600019299), -1, sizeof(ProductCollection_t3600019299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1662[6] = 
{
	ProductCollection_t3600019299::get_offset_of_m_IdToProduct_0(),
	ProductCollection_t3600019299::get_offset_of_m_StoreSpecificIdToProduct_1(),
	ProductCollection_t3600019299::get_offset_of_m_Products_2(),
	ProductCollection_t3600019299::get_offset_of_m_ProductSet_3(),
	ProductCollection_t3600019299_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ProductCollection_t3600019299_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (ProductDefinition_t1942475268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1663[3] = 
{
	ProductDefinition_t1942475268::get_offset_of_U3CidU3Ek__BackingField_0(),
	ProductDefinition_t1942475268::get_offset_of_U3CstoreSpecificIdU3Ek__BackingField_1(),
	ProductDefinition_t1942475268::get_offset_of_U3CtypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (ProductMetadata_t1573242544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[5] = 
{
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedPriceStringU3Ek__BackingField_0(),
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedTitleU3Ek__BackingField_1(),
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedDescriptionU3Ek__BackingField_2(),
	ProductMetadata_t1573242544::get_offset_of_U3CisoCurrencyCodeU3Ek__BackingField_3(),
	ProductMetadata_t1573242544::get_offset_of_U3ClocalizedPriceU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (ProductType_t2754455291)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1665[4] = 
{
	ProductType_t2754455291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (PurchaseEventArgs_t547992434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1666[1] = 
{
	PurchaseEventArgs_t547992434::get_offset_of_U3CpurchasedProductU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (PurchaseFailureDescription_t1607114611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[3] = 
{
	PurchaseFailureDescription_t1607114611::get_offset_of_U3CproductIdU3Ek__BackingField_0(),
	PurchaseFailureDescription_t1607114611::get_offset_of_U3CreasonU3Ek__BackingField_1(),
	PurchaseFailureDescription_t1607114611::get_offset_of_U3CmessageU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (PurchaseFailureReason_t1322959839)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1668[8] = 
{
	PurchaseFailureReason_t1322959839::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (PurchaseProcessingResult_t2407199463)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1669[3] = 
{
	PurchaseProcessingResult_t2407199463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (PurchasingFactory_t3525211160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1670[4] = 
{
	PurchasingFactory_t3525211160::get_offset_of_m_ConfigMap_0(),
	PurchasingFactory_t3525211160::get_offset_of_m_ExtensionMap_1(),
	PurchasingFactory_t3525211160::get_offset_of_m_Store_2(),
	PurchasingFactory_t3525211160::get_offset_of_U3CstoreNameU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (PurchasingManager_t728606867), -1, sizeof(PurchasingManager_t728606867_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1671[11] = 
{
	PurchasingManager_t728606867::get_offset_of_m_Store_0(),
	PurchasingManager_t728606867::get_offset_of_m_Listener_1(),
	PurchasingManager_t728606867::get_offset_of_m_Logger_2(),
	PurchasingManager_t728606867::get_offset_of_m_TransactionLog_3(),
	PurchasingManager_t728606867::get_offset_of_m_StoreName_4(),
	PurchasingManager_t728606867::get_offset_of_m_AdditionalProductsCallback_5(),
	PurchasingManager_t728606867::get_offset_of_m_AdditionalProductsFailCallback_6(),
	PurchasingManager_t728606867::get_offset_of_U3CuseTransactionLogU3Ek__BackingField_7(),
	PurchasingManager_t728606867::get_offset_of_U3CproductsU3Ek__BackingField_8(),
	PurchasingManager_t728606867::get_offset_of_initialized_9(),
	PurchasingManager_t728606867_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (StoreListenerProxy_t2597962341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1672[3] = 
{
	StoreListenerProxy_t2597962341::get_offset_of_m_Analytics_0(),
	StoreListenerProxy_t2597962341::get_offset_of_m_ForwardTo_1(),
	StoreListenerProxy_t2597962341::get_offset_of_m_Extensions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (TransactionLog_t45391254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[2] = 
{
	TransactionLog_t45391254::get_offset_of_logger_0(),
	TransactionLog_t45391254::get_offset_of_persistentDataPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (UnityAnalytics_t4076614841), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (UnityPurchasing_t3301441281), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (U3CInitializeU3Ec__AnonStorey0_t768337690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1676[2] = 
{
	U3CInitializeU3Ec__AnonStorey0_t768337690::get_offset_of_manager_0(),
	U3CInitializeU3Ec__AnonStorey0_t768337690::get_offset_of_proxy_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (U3CFetchAndMergeProductsU3Ec__AnonStorey1_t1673686536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[2] = 
{
	U3CFetchAndMergeProductsU3Ec__AnonStorey1_t1673686536::get_offset_of_applicationProducts_0(),
	U3CFetchAndMergeProductsU3Ec__AnonStorey1_t1673686536::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (AbstractPurchasingModule_t4102635892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1678[1] = 
{
	AbstractPurchasingModule_t4102635892::get_offset_of_m_Binder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (AbstractStore_t2787096497), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (ProductDescription_t3318267523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1686[5] = 
{
	ProductDescription_t3318267523::get_offset_of_U3CstoreSpecificIdU3Ek__BackingField_0(),
	ProductDescription_t3318267523::get_offset_of_type_1(),
	ProductDescription_t3318267523::get_offset_of_U3CmetadataU3Ek__BackingField_2(),
	ProductDescription_t3318267523::get_offset_of_U3CreceiptU3Ek__BackingField_3(),
	ProductDescription_t3318267523::get_offset_of_U3CtransactionIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (U3CModuleU3E_t3783534227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1688[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
