﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.String
struct String_t;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t3256166369;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// UnityEngine.Purchasing.MoolahStoreImpl
struct MoolahStoreImpl_t4206626141;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22
struct  U3CRequestAuthCodeU3Ed__22_t1220668507  : public Il2CppObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<>2__current
	Il2CppObject * ___U3CU3E2__current_1;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::wf
	WWWForm_t3950226929 * ___wf_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::productID
	String_t* ___productID_3;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::transactionId
	String_t* ___transactionId_4;
	// System.Action`3<System.String,System.String,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::succeed
	Action_3_t3256166369 * ___succeed_5;
	// System.Action`2<System.String,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::failed
	Action_2_t4234541925 * ___failed_6;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<>4__this
	MoolahStoreImpl_t4206626141 * ___U3CU3E4__this_7;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<w>5__1
	WWW_t2919945039 * ___U3CwU3E5__1_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<authCodeResult>5__2
	Dictionary_2_t309261261 * ___U3CauthCodeResultU3E5__2_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<authCodeValues>5__3
	Dictionary_2_t309261261 * ___U3CauthCodeValuesU3E5__3_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<authCode>5__4
	String_t* ___U3CauthCodeU3E5__4_11;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<paymentURL>5__5
	String_t* ___U3CpaymentURLU3E5__5_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___U3CU3E2__current_1)); }
	inline Il2CppObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Il2CppObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Il2CppObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_1, value);
	}

	inline static int32_t get_offset_of_wf_2() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___wf_2)); }
	inline WWWForm_t3950226929 * get_wf_2() const { return ___wf_2; }
	inline WWWForm_t3950226929 ** get_address_of_wf_2() { return &___wf_2; }
	inline void set_wf_2(WWWForm_t3950226929 * value)
	{
		___wf_2 = value;
		Il2CppCodeGenWriteBarrier(&___wf_2, value);
	}

	inline static int32_t get_offset_of_productID_3() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___productID_3)); }
	inline String_t* get_productID_3() const { return ___productID_3; }
	inline String_t** get_address_of_productID_3() { return &___productID_3; }
	inline void set_productID_3(String_t* value)
	{
		___productID_3 = value;
		Il2CppCodeGenWriteBarrier(&___productID_3, value);
	}

	inline static int32_t get_offset_of_transactionId_4() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___transactionId_4)); }
	inline String_t* get_transactionId_4() const { return ___transactionId_4; }
	inline String_t** get_address_of_transactionId_4() { return &___transactionId_4; }
	inline void set_transactionId_4(String_t* value)
	{
		___transactionId_4 = value;
		Il2CppCodeGenWriteBarrier(&___transactionId_4, value);
	}

	inline static int32_t get_offset_of_succeed_5() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___succeed_5)); }
	inline Action_3_t3256166369 * get_succeed_5() const { return ___succeed_5; }
	inline Action_3_t3256166369 ** get_address_of_succeed_5() { return &___succeed_5; }
	inline void set_succeed_5(Action_3_t3256166369 * value)
	{
		___succeed_5 = value;
		Il2CppCodeGenWriteBarrier(&___succeed_5, value);
	}

	inline static int32_t get_offset_of_failed_6() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___failed_6)); }
	inline Action_2_t4234541925 * get_failed_6() const { return ___failed_6; }
	inline Action_2_t4234541925 ** get_address_of_failed_6() { return &___failed_6; }
	inline void set_failed_6(Action_2_t4234541925 * value)
	{
		___failed_6 = value;
		Il2CppCodeGenWriteBarrier(&___failed_6, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___U3CU3E4__this_7)); }
	inline MoolahStoreImpl_t4206626141 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline MoolahStoreImpl_t4206626141 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(MoolahStoreImpl_t4206626141 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_7, value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__1_8() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___U3CwU3E5__1_8)); }
	inline WWW_t2919945039 * get_U3CwU3E5__1_8() const { return ___U3CwU3E5__1_8; }
	inline WWW_t2919945039 ** get_address_of_U3CwU3E5__1_8() { return &___U3CwU3E5__1_8; }
	inline void set_U3CwU3E5__1_8(WWW_t2919945039 * value)
	{
		___U3CwU3E5__1_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwU3E5__1_8, value);
	}

	inline static int32_t get_offset_of_U3CauthCodeResultU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___U3CauthCodeResultU3E5__2_9)); }
	inline Dictionary_2_t309261261 * get_U3CauthCodeResultU3E5__2_9() const { return ___U3CauthCodeResultU3E5__2_9; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CauthCodeResultU3E5__2_9() { return &___U3CauthCodeResultU3E5__2_9; }
	inline void set_U3CauthCodeResultU3E5__2_9(Dictionary_2_t309261261 * value)
	{
		___U3CauthCodeResultU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CauthCodeResultU3E5__2_9, value);
	}

	inline static int32_t get_offset_of_U3CauthCodeValuesU3E5__3_10() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___U3CauthCodeValuesU3E5__3_10)); }
	inline Dictionary_2_t309261261 * get_U3CauthCodeValuesU3E5__3_10() const { return ___U3CauthCodeValuesU3E5__3_10; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CauthCodeValuesU3E5__3_10() { return &___U3CauthCodeValuesU3E5__3_10; }
	inline void set_U3CauthCodeValuesU3E5__3_10(Dictionary_2_t309261261 * value)
	{
		___U3CauthCodeValuesU3E5__3_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CauthCodeValuesU3E5__3_10, value);
	}

	inline static int32_t get_offset_of_U3CauthCodeU3E5__4_11() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___U3CauthCodeU3E5__4_11)); }
	inline String_t* get_U3CauthCodeU3E5__4_11() const { return ___U3CauthCodeU3E5__4_11; }
	inline String_t** get_address_of_U3CauthCodeU3E5__4_11() { return &___U3CauthCodeU3E5__4_11; }
	inline void set_U3CauthCodeU3E5__4_11(String_t* value)
	{
		___U3CauthCodeU3E5__4_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CauthCodeU3E5__4_11, value);
	}

	inline static int32_t get_offset_of_U3CpaymentURLU3E5__5_12() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t1220668507, ___U3CpaymentURLU3E5__5_12)); }
	inline String_t* get_U3CpaymentURLU3E5__5_12() const { return ___U3CpaymentURLU3E5__5_12; }
	inline String_t** get_address_of_U3CpaymentURLU3E5__5_12() { return &___U3CpaymentURLU3E5__5_12; }
	inline void set_U3CpaymentURLU3E5__5_12(String_t* value)
	{
		___U3CpaymentURLU3E5__5_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpaymentURLU3E5__5_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
