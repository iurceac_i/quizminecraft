﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/UnityChannelPurchaseInfo
struct  UnityChannelPurchaseInfo_t855507751  : public Il2CppObject
{
public:
	// System.String IAPDemo/UnityChannelPurchaseInfo::productCode
	String_t* ___productCode_0;
	// System.String IAPDemo/UnityChannelPurchaseInfo::gameOrderId
	String_t* ___gameOrderId_1;
	// System.String IAPDemo/UnityChannelPurchaseInfo::orderQueryToken
	String_t* ___orderQueryToken_2;

public:
	inline static int32_t get_offset_of_productCode_0() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t855507751, ___productCode_0)); }
	inline String_t* get_productCode_0() const { return ___productCode_0; }
	inline String_t** get_address_of_productCode_0() { return &___productCode_0; }
	inline void set_productCode_0(String_t* value)
	{
		___productCode_0 = value;
		Il2CppCodeGenWriteBarrier(&___productCode_0, value);
	}

	inline static int32_t get_offset_of_gameOrderId_1() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t855507751, ___gameOrderId_1)); }
	inline String_t* get_gameOrderId_1() const { return ___gameOrderId_1; }
	inline String_t** get_address_of_gameOrderId_1() { return &___gameOrderId_1; }
	inline void set_gameOrderId_1(String_t* value)
	{
		___gameOrderId_1 = value;
		Il2CppCodeGenWriteBarrier(&___gameOrderId_1, value);
	}

	inline static int32_t get_offset_of_orderQueryToken_2() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t855507751, ___orderQueryToken_2)); }
	inline String_t* get_orderQueryToken_2() const { return ___orderQueryToken_2; }
	inline String_t** get_address_of_orderQueryToken_2() { return &___orderQueryToken_2; }
	inline void set_orderQueryToken_2(String_t* value)
	{
		___orderQueryToken_2 = value;
		Il2CppCodeGenWriteBarrier(&___orderQueryToken_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
