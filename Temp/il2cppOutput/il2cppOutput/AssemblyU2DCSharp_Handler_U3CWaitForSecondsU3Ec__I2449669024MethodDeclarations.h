﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Handler/<WaitForSeconds>c__Iterator1
struct U3CWaitForSecondsU3Ec__Iterator1_t2449669024;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Handler/<WaitForSeconds>c__Iterator1::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator1__ctor_m2602417577 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Handler/<WaitForSeconds>c__Iterator1::MoveNext()
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator1_MoveNext_m882073451 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Handler/<WaitForSeconds>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m762440515 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Handler/<WaitForSeconds>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2115095867 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/<WaitForSeconds>c__Iterator1::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator1_Dispose_m3768844650 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler/<WaitForSeconds>c__Iterator1::Reset()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator1_Reset_m1526271408 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
