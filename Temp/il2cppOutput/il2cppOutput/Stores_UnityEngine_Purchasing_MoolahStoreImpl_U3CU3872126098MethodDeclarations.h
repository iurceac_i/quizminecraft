﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.MoolahStoreImpl/<>c__DisplayClass18_0
struct U3CU3Ec__DisplayClass18_0_t3872126098;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<>c__DisplayClass18_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass18_0__ctor_m2562851263 (U3CU3Ec__DisplayClass18_0_t3872126098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<>c__DisplayClass18_0::<Purchase>b__2(System.String,System.String,System.String)
extern "C"  void U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__2_m2888448844 (U3CU3Ec__DisplayClass18_0_t3872126098 * __this, String_t* ___transactionId0, String_t* ___authGlobal1, String_t* ___paymentUrl2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
