﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenOrientation
struct ScreenOrientation_t2584746402;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenOrientation::.ctor()
extern "C"  void ScreenOrientation__ctor_m1408293085 (ScreenOrientation_t2584746402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenOrientation::Start()
extern "C"  void ScreenOrientation_Start_m3374179005 (ScreenOrientation_t2584746402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenOrientation::Update()
extern "C"  void ScreenOrientation_Update_m3072168224 (ScreenOrientation_t2584746402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
