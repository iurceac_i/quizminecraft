﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.WinRTStore/<>c
struct U3CU3Ec_t4236842726;
// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t1942475268;
// UnityEngine.Purchasing.Default.WinProductDescription
struct WinProductDescription_t1075111405;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"

// System.Void UnityEngine.Purchasing.WinRTStore/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m2748256470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.WinRTStore/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m161161559 (U3CU3Ec_t4236842726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.WinRTStore/<>c::<RetrieveProducts>b__8_0(UnityEngine.Purchasing.ProductDefinition)
extern "C"  bool U3CU3Ec_U3CRetrieveProductsU3Eb__8_0_m3748846711 (U3CU3Ec_t4236842726 * __this, ProductDefinition_t1942475268 * ___def0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Default.WinProductDescription UnityEngine.Purchasing.WinRTStore/<>c::<RetrieveProducts>b__8_1(UnityEngine.Purchasing.ProductDefinition)
extern "C"  WinProductDescription_t1075111405 * U3CU3Ec_U3CRetrieveProductsU3Eb__8_1_m823723958 (U3CU3Ec_t4236842726 * __this, ProductDefinition_t1942475268 * ___def0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
