﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Handler/ExecuteWithGameObjcet
struct ExecuteWithGameObjcet_t1191175011;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Handler/<WaitForSeconds>c__Iterator2
struct  U3CWaitForSecondsU3Ec__Iterator2_t2852953551  : public Il2CppObject
{
public:
	// System.Single Handler/<WaitForSeconds>c__Iterator2::time
	float ___time_0;
	// Handler/ExecuteWithGameObjcet Handler/<WaitForSeconds>c__Iterator2::method
	ExecuteWithGameObjcet_t1191175011 * ___method_1;
	// UnityEngine.GameObject Handler/<WaitForSeconds>c__Iterator2::GO
	GameObject_t1756533147 * ___GO_2;
	// System.Object Handler/<WaitForSeconds>c__Iterator2::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean Handler/<WaitForSeconds>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 Handler/<WaitForSeconds>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator2_t2852953551, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator2_t2852953551, ___method_1)); }
	inline ExecuteWithGameObjcet_t1191175011 * get_method_1() const { return ___method_1; }
	inline ExecuteWithGameObjcet_t1191175011 ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(ExecuteWithGameObjcet_t1191175011 * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier(&___method_1, value);
	}

	inline static int32_t get_offset_of_GO_2() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator2_t2852953551, ___GO_2)); }
	inline GameObject_t1756533147 * get_GO_2() const { return ___GO_2; }
	inline GameObject_t1756533147 ** get_address_of_GO_2() { return &___GO_2; }
	inline void set_GO_2(GameObject_t1756533147 * value)
	{
		___GO_2 = value;
		Il2CppCodeGenWriteBarrier(&___GO_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator2_t2852953551, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator2_t2852953551, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsU3Ec__Iterator2_t2852953551, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
