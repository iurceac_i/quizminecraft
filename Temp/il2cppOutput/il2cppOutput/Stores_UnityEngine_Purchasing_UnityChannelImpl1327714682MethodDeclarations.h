﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.UnityChannelImpl
struct UnityChannelImpl_t1327714682;
// UnityEngine.Purchasing.INativeUnityChannelStore
struct INativeUnityChannelStore_t670691399;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>
struct ReadOnlyCollection_1_t2128260960;
// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t1942475268;
// System.String
struct String_t;
// System.Action`3<System.Boolean,System.String,System.String>
struct Action_3_t3864773326;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.UnityChannelImpl::.ctor()
extern "C"  void UnityChannelImpl__ctor_m3372672074 (UnityChannelImpl_t1327714682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelImpl::SetNativeStore(UnityEngine.Purchasing.INativeUnityChannelStore)
extern "C"  void UnityChannelImpl_SetNativeStore_m3720432 (UnityChannelImpl_t1327714682 * __this, Il2CppObject * ___unityChannelBindings0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelImpl::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern "C"  void UnityChannelImpl_RetrieveProducts_m86248098 (UnityChannelImpl_t1327714682 * __this, ReadOnlyCollection_1_t2128260960 * ___products0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelImpl::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern "C"  void UnityChannelImpl_Purchase_m1906667446 (UnityChannelImpl_t1327714682 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelImpl::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern "C"  void UnityChannelImpl_FinishTransaction_m1796617110 (UnityChannelImpl_t1327714682 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___transactionId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.UnityChannelImpl::get_fetchReceiptPayloadOnPurchase()
extern "C"  bool UnityChannelImpl_get_fetchReceiptPayloadOnPurchase_m154957409 (UnityChannelImpl_t1327714682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelImpl::set_fetchReceiptPayloadOnPurchase(System.Boolean)
extern "C"  void UnityChannelImpl_set_fetchReceiptPayloadOnPurchase_m826422872 (UnityChannelImpl_t1327714682 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelImpl::ValidateReceipt(System.String,System.Action`3<System.Boolean,System.String,System.String>)
extern "C"  void UnityChannelImpl_ValidateReceipt_m2670145539 (UnityChannelImpl_t1327714682 * __this, String_t* ___transactionIdentifier0, Action_3_t3864773326 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.UnityChannelImpl::GetLastPurchaseError()
extern "C"  String_t* UnityChannelImpl_GetLastPurchaseError_m3414529960 (UnityChannelImpl_t1327714682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelImpl::<RetrieveProducts>b__6_0(System.Boolean,System.String)
extern "C"  void UnityChannelImpl_U3CRetrieveProductsU3Eb__6_0_m3443241054 (UnityChannelImpl_t1327714682 * __this, bool ___result0, String_t* ___json1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
