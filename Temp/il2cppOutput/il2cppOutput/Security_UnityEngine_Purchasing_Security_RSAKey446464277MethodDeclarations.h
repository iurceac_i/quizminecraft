﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.RSAKey
struct RSAKey_t446464277;
// System.Security.Cryptography.RSACryptoServiceProvider
struct RSACryptoServiceProvider_t4229286967;
// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_RSACryptoSer4229286967.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "mscorlib_System_String2029220233.h"

// System.Security.Cryptography.RSACryptoServiceProvider UnityEngine.Purchasing.Security.RSAKey::get_rsa()
extern "C"  RSACryptoServiceProvider_t4229286967 * RSAKey_get_rsa_m255744627 (RSAKey_t446464277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.RSAKey::set_rsa(System.Security.Cryptography.RSACryptoServiceProvider)
extern "C"  void RSAKey_set_rsa_m66955140 (RSAKey_t446464277 * __this, RSACryptoServiceProvider_t4229286967 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.RSAKey::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void RSAKey__ctor_m79110961 (RSAKey_t446464277 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.Security.RSAKey::Verify(System.Byte[],System.Byte[])
extern "C"  bool RSAKey_Verify_m2487225620 (RSAKey_t446464277 * __this, ByteU5BU5D_t3397334013* ___message0, ByteU5BU5D_t3397334013* ___signature1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSACryptoServiceProvider UnityEngine.Purchasing.Security.RSAKey::ParseNode(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  RSACryptoServiceProvider_t4229286967 * RSAKey_ParseNode_m1392584771 (RSAKey_t446464277 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.RSAKey::ToXML(System.String,System.String)
extern "C"  String_t* RSAKey_ToXML_m3567447122 (RSAKey_t446464277 * __this, String_t* ___modulus0, String_t* ___exponent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
