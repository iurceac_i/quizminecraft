﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// IAPDemo
struct IAPDemo_t823941185;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.IAppleExtensions
struct IAppleExtensions_t1627764765;
// System.Object
struct Il2CppObject;
// UnityEngine.Purchasing.ISamsungAppsExtensions
struct ISamsungAppsExtensions_t3429739537;
// UnityEngine.Purchasing.IMoolahExtension
struct IMoolahExtension_t3195861654;
// UnityEngine.Purchasing.IMicrosoftExtensions
struct IMicrosoftExtensions_t1101930285;
// UnityEngine.Purchasing.IUnityChannelExtensions
struct IUnityChannelExtensions_t4012708657;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_t547992434;
// UnityEngine.Purchasing.UnifiedReceipt
struct UnifiedReceipt_t2654419430;
// System.String
struct String_t;
// UnityEngine.Purchasing.UnityChannelPurchaseReceipt
struct UnityChannelPurchaseReceipt_t1964142665;
// UnityEngine.Purchasing.Product
struct Product_t1203687971;
// IAPDemo/UnityChannelPurchaseError
struct UnityChannelPurchaseError_t594865545;
// UnityEngine.Purchasing.IMicrosoftConfiguration
struct IMicrosoftConfiguration_t1212838845;
// UnityEngine.Purchasing.IMoolahConfiguration
struct IMoolahConfiguration_t3241385415;
// UnityEngine.Purchasing.IUnityChannelConfiguration
struct IUnityChannelConfiguration_t1883011117;
// UnityEngine.Purchasing.IAmazonConfiguration
struct IAmazonConfiguration_t3016942165;
// UnityEngine.Purchasing.ISamsungAppsConfiguration
struct ISamsungAppsConfiguration_t4066821689;
// UnityEngine.Purchasing.ITizenStoreConfiguration
struct ITizenStoreConfiguration_t2900348728;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>
struct IEnumerable_1_t1495815016;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Store.UserInfo
struct UserInfo_t741955747;
// IAPDemo/<Awake>c__AnonStorey0
struct U3CAwakeU3Ec__AnonStorey0_t2163001806;
// IAPDemo/<InitUI>c__AnonStorey1
struct U3CInitUIU3Ec__AnonStorey1_t996539604;
// IAPDemo/UnityChannelLoginHandler
struct UnityChannelLoginHandler_t2253884029;
// IAPDemo/UnityChannelPurchaseInfo
struct UnityChannelPurchaseInfo_t855507751;
// UnityEngine.Purchasing.DemoInventory
struct DemoInventory_t2756307745;
// UnityEngine.Purchasing.IAPButton
struct IAPButton_t3077837360;
// UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager
struct IAPButtonStoreManager_t911589174;
// UnityEngine.Purchasing.IAPListener
struct IAPListener_t3789552708;
// UnityEngine.Purchasing.IAPButton/OnPurchaseCompletedEvent
struct OnPurchaseCompletedEvent_t4018783659;
// UnityEngine.Purchasing.IAPButton/OnPurchaseFailedEvent
struct OnPurchaseFailedEvent_t2813769101;
// UnityEngine.Purchasing.ConfigurationBuilder
struct ConfigurationBuilder_t1298400415;
// UnityEngine.Purchasing.ProductCatalog
struct ProductCatalog_t2667590766;
// UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent
struct OnPurchaseCompletedEvent_t3384326983;
// UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent
struct OnPurchaseFailedEvent_t3694879381;
// UnityEngine.Purchasing.Security.AppleTangle
struct AppleTangle_t53875121;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.Purchasing.Security.GooglePlayTangle
struct GooglePlayTangle_t2749524914;
// UnityEngine.Purchasing.Security.UnityChannelTangle
struct UnityChannelTangle_t1696213691;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1500295812.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1500295812MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo823941185.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo823941185MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1005487353MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData2420267500MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1789388632MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine.Purchasing_ArrayTypes.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299.h"
#include "mscorlib_System_Action_1_gen1005487353.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544.h"
#include "mscorlib_System_Decimal724701077MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1789388632.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData2420267500.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JsonUtility653638946MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_UnifiedReceipt2654419430.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelPurchase1964142665.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"
#include "UnityEngine_UnityEngine_JsonUtility653638946.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannelP594865545.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannelP855507751.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_U3CAwakeU3Ec2163001806MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalog2667590766MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogItem977711995MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_IDs3808979560MethodDeclarations.h"
#include "System_Core_System_Action3226471752MethodDeclarations.h"
#include "UnityStore_UnityEngine_Store_AppInfo2080248435MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannel2253884029MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1831019615MethodDeclarations.h"
#include "UnityStore_UnityEngine_Store_StoreService3339196318MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_U3CAwakeU3Ec2163001806.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalog2667590766.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogItem977711995.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_IDs3808979560.h"
#include "Stores_UnityEngine_Purchasing_StoreID471452324.h"
#include "UnityStore_UnityEngine_Store_AppInfo2080248435.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "Stores_UnityEngine_Purchasing_AppStore379104228.h"
#include "Stores_UnityEngine_Purchasing_CloudMoolahMode206291964.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod2754455291.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge275936122.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsMode2214306743.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannel2253884029.h"
#include "mscorlib_System_Action_1_gen1831019615.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2110227463MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEve2203087800.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2289103034MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_RestoreTransactionID2487303652.h"
#include "mscorlib_System_Action_1_gen2289103034.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_Action_1_gen543755129MethodDeclarations.h"
#include "UnityStore_UnityEngine_Store_UserInfo741955747.h"
#include "mscorlib_System_Action_1_gen543755129.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_U3CInitUIU3Ec996539604MethodDeclarations.h"
#include "System_Core_System_Action_3_gen3864773326MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_U3CInitUIU3Ec996539604.h"
#include "System_Core_System_Action_3_gen3864773326.h"
#include "UnityStore_UnityEngine_Store_UserInfo741955747MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unit3301441281MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannelP594865545MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannelP855507751MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2756307745.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2756307745MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3077837360.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3077837360MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasin911589174MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasin593747732.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasin911589174.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen1242037986MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi4018783659.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen1414078924MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2813769101.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasin593747732MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2446958492MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2142434692MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2446958492.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3789552708.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1981688166.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1981688166MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3789552708MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi4018783659MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2813769101MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2142434692.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3384326983.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3694879381.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3384326983MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3694879381MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasing53875121.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasing53875121MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato2878230988MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2749524914.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi2749524914MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi1696213691.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi1696213691MethodDeclarations.h"

// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<System.Object>()
extern "C"  Il2CppObject * IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IExtensionProvider_GetExtension_TisIl2CppObject_m925367407(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<UnityEngine.Purchasing.IAppleExtensions>()
#define IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<UnityEngine.Purchasing.ISamsungAppsExtensions>()
#define IExtensionProvider_GetExtension_TisISamsungAppsExtensions_t3429739537_m4249086459(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<UnityEngine.Purchasing.IMoolahExtension>()
#define IExtensionProvider_GetExtension_TisIMoolahExtension_t3195861654_m2257603670(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<UnityEngine.Purchasing.IMicrosoftExtensions>()
#define IExtensionProvider_GetExtension_TisIMicrosoftExtensions_t1101930285_m566281781(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<UnityEngine.Purchasing.IUnityChannelExtensions>()
#define IExtensionProvider_GetExtension_TisIUnityChannelExtensions_t4012708657_m3096658385(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern "C"  Il2CppObject * JsonUtility_FromJson_TisIl2CppObject_m1534990071_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define JsonUtility_FromJson_TisIl2CppObject_m1534990071(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m1534990071_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<UnityEngine.Purchasing.UnifiedReceipt>(System.String)
#define JsonUtility_FromJson_TisUnifiedReceipt_t2654419430_m2641857726(__this /* static, unused */, p0, method) ((  UnifiedReceipt_t2654419430 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m1534990071_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<UnityEngine.Purchasing.UnityChannelPurchaseReceipt>(System.String)
#define JsonUtility_FromJson_TisUnityChannelPurchaseReceipt_t1964142665_m3059962635(__this /* static, unused */, p0, method) ((  UnityChannelPurchaseReceipt_t1964142665 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m1534990071_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<IAPDemo/UnityChannelPurchaseError>(System.String)
#define JsonUtility_FromJson_TisUnityChannelPurchaseError_t594865545_m3918988582(__this /* static, unused */, p0, method) ((  UnityChannelPurchaseError_t594865545 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m1534990071_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<System.Object>()
extern "C"  Il2CppObject * ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared (ConfigurationBuilder_t1298400415 * __this, const MethodInfo* method);
#define ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IMicrosoftConfiguration>()
#define ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IMoolahConfiguration>()
#define ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IUnityChannelConfiguration>()
#define ConfigurationBuilder_Configure_TisIUnityChannelConfiguration_t1883011117_m1357048912(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IAmazonConfiguration>()
#define ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.ISamsungAppsConfiguration>()
#define ConfigurationBuilder_Configure_TisISamsungAppsConfiguration_t4066821689_m2324730914(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.ITizenStoreConfiguration>()
#define ConfigurationBuilder_Configure_TisITizenStoreConfiguration_t2900348728_m1247129795(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m4280536079(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Dropdown>()
#define GameObject_GetComponent_TisDropdown_t1985816271_m1750975685(__this, method) ((  Dropdown_t1985816271 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t2872111280_m3862106414(__this, method) ((  Button_t2872111280 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
#define Component_GetComponent_TisButton_t2872111280_m3197608264(__this, method) ((  Button_t2872111280 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void IAPDemo::.ctor()
extern "C"  void IAPDemo__ctor_m3267573714 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	{
		__this->set_m_SelectedItemIndex_17((-1));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1005487353_il2cpp_TypeInfo_var;
extern Il2CppClass* IAppleExtensions_t1627764765_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* OptionData_t2420267500_il2cpp_TypeInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisISamsungAppsExtensions_t3429739537_m4249086459_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIMoolahExtension_t3195861654_m2257603670_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIMicrosoftExtensions_t1101930285_m566281781_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIUnityChannelExtensions_t4012708657_m3096658385_MethodInfo_var;
extern const MethodInfo* IAPDemo_OnDeferred_m801123048_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2354061435_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m288507185_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral820171765;
extern Il2CppCodeGenString* _stringLiteral1220271277;
extern Il2CppCodeGenString* _stringLiteral4277465922;
extern const uint32_t IAPDemo_OnInitialized_m2628601748_MetadataUsageId;
extern "C"  void IAPDemo_OnInitialized_m2628601748 (IAPDemo_t823941185 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnInitialized_m2628601748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Product_t1203687971 * V_0 = NULL;
	ProductU5BU5D_t728099314* V_1 = NULL;
	int32_t V_2 = 0;
	Decimal_t724701077  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Product_t1203687971 * V_5 = NULL;
	String_t* V_6 = NULL;
	{
		Il2CppObject * L_0 = ___controller0;
		__this->set_m_Controller_2(L_0);
		Il2CppObject * L_1 = ___extensions1;
		NullCheck(L_1);
		Il2CppObject * L_2 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var, L_1);
		__this->set_m_AppleExtensions_3(L_2);
		Il2CppObject * L_3 = ___extensions1;
		NullCheck(L_3);
		Il2CppObject * L_4 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisISamsungAppsExtensions_t3429739537_m4249086459_MethodInfo_var, L_3);
		__this->set_m_SamsungExtensions_5(L_4);
		Il2CppObject * L_5 = ___extensions1;
		NullCheck(L_5);
		Il2CppObject * L_6 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIMoolahExtension_t3195861654_m2257603670_MethodInfo_var, L_5);
		__this->set_m_MoolahExtensions_4(L_6);
		Il2CppObject * L_7 = ___extensions1;
		NullCheck(L_7);
		Il2CppObject * L_8 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIMicrosoftExtensions_t1101930285_m566281781_MethodInfo_var, L_7);
		__this->set_m_MicrosoftExtensions_6(L_8);
		Il2CppObject * L_9 = ___extensions1;
		NullCheck(L_9);
		Il2CppObject * L_10 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIUnityChannelExtensions_t4012708657_m3096658385_MethodInfo_var, L_9);
		__this->set_m_UnityChannelExtensions_7(L_10);
		Il2CppObject * L_11 = ___controller0;
		NullCheck(L_11);
		ProductCollection_t3600019299 * L_12 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_11);
		NullCheck(L_12);
		ProductU5BU5D_t728099314* L_13 = ProductCollection_get_all_m3364167965(L_12, /*hidden argument*/NULL);
		IAPDemo_InitUI_m3350388155(__this, (Il2CppObject*)(Il2CppObject*)L_13, /*hidden argument*/NULL);
		Il2CppObject * L_14 = __this->get_m_AppleExtensions_3();
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)IAPDemo_OnDeferred_m801123048_MethodInfo_var);
		Action_1_t1005487353 * L_16 = (Action_1_t1005487353 *)il2cpp_codegen_object_new(Action_1_t1005487353_il2cpp_TypeInfo_var);
		Action_1__ctor_m2354061435(L_16, __this, L_15, /*hidden argument*/Action_1__ctor_m2354061435_MethodInfo_var);
		NullCheck(L_14);
		InterfaceActionInvoker1< Action_1_t1005487353 * >::Invoke(1 /* System.Void UnityEngine.Purchasing.IAppleExtensions::RegisterPurchaseDeferredListener(System.Action`1<UnityEngine.Purchasing.Product>) */, IAppleExtensions_t1627764765_il2cpp_TypeInfo_var, L_14, L_16);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral820171765, /*hidden argument*/NULL);
		Il2CppObject * L_17 = ___controller0;
		NullCheck(L_17);
		ProductCollection_t3600019299 * L_18 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_17);
		NullCheck(L_18);
		ProductU5BU5D_t728099314* L_19 = ProductCollection_get_all_m3364167965(L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		V_2 = 0;
		goto IL_0116;
	}

IL_0088:
	{
		ProductU5BU5D_t728099314* L_20 = V_1;
		int32_t L_21 = V_2;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		Product_t1203687971 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		V_0 = L_23;
		Product_t1203687971 * L_24 = V_0;
		NullCheck(L_24);
		bool L_25 = Product_get_availableToPurchase_m3285924692(L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0112;
		}
	}
	{
		StringU5BU5D_t1642385972* L_26 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
		Product_t1203687971 * L_27 = V_0;
		NullCheck(L_27);
		ProductMetadata_t1573242544 * L_28 = Product_get_metadata_m263398044(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		String_t* L_29 = ProductMetadata_get_localizedTitle_m1010599344(L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_29);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_29);
		StringU5BU5D_t1642385972* L_30 = L_26;
		Product_t1203687971 * L_31 = V_0;
		NullCheck(L_31);
		ProductMetadata_t1573242544 * L_32 = Product_get_metadata_m263398044(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		String_t* L_33 = ProductMetadata_get_localizedDescription_m3665253400(L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_33);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_33);
		StringU5BU5D_t1642385972* L_34 = L_30;
		Product_t1203687971 * L_35 = V_0;
		NullCheck(L_35);
		ProductMetadata_t1573242544 * L_36 = Product_get_metadata_m263398044(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		String_t* L_37 = ProductMetadata_get_isoCurrencyCode_m2098971654(L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, L_37);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_37);
		StringU5BU5D_t1642385972* L_38 = L_34;
		Product_t1203687971 * L_39 = V_0;
		NullCheck(L_39);
		ProductMetadata_t1573242544 * L_40 = Product_get_metadata_m263398044(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		Decimal_t724701077  L_41 = ProductMetadata_get_localizedPrice_m158316127(L_40, /*hidden argument*/NULL);
		V_3 = L_41;
		String_t* L_42 = Decimal_ToString_m759431975((&V_3), /*hidden argument*/NULL);
		NullCheck(L_38);
		ArrayElementTypeCheck (L_38, L_42);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_42);
		StringU5BU5D_t1642385972* L_43 = L_38;
		Product_t1203687971 * L_44 = V_0;
		NullCheck(L_44);
		ProductMetadata_t1573242544 * L_45 = Product_get_metadata_m263398044(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		String_t* L_46 = ProductMetadata_get_localizedPriceString_m2216629954(L_45, /*hidden argument*/NULL);
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, L_46);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_46);
		StringU5BU5D_t1642385972* L_47 = L_43;
		Product_t1203687971 * L_48 = V_0;
		NullCheck(L_48);
		String_t* L_49 = Product_get_transactionID_m2023701007(L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		ArrayElementTypeCheck (L_47, L_49);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_49);
		StringU5BU5D_t1642385972* L_50 = L_47;
		Product_t1203687971 * L_51 = V_0;
		NullCheck(L_51);
		String_t* L_52 = Product_get_receipt_m1045158498(L_51, /*hidden argument*/NULL);
		NullCheck(L_50);
		ArrayElementTypeCheck (L_50, L_52);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)L_52);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = String_Join_m1966872927(NULL /*static, unused*/, _stringLiteral1220271277, L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
	}

IL_0112:
	{
		int32_t L_54 = V_2;
		V_2 = ((int32_t)((int32_t)L_54+(int32_t)1));
	}

IL_0116:
	{
		int32_t L_55 = V_2;
		ProductU5BU5D_t728099314* L_56 = V_1;
		NullCheck(L_56);
		if ((((int32_t)L_55) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_56)->max_length)))))))
		{
			goto IL_0088;
		}
	}
	{
		Il2CppObject * L_57 = __this->get_m_Controller_2();
		NullCheck(L_57);
		ProductCollection_t3600019299 * L_58 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_57);
		NullCheck(L_58);
		ProductU5BU5D_t728099314* L_59 = ProductCollection_get_all_m3364167965(L_58, /*hidden argument*/NULL);
		NullCheck(L_59);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_59)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_013e;
		}
	}
	{
		__this->set_m_SelectedItemIndex_17(0);
	}

IL_013e:
	{
		V_4 = 0;
		goto IL_01af;
	}

IL_0146:
	{
		Il2CppObject * L_60 = __this->get_m_Controller_2();
		NullCheck(L_60);
		ProductCollection_t3600019299 * L_61 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_60);
		NullCheck(L_61);
		ProductU5BU5D_t728099314* L_62 = ProductCollection_get_all_m3364167965(L_61, /*hidden argument*/NULL);
		int32_t L_63 = V_4;
		NullCheck(L_62);
		int32_t L_64 = L_63;
		Product_t1203687971 * L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		V_5 = L_65;
		Product_t1203687971 * L_66 = V_5;
		NullCheck(L_66);
		ProductMetadata_t1573242544 * L_67 = Product_get_metadata_m263398044(L_66, /*hidden argument*/NULL);
		NullCheck(L_67);
		String_t* L_68 = ProductMetadata_get_localizedTitle_m1010599344(L_67, /*hidden argument*/NULL);
		Product_t1203687971 * L_69 = V_5;
		NullCheck(L_69);
		ProductMetadata_t1573242544 * L_70 = Product_get_metadata_m263398044(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		String_t* L_71 = ProductMetadata_get_localizedPriceString_m2216629954(L_70, /*hidden argument*/NULL);
		Product_t1203687971 * L_72 = V_5;
		NullCheck(L_72);
		ProductMetadata_t1573242544 * L_73 = Product_get_metadata_m263398044(L_72, /*hidden argument*/NULL);
		NullCheck(L_73);
		Decimal_t724701077  L_74 = ProductMetadata_get_localizedPrice_m158316127(L_73, /*hidden argument*/NULL);
		Decimal_t724701077  L_75 = L_74;
		Il2CppObject * L_76 = Box(Decimal_t724701077_il2cpp_TypeInfo_var, &L_75);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_77 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral4277465922, L_68, L_71, L_76, /*hidden argument*/NULL);
		V_6 = L_77;
		Dropdown_t1985816271 * L_78 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_78);
		List_1_t1789388632 * L_79 = Dropdown_get_options_m2669836220(L_78, /*hidden argument*/NULL);
		int32_t L_80 = V_4;
		String_t* L_81 = V_6;
		OptionData_t2420267500 * L_82 = (OptionData_t2420267500 *)il2cpp_codegen_object_new(OptionData_t2420267500_il2cpp_TypeInfo_var);
		OptionData__ctor_m743450704(L_82, L_81, /*hidden argument*/NULL);
		NullCheck(L_79);
		List_1_set_Item_m288507185(L_79, L_80, L_82, /*hidden argument*/List_1_set_Item_m288507185_MethodInfo_var);
		int32_t L_83 = V_4;
		V_4 = ((int32_t)((int32_t)L_83+(int32_t)1));
	}

IL_01af:
	{
		int32_t L_84 = V_4;
		Il2CppObject * L_85 = __this->get_m_Controller_2();
		NullCheck(L_85);
		ProductCollection_t3600019299 * L_86 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_85);
		NullCheck(L_86);
		ProductU5BU5D_t728099314* L_87 = ProductCollection_get_all_m3364167965(L_86, /*hidden argument*/NULL);
		NullCheck(L_87);
		if ((((int32_t)L_84) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_87)->max_length)))))))
		{
			goto IL_0146;
		}
	}
	{
		Dropdown_t1985816271 * L_88 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_88);
		Dropdown_RefreshShownValue_m3113581237(L_88, /*hidden argument*/NULL);
		IAPDemo_UpdateHistoryUI_m2607563357(__this, /*hidden argument*/NULL);
		IAPDemo_LogProductDefinitions_m1239761007(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult IAPDemo::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisUnifiedReceipt_t2654419430_m2641857726_MethodInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisUnityChannelPurchaseReceipt_t1964142665_m3059962635_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1546350049;
extern Il2CppCodeGenString* _stringLiteral1530557140;
extern Il2CppCodeGenString* _stringLiteral3366666444;
extern const uint32_t IAPDemo_ProcessPurchase_m3949646312_MetadataUsageId;
extern "C"  int32_t IAPDemo_ProcessPurchase_m3949646312 (IAPDemo_t823941185 * __this, PurchaseEventArgs_t547992434 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_ProcessPurchase_m3949646312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnifiedReceipt_t2654419430 * V_0 = NULL;
	UnityChannelPurchaseReceipt_t1964142665 * V_1 = NULL;
	{
		PurchaseEventArgs_t547992434 * L_0 = ___e0;
		NullCheck(L_0);
		Product_t1203687971 * L_1 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		ProductDefinition_t1942475268 * L_2 = Product_get_definition_m2035415516(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ProductDefinition_get_id_m264072292(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1546350049, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		PurchaseEventArgs_t547992434 * L_5 = ___e0;
		NullCheck(L_5);
		Product_t1203687971 * L_6 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = Product_get_receipt_m1045158498(L_6, /*hidden argument*/NULL);
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1530557140, L_7, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		PurchaseEventArgs_t547992434 * L_9 = ___e0;
		NullCheck(L_9);
		Product_t1203687971 * L_10 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = Product_get_transactionID_m2023701007(L_10, /*hidden argument*/NULL);
		__this->set_m_LastTransationID_11(L_11);
		PurchaseEventArgs_t547992434 * L_12 = ___e0;
		NullCheck(L_12);
		Product_t1203687971 * L_13 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_14 = Product_get_receipt_m1045158498(L_13, /*hidden argument*/NULL);
		__this->set_m_LastReceipt_12(L_14);
		__this->set_m_PurchaseInProgress_18((bool)0);
		bool L_15 = __this->get_m_IsUnityChannelSelected_10();
		if (!L_15)
		{
			goto IL_00cb;
		}
	}
	{
		PurchaseEventArgs_t547992434 * L_16 = ___e0;
		NullCheck(L_16);
		Product_t1203687971 * L_17 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_18 = Product_get_receipt_m1045158498(L_17, /*hidden argument*/NULL);
		UnifiedReceipt_t2654419430 * L_19 = JsonUtility_FromJson_TisUnifiedReceipt_t2654419430_m2641857726(NULL /*static, unused*/, L_18, /*hidden argument*/JsonUtility_FromJson_TisUnifiedReceipt_t2654419430_m2641857726_MethodInfo_var);
		V_0 = L_19;
		UnifiedReceipt_t2654419430 * L_20 = V_0;
		if (!L_20)
		{
			goto IL_00cb;
		}
	}
	{
		UnifiedReceipt_t2654419430 * L_21 = V_0;
		NullCheck(L_21);
		String_t* L_22 = L_21->get_Payload_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_00cb;
		}
	}
	{
		UnifiedReceipt_t2654419430 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = L_24->get_Payload_0();
		UnityChannelPurchaseReceipt_t1964142665 * L_26 = JsonUtility_FromJson_TisUnityChannelPurchaseReceipt_t1964142665_m3059962635(NULL /*static, unused*/, L_25, /*hidden argument*/JsonUtility_FromJson_TisUnityChannelPurchaseReceipt_t1964142665_m3059962635_MethodInfo_var);
		V_1 = L_26;
		ObjectU5BU5D_t3614634134* L_27 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3));
		UnityChannelPurchaseReceipt_t1964142665 * L_28 = V_1;
		NullCheck(L_28);
		String_t* L_29 = L_28->get_storeSpecificId_0();
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, L_29);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_29);
		ObjectU5BU5D_t3614634134* L_30 = L_27;
		UnityChannelPurchaseReceipt_t1964142665 * L_31 = V_1;
		NullCheck(L_31);
		String_t* L_32 = L_31->get_transactionId_1();
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_32);
		ObjectU5BU5D_t3614634134* L_33 = L_30;
		UnityChannelPurchaseReceipt_t1964142665 * L_34 = V_1;
		NullCheck(L_34);
		String_t* L_35 = L_34->get_orderQueryToken_2();
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_35);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_35);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogFormat_m3816524933(NULL /*static, unused*/, _stringLiteral3366666444, L_33, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		IAPDemo_UpdateHistoryUI_m2607563357(__this, /*hidden argument*/NULL);
		return (int32_t)(0);
	}
}
// System.Void IAPDemo::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern Il2CppClass* IUnityChannelExtensions_t4012708657_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisUnityChannelPurchaseError_t594865545_m3918988582_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral830689752;
extern Il2CppCodeGenString* _stringLiteral4158562299;
extern Il2CppCodeGenString* _stringLiteral466964775;
extern Il2CppCodeGenString* _stringLiteral772573384;
extern const uint32_t IAPDemo_OnPurchaseFailed_m2053066303_MetadataUsageId;
extern "C"  void IAPDemo_OnPurchaseFailed_m2053066303 (IAPDemo_t823941185 * __this, Product_t1203687971 * ___item0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnPurchaseFailed_m2053066303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	UnityChannelPurchaseError_t594865545 * V_1 = NULL;
	UnityChannelPurchaseInfo_t855507751 * V_2 = NULL;
	{
		Product_t1203687971 * L_0 = ___item0;
		NullCheck(L_0);
		ProductDefinition_t1942475268 * L_1 = Product_get_definition_m2035415516(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_id_m264072292(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral830689752, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___r1;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, &L_5);
		Debug_Log_m920475918(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		bool L_7 = __this->get_m_IsUnityChannelSelected_10();
		if (!L_7)
		{
			goto IL_00bd;
		}
	}
	{
		Il2CppObject * L_8 = __this->get_m_UnityChannelExtensions_7();
		NullCheck(L_8);
		String_t* L_9 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String UnityEngine.Purchasing.IUnityChannelExtensions::GetLastPurchaseError() */, IUnityChannelExtensions_t4012708657_il2cpp_TypeInfo_var, L_8);
		V_0 = L_9;
		String_t* L_10 = V_0;
		UnityChannelPurchaseError_t594865545 * L_11 = JsonUtility_FromJson_TisUnityChannelPurchaseError_t594865545_m3918988582(NULL /*static, unused*/, L_10, /*hidden argument*/JsonUtility_FromJson_TisUnityChannelPurchaseError_t594865545_m3918988582_MethodInfo_var);
		V_1 = L_11;
		UnityChannelPurchaseError_t594865545 * L_12 = V_1;
		if (!L_12)
		{
			goto IL_0086;
		}
	}
	{
		UnityChannelPurchaseError_t594865545 * L_13 = V_1;
		NullCheck(L_13);
		UnityChannelPurchaseInfo_t855507751 * L_14 = L_13->get_purchaseInfo_1();
		if (!L_14)
		{
			goto IL_0086;
		}
	}
	{
		UnityChannelPurchaseError_t594865545 * L_15 = V_1;
		NullCheck(L_15);
		UnityChannelPurchaseInfo_t855507751 * L_16 = L_15->get_purchaseInfo_1();
		V_2 = L_16;
		ObjectU5BU5D_t3614634134* L_17 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3));
		UnityChannelPurchaseInfo_t855507751 * L_18 = V_2;
		NullCheck(L_18);
		String_t* L_19 = L_18->get_productCode_0();
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_19);
		ObjectU5BU5D_t3614634134* L_20 = L_17;
		UnityChannelPurchaseInfo_t855507751 * L_21 = V_2;
		NullCheck(L_21);
		String_t* L_22 = L_21->get_gameOrderId_1();
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = L_20;
		UnityChannelPurchaseInfo_t855507751 * L_24 = V_2;
		NullCheck(L_24);
		String_t* L_25 = L_24->get_orderQueryToken_2();
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_25);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogFormat_m3816524933(NULL /*static, unused*/, _stringLiteral4158562299, L_23, /*hidden argument*/NULL);
	}

IL_0086:
	{
		int32_t L_26 = ___r1;
		if ((!(((uint32_t)L_26) == ((uint32_t)6))))
		{
			goto IL_00bd;
		}
	}
	{
		UnityChannelPurchaseError_t594865545 * L_27 = V_1;
		if (!L_27)
		{
			goto IL_00bd;
		}
	}
	{
		UnityChannelPurchaseError_t594865545 * L_28 = V_1;
		NullCheck(L_28);
		String_t* L_29 = L_28->get_error_0();
		if (!L_29)
		{
			goto IL_00bd;
		}
	}
	{
		UnityChannelPurchaseError_t594865545 * L_30 = V_1;
		NullCheck(L_30);
		String_t* L_31 = L_30->get_error_0();
		NullCheck(L_31);
		bool L_32 = String_Equals_m2633592423(L_31, _stringLiteral466964775, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00bd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral772573384, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		__this->set_m_PurchaseInProgress_18((bool)0);
		return;
	}
}
// System.Void IAPDemo::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1657966450;
extern Il2CppCodeGenString* _stringLiteral1153205404;
extern Il2CppCodeGenString* _stringLiteral3815334426;
extern Il2CppCodeGenString* _stringLiteral417931313;
extern const uint32_t IAPDemo_OnInitializeFailed_m1037377075_MetadataUsageId;
extern "C"  void IAPDemo_OnInitializeFailed_m1037377075 (IAPDemo_t823941185 * __this, int32_t ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnInitializeFailed_m1037377075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1657966450, /*hidden argument*/NULL);
		int32_t L_0 = ___error0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_1 = ___error0;
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_2 = ___error0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0041;
		}
	}
	{
		goto IL_0050;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1153205404, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3815334426, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral417931313, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0050:
	{
		return;
	}
}
// System.Void IAPDemo::Awake()
extern Il2CppClass* U3CAwakeU3Ec__AnonStorey0_t2163001806_il2cpp_TypeInfo_var;
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern Il2CppClass* IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var;
extern Il2CppClass* IMicrosoftConfiguration_t1212838845_il2cpp_TypeInfo_var;
extern Il2CppClass* IMoolahConfiguration_t3241385415_il2cpp_TypeInfo_var;
extern Il2CppClass* IUnityChannelConfiguration_t1883011117_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1269839040_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2748203118_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t1423527629_il2cpp_TypeInfo_var;
extern Il2CppClass* IDs_t3808979560_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t763579369_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2241943447_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* IAmazonConfiguration_t3016942165_il2cpp_TypeInfo_var;
extern Il2CppClass* ISamsungAppsConfiguration_t4066821689_il2cpp_TypeInfo_var;
extern Il2CppClass* ITizenStoreConfiguration_t2900348728_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern Il2CppClass* AppInfo_t2080248435_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityChannelLoginHandler_t2253884029_il2cpp_TypeInfo_var;
extern Il2CppClass* IAPDemo_t823941185_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1831019615_il2cpp_TypeInfo_var;
extern Il2CppClass* StoreService_t3339196318_il2cpp_TypeInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIUnityChannelConfiguration_t1883011117_m1357048912_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisISamsungAppsConfiguration_t4066821689_m2324730914_MethodInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisITizenStoreConfiguration_t2900348728_m1247129795_MethodInfo_var;
extern const MethodInfo* U3CAwakeU3Ec__AnonStorey0_U3CU3Em__0_m3852349590_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CAwakeU3Em__0_m3196988796_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1193014778_MethodInfo_var;
extern const MethodInfo* U3CAwakeU3Ec__AnonStorey0_U3CU3Em__1_m3852349495_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral579628896;
extern Il2CppCodeGenString* _stringLiteral3822513892;
extern Il2CppCodeGenString* _stringLiteral1830547213;
extern Il2CppCodeGenString* _stringLiteral2946374258;
extern Il2CppCodeGenString* _stringLiteral2700534657;
extern Il2CppCodeGenString* _stringLiteral473744409;
extern Il2CppCodeGenString* _stringLiteral859071123;
extern Il2CppCodeGenString* _stringLiteral1461038519;
extern Il2CppCodeGenString* _stringLiteral1645711892;
extern Il2CppCodeGenString* _stringLiteral968850961;
extern Il2CppCodeGenString* _stringLiteral862026094;
extern Il2CppCodeGenString* _stringLiteral2396058710;
extern Il2CppCodeGenString* _stringLiteral4189921875;
extern Il2CppCodeGenString* _stringLiteral1070925405;
extern Il2CppCodeGenString* _stringLiteral1346993374;
extern Il2CppCodeGenString* _stringLiteral1233259296;
extern Il2CppCodeGenString* _stringLiteral2315729507;
extern Il2CppCodeGenString* _stringLiteral784127634;
extern Il2CppCodeGenString* _stringLiteral1451147361;
extern Il2CppCodeGenString* _stringLiteral2297758188;
extern Il2CppCodeGenString* _stringLiteral2288302069;
extern Il2CppCodeGenString* _stringLiteral1739072345;
extern Il2CppCodeGenString* _stringLiteral3545049635;
extern const uint32_t IAPDemo_Awake_m1872671099_MetadataUsageId;
extern "C"  void IAPDemo_Awake_m1872671099 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_Awake_m1872671099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAwakeU3Ec__AnonStorey0_t2163001806 * V_0 = NULL;
	StandardPurchasingModule_t4003664591 * V_1 = NULL;
	ProductCatalog_t2667590766 * V_2 = NULL;
	ProductCatalogItem_t977711995 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	IDs_t3808979560 * V_5 = NULL;
	StoreID_t471452324 * V_6 = NULL;
	Il2CppObject* V_7 = NULL;
	IDs_t3808979560 * V_8 = NULL;
	bool V_9 = false;
	AppInfo_t2080248435 * V_10 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	IAPDemo_t823941185 * G_B2_0 = NULL;
	IAPDemo_t823941185 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	IAPDemo_t823941185 * G_B3_1 = NULL;
	IAPDemo_t823941185 * G_B5_0 = NULL;
	IAPDemo_t823941185 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	IAPDemo_t823941185 * G_B6_1 = NULL;
	IAPDemo_t823941185 * G_B8_0 = NULL;
	IAPDemo_t823941185 * G_B7_0 = NULL;
	int32_t G_B9_0 = 0;
	IAPDemo_t823941185 * G_B9_1 = NULL;
	UnityChannelLoginHandler_t2253884029 * G_B31_0 = NULL;
	UnityChannelLoginHandler_t2253884029 * G_B30_0 = NULL;
	{
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_0 = (U3CAwakeU3Ec__AnonStorey0_t2163001806 *)il2cpp_codegen_object_new(U3CAwakeU3Ec__AnonStorey0_t2163001806_il2cpp_TypeInfo_var);
		U3CAwakeU3Ec__AnonStorey0__ctor_m1436498841(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_2 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		StandardPurchasingModule_t4003664591 * L_3 = V_1;
		NullCheck(L_3);
		StandardPurchasingModule_set_useFakeStoreUIMode_m3278247934(L_3, 1, /*hidden argument*/NULL);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_4 = V_0;
		StandardPurchasingModule_t4003664591 * L_5 = V_1;
		ConfigurationBuilder_t1298400415 * L_6 = ConfigurationBuilder_Instance_m4243979412(NULL /*static, unused*/, L_5, ((IPurchasingModuleU5BU5D_t4128245854*)SZArrayNew(IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_builder_0(L_6);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_7 = V_0;
		NullCheck(L_7);
		ConfigurationBuilder_t1298400415 * L_8 = L_7->get_builder_0();
		NullCheck(L_8);
		Il2CppObject * L_9 = ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418(L_8, /*hidden argument*/ConfigurationBuilder_Configure_TisIMicrosoftConfiguration_t1212838845_m2434552418_MethodInfo_var);
		NullCheck(L_9);
		InterfaceActionInvoker1< bool >::Invoke(0 /* System.Void UnityEngine.Purchasing.IMicrosoftConfiguration::set_useMockBillingSystem(System.Boolean) */, IMicrosoftConfiguration_t1212838845_il2cpp_TypeInfo_var, L_9, (bool)1);
		int32_t L_10 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)11)))))
		{
			G_B2_0 = __this;
			goto IL_0055;
		}
	}
	{
		StandardPurchasingModule_t4003664591 * L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = StandardPurchasingModule_get_appStore_m2800198910(L_11, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_12) == ((int32_t)1))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0056;
	}

IL_0055:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0056:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_m_IsGooglePlayStoreSelected_8((bool)G_B3_0);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_13 = V_0;
		NullCheck(L_13);
		ConfigurationBuilder_t1298400415 * L_14 = L_13->get_builder_0();
		NullCheck(L_14);
		Il2CppObject * L_15 = ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946(L_14, /*hidden argument*/ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946_MethodInfo_var);
		NullCheck(L_15);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void UnityEngine.Purchasing.IMoolahConfiguration::set_appKey(System.String) */, IMoolahConfiguration_t3241385415_il2cpp_TypeInfo_var, L_15, _stringLiteral579628896);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_16 = V_0;
		NullCheck(L_16);
		ConfigurationBuilder_t1298400415 * L_17 = L_16->get_builder_0();
		NullCheck(L_17);
		Il2CppObject * L_18 = ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946(L_17, /*hidden argument*/ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946_MethodInfo_var);
		NullCheck(L_18);
		InterfaceActionInvoker1< String_t* >::Invoke(1 /* System.Void UnityEngine.Purchasing.IMoolahConfiguration::set_hashKey(System.String) */, IMoolahConfiguration_t3241385415_il2cpp_TypeInfo_var, L_18, _stringLiteral3822513892);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_19 = V_0;
		NullCheck(L_19);
		ConfigurationBuilder_t1298400415 * L_20 = L_19->get_builder_0();
		NullCheck(L_20);
		Il2CppObject * L_21 = ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946(L_20, /*hidden argument*/ConfigurationBuilder_Configure_TisIMoolahConfiguration_t3241385415_m574073946_MethodInfo_var);
		NullCheck(L_21);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UnityEngine.Purchasing.IMoolahConfiguration::SetMode(UnityEngine.Purchasing.CloudMoolahMode) */, IMoolahConfiguration_t3241385415_il2cpp_TypeInfo_var, L_21, 1);
		int32_t L_22 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = __this;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)11)))))
		{
			G_B5_0 = __this;
			goto IL_00ae;
		}
	}
	{
		StandardPurchasingModule_t4003664591 * L_23 = V_1;
		NullCheck(L_23);
		int32_t L_24 = StandardPurchasingModule_get_appStore_m2800198910(L_23, /*hidden argument*/NULL);
		G_B6_0 = ((((int32_t)L_24) == ((int32_t)3))? 1 : 0);
		G_B6_1 = G_B4_0;
		goto IL_00af;
	}

IL_00ae:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_00af:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_m_IsCloudMoolahStoreSelected_9((bool)G_B6_0);
		int32_t L_25 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B7_0 = __this;
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)11)))))
		{
			G_B8_0 = __this;
			goto IL_00cc;
		}
	}
	{
		StandardPurchasingModule_t4003664591 * L_26 = V_1;
		NullCheck(L_26);
		int32_t L_27 = StandardPurchasingModule_get_appStore_m2800198910(L_26, /*hidden argument*/NULL);
		G_B9_0 = ((((int32_t)L_27) == ((int32_t)5))? 1 : 0);
		G_B9_1 = G_B7_0;
		goto IL_00cd;
	}

IL_00cc:
	{
		G_B9_0 = 0;
		G_B9_1 = G_B8_0;
	}

IL_00cd:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_m_IsUnityChannelSelected_10((bool)G_B9_0);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_28 = V_0;
		NullCheck(L_28);
		ConfigurationBuilder_t1298400415 * L_29 = L_28->get_builder_0();
		NullCheck(L_29);
		Il2CppObject * L_30 = ConfigurationBuilder_Configure_TisIUnityChannelConfiguration_t1883011117_m1357048912(L_29, /*hidden argument*/ConfigurationBuilder_Configure_TisIUnityChannelConfiguration_t1883011117_m1357048912_MethodInfo_var);
		bool L_31 = __this->get_m_FetchReceiptPayloadOnPurchase_16();
		NullCheck(L_30);
		InterfaceActionInvoker1< bool >::Invoke(0 /* System.Void UnityEngine.Purchasing.IUnityChannelConfiguration::set_fetchReceiptPayloadOnPurchase(System.Boolean) */, IUnityChannelConfiguration_t1883011117_il2cpp_TypeInfo_var, L_30, L_31);
		ProductCatalog_t2667590766 * L_32 = ProductCatalog_LoadDefaultCatalog_m589345558(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_32;
		ProductCatalog_t2667590766 * L_33 = V_2;
		NullCheck(L_33);
		Il2CppObject* L_34 = ProductCatalog_get_allProducts_m1511651788(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Il2CppObject* L_35 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductCatalogItem>::GetEnumerator() */, IEnumerable_1_t1269839040_il2cpp_TypeInfo_var, L_34);
		V_4 = L_35;
	}

IL_00fb:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01b0;
		}

IL_0100:
		{
			Il2CppObject* L_36 = V_4;
			NullCheck(L_36);
			ProductCatalogItem_t977711995 * L_37 = InterfaceFuncInvoker0< ProductCatalogItem_t977711995 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductCatalogItem>::get_Current() */, IEnumerator_1_t2748203118_il2cpp_TypeInfo_var, L_36);
			V_3 = L_37;
			ProductCatalogItem_t977711995 * L_38 = V_3;
			NullCheck(L_38);
			Il2CppObject* L_39 = ProductCatalogItem_get_allStoreIDs_m2785218113(L_38, /*hidden argument*/NULL);
			NullCheck(L_39);
			int32_t L_40 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.StoreID>::get_Count() */, ICollection_1_t1423527629_il2cpp_TypeInfo_var, L_39);
			if ((((int32_t)L_40) <= ((int32_t)0)))
			{
				goto IL_0198;
			}
		}

IL_0119:
		{
			IDs_t3808979560 * L_41 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
			IDs__ctor_m2525416339(L_41, /*hidden argument*/NULL);
			V_5 = L_41;
			ProductCatalogItem_t977711995 * L_42 = V_3;
			NullCheck(L_42);
			Il2CppObject* L_43 = ProductCatalogItem_get_allStoreIDs_m2785218113(L_42, /*hidden argument*/NULL);
			NullCheck(L_43);
			Il2CppObject* L_44 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.StoreID>::GetEnumerator() */, IEnumerable_1_t763579369_il2cpp_TypeInfo_var, L_43);
			V_7 = L_44;
		}

IL_012d:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0159;
			}

IL_0132:
			{
				Il2CppObject* L_45 = V_7;
				NullCheck(L_45);
				StoreID_t471452324 * L_46 = InterfaceFuncInvoker0< StoreID_t471452324 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.StoreID>::get_Current() */, IEnumerator_1_t2241943447_il2cpp_TypeInfo_var, L_45);
				V_6 = L_46;
				IDs_t3808979560 * L_47 = V_5;
				StoreID_t471452324 * L_48 = V_6;
				NullCheck(L_48);
				String_t* L_49 = L_48->get_id_1();
				StringU5BU5D_t1642385972* L_50 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
				StoreID_t471452324 * L_51 = V_6;
				NullCheck(L_51);
				String_t* L_52 = L_51->get_store_0();
				NullCheck(L_50);
				ArrayElementTypeCheck (L_50, L_52);
				(L_50)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_52);
				NullCheck(L_47);
				IDs_Add_m217445704(L_47, L_49, L_50, /*hidden argument*/NULL);
			}

IL_0159:
			{
				Il2CppObject* L_53 = V_7;
				NullCheck(L_53);
				bool L_54 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_53);
				if (L_54)
				{
					goto IL_0132;
				}
			}

IL_0165:
			{
				IL2CPP_LEAVE(0x179, FINALLY_016a);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_016a;
		}

FINALLY_016a:
		{ // begin finally (depth: 2)
			{
				Il2CppObject* L_55 = V_7;
				if (!L_55)
				{
					goto IL_0178;
				}
			}

IL_0171:
			{
				Il2CppObject* L_56 = V_7;
				NullCheck(L_56);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_56);
			}

IL_0178:
			{
				IL2CPP_END_FINALLY(362)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(362)
		{
			IL2CPP_JUMP_TBL(0x179, IL_0179)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0179:
		{
			U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_57 = V_0;
			NullCheck(L_57);
			ConfigurationBuilder_t1298400415 * L_58 = L_57->get_builder_0();
			ProductCatalogItem_t977711995 * L_59 = V_3;
			NullCheck(L_59);
			String_t* L_60 = L_59->get_id_0();
			ProductCatalogItem_t977711995 * L_61 = V_3;
			NullCheck(L_61);
			int32_t L_62 = L_61->get_type_1();
			IDs_t3808979560 * L_63 = V_5;
			NullCheck(L_58);
			ConfigurationBuilder_AddProduct_m918244722(L_58, L_60, L_62, L_63, /*hidden argument*/NULL);
			goto IL_01b0;
		}

IL_0198:
		{
			U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_64 = V_0;
			NullCheck(L_64);
			ConfigurationBuilder_t1298400415 * L_65 = L_64->get_builder_0();
			ProductCatalogItem_t977711995 * L_66 = V_3;
			NullCheck(L_66);
			String_t* L_67 = L_66->get_id_0();
			ProductCatalogItem_t977711995 * L_68 = V_3;
			NullCheck(L_68);
			int32_t L_69 = L_68->get_type_1();
			NullCheck(L_65);
			ConfigurationBuilder_AddProduct_m3779153393(L_65, L_67, L_69, /*hidden argument*/NULL);
		}

IL_01b0:
		{
			Il2CppObject* L_70 = V_4;
			NullCheck(L_70);
			bool L_71 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_70);
			if (L_71)
			{
				goto IL_0100;
			}
		}

IL_01bc:
		{
			IL2CPP_LEAVE(0x1D0, FINALLY_01c1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01c1;
	}

FINALLY_01c1:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_72 = V_4;
			if (!L_72)
			{
				goto IL_01cf;
			}
		}

IL_01c8:
		{
			Il2CppObject* L_73 = V_4;
			NullCheck(L_73);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_73);
		}

IL_01cf:
		{
			IL2CPP_END_FINALLY(449)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(449)
	{
		IL2CPP_JUMP_TBL(0x1D0, IL_01d0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01d0:
	{
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_74 = V_0;
		NullCheck(L_74);
		ConfigurationBuilder_t1298400415 * L_75 = L_74->get_builder_0();
		IDs_t3808979560 * L_76 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_76, /*hidden argument*/NULL);
		V_8 = L_76;
		IDs_t3808979560 * L_77 = V_8;
		StringU5BU5D_t1642385972* L_78 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_78);
		ArrayElementTypeCheck (L_78, _stringLiteral2700534657);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_77);
		IDs_Add_m217445704(L_77, _stringLiteral2946374258, L_78, /*hidden argument*/NULL);
		IDs_t3808979560 * L_79 = V_8;
		StringU5BU5D_t1642385972* L_80 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_80);
		ArrayElementTypeCheck (L_80, _stringLiteral859071123);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral859071123);
		NullCheck(L_79);
		IDs_Add_m217445704(L_79, _stringLiteral473744409, L_80, /*hidden argument*/NULL);
		IDs_t3808979560 * L_81 = V_8;
		StringU5BU5D_t1642385972* L_82 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_82);
		ArrayElementTypeCheck (L_82, _stringLiteral1645711892);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1645711892);
		NullCheck(L_81);
		IDs_Add_m217445704(L_81, _stringLiteral1461038519, L_82, /*hidden argument*/NULL);
		IDs_t3808979560 * L_83 = V_8;
		NullCheck(L_75);
		ConfigurationBuilder_AddProduct_m918244722(L_75, _stringLiteral1830547213, 0, L_83, /*hidden argument*/NULL);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_84 = V_0;
		NullCheck(L_84);
		ConfigurationBuilder_t1298400415 * L_85 = L_84->get_builder_0();
		IDs_t3808979560 * L_86 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_86, /*hidden argument*/NULL);
		V_8 = L_86;
		IDs_t3808979560 * L_87 = V_8;
		StringU5BU5D_t1642385972* L_88 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_88);
		ArrayElementTypeCheck (L_88, _stringLiteral2700534657);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_87);
		IDs_Add_m217445704(L_87, _stringLiteral862026094, L_88, /*hidden argument*/NULL);
		IDs_t3808979560 * L_89 = V_8;
		StringU5BU5D_t1642385972* L_90 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_90);
		ArrayElementTypeCheck (L_90, _stringLiteral859071123);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral859071123);
		NullCheck(L_89);
		IDs_Add_m217445704(L_89, _stringLiteral2396058710, L_90, /*hidden argument*/NULL);
		IDs_t3808979560 * L_91 = V_8;
		StringU5BU5D_t1642385972* L_92 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_92);
		ArrayElementTypeCheck (L_92, _stringLiteral1645711892);
		(L_92)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1645711892);
		NullCheck(L_91);
		IDs_Add_m217445704(L_91, _stringLiteral4189921875, L_92, /*hidden argument*/NULL);
		IDs_t3808979560 * L_93 = V_8;
		NullCheck(L_85);
		ConfigurationBuilder_AddProduct_m918244722(L_85, _stringLiteral968850961, 0, L_93, /*hidden argument*/NULL);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_94 = V_0;
		NullCheck(L_94);
		ConfigurationBuilder_t1298400415 * L_95 = L_94->get_builder_0();
		IDs_t3808979560 * L_96 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_96, /*hidden argument*/NULL);
		V_8 = L_96;
		IDs_t3808979560 * L_97 = V_8;
		StringU5BU5D_t1642385972* L_98 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_98);
		ArrayElementTypeCheck (L_98, _stringLiteral2700534657);
		(L_98)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_97);
		IDs_Add_m217445704(L_97, _stringLiteral1346993374, L_98, /*hidden argument*/NULL);
		IDs_t3808979560 * L_99 = V_8;
		StringU5BU5D_t1642385972* L_100 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_100);
		ArrayElementTypeCheck (L_100, _stringLiteral859071123);
		(L_100)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral859071123);
		NullCheck(L_99);
		IDs_Add_m217445704(L_99, _stringLiteral1233259296, L_100, /*hidden argument*/NULL);
		IDs_t3808979560 * L_101 = V_8;
		NullCheck(L_95);
		ConfigurationBuilder_AddProduct_m918244722(L_95, _stringLiteral1070925405, 1, L_101, /*hidden argument*/NULL);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_102 = V_0;
		NullCheck(L_102);
		ConfigurationBuilder_t1298400415 * L_103 = L_102->get_builder_0();
		IDs_t3808979560 * L_104 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
		IDs__ctor_m2525416339(L_104, /*hidden argument*/NULL);
		V_8 = L_104;
		IDs_t3808979560 * L_105 = V_8;
		StringU5BU5D_t1642385972* L_106 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_106);
		ArrayElementTypeCheck (L_106, _stringLiteral2700534657);
		(L_106)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2700534657);
		NullCheck(L_105);
		IDs_Add_m217445704(L_105, _stringLiteral784127634, L_106, /*hidden argument*/NULL);
		IDs_t3808979560 * L_107 = V_8;
		NullCheck(L_103);
		ConfigurationBuilder_AddProduct_m918244722(L_103, _stringLiteral2315729507, 2, L_107, /*hidden argument*/NULL);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_108 = V_0;
		NullCheck(L_108);
		ConfigurationBuilder_t1298400415 * L_109 = L_108->get_builder_0();
		NullCheck(L_109);
		Il2CppObject * L_110 = ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324(L_109, /*hidden argument*/ConfigurationBuilder_Configure_TisIAmazonConfiguration_t3016942165_m4264701324_MethodInfo_var);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_111 = V_0;
		NullCheck(L_111);
		ConfigurationBuilder_t1298400415 * L_112 = L_111->get_builder_0();
		NullCheck(L_112);
		HashSet_1_t275936122 * L_113 = ConfigurationBuilder_get_products_m3201377931(L_112, /*hidden argument*/NULL);
		NullCheck(L_110);
		InterfaceActionInvoker1< HashSet_1_t275936122 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IAmazonConfiguration::WriteSandboxJSON(System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>) */, IAmazonConfiguration_t3016942165_il2cpp_TypeInfo_var, L_110, L_113);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_114 = V_0;
		NullCheck(L_114);
		ConfigurationBuilder_t1298400415 * L_115 = L_114->get_builder_0();
		NullCheck(L_115);
		Il2CppObject * L_116 = ConfigurationBuilder_Configure_TisISamsungAppsConfiguration_t4066821689_m2324730914(L_115, /*hidden argument*/ConfigurationBuilder_Configure_TisISamsungAppsConfiguration_t4066821689_m2324730914_MethodInfo_var);
		NullCheck(L_116);
		InterfaceActionInvoker1< int32_t >::Invoke(0 /* System.Void UnityEngine.Purchasing.ISamsungAppsConfiguration::SetMode(UnityEngine.Purchasing.SamsungAppsMode) */, ISamsungAppsConfiguration_t4066821689_il2cpp_TypeInfo_var, L_116, 1);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_117 = V_0;
		NullCheck(L_117);
		ConfigurationBuilder_t1298400415 * L_118 = L_117->get_builder_0();
		NullCheck(L_118);
		Il2CppObject * L_119 = ConfigurationBuilder_Configure_TisITizenStoreConfiguration_t2900348728_m1247129795(L_118, /*hidden argument*/ConfigurationBuilder_Configure_TisITizenStoreConfiguration_t2900348728_m1247129795_MethodInfo_var);
		NullCheck(L_119);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void UnityEngine.Purchasing.ITizenStoreConfiguration::SetGroupId(System.String) */, ITizenStoreConfiguration_t2900348728_il2cpp_TypeInfo_var, L_119, _stringLiteral1451147361);
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_120 = V_0;
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_121 = V_0;
		IntPtr_t L_122;
		L_122.set_m_value_0((void*)(void*)U3CAwakeU3Ec__AnonStorey0_U3CU3Em__0_m3852349590_MethodInfo_var);
		Action_t3226471752 * L_123 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_123, L_121, L_122, /*hidden argument*/NULL);
		NullCheck(L_120);
		L_120->set_initializeUnityIap_1(L_123);
		bool L_124 = __this->get_m_IsUnityChannelSelected_10();
		V_9 = L_124;
		bool L_125 = V_9;
		if (L_125)
		{
			goto IL_0398;
		}
	}
	{
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_126 = V_0;
		NullCheck(L_126);
		Action_t3226471752 * L_127 = L_126->get_initializeUnityIap_1();
		NullCheck(L_127);
		Action_Invoke_m3801112262(L_127, /*hidden argument*/NULL);
		goto IL_042e;
	}

IL_0398:
	{
		AppInfo_t2080248435 * L_128 = (AppInfo_t2080248435 *)il2cpp_codegen_object_new(AppInfo_t2080248435_il2cpp_TypeInfo_var);
		AppInfo__ctor_m2870035846(L_128, /*hidden argument*/NULL);
		V_10 = L_128;
		AppInfo_t2080248435 * L_129 = V_10;
		NullCheck(L_129);
		AppInfo_set_appId_m1112756485(L_129, _stringLiteral2297758188, /*hidden argument*/NULL);
		AppInfo_t2080248435 * L_130 = V_10;
		NullCheck(L_130);
		AppInfo_set_appKey_m3344021451(L_130, _stringLiteral2288302069, /*hidden argument*/NULL);
		AppInfo_t2080248435 * L_131 = V_10;
		NullCheck(L_131);
		AppInfo_set_clientId_m1298558813(L_131, _stringLiteral1739072345, /*hidden argument*/NULL);
		AppInfo_t2080248435 * L_132 = V_10;
		NullCheck(L_132);
		AppInfo_set_clientKey_m1161742527(L_132, _stringLiteral3545049635, /*hidden argument*/NULL);
		AppInfo_t2080248435 * L_133 = V_10;
		NullCheck(L_133);
		AppInfo_set_debug_m4042117453(L_133, (bool)0, /*hidden argument*/NULL);
		UnityChannelLoginHandler_t2253884029 * L_134 = (UnityChannelLoginHandler_t2253884029 *)il2cpp_codegen_object_new(UnityChannelLoginHandler_t2253884029_il2cpp_TypeInfo_var);
		UnityChannelLoginHandler__ctor_m3480826282(L_134, /*hidden argument*/NULL);
		__this->set_unityChannelLoginHandler_15(L_134);
		UnityChannelLoginHandler_t2253884029 * L_135 = __this->get_unityChannelLoginHandler_15();
		Action_1_t1831019615 * L_136 = ((IAPDemo_t823941185_StaticFields*)IAPDemo_t823941185_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_20();
		G_B30_0 = L_135;
		if (L_136)
		{
			G_B31_0 = L_135;
			goto IL_0400;
		}
	}
	{
		IntPtr_t L_137;
		L_137.set_m_value_0((void*)(void*)IAPDemo_U3CAwakeU3Em__0_m3196988796_MethodInfo_var);
		Action_1_t1831019615 * L_138 = (Action_1_t1831019615 *)il2cpp_codegen_object_new(Action_1_t1831019615_il2cpp_TypeInfo_var);
		Action_1__ctor_m1193014778(L_138, NULL, L_137, /*hidden argument*/Action_1__ctor_m1193014778_MethodInfo_var);
		((IAPDemo_t823941185_StaticFields*)IAPDemo_t823941185_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_20(L_138);
		G_B31_0 = G_B30_0;
	}

IL_0400:
	{
		Action_1_t1831019615 * L_139 = ((IAPDemo_t823941185_StaticFields*)IAPDemo_t823941185_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_20();
		NullCheck(G_B31_0);
		G_B31_0->set_initializeFailedAction_1(L_139);
		UnityChannelLoginHandler_t2253884029 * L_140 = __this->get_unityChannelLoginHandler_15();
		U3CAwakeU3Ec__AnonStorey0_t2163001806 * L_141 = V_0;
		IntPtr_t L_142;
		L_142.set_m_value_0((void*)(void*)U3CAwakeU3Ec__AnonStorey0_U3CU3Em__1_m3852349495_MethodInfo_var);
		Action_t3226471752 * L_143 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_143, L_141, L_142, /*hidden argument*/NULL);
		NullCheck(L_140);
		L_140->set_initializeSucceededAction_0(L_143);
		AppInfo_t2080248435 * L_144 = V_10;
		UnityChannelLoginHandler_t2253884029 * L_145 = __this->get_unityChannelLoginHandler_15();
		IL2CPP_RUNTIME_CLASS_INIT(StoreService_t3339196318_il2cpp_TypeInfo_var);
		StoreService_Initialize_m2334922442(NULL /*static, unused*/, L_144, L_145, /*hidden argument*/NULL);
	}

IL_042e:
	{
		return;
	}
}
// System.Void IAPDemo::OnTransactionsRestored(System.Boolean)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1900125737;
extern const uint32_t IAPDemo_OnTransactionsRestored_m2633548551_MetadataUsageId;
extern "C"  void IAPDemo_OnTransactionsRestored_m2633548551 (IAPDemo_t823941185 * __this, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnTransactionsRestored_m2633548551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1900125737, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::OnDeferred(UnityEngine.Purchasing.Product)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1205201122;
extern const uint32_t IAPDemo_OnDeferred_m801123048_MetadataUsageId;
extern "C"  void IAPDemo_OnDeferred_m801123048 (IAPDemo_t823941185 * __this, Product_t1203687971 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_OnDeferred_m801123048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Product_t1203687971 * L_0 = ___item0;
		NullCheck(L_0);
		ProductDefinition_t1942475268 * L_1 = Product_get_definition_m2035415516(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_id_m264072292(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1205201122, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::InitUI(System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>)
extern Il2CppClass* IEnumerable_1_t1495815016_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2974179094_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductType_t2754455291_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OptionData_t2420267500_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3438463199_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m3486116920_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__1_m934010843_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m4197675336_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m404084168_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__2_m905417093_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__3_m905417060_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__4_m905417027_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__5_m905416994_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4265067804;
extern const uint32_t IAPDemo_InitUI_m3350388155_MetadataUsageId;
extern "C"  void IAPDemo_InitUI_m3350388155 (IAPDemo_t823941185 * __this, Il2CppObject* ___items0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_InitUI_m3350388155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Product_t1203687971 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dropdown_t1985816271 * L_0 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		__this->set_m_InteractableSelectable_19(L_0);
		bool L_1 = IAPDemo_NeedRestoreButton_m207213810(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		Button_t2872111280 * L_2 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0028:
	{
		Button_t2872111280 * L_4 = IAPDemo_GetRegisterButton_m1221450815(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		bool L_6 = IAPDemo_NeedRegisterButton_m2725603907(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, L_6, /*hidden argument*/NULL);
		Button_t2872111280 * L_7 = IAPDemo_GetLoginButton_m4237916829(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(L_7, /*hidden argument*/NULL);
		bool L_9 = IAPDemo_NeedLoginButton_m2037060853(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, L_9, /*hidden argument*/NULL);
		Button_t2872111280 * L_10 = IAPDemo_GetValidateButton_m3687892462(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(L_10, /*hidden argument*/NULL);
		bool L_12 = IAPDemo_NeedValidateButton_m1619059604(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_SetActive_m2887581199(L_11, L_12, /*hidden argument*/NULL);
		Il2CppObject* L_13 = ___items0;
		NullCheck(L_13);
		Il2CppObject* L_14 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>::GetEnumerator() */, IEnumerable_1_t1495815016_il2cpp_TypeInfo_var, L_13);
		V_1 = L_14;
	}

IL_0071:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b9;
		}

IL_0076:
		{
			Il2CppObject* L_15 = V_1;
			NullCheck(L_15);
			Product_t1203687971 * L_16 = InterfaceFuncInvoker0< Product_t1203687971 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.Product>::get_Current() */, IEnumerator_1_t2974179094_il2cpp_TypeInfo_var, L_15);
			V_0 = L_16;
			Product_t1203687971 * L_17 = V_0;
			NullCheck(L_17);
			ProductDefinition_t1942475268 * L_18 = Product_get_definition_m2035415516(L_17, /*hidden argument*/NULL);
			NullCheck(L_18);
			String_t* L_19 = ProductDefinition_get_id_m264072292(L_18, /*hidden argument*/NULL);
			Product_t1203687971 * L_20 = V_0;
			NullCheck(L_20);
			ProductDefinition_t1942475268 * L_21 = Product_get_definition_m2035415516(L_20, /*hidden argument*/NULL);
			NullCheck(L_21);
			int32_t L_22 = ProductDefinition_get_type_m590914665(L_21, /*hidden argument*/NULL);
			int32_t L_23 = L_22;
			Il2CppObject * L_24 = Box(ProductType_t2754455291_il2cpp_TypeInfo_var, &L_23);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_25 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral4265067804, L_19, L_24, /*hidden argument*/NULL);
			V_2 = L_25;
			Dropdown_t1985816271 * L_26 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
			NullCheck(L_26);
			List_1_t1789388632 * L_27 = Dropdown_get_options_m2669836220(L_26, /*hidden argument*/NULL);
			String_t* L_28 = V_2;
			OptionData_t2420267500 * L_29 = (OptionData_t2420267500 *)il2cpp_codegen_object_new(OptionData_t2420267500_il2cpp_TypeInfo_var);
			OptionData__ctor_m743450704(L_29, L_28, /*hidden argument*/NULL);
			NullCheck(L_27);
			List_1_Add_m3486116920(L_27, L_29, /*hidden argument*/List_1_Add_m3486116920_MethodInfo_var);
		}

IL_00b9:
		{
			Il2CppObject* L_30 = V_1;
			NullCheck(L_30);
			bool L_31 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_30);
			if (L_31)
			{
				goto IL_0076;
			}
		}

IL_00c4:
		{
			IL2CPP_LEAVE(0xD6, FINALLY_00c9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c9;
	}

FINALLY_00c9:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_32 = V_1;
			if (!L_32)
			{
				goto IL_00d5;
			}
		}

IL_00cf:
		{
			Il2CppObject* L_33 = V_1;
			NullCheck(L_33);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_33);
		}

IL_00d5:
		{
			IL2CPP_END_FINALLY(201)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(201)
	{
		IL2CPP_JUMP_TBL(0xD6, IL_00d6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00d6:
	{
		Dropdown_t1985816271 * L_34 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		Dropdown_RefreshShownValue_m3113581237(L_34, /*hidden argument*/NULL);
		Dropdown_t1985816271 * L_35 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		DropdownEvent_t2203087800 * L_36 = Dropdown_get_onValueChanged_m3334401942(L_35, /*hidden argument*/NULL);
		IntPtr_t L_37;
		L_37.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__1_m934010843_MethodInfo_var);
		UnityAction_1_t3438463199 * L_38 = (UnityAction_1_t3438463199 *)il2cpp_codegen_object_new(UnityAction_1_t3438463199_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4197675336(L_38, __this, L_37, /*hidden argument*/UnityAction_1__ctor_m4197675336_MethodInfo_var);
		NullCheck(L_36);
		UnityEvent_1_AddListener_m404084168(L_36, L_38, /*hidden argument*/UnityEvent_1_AddListener_m404084168_MethodInfo_var);
		Button_t2872111280 * L_39 = IAPDemo_GetBuyButton_m514831370(__this, /*hidden argument*/NULL);
		NullCheck(L_39);
		ButtonClickedEvent_t2455055323 * L_40 = Button_get_onClick_m1595880935(L_39, /*hidden argument*/NULL);
		IntPtr_t L_41;
		L_41.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__2_m905417093_MethodInfo_var);
		UnityAction_t4025899511 * L_42 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_42, __this, L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		UnityEvent_AddListener_m1596810379(L_40, L_42, /*hidden argument*/NULL);
		Button_t2872111280 * L_43 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_44 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_43, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0146;
		}
	}
	{
		Button_t2872111280 * L_45 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		NullCheck(L_45);
		ButtonClickedEvent_t2455055323 * L_46 = Button_get_onClick_m1595880935(L_45, /*hidden argument*/NULL);
		IntPtr_t L_47;
		L_47.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__3_m905417060_MethodInfo_var);
		UnityAction_t4025899511 * L_48 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_48, __this, L_47, /*hidden argument*/NULL);
		NullCheck(L_46);
		UnityEvent_AddListener_m1596810379(L_46, L_48, /*hidden argument*/NULL);
	}

IL_0146:
	{
		Button_t2872111280 * L_49 = IAPDemo_GetLoginButton_m4237916829(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_50 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_49, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_017e;
		}
	}
	{
		bool L_51 = __this->get_m_IsUnityChannelSelected_10();
		if (!L_51)
		{
			goto IL_017e;
		}
	}
	{
		Button_t2872111280 * L_52 = IAPDemo_GetLoginButton_m4237916829(__this, /*hidden argument*/NULL);
		NullCheck(L_52);
		ButtonClickedEvent_t2455055323 * L_53 = Button_get_onClick_m1595880935(L_52, /*hidden argument*/NULL);
		IntPtr_t L_54;
		L_54.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__4_m905417027_MethodInfo_var);
		UnityAction_t4025899511 * L_55 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_55, __this, L_54, /*hidden argument*/NULL);
		NullCheck(L_53);
		UnityEvent_AddListener_m1596810379(L_53, L_55, /*hidden argument*/NULL);
	}

IL_017e:
	{
		Button_t2872111280 * L_56 = IAPDemo_GetValidateButton_m3687892462(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_57 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_56, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_01b6;
		}
	}
	{
		bool L_58 = __this->get_m_IsUnityChannelSelected_10();
		if (!L_58)
		{
			goto IL_01b6;
		}
	}
	{
		Button_t2872111280 * L_59 = IAPDemo_GetValidateButton_m3687892462(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		ButtonClickedEvent_t2455055323 * L_60 = Button_get_onClick_m1595880935(L_59, /*hidden argument*/NULL);
		IntPtr_t L_61;
		L_61.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__5_m905416994_MethodInfo_var);
		UnityAction_t4025899511 * L_62 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_62, __this, L_61, /*hidden argument*/NULL);
		NullCheck(L_60);
		UnityEvent_AddListener_m1596810379(L_60, L_62, /*hidden argument*/NULL);
	}

IL_01b6:
	{
		return;
	}
}
// System.Void IAPDemo::UpdateHistoryUI()
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral676257233;
extern Il2CppCodeGenString* _stringLiteral1576255139;
extern Il2CppCodeGenString* _stringLiteral2162321594;
extern const uint32_t IAPDemo_UpdateHistoryUI_m2607563357_MetadataUsageId;
extern "C"  void IAPDemo_UpdateHistoryUI_m2607563357 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_UpdateHistoryUI_m2607563357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	Product_t1203687971 * V_2 = NULL;
	ProductU5BU5D_t728099314* V_3 = NULL;
	int32_t V_4 = 0;
	bool V_5 = false;
	{
		Il2CppObject * L_0 = __this->get_m_Controller_2();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		V_0 = _stringLiteral676257233;
		V_1 = _stringLiteral1576255139;
		Il2CppObject * L_1 = __this->get_m_Controller_2();
		NullCheck(L_1);
		ProductCollection_t3600019299 * L_2 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		ProductU5BU5D_t728099314* L_3 = ProductCollection_get_all_m3364167965(L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		V_4 = 0;
		goto IL_007b;
	}

IL_0031:
	{
		ProductU5BU5D_t728099314* L_4 = V_3;
		int32_t L_5 = V_4;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Product_t1203687971 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = L_7;
		String_t* L_8 = V_0;
		Product_t1203687971 * L_9 = V_2;
		NullCheck(L_9);
		ProductDefinition_t1942475268 * L_10 = Product_get_definition_m2035415516(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = ProductDefinition_get_id_m264072292(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m612901809(NULL /*static, unused*/, L_8, _stringLiteral2162321594, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		String_t* L_13 = V_1;
		String_t* L_14 = String_Concat_m2596409543(NULL /*static, unused*/, L_13, _stringLiteral2162321594, /*hidden argument*/NULL);
		V_1 = L_14;
		String_t* L_15 = V_1;
		Product_t1203687971 * L_16 = V_2;
		NullCheck(L_16);
		bool L_17 = Product_get_hasReceipt_m617723237(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		String_t* L_18 = Boolean_ToString_m1253164328((&V_5), /*hidden argument*/NULL);
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		int32_t L_20 = V_4;
		V_4 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_007b:
	{
		int32_t L_21 = V_4;
		ProductU5BU5D_t728099314* L_22 = V_3;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		Text_t356221433 * L_23 = IAPDemo_GetText_m3382056873(__this, (bool)0, /*hidden argument*/NULL);
		String_t* L_24 = V_0;
		NullCheck(L_23);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_23, L_24);
		Text_t356221433 * L_25 = IAPDemo_GetText_m3382056873(__this, (bool)1, /*hidden argument*/NULL);
		String_t* L_26 = V_1;
		NullCheck(L_25);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_25, L_26);
		return;
	}
}
// System.Void IAPDemo::UpdateInteractable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t IAPDemo_UpdateInteractable_m2228859499_MetadataUsageId;
extern "C"  void IAPDemo_UpdateInteractable_m2228859499 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_UpdateInteractable_m2228859499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Selectable_t1490392188 * L_0 = __this->get_m_InteractableSelectable_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Il2CppObject * L_2 = __this->get_m_Controller_2();
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_0;
		Selectable_t1490392188 * L_4 = __this->get_m_InteractableSelectable_19();
		NullCheck(L_4);
		bool L_5 = Selectable_get_interactable_m1725029500(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)L_5)))
		{
			goto IL_007d;
		}
	}
	{
		Button_t2872111280 * L_6 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Button_t2872111280 * L_8 = IAPDemo_GetRestoreButton_m3830435552(__this, /*hidden argument*/NULL);
		bool L_9 = V_0;
		NullCheck(L_8);
		Selectable_set_interactable_m63718297(L_8, L_9, /*hidden argument*/NULL);
	}

IL_004d:
	{
		Button_t2872111280 * L_10 = IAPDemo_GetBuyButton_m514831370(__this, /*hidden argument*/NULL);
		bool L_11 = V_0;
		NullCheck(L_10);
		Selectable_set_interactable_m63718297(L_10, L_11, /*hidden argument*/NULL);
		Dropdown_t1985816271 * L_12 = IAPDemo_GetDropdown_m1658389538(__this, /*hidden argument*/NULL);
		bool L_13 = V_0;
		NullCheck(L_12);
		Selectable_set_interactable_m63718297(L_12, L_13, /*hidden argument*/NULL);
		Button_t2872111280 * L_14 = IAPDemo_GetRegisterButton_m1221450815(__this, /*hidden argument*/NULL);
		bool L_15 = V_0;
		NullCheck(L_14);
		Selectable_set_interactable_m63718297(L_14, L_15, /*hidden argument*/NULL);
		Button_t2872111280 * L_16 = IAPDemo_GetLoginButton_m4237916829(__this, /*hidden argument*/NULL);
		bool L_17 = V_0;
		NullCheck(L_16);
		Selectable_set_interactable_m63718297(L_16, L_17, /*hidden argument*/NULL);
	}

IL_007d:
	{
		return;
	}
}
// System.Void IAPDemo::Update()
extern "C"  void IAPDemo_Update_m412123535 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	{
		IAPDemo_UpdateInteractable_m2228859499(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean IAPDemo::NeedRestoreButton()
extern "C"  bool IAPDemo_NeedRestoreButton_m207213810 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	int32_t G_B8_0 = 0;
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)8)))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_2 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)31))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)((int32_t)18))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_4 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)19))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_5 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)20))))
		{
			goto IL_004e;
		}
	}
	{
		bool L_6 = __this->get_m_IsCloudMoolahStoreSelected_9();
		G_B8_0 = ((int32_t)(L_6));
		goto IL_004f;
	}

IL_004e:
	{
		G_B8_0 = 1;
	}

IL_004f:
	{
		return (bool)G_B8_0;
	}
}
// System.Boolean IAPDemo::NeedRegisterButton()
extern "C"  bool IAPDemo_NeedRegisterButton_m2725603907 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean IAPDemo::NeedLoginButton()
extern "C"  bool IAPDemo_NeedLoginButton_m2037060853 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_IsUnityChannelSelected_10();
		return L_0;
	}
}
// System.Boolean IAPDemo::NeedValidateButton()
extern "C"  bool IAPDemo_NeedValidateButton_m1619059604 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_IsUnityChannelSelected_10();
		return L_0;
	}
}
// UnityEngine.UI.Text IAPDemo::GetText(System.Boolean)
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3611362783;
extern Il2CppCodeGenString* _stringLiteral3611362761;
extern const uint32_t IAPDemo_GetText_m3382056873_MetadataUsageId;
extern "C"  Text_t356221433 * IAPDemo_GetText_m3382056873 (IAPDemo_t823941185 * __this, bool ___right0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetText_m3382056873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		bool L_0 = ___right0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = _stringLiteral3611362783;
		goto IL_0015;
	}

IL_0010:
	{
		G_B3_0 = _stringLiteral3611362761;
	}

IL_0015:
	{
		V_0 = G_B3_0;
		String_t* L_1 = V_0;
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Text_t356221433 * L_3 = GameObject_GetComponent_TisText_t356221433_m4280536079(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var);
		return L_3;
	}
}
// UnityEngine.UI.Dropdown IAPDemo::GetDropdown()
extern const MethodInfo* GameObject_GetComponent_TisDropdown_t1985816271_m1750975685_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1625601915;
extern const uint32_t IAPDemo_GetDropdown_m1658389538_MetadataUsageId;
extern "C"  Dropdown_t1985816271 * IAPDemo_GetDropdown_m1658389538 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetDropdown_m1658389538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1625601915, /*hidden argument*/NULL);
		NullCheck(L_0);
		Dropdown_t1985816271 * L_1 = GameObject_GetComponent_TisDropdown_t1985816271_m1750975685(L_0, /*hidden argument*/GameObject_GetComponent_TisDropdown_t1985816271_m1750975685_MethodInfo_var);
		return L_1;
	}
}
// UnityEngine.UI.Button IAPDemo::GetBuyButton()
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2309168590;
extern const uint32_t IAPDemo_GetBuyButton_m514831370_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetBuyButton_m514831370 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetBuyButton_m514831370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2309168590, /*hidden argument*/NULL);
		NullCheck(L_0);
		Button_t2872111280 * L_1 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_0, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		return L_1;
	}
}
// UnityEngine.UI.Button IAPDemo::GetRestoreButton()
extern Il2CppCodeGenString* _stringLiteral1335657580;
extern const uint32_t IAPDemo_GetRestoreButton_m3830435552_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetRestoreButton_m3830435552 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetRestoreButton_m3830435552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Button_t2872111280 * L_0 = IAPDemo_GetButton_m892847222(__this, _stringLiteral1335657580, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.UI.Button IAPDemo::GetRegisterButton()
extern Il2CppCodeGenString* _stringLiteral24124625;
extern const uint32_t IAPDemo_GetRegisterButton_m1221450815_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetRegisterButton_m1221450815 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetRegisterButton_m1221450815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Button_t2872111280 * L_0 = IAPDemo_GetButton_m892847222(__this, _stringLiteral24124625, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.UI.Button IAPDemo::GetLoginButton()
extern Il2CppCodeGenString* _stringLiteral4283667535;
extern const uint32_t IAPDemo_GetLoginButton_m4237916829_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetLoginButton_m4237916829 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetLoginButton_m4237916829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Button_t2872111280 * L_0 = IAPDemo_GetButton_m892847222(__this, _stringLiteral4283667535, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.UI.Button IAPDemo::GetValidateButton()
extern Il2CppCodeGenString* _stringLiteral3570268284;
extern const uint32_t IAPDemo_GetValidateButton_m3687892462_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetValidateButton_m3687892462 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetValidateButton_m3687892462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Button_t2872111280 * L_0 = IAPDemo_GetButton_m892847222(__this, _stringLiteral3570268284, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.UI.Button IAPDemo::GetButton(System.String)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern const uint32_t IAPDemo_GetButton_m892847222_MetadataUsageId;
extern "C"  Button_t2872111280 * IAPDemo_GetButton_m892847222 (IAPDemo_t823941185 * __this, String_t* ___buttonName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_GetButton_m892847222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		String_t* L_0 = ___buttonName0;
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck(L_4);
		Button_t2872111280 * L_5 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_4, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		return L_5;
	}

IL_001a:
	{
		return (Button_t2872111280 *)NULL;
	}
}
// System.Void IAPDemo::LogProductDefinitions()
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductType_t2754455291_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2284497275;
extern const uint32_t IAPDemo_LogProductDefinitions_m1239761007_MetadataUsageId;
extern "C"  void IAPDemo_LogProductDefinitions_m1239761007 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_LogProductDefinitions_m1239761007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ProductU5BU5D_t728099314* V_0 = NULL;
	Product_t1203687971 * V_1 = NULL;
	ProductU5BU5D_t728099314* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Il2CppObject * L_0 = __this->get_m_Controller_2();
		NullCheck(L_0);
		ProductCollection_t3600019299 * L_1 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_0);
		NullCheck(L_1);
		ProductU5BU5D_t728099314* L_2 = ProductCollection_get_all_m3364167965(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ProductU5BU5D_t728099314* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_0061;
	}

IL_001a:
	{
		ProductU5BU5D_t728099314* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Product_t1203687971 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		Product_t1203687971 * L_8 = V_1;
		NullCheck(L_8);
		ProductDefinition_t1942475268 * L_9 = Product_get_definition_m2035415516(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = ProductDefinition_get_id_m264072292(L_9, /*hidden argument*/NULL);
		Product_t1203687971 * L_11 = V_1;
		NullCheck(L_11);
		ProductDefinition_t1942475268 * L_12 = Product_get_definition_m2035415516(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = ProductDefinition_get_storeSpecificId_m2251287741(L_12, /*hidden argument*/NULL);
		Product_t1203687971 * L_14 = V_1;
		NullCheck(L_14);
		ProductDefinition_t1942475268 * L_15 = Product_get_definition_m2035415516(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = ProductDefinition_get_type_m590914665(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		Il2CppObject * L_17 = Box(ProductType_t2754455291_il2cpp_TypeInfo_var, (&V_4));
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral2284497275, L_10, L_13, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_3;
		V_3 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_21 = V_3;
		ProductU5BU5D_t728099314* L_22 = V_2;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		return;
	}
}
// System.Void IAPDemo::<Awake>m__0(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2192145769;
extern const uint32_t IAPDemo_U3CAwakeU3Em__0_m3196988796_MetadataUsageId;
extern "C"  void IAPDemo_U3CAwakeU3Em__0_m3196988796 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CAwakeU3Em__0_m3196988796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2192145769, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__1(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral253251037;
extern const uint32_t IAPDemo_U3CInitUIU3Em__1_m934010843_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__1_m934010843 (IAPDemo_t823941185 * __this, int32_t ___selectedItem0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__1_m934010843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___selectedItem0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral253251037, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___selectedItem0;
		__this->set_m_SelectedItemIndex_17(L_4);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__2()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1306873581;
extern Il2CppCodeGenString* _stringLiteral2214379573;
extern Il2CppCodeGenString* _stringLiteral466078066;
extern const uint32_t IAPDemo_U3CInitUIU3Em__2_m905417093_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__2_m905417093 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__2_m905417093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_PurchaseInProgress_18();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1306873581, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
		bool L_1 = IAPDemo_NeedLoginButton_m2037060853(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		bool L_2 = __this->get_m_IsLoggedIn_14();
		if (L_2)
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2214379573, /*hidden argument*/NULL);
	}

IL_0036:
	{
		__this->set_m_PurchaseInProgress_18((bool)1);
		Il2CppObject * L_3 = __this->get_m_Controller_2();
		Il2CppObject * L_4 = __this->get_m_Controller_2();
		NullCheck(L_4);
		ProductCollection_t3600019299 * L_5 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_4);
		NullCheck(L_5);
		ProductU5BU5D_t728099314* L_6 = ProductCollection_get_all_m3364167965(L_5, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_m_SelectedItemIndex_17();
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Product_t1203687971 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_3);
		InterfaceActionInvoker2< Product_t1203687971 *, String_t* >::Invoke(1 /* System.Void UnityEngine.Purchasing.IStoreController::InitiatePurchase(UnityEngine.Purchasing.Product,System.String) */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_3, L_9, _stringLiteral466078066);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__3()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2289103034_il2cpp_TypeInfo_var;
extern Il2CppClass* IMoolahExtension_t3195861654_il2cpp_TypeInfo_var;
extern Il2CppClass* IMicrosoftExtensions_t1101930285_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern Il2CppClass* IAppleExtensions_t1627764765_il2cpp_TypeInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__6_m494375636_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3424424418_MethodInfo_var;
extern const MethodInfo* IAPDemo_OnTransactionsRestored_m2633548551_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m309821356_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral778054244;
extern const uint32_t IAPDemo_U3CInitUIU3Em__3_m905417060_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__3_m905417060 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__3_m905417060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_IsCloudMoolahStoreSelected_9();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		bool L_1 = __this->get_m_IsLoggedIn_14();
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral778054244, /*hidden argument*/NULL);
		goto IL_003c;
	}

IL_0025:
	{
		Il2CppObject * L_2 = __this->get_m_MoolahExtensions_4();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__6_m494375636_MethodInfo_var);
		Action_1_t2289103034 * L_4 = (Action_1_t2289103034 *)il2cpp_codegen_object_new(Action_1_t2289103034_il2cpp_TypeInfo_var);
		Action_1__ctor_m3424424418(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m3424424418_MethodInfo_var);
		NullCheck(L_2);
		InterfaceActionInvoker1< Action_1_t2289103034 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IMoolahExtension::RestoreTransactionID(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>) */, IMoolahExtension_t3195861654_il2cpp_TypeInfo_var, L_2, L_4);
	}

IL_003c:
	{
		goto IL_008c;
	}

IL_0041:
	{
		int32_t L_5 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)18))))
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_6 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)((int32_t)19))))
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_7 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_0075;
		}
	}

IL_0065:
	{
		Il2CppObject * L_8 = __this->get_m_MicrosoftExtensions_6();
		NullCheck(L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.Purchasing.IMicrosoftExtensions::RestoreTransactions() */, IMicrosoftExtensions_t1101930285_il2cpp_TypeInfo_var, L_8);
		goto IL_008c;
	}

IL_0075:
	{
		Il2CppObject * L_9 = __this->get_m_AppleExtensions_3();
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IAPDemo_OnTransactionsRestored_m2633548551_MethodInfo_var);
		Action_1_t3627374100 * L_11 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_11, __this, L_10, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		NullCheck(L_9);
		InterfaceActionInvoker1< Action_1_t3627374100 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>) */, IAppleExtensions_t1627764765_il2cpp_TypeInfo_var, L_9, L_11);
	}

IL_008c:
	{
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__4()
extern Il2CppClass* Action_1_t543755129_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1831019615_il2cpp_TypeInfo_var;
extern Il2CppClass* StoreService_t3339196318_il2cpp_TypeInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__7_m2263701873_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2060447650_MethodInfo_var;
extern const MethodInfo* IAPDemo_U3CInitUIU3Em__8_m2741746229_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1193014778_MethodInfo_var;
extern const uint32_t IAPDemo_U3CInitUIU3Em__4_m905417027_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__4_m905417027 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__4_m905417027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityChannelLoginHandler_t2253884029 * L_0 = __this->get_unityChannelLoginHandler_15();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__7_m2263701873_MethodInfo_var);
		Action_1_t543755129 * L_2 = (Action_1_t543755129 *)il2cpp_codegen_object_new(Action_1_t543755129_il2cpp_TypeInfo_var);
		Action_1__ctor_m2060447650(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m2060447650_MethodInfo_var);
		NullCheck(L_0);
		L_0->set_loginSucceededAction_2(L_2);
		UnityChannelLoginHandler_t2253884029 * L_3 = __this->get_unityChannelLoginHandler_15();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IAPDemo_U3CInitUIU3Em__8_m2741746229_MethodInfo_var);
		Action_1_t1831019615 * L_5 = (Action_1_t1831019615 *)il2cpp_codegen_object_new(Action_1_t1831019615_il2cpp_TypeInfo_var);
		Action_1__ctor_m1193014778(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m1193014778_MethodInfo_var);
		NullCheck(L_3);
		L_3->set_loginFailedAction_3(L_5);
		UnityChannelLoginHandler_t2253884029 * L_6 = __this->get_unityChannelLoginHandler_15();
		IL2CPP_RUNTIME_CLASS_INIT(StoreService_t3339196318_il2cpp_TypeInfo_var);
		StoreService_Login_m2535849060(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__5()
extern Il2CppClass* U3CInitUIU3Ec__AnonStorey1_t996539604_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3864773326_il2cpp_TypeInfo_var;
extern Il2CppClass* IUnityChannelExtensions_t4012708657_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CInitUIU3Ec__AnonStorey1_U3CU3Em__0_m1768299555_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m3578948342_MethodInfo_var;
extern const uint32_t IAPDemo_U3CInitUIU3Em__5_m905416994_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__5_m905416994 (IAPDemo_t823941185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__5_m905416994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CInitUIU3Ec__AnonStorey1_t996539604 * V_0 = NULL;
	{
		U3CInitUIU3Ec__AnonStorey1_t996539604 * L_0 = (U3CInitUIU3Ec__AnonStorey1_t996539604 *)il2cpp_codegen_object_new(U3CInitUIU3Ec__AnonStorey1_t996539604_il2cpp_TypeInfo_var);
		U3CInitUIU3Ec__AnonStorey1__ctor_m1248594915(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CInitUIU3Ec__AnonStorey1_t996539604 * L_1 = V_0;
		String_t* L_2 = __this->get_m_LastTransationID_11();
		NullCheck(L_1);
		L_1->set_txId_0(L_2);
		Il2CppObject * L_3 = __this->get_m_UnityChannelExtensions_7();
		U3CInitUIU3Ec__AnonStorey1_t996539604 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_txId_0();
		U3CInitUIU3Ec__AnonStorey1_t996539604 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CInitUIU3Ec__AnonStorey1_U3CU3Em__0_m1768299555_MethodInfo_var);
		Action_3_t3864773326 * L_8 = (Action_3_t3864773326 *)il2cpp_codegen_object_new(Action_3_t3864773326_il2cpp_TypeInfo_var);
		Action_3__ctor_m3578948342(L_8, L_6, L_7, /*hidden argument*/Action_3__ctor_m3578948342_MethodInfo_var);
		NullCheck(L_3);
		InterfaceActionInvoker2< String_t*, Action_3_t3864773326 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IUnityChannelExtensions::ValidateReceipt(System.String,System.Action`3<System.Boolean,System.String,System.String>) */, IUnityChannelExtensions_t4012708657_il2cpp_TypeInfo_var, L_3, L_5, L_8);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__6(UnityEngine.Purchasing.RestoreTransactionIDState)
extern Il2CppClass* RestoreTransactionIDState_t2487303652_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral509298699;
extern const uint32_t IAPDemo_U3CInitUIU3Em__6_m494375636_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__6_m494375636 (IAPDemo_t823941185 * __this, int32_t ___restoreTransactionIDState0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__6_m494375636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = Box(RestoreTransactionIDState_t2487303652_il2cpp_TypeInfo_var, (&___restoreTransactionIDState0));
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral509298699, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___restoreTransactionIDState0;
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_4 = ___restoreTransactionIDState0;
		G_B3_0 = ((((int32_t)((((int32_t)L_4) == ((int32_t)3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002d;
	}

IL_002c:
	{
		G_B3_0 = 0;
	}

IL_002d:
	{
		V_0 = (bool)G_B3_0;
		bool L_5 = V_0;
		IAPDemo_OnTransactionsRestored_m2633548551(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__7(UnityEngine.Store.UserInfo)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3610799505;
extern const uint32_t IAPDemo_U3CInitUIU3Em__7_m2263701873_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__7_m2263701873 (IAPDemo_t823941185 * __this, UserInfo_t741955747 * ___userInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__7_m2263701873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_IsLoggedIn_14((bool)1);
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3));
		UserInfo_t741955747 * L_1 = ___userInfo0;
		NullCheck(L_1);
		String_t* L_2 = UserInfo_get_channel_m1234361509(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		ObjectU5BU5D_t3614634134* L_3 = L_0;
		UserInfo_t741955747 * L_4 = ___userInfo0;
		NullCheck(L_4);
		String_t* L_5 = UserInfo_get_userId_m3160010982(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t3614634134* L_6 = L_3;
		UserInfo_t741955747 * L_7 = ___userInfo0;
		NullCheck(L_7);
		String_t* L_8 = UserInfo_get_userLoginToken_m706751899(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_8);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogFormat_m3816524933(NULL /*static, unused*/, _stringLiteral3610799505, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo::<InitUI>m__8(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1388309222;
extern const uint32_t IAPDemo_U3CInitUIU3Em__8_m2741746229_MetadataUsageId;
extern "C"  void IAPDemo_U3CInitUIU3Em__8_m2741746229 (IAPDemo_t823941185 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPDemo_U3CInitUIU3Em__8_m2741746229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_IsLoggedIn_14((bool)0);
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1388309222, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo/<Awake>c__AnonStorey0::.ctor()
extern "C"  void U3CAwakeU3Ec__AnonStorey0__ctor_m1436498841 (U3CAwakeU3Ec__AnonStorey0_t2163001806 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo/<Awake>c__AnonStorey0::<>m__0()
extern "C"  void U3CAwakeU3Ec__AnonStorey0_U3CU3Em__0_m3852349590 (U3CAwakeU3Ec__AnonStorey0_t2163001806 * __this, const MethodInfo* method)
{
	{
		IAPDemo_t823941185 * L_0 = __this->get_U24this_2();
		ConfigurationBuilder_t1298400415 * L_1 = __this->get_builder_0();
		UnityPurchasing_Initialize_m1012234273(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo/<Awake>c__AnonStorey0::<>m__1()
extern "C"  void U3CAwakeU3Ec__AnonStorey0_U3CU3Em__1_m3852349495 (U3CAwakeU3Ec__AnonStorey0_t2163001806 * __this, const MethodInfo* method)
{
	{
		Action_t3226471752 * L_0 = __this->get_initializeUnityIap_1();
		NullCheck(L_0);
		Action_Invoke_m3801112262(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo/<InitUI>c__AnonStorey1::.ctor()
extern "C"  void U3CInitUIU3Ec__AnonStorey1__ctor_m1248594915 (U3CInitUIU3Ec__AnonStorey1_t996539604 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo/<InitUI>c__AnonStorey1::<>m__0(System.Boolean,System.String,System.String)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3549087679;
extern const uint32_t U3CInitUIU3Ec__AnonStorey1_U3CU3Em__0_m1768299555_MetadataUsageId;
extern "C"  void U3CInitUIU3Ec__AnonStorey1_U3CU3Em__0_m1768299555 (U3CInitUIU3Ec__AnonStorey1_t996539604 * __this, bool ___success0, String_t* ___signData1, String_t* ___signature2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CInitUIU3Ec__AnonStorey1_U3CU3Em__0_m1768299555_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_1 = __this->get_txId_0();
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t3614634134* L_2 = L_0;
		bool L_3 = ___success0;
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t3614634134* L_6 = L_2;
		String_t* L_7 = ___signData1;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_6;
		String_t* L_9 = ___signature2;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogFormat_m3816524933(NULL /*static, unused*/, _stringLiteral3549087679, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo/UnityChannelLoginHandler::.ctor()
extern "C"  void UnityChannelLoginHandler__ctor_m3480826282 (UnityChannelLoginHandler_t2253884029 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo/UnityChannelLoginHandler::OnInitialized()
extern "C"  void UnityChannelLoginHandler_OnInitialized_m541166015 (UnityChannelLoginHandler_t2253884029 * __this, const MethodInfo* method)
{
	{
		Action_t3226471752 * L_0 = __this->get_initializeSucceededAction_0();
		NullCheck(L_0);
		Action_Invoke_m3801112262(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo/UnityChannelLoginHandler::OnInitializeFailed(System.String)
extern const MethodInfo* Action_1_Invoke_m3811540703_MethodInfo_var;
extern const uint32_t UnityChannelLoginHandler_OnInitializeFailed_m1588211160_MetadataUsageId;
extern "C"  void UnityChannelLoginHandler_OnInitializeFailed_m1588211160 (UnityChannelLoginHandler_t2253884029 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelLoginHandler_OnInitializeFailed_m1588211160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t1831019615 * L_0 = __this->get_initializeFailedAction_1();
		String_t* L_1 = ___message0;
		NullCheck(L_0);
		Action_1_Invoke_m3811540703(L_0, L_1, /*hidden argument*/Action_1_Invoke_m3811540703_MethodInfo_var);
		return;
	}
}
// System.Void IAPDemo/UnityChannelLoginHandler::OnLogin(UnityEngine.Store.UserInfo)
extern const MethodInfo* Action_1_Invoke_m1491830263_MethodInfo_var;
extern const uint32_t UnityChannelLoginHandler_OnLogin_m2000746577_MetadataUsageId;
extern "C"  void UnityChannelLoginHandler_OnLogin_m2000746577 (UnityChannelLoginHandler_t2253884029 * __this, UserInfo_t741955747 * ___userInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelLoginHandler_OnLogin_m2000746577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t543755129 * L_0 = __this->get_loginSucceededAction_2();
		UserInfo_t741955747 * L_1 = ___userInfo0;
		NullCheck(L_0);
		Action_1_Invoke_m1491830263(L_0, L_1, /*hidden argument*/Action_1_Invoke_m1491830263_MethodInfo_var);
		return;
	}
}
// System.Void IAPDemo/UnityChannelLoginHandler::OnLoginFailed(System.String)
extern const MethodInfo* Action_1_Invoke_m3811540703_MethodInfo_var;
extern const uint32_t UnityChannelLoginHandler_OnLoginFailed_m2261121525_MetadataUsageId;
extern "C"  void UnityChannelLoginHandler_OnLoginFailed_m2261121525 (UnityChannelLoginHandler_t2253884029 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelLoginHandler_OnLoginFailed_m2261121525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t1831019615 * L_0 = __this->get_loginFailedAction_3();
		String_t* L_1 = ___message0;
		NullCheck(L_0);
		Action_1_Invoke_m3811540703(L_0, L_1, /*hidden argument*/Action_1_Invoke_m3811540703_MethodInfo_var);
		return;
	}
}
// System.Void IAPDemo/UnityChannelPurchaseError::.ctor()
extern "C"  void UnityChannelPurchaseError__ctor_m1716331930 (UnityChannelPurchaseError_t594865545 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IAPDemo/UnityChannelPurchaseInfo::.ctor()
extern "C"  void UnityChannelPurchaseInfo__ctor_m932370414 (UnityChannelPurchaseInfo_t855507751 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.DemoInventory::.ctor()
extern "C"  void DemoInventory__ctor_m3811379471 (DemoInventory_t2756307745 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.DemoInventory::Fulfill(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1830547213;
extern Il2CppCodeGenString* _stringLiteral852982118;
extern Il2CppCodeGenString* _stringLiteral2538700183;
extern const uint32_t DemoInventory_Fulfill_m2814084525_MetadataUsageId;
extern "C"  void DemoInventory_Fulfill_m2814084525 (DemoInventory_t2756307745 * __this, String_t* ___productId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DemoInventory_Fulfill_m2814084525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___productId0;
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_1 = ___productId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral1830547213, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		goto IL_002a;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral852982118, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_002a:
	{
		String_t* L_3 = ___productId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2538700183, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_003f:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::.ctor()
extern "C"  void IAPButton__ctor_m3388164132 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	{
		__this->set_consumePurchase_4((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisButton_t2872111280_m3197608264_MethodInfo_var;
extern const MethodInfo* IAPButton_PurchaseProduct_m225290346_MethodInfo_var;
extern const MethodInfo* IAPButton_Restore_m2361172162_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1042601999;
extern Il2CppCodeGenString* _stringLiteral624664449;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern const uint32_t IAPButton_Start_m3426314436_MetadataUsageId;
extern "C"  void IAPButton_Start_m3426314436 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_Start_m3426314436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	{
		Button_t2872111280 * L_0 = Component_GetComponent_TisButton_t2872111280_m3197608264(__this, /*hidden argument*/Component_GetComponent_TisButton_t2872111280_m3197608264_MethodInfo_var);
		V_0 = L_0;
		int32_t L_1 = __this->get_buttonType_3();
		if (L_1)
		{
			goto IL_0082;
		}
	}
	{
		Button_t2872111280 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		Button_t2872111280 * L_4 = V_0;
		NullCheck(L_4);
		ButtonClickedEvent_t2455055323 * L_5 = Button_get_onClick_m1595880935(L_4, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IAPButton_PurchaseProduct_m225290346_MethodInfo_var);
		UnityAction_t4025899511 * L_7 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		UnityEvent_AddListener_m1596810379(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		String_t* L_8 = __this->get_productId_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1042601999, /*hidden argument*/NULL);
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_10 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_11 = __this->get_productId_2();
		NullCheck(L_10);
		bool L_12 = IAPButtonStoreManager_HasProductInCatalog_m3023623296(L_10, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_007d;
		}
	}
	{
		String_t* L_13 = __this->get_productId_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral624664449, L_13, _stringLiteral372029312, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_007d:
	{
		goto IL_00b0;
	}

IL_0082:
	{
		int32_t L_15 = __this->get_buttonType_3();
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_00b0;
		}
	}
	{
		Button_t2872111280 * L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00b0;
		}
	}
	{
		Button_t2872111280 * L_18 = V_0;
		NullCheck(L_18);
		ButtonClickedEvent_t2455055323 * L_19 = Button_get_onClick_m1595880935(L_18, /*hidden argument*/NULL);
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)IAPButton_Restore_m2361172162_MethodInfo_var);
		UnityAction_t4025899511 * L_21 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_21, __this, L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		UnityEvent_AddListener_m1596810379(L_19, L_21, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::OnEnable()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const uint32_t IAPButton_OnEnable_m935239492_MetadataUsageId;
extern "C"  void IAPButton_OnEnable_m935239492 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_OnEnable_m935239492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_buttonType_3();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_1 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		IAPButtonStoreManager_AddButton_m3524258615(L_1, __this, /*hidden argument*/NULL);
		IAPButton_UpdateText_m1500550460(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::OnDisable()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const uint32_t IAPButton_OnDisable_m1628155281_MetadataUsageId;
extern "C"  void IAPButton_OnDisable_m1628155281 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_OnDisable_m1628155281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_buttonType_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_1 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		IAPButtonStoreManager_RemoveButton_m2931652138(L_1, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::PurchaseProduct()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1407997759;
extern const uint32_t IAPButton_PurchaseProduct_m225290346_MetadataUsageId;
extern "C"  void IAPButton_PurchaseProduct_m225290346 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_PurchaseProduct_m225290346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_buttonType_3();
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_1 = __this->get_productId_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1407997759, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_3 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = __this->get_productId_2();
		NullCheck(L_3);
		IAPButtonStoreManager_InitiatePurchase_m374565683(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::Restore()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern Il2CppClass* IMicrosoftExtensions_t1101930285_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern Il2CppClass* IAppleExtensions_t1627764765_il2cpp_TypeInfo_var;
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern Il2CppClass* ISamsungAppsExtensions_t3429739537_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2289103034_il2cpp_TypeInfo_var;
extern Il2CppClass* IMoolahExtension_t3195861654_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimePlatform_t1869584967_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIMicrosoftExtensions_t1101930285_m566281781_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var;
extern const MethodInfo* IAPButton_OnTransactionsRestored_m3924036197_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m309821356_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisISamsungAppsExtensions_t3429739537_m4249086459_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIMoolahExtension_t3195861654_m2257603670_MethodInfo_var;
extern const MethodInfo* IAPButton_U3CRestoreU3Em__0_m4194234704_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3424424418_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1163488363;
extern const uint32_t IAPButton_Restore_m2361172162_MetadataUsageId;
extern "C"  void IAPButton_Restore_m2361172162 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_Restore_m2361172162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_buttonType_3();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0134;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)18))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_2 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)19))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_0049;
		}
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_4 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Il2CppObject * L_5 = IAPButtonStoreManager_get_ExtensionProvider_m473841661(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Il2CppObject * L_6 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIMicrosoftExtensions_t1101930285_m566281781_MethodInfo_var, L_5);
		NullCheck(L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UnityEngine.Purchasing.IMicrosoftExtensions::RestoreTransactions() */, IMicrosoftExtensions_t1101930285_il2cpp_TypeInfo_var, L_6);
		goto IL_0134;
	}

IL_0049:
	{
		int32_t L_7 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)8)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_8 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)1)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_9 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)31)))))
		{
			goto IL_0090;
		}
	}

IL_006b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_10 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		Il2CppObject * L_11 = IAPButtonStoreManager_get_ExtensionProvider_m473841661(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Il2CppObject * L_12 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var, L_11);
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)IAPButton_OnTransactionsRestored_m3924036197_MethodInfo_var);
		Action_1_t3627374100 * L_14 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_14, __this, L_13, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		NullCheck(L_12);
		InterfaceActionInvoker1< Action_1_t3627374100 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>) */, IAppleExtensions_t1627764765_il2cpp_TypeInfo_var, L_12, L_14);
		goto IL_0134;
	}

IL_0090:
	{
		int32_t L_15 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_00d1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_16 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = StandardPurchasingModule_get_appStore_m2800198910(L_16, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)4))))
		{
			goto IL_00d1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_18 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		Il2CppObject * L_19 = IAPButtonStoreManager_get_ExtensionProvider_m473841661(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Il2CppObject * L_20 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisISamsungAppsExtensions_t3429739537_m4249086459_MethodInfo_var, L_19);
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)IAPButton_OnTransactionsRestored_m3924036197_MethodInfo_var);
		Action_1_t3627374100 * L_22 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_22, __this, L_21, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		NullCheck(L_20);
		InterfaceActionInvoker1< Action_1_t3627374100 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.ISamsungAppsExtensions::RestoreTransactions(System.Action`1<System.Boolean>) */, ISamsungAppsExtensions_t3429739537_il2cpp_TypeInfo_var, L_20, L_22);
		goto IL_0134;
	}

IL_00d1:
	{
		int32_t L_23 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0112;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_24 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_25 = StandardPurchasingModule_get_appStore_m2800198910(L_24, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)3))))
		{
			goto IL_0112;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_26 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		Il2CppObject * L_27 = IAPButtonStoreManager_get_ExtensionProvider_m473841661(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Il2CppObject * L_28 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIMoolahExtension_t3195861654_m2257603670_MethodInfo_var, L_27);
		IntPtr_t L_29;
		L_29.set_m_value_0((void*)(void*)IAPButton_U3CRestoreU3Em__0_m4194234704_MethodInfo_var);
		Action_1_t2289103034 * L_30 = (Action_1_t2289103034 *)il2cpp_codegen_object_new(Action_1_t2289103034_il2cpp_TypeInfo_var);
		Action_1__ctor_m3424424418(L_30, __this, L_29, /*hidden argument*/Action_1__ctor_m3424424418_MethodInfo_var);
		NullCheck(L_28);
		InterfaceActionInvoker1< Action_1_t2289103034 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IMoolahExtension::RestoreTransactionID(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>) */, IMoolahExtension_t3195861654_il2cpp_TypeInfo_var, L_28, L_30);
		goto IL_0134;
	}

IL_0112:
	{
		int32_t L_31 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_31;
		Il2CppObject * L_32 = Box(RuntimePlatform_t1869584967_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m2596409543(NULL /*static, unused*/, L_33, _stringLiteral1163488363, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
	}

IL_0134:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::OnTransactionsRestored(System.Boolean)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3057734245;
extern const uint32_t IAPButton_OnTransactionsRestored_m3924036197_MetadataUsageId;
extern "C"  void IAPButton_OnTransactionsRestored_m3924036197 (IAPButton_t3077837360 * __this, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_OnTransactionsRestored_m3924036197_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___success0;
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3057734245, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult UnityEngine.Purchasing.IAPButton::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityEvent_1_Invoke_m3308421134_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3952801405;
extern const uint32_t IAPButton_ProcessPurchase_m705789944_MetadataUsageId;
extern "C"  int32_t IAPButton_ProcessPurchase_m705789944 (IAPButton_t3077837360 * __this, PurchaseEventArgs_t547992434 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_ProcessPurchase_m705789944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		PurchaseEventArgs_t547992434 * L_0 = ___e0;
		PurchaseEventArgs_t547992434 * L_1 = ___e0;
		NullCheck(L_1);
		Product_t1203687971 * L_2 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		ProductDefinition_t1942475268 * L_3 = Product_get_definition_m2035415516(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = ProductDefinition_get_id_m264072292(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3952801405, L_0, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		OnPurchaseCompletedEvent_t4018783659 * L_6 = __this->get_onPurchaseComplete_5();
		PurchaseEventArgs_t547992434 * L_7 = ___e0;
		NullCheck(L_7);
		Product_t1203687971 * L_8 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		UnityEvent_1_Invoke_m3308421134(L_6, L_8, /*hidden argument*/UnityEvent_1_Invoke_m3308421134_MethodInfo_var);
		bool L_9 = __this->get_consumePurchase_4();
		if (!L_9)
		{
			goto IL_0042;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0043;
	}

IL_0042:
	{
		G_B3_0 = 1;
	}

IL_0043:
	{
		return (int32_t)(G_B3_0);
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityEvent_2_Invoke_m3835768897_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral480745;
extern const uint32_t IAPButton_OnPurchaseFailed_m2148672501_MetadataUsageId;
extern "C"  void IAPButton_OnPurchaseFailed_m2148672501 (IAPButton_t3077837360 * __this, Product_t1203687971 * ___product0, int32_t ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_OnPurchaseFailed_m2148672501_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Product_t1203687971 * L_0 = ___product0;
		int32_t L_1 = ___reason1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral480745, L_0, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		OnPurchaseFailedEvent_t2813769101 * L_5 = __this->get_onPurchaseFailed_6();
		Product_t1203687971 * L_6 = ___product0;
		int32_t L_7 = ___reason1;
		NullCheck(L_5);
		UnityEvent_2_Invoke_m3835768897(L_5, L_6, L_7, /*hidden argument*/UnityEvent_2_Invoke_m3835768897_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::UpdateText()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t IAPButton_UpdateText_m1500550460_MetadataUsageId;
extern "C"  void IAPButton_UpdateText_m1500550460 (IAPButton_t3077837360 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButton_UpdateText_m1500550460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Product_t1203687971 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_0 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = __this->get_productId_2();
		NullCheck(L_0);
		Product_t1203687971 * L_2 = IAPButtonStoreManager_GetProduct_m1505647265(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Product_t1203687971 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_008c;
		}
	}
	{
		Text_t356221433 * L_4 = __this->get_titleText_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		Text_t356221433 * L_6 = __this->get_titleText_7();
		Product_t1203687971 * L_7 = V_0;
		NullCheck(L_7);
		ProductMetadata_t1573242544 * L_8 = Product_get_metadata_m263398044(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = ProductMetadata_get_localizedTitle_m1010599344(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_9);
	}

IL_003e:
	{
		Text_t356221433 * L_10 = __this->get_descriptionText_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0065;
		}
	}
	{
		Text_t356221433 * L_12 = __this->get_descriptionText_8();
		Product_t1203687971 * L_13 = V_0;
		NullCheck(L_13);
		ProductMetadata_t1573242544 * L_14 = Product_get_metadata_m263398044(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = ProductMetadata_get_localizedDescription_m3665253400(L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_15);
	}

IL_0065:
	{
		Text_t356221433 * L_16 = __this->get_priceText_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_16, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008c;
		}
	}
	{
		Text_t356221433 * L_18 = __this->get_priceText_9();
		Product_t1203687971 * L_19 = V_0;
		NullCheck(L_19);
		ProductMetadata_t1573242544 * L_20 = Product_get_metadata_m263398044(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = ProductMetadata_get_localizedPriceString_m2216629954(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_21);
	}

IL_008c:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton::<Restore>m__0(UnityEngine.Purchasing.RestoreTransactionIDState)
extern "C"  void IAPButton_U3CRestoreU3Em__0_m4194234704 (IAPButton_t3077837360 * __this, int32_t ___restoreTransactionIDState0, const MethodInfo* method)
{
	IAPButton_t3077837360 * G_B2_0 = NULL;
	IAPButton_t3077837360 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	IAPButton_t3077837360 * G_B3_1 = NULL;
	{
		int32_t L_0 = ___restoreTransactionIDState0;
		G_B1_0 = __this;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			G_B2_0 = __this;
			goto IL_0011;
		}
	}
	{
		int32_t L_1 = ___restoreTransactionIDState0;
		G_B3_0 = ((((int32_t)((((int32_t)L_1) == ((int32_t)3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		NullCheck(G_B3_1);
		IAPButton_OnTransactionsRestored_m3924036197(G_B3_1, (bool)G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::.ctor()
extern Il2CppClass* List_1_t2446958492_il2cpp_TypeInfo_var;
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern Il2CppClass* IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1288022886_MethodInfo_var;
extern const uint32_t IAPButtonStoreManager__ctor_m3083407869_MetadataUsageId;
extern "C"  void IAPButtonStoreManager__ctor_m3083407869 (IAPButtonStoreManager_t911589174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager__ctor_m3083407869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StandardPurchasingModule_t4003664591 * V_0 = NULL;
	ConfigurationBuilder_t1298400415 * V_1 = NULL;
	{
		List_1_t2446958492 * L_0 = (List_1_t2446958492 *)il2cpp_codegen_object_new(List_1_t2446958492_il2cpp_TypeInfo_var);
		List_1__ctor_m1288022886(L_0, /*hidden argument*/List_1__ctor_m1288022886_MethodInfo_var);
		__this->set_activeButtons_2(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ProductCatalog_t2667590766 * L_1 = ProductCatalog_LoadDefaultCatalog_m589345558(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_catalog_1(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_2 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		StandardPurchasingModule_t4003664591 * L_3 = V_0;
		NullCheck(L_3);
		StandardPurchasingModule_set_useFakeStoreUIMode_m3278247934(L_3, 1, /*hidden argument*/NULL);
		StandardPurchasingModule_t4003664591 * L_4 = V_0;
		ConfigurationBuilder_t1298400415 * L_5 = ConfigurationBuilder_Instance_m4243979412(NULL /*static, unused*/, L_4, ((IPurchasingModuleU5BU5D_t4128245854*)SZArrayNew(IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_1 = L_5;
		ProductCatalog_t2667590766 * L_6 = __this->get_catalog_1();
		IAPConfigurationHelper_PopulateConfigurationBuilder_m3523216658(NULL /*static, unused*/, (&V_1), L_6, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_7 = V_1;
		UnityPurchasing_Initialize_m1012234273(NULL /*static, unused*/, __this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::get_Instance()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const uint32_t IAPButtonStoreManager_get_Instance_m3910054168_MetadataUsageId;
extern "C"  IAPButtonStoreManager_t911589174 * IAPButtonStoreManager_get_Instance_m3910054168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_get_Instance_m3910054168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_0 = ((IAPButtonStoreManager_t911589174_StaticFields*)IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var->static_fields)->get_instance_0();
		return L_0;
	}
}
// UnityEngine.Purchasing.IStoreController UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::get_StoreController()
extern "C"  Il2CppObject * IAPButtonStoreManager_get_StoreController_m527834269 (IAPButtonStoreManager_t911589174 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_controller_4();
		return L_0;
	}
}
// UnityEngine.Purchasing.IExtensionProvider UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::get_ExtensionProvider()
extern "C"  Il2CppObject * IAPButtonStoreManager_get_ExtensionProvider_m473841661 (IAPButtonStoreManager_t911589174 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_extensions_5();
		return L_0;
	}
}
// System.Boolean UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::HasProductInCatalog(System.String)
extern Il2CppClass* IEnumerable_1_t1269839040_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2748203118_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t IAPButtonStoreManager_HasProductInCatalog_m3023623296_MetadataUsageId;
extern "C"  bool IAPButtonStoreManager_HasProductInCatalog_m3023623296 (IAPButtonStoreManager_t911589174 * __this, String_t* ___productID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_HasProductInCatalog_m3023623296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ProductCatalogItem_t977711995 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ProductCatalog_t2667590766 * L_0 = __this->get_catalog_1();
		NullCheck(L_0);
		Il2CppObject* L_1 = ProductCatalog_get_allProducts_m1511651788(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductCatalogItem>::GetEnumerator() */, IEnumerable_1_t1269839040_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_0016:
		{
			Il2CppObject* L_3 = V_1;
			NullCheck(L_3);
			ProductCatalogItem_t977711995 * L_4 = InterfaceFuncInvoker0< ProductCatalogItem_t977711995 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductCatalogItem>::get_Current() */, IEnumerator_1_t2748203118_il2cpp_TypeInfo_var, L_3);
			V_0 = L_4;
			ProductCatalogItem_t977711995 * L_5 = V_0;
			NullCheck(L_5);
			String_t* L_6 = L_5->get_id_0();
			String_t* L_7 = ___productID0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0035;
			}
		}

IL_002e:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x54, FINALLY_0045);
		}

IL_0035:
		{
			Il2CppObject* L_9 = V_1;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0016;
			}
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x52, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_11 = V_1;
			if (!L_11)
			{
				goto IL_0051;
			}
		}

IL_004b:
		{
			Il2CppObject* L_12 = V_1;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_12);
		}

IL_0051:
		{
			IL2CPP_END_FINALLY(69)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0052:
	{
		return (bool)0;
	}

IL_0054:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// UnityEngine.Purchasing.Product UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::GetProduct(System.String)
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t IAPButtonStoreManager_GetProduct_m1505647265_MetadataUsageId;
extern "C"  Product_t1203687971 * IAPButtonStoreManager_GetProduct_m1505647265 (IAPButtonStoreManager_t911589174 * __this, String_t* ___productID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_GetProduct_m1505647265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_controller_4();
		if (!L_0)
		{
			goto IL_0038;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_controller_4();
		NullCheck(L_1);
		ProductCollection_t3600019299 * L_2 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_1);
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_3 = ___productID0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0038;
		}
	}
	{
		Il2CppObject * L_5 = __this->get_controller_4();
		NullCheck(L_5);
		ProductCollection_t3600019299 * L_6 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_5);
		String_t* L_7 = ___productID0;
		NullCheck(L_6);
		Product_t1203687971 * L_8 = ProductCollection_WithID_m3999574440(L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0038:
	{
		return (Product_t1203687971 *)NULL;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::AddButton(UnityEngine.Purchasing.IAPButton)
extern const MethodInfo* List_1_Add_m3151945002_MethodInfo_var;
extern const uint32_t IAPButtonStoreManager_AddButton_m3524258615_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_AddButton_m3524258615 (IAPButtonStoreManager_t911589174 * __this, IAPButton_t3077837360 * ___button0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_AddButton_m3524258615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2446958492 * L_0 = __this->get_activeButtons_2();
		IAPButton_t3077837360 * L_1 = ___button0;
		NullCheck(L_0);
		List_1_Add_m3151945002(L_0, L_1, /*hidden argument*/List_1_Add_m3151945002_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::RemoveButton(UnityEngine.Purchasing.IAPButton)
extern const MethodInfo* List_1_Remove_m4067983529_MethodInfo_var;
extern const uint32_t IAPButtonStoreManager_RemoveButton_m2931652138_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_RemoveButton_m2931652138 (IAPButtonStoreManager_t911589174 * __this, IAPButton_t3077837360 * ___button0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_RemoveButton_m2931652138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2446958492 * L_0 = __this->get_activeButtons_2();
		IAPButton_t3077837360 * L_1 = ___button0;
		NullCheck(L_0);
		List_1_Remove_m4067983529(L_0, L_1, /*hidden argument*/List_1_Remove_m4067983529_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::AddListener(UnityEngine.Purchasing.IAPListener)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1119646850;
extern const uint32_t IAPButtonStoreManager_AddListener_m4120254135_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_AddListener_m4120254135 (IAPButtonStoreManager_t911589174 * __this, IAPListener_t3789552708 * ___listener0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_AddListener_m4120254135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IAPListener_t3789552708 * L_0 = __this->get_m_Listener_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1119646850, /*hidden argument*/NULL);
	}

IL_001b:
	{
		IAPListener_t3789552708 * L_2 = ___listener0;
		__this->set_m_Listener_3(L_2);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::RemoveListener(UnityEngine.Purchasing.IAPListener)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t IAPButtonStoreManager_RemoveListener_m1366820074_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_RemoveListener_m1366820074 (IAPButtonStoreManager_t911589174 * __this, IAPListener_t3789552708 * ___listener0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_RemoveListener_m1366820074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IAPListener_t3789552708 * L_0 = __this->get_m_Listener_3();
		IAPListener_t3789552708 * L_1 = ___listener0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		__this->set_m_Listener_3((IAPListener_t3789552708 *)NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::InitiatePurchase(System.String)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1699546439_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3656253947_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m708814495_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1115631095_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral606075875;
extern const uint32_t IAPButtonStoreManager_InitiatePurchase_m374565683_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_InitiatePurchase_m374565683 (IAPButtonStoreManager_t911589174 * __this, String_t* ___productID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_InitiatePurchase_m374565683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IAPButton_t3077837360 * V_0 = NULL;
	Enumerator_t1981688166  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get_controller_4();
		if (L_0)
		{
			goto IL_0067;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral606075875, /*hidden argument*/NULL);
		List_1_t2446958492 * L_1 = __this->get_activeButtons_2();
		NullCheck(L_1);
		Enumerator_t1981688166  L_2 = List_1_GetEnumerator_m1699546439(L_1, /*hidden argument*/List_1_GetEnumerator_m1699546439_MethodInfo_var);
		V_1 = L_2;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0047;
		}

IL_0026:
		{
			IAPButton_t3077837360 * L_3 = Enumerator_get_Current_m3656253947((&V_1), /*hidden argument*/Enumerator_get_Current_m3656253947_MethodInfo_var);
			V_0 = L_3;
			IAPButton_t3077837360 * L_4 = V_0;
			NullCheck(L_4);
			String_t* L_5 = L_4->get_productId_2();
			String_t* L_6 = ___productID0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0047;
			}
		}

IL_003f:
		{
			IAPButton_t3077837360 * L_8 = V_0;
			NullCheck(L_8);
			IAPButton_OnPurchaseFailed_m2148672501(L_8, (Product_t1203687971 *)NULL, 0, /*hidden argument*/NULL);
		}

IL_0047:
		{
			bool L_9 = Enumerator_MoveNext_m708814495((&V_1), /*hidden argument*/Enumerator_MoveNext_m708814495_MethodInfo_var);
			if (L_9)
			{
				goto IL_0026;
			}
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x66, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1115631095((&V_1), /*hidden argument*/Enumerator_Dispose_m1115631095_MethodInfo_var);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0066:
	{
		return;
	}

IL_0067:
	{
		Il2CppObject * L_10 = __this->get_controller_4();
		String_t* L_11 = ___productID0;
		NullCheck(L_10);
		InterfaceActionInvoker1< String_t* >::Invoke(3 /* System.Void UnityEngine.Purchasing.IStoreController::InitiatePurchase(System.String) */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_10, L_11);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern const MethodInfo* List_1_GetEnumerator_m1699546439_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3656253947_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m708814495_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1115631095_MethodInfo_var;
extern const uint32_t IAPButtonStoreManager_OnInitialized_m3699105403_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_OnInitialized_m3699105403 (IAPButtonStoreManager_t911589174 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_OnInitialized_m3699105403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IAPButton_t3077837360 * V_0 = NULL;
	Enumerator_t1981688166  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___controller0;
		__this->set_controller_4(L_0);
		Il2CppObject * L_1 = ___extensions1;
		__this->set_extensions_5(L_1);
		List_1_t2446958492 * L_2 = __this->get_activeButtons_2();
		NullCheck(L_2);
		Enumerator_t1981688166  L_3 = List_1_GetEnumerator_m1699546439(L_2, /*hidden argument*/List_1_GetEnumerator_m1699546439_MethodInfo_var);
		V_1 = L_3;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_001f:
		{
			IAPButton_t3077837360 * L_4 = Enumerator_get_Current_m3656253947((&V_1), /*hidden argument*/Enumerator_get_Current_m3656253947_MethodInfo_var);
			V_0 = L_4;
			IAPButton_t3077837360 * L_5 = V_0;
			NullCheck(L_5);
			IAPButton_UpdateText_m1500550460(L_5, /*hidden argument*/NULL);
		}

IL_002d:
		{
			bool L_6 = Enumerator_MoveNext_m708814495((&V_1), /*hidden argument*/Enumerator_MoveNext_m708814495_MethodInfo_var);
			if (L_6)
			{
				goto IL_001f;
			}
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x4C, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1115631095((&V_1), /*hidden argument*/Enumerator_Dispose_m1115631095_MethodInfo_var);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004c:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern Il2CppClass* InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4122218072;
extern const uint32_t IAPButtonStoreManager_OnInitializeFailed_m2396353392_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_OnInitializeFailed_m2396353392 (IAPButtonStoreManager_t911589174 * __this, int32_t ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_OnInitializeFailed_m2396353392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = Box(InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var, (&___error0));
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral4122218072, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1699546439_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3656253947_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m708814495_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1115631095_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral294341169;
extern Il2CppCodeGenString* _stringLiteral1482618225;
extern const uint32_t IAPButtonStoreManager_ProcessPurchase_m2373817739_MetadataUsageId;
extern "C"  int32_t IAPButtonStoreManager_ProcessPurchase_m2373817739 (IAPButtonStoreManager_t911589174 * __this, PurchaseEventArgs_t547992434 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_ProcessPurchase_m2373817739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IAPButton_t3077837360 * V_0 = NULL;
	Enumerator_t1981688166  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2446958492 * L_0 = __this->get_activeButtons_2();
		NullCheck(L_0);
		Enumerator_t1981688166  L_1 = List_1_GetEnumerator_m1699546439(L_0, /*hidden argument*/List_1_GetEnumerator_m1699546439_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0046;
		}

IL_0011:
		{
			IAPButton_t3077837360 * L_2 = Enumerator_get_Current_m3656253947((&V_1), /*hidden argument*/Enumerator_get_Current_m3656253947_MethodInfo_var);
			V_0 = L_2;
			IAPButton_t3077837360 * L_3 = V_0;
			NullCheck(L_3);
			String_t* L_4 = L_3->get_productId_2();
			PurchaseEventArgs_t547992434 * L_5 = ___e0;
			NullCheck(L_5);
			Product_t1203687971 * L_6 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_5, /*hidden argument*/NULL);
			NullCheck(L_6);
			ProductDefinition_t1942475268 * L_7 = Product_get_definition_m2035415516(L_6, /*hidden argument*/NULL);
			NullCheck(L_7);
			String_t* L_8 = ProductDefinition_get_id_m264072292(L_7, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, L_8, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_0046;
			}
		}

IL_0039:
		{
			IAPButton_t3077837360 * L_10 = V_0;
			PurchaseEventArgs_t547992434 * L_11 = ___e0;
			NullCheck(L_10);
			int32_t L_12 = IAPButton_ProcessPurchase_m705789944(L_10, L_11, /*hidden argument*/NULL);
			V_2 = L_12;
			IL2CPP_LEAVE(0xA9, FINALLY_0057);
		}

IL_0046:
		{
			bool L_13 = Enumerator_MoveNext_m708814495((&V_1), /*hidden argument*/Enumerator_MoveNext_m708814495_MethodInfo_var);
			if (L_13)
			{
				goto IL_0011;
			}
		}

IL_0052:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0057);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0057;
	}

FINALLY_0057:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1115631095((&V_1), /*hidden argument*/Enumerator_Dispose_m1115631095_MethodInfo_var);
		IL2CPP_END_FINALLY(87)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(87)
	{
		IL2CPP_JUMP_TBL(0xA9, IL_00a9)
		IL2CPP_JUMP_TBL(0x65, IL_0065)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0065:
	{
		IAPListener_t3789552708 * L_14 = __this->get_m_Listener_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0083;
		}
	}
	{
		IAPListener_t3789552708 * L_16 = __this->get_m_Listener_3();
		PurchaseEventArgs_t547992434 * L_17 = ___e0;
		NullCheck(L_16);
		int32_t L_18 = IAPListener_ProcessPurchase_m2550675376(L_16, L_17, /*hidden argument*/NULL);
		return L_18;
	}

IL_0083:
	{
		PurchaseEventArgs_t547992434 * L_19 = ___e0;
		NullCheck(L_19);
		Product_t1203687971 * L_20 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		ProductDefinition_t1942475268 * L_21 = Product_get_definition_m2035415516(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = ProductDefinition_get_id_m264072292(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral294341169, L_22, _stringLiteral1482618225, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		return (int32_t)(1);
	}

IL_00a9:
	{
		int32_t L_24 = V_2;
		return L_24;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1699546439_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3656253947_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m708814495_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1115631095_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4283580658;
extern Il2CppCodeGenString* _stringLiteral1746206827;
extern const uint32_t IAPButtonStoreManager_OnPurchaseFailed_m2419505534_MetadataUsageId;
extern "C"  void IAPButtonStoreManager_OnPurchaseFailed_m2419505534 (IAPButtonStoreManager_t911589174 * __this, Product_t1203687971 * ___product0, int32_t ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager_OnPurchaseFailed_m2419505534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IAPButton_t3077837360 * V_0 = NULL;
	Enumerator_t1981688166  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2446958492 * L_0 = __this->get_activeButtons_2();
		NullCheck(L_0);
		Enumerator_t1981688166  L_1 = List_1_GetEnumerator_m1699546439(L_0, /*hidden argument*/List_1_GetEnumerator_m1699546439_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0041;
		}

IL_0011:
		{
			IAPButton_t3077837360 * L_2 = Enumerator_get_Current_m3656253947((&V_1), /*hidden argument*/Enumerator_get_Current_m3656253947_MethodInfo_var);
			V_0 = L_2;
			IAPButton_t3077837360 * L_3 = V_0;
			NullCheck(L_3);
			String_t* L_4 = L_3->get_productId_2();
			Product_t1203687971 * L_5 = ___product0;
			NullCheck(L_5);
			ProductDefinition_t1942475268 * L_6 = Product_get_definition_m2035415516(L_5, /*hidden argument*/NULL);
			NullCheck(L_6);
			String_t* L_7 = ProductDefinition_get_id_m264072292(L_6, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0041;
			}
		}

IL_0034:
		{
			IAPButton_t3077837360 * L_9 = V_0;
			Product_t1203687971 * L_10 = ___product0;
			int32_t L_11 = ___reason1;
			NullCheck(L_9);
			IAPButton_OnPurchaseFailed_m2148672501(L_9, L_10, L_11, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x9E, FINALLY_0052);
		}

IL_0041:
		{
			bool L_12 = Enumerator_MoveNext_m708814495((&V_1), /*hidden argument*/Enumerator_MoveNext_m708814495_MethodInfo_var);
			if (L_12)
			{
				goto IL_0011;
			}
		}

IL_004d:
		{
			IL2CPP_LEAVE(0x60, FINALLY_0052);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1115631095((&V_1), /*hidden argument*/Enumerator_Dispose_m1115631095_MethodInfo_var);
		IL2CPP_END_FINALLY(82)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x9E, IL_009e)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0060:
	{
		IAPListener_t3789552708 * L_13 = __this->get_m_Listener_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_13, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007f;
		}
	}
	{
		IAPListener_t3789552708 * L_15 = __this->get_m_Listener_3();
		Product_t1203687971 * L_16 = ___product0;
		int32_t L_17 = ___reason1;
		NullCheck(L_15);
		IAPListener_OnPurchaseFailed_m1842770977(L_15, L_16, L_17, /*hidden argument*/NULL);
		return;
	}

IL_007f:
	{
		Product_t1203687971 * L_18 = ___product0;
		NullCheck(L_18);
		ProductDefinition_t1942475268 * L_19 = Product_get_definition_m2035415516(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_20 = ProductDefinition_get_id_m264072292(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral4283580658, L_20, _stringLiteral1746206827, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
	}

IL_009e:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/IAPButtonStoreManager::.cctor()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const uint32_t IAPButtonStoreManager__cctor_m2118049118_MetadataUsageId;
extern "C"  void IAPButtonStoreManager__cctor_m2118049118 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPButtonStoreManager__cctor_m2118049118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IAPButtonStoreManager_t911589174 * L_0 = (IAPButtonStoreManager_t911589174 *)il2cpp_codegen_object_new(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager__ctor_m3083407869(L_0, /*hidden argument*/NULL);
		((IAPButtonStoreManager_t911589174_StaticFields*)IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var->static_fields)->set_instance_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/OnPurchaseCompletedEvent::.ctor()
extern const MethodInfo* UnityEvent_1__ctor_m4041951455_MethodInfo_var;
extern const uint32_t OnPurchaseCompletedEvent__ctor_m3087740950_MetadataUsageId;
extern "C"  void OnPurchaseCompletedEvent__ctor_m3087740950 (OnPurchaseCompletedEvent_t4018783659 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnPurchaseCompletedEvent__ctor_m3087740950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m4041951455(__this, /*hidden argument*/UnityEvent_1__ctor_m4041951455_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPButton/OnPurchaseFailedEvent::.ctor()
extern const MethodInfo* UnityEvent_2__ctor_m2157306554_MethodInfo_var;
extern const uint32_t OnPurchaseFailedEvent__ctor_m1226889690_MetadataUsageId;
extern "C"  void OnPurchaseFailedEvent__ctor_m1226889690 (OnPurchaseFailedEvent_t2813769101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnPurchaseFailedEvent__ctor_m1226889690_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_2__ctor_m2157306554(__this, /*hidden argument*/UnityEvent_2__ctor_m2157306554_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPConfigurationHelper::PopulateConfigurationBuilder(UnityEngine.Purchasing.ConfigurationBuilder&,UnityEngine.Purchasing.ProductCatalog)
extern Il2CppClass* IEnumerable_1_t1269839040_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2748203118_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t1423527629_il2cpp_TypeInfo_var;
extern Il2CppClass* IDs_t3808979560_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t763579369_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2241943447_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t IAPConfigurationHelper_PopulateConfigurationBuilder_m3523216658_MetadataUsageId;
extern "C"  void IAPConfigurationHelper_PopulateConfigurationBuilder_m3523216658 (Il2CppObject * __this /* static, unused */, ConfigurationBuilder_t1298400415 ** ___builder0, ProductCatalog_t2667590766 * ___catalog1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPConfigurationHelper_PopulateConfigurationBuilder_m3523216658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ProductCatalogItem_t977711995 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	IDs_t3808979560 * V_2 = NULL;
	StoreID_t471452324 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ProductCatalog_t2667590766 * L_0 = ___catalog1;
		NullCheck(L_0);
		Il2CppObject* L_1 = ProductCatalog_get_allProducts_m1511651788(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductCatalogItem>::GetEnumerator() */, IEnumerable_1_t1269839040_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009b;
		}

IL_0011:
		{
			Il2CppObject* L_3 = V_1;
			NullCheck(L_3);
			ProductCatalogItem_t977711995 * L_4 = InterfaceFuncInvoker0< ProductCatalogItem_t977711995 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductCatalogItem>::get_Current() */, IEnumerator_1_t2748203118_il2cpp_TypeInfo_var, L_3);
			V_0 = L_4;
			V_2 = (IDs_t3808979560 *)NULL;
			ProductCatalogItem_t977711995 * L_5 = V_0;
			NullCheck(L_5);
			Il2CppObject* L_6 = ProductCatalogItem_get_allStoreIDs_m2785218113(L_5, /*hidden argument*/NULL);
			NullCheck(L_6);
			int32_t L_7 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.StoreID>::get_Count() */, ICollection_1_t1423527629_il2cpp_TypeInfo_var, L_6);
			if ((((int32_t)L_7) <= ((int32_t)0)))
			{
				goto IL_0086;
			}
		}

IL_002b:
		{
			IDs_t3808979560 * L_8 = (IDs_t3808979560 *)il2cpp_codegen_object_new(IDs_t3808979560_il2cpp_TypeInfo_var);
			IDs__ctor_m2525416339(L_8, /*hidden argument*/NULL);
			V_2 = L_8;
			ProductCatalogItem_t977711995 * L_9 = V_0;
			NullCheck(L_9);
			Il2CppObject* L_10 = ProductCatalogItem_get_allStoreIDs_m2785218113(L_9, /*hidden argument*/NULL);
			NullCheck(L_10);
			Il2CppObject* L_11 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.StoreID>::GetEnumerator() */, IEnumerable_1_t763579369_il2cpp_TypeInfo_var, L_10);
			V_4 = L_11;
		}

IL_003e:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0066;
			}

IL_0043:
			{
				Il2CppObject* L_12 = V_4;
				NullCheck(L_12);
				StoreID_t471452324 * L_13 = InterfaceFuncInvoker0< StoreID_t471452324 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.StoreID>::get_Current() */, IEnumerator_1_t2241943447_il2cpp_TypeInfo_var, L_12);
				V_3 = L_13;
				IDs_t3808979560 * L_14 = V_2;
				StoreID_t471452324 * L_15 = V_3;
				NullCheck(L_15);
				String_t* L_16 = L_15->get_id_1();
				StringU5BU5D_t1642385972* L_17 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
				StoreID_t471452324 * L_18 = V_3;
				NullCheck(L_18);
				String_t* L_19 = L_18->get_store_0();
				NullCheck(L_17);
				ArrayElementTypeCheck (L_17, L_19);
				(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_19);
				NullCheck(L_14);
				IDs_Add_m217445704(L_14, L_16, L_17, /*hidden argument*/NULL);
			}

IL_0066:
			{
				Il2CppObject* L_20 = V_4;
				NullCheck(L_20);
				bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_20);
				if (L_21)
				{
					goto IL_0043;
				}
			}

IL_0072:
			{
				IL2CPP_LEAVE(0x86, FINALLY_0077);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0077;
		}

FINALLY_0077:
		{ // begin finally (depth: 2)
			{
				Il2CppObject* L_22 = V_4;
				if (!L_22)
				{
					goto IL_0085;
				}
			}

IL_007e:
			{
				Il2CppObject* L_23 = V_4;
				NullCheck(L_23);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_23);
			}

IL_0085:
			{
				IL2CPP_END_FINALLY(119)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(119)
		{
			IL2CPP_JUMP_TBL(0x86, IL_0086)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0086:
		{
			ConfigurationBuilder_t1298400415 ** L_24 = ___builder0;
			ProductCatalogItem_t977711995 * L_25 = V_0;
			NullCheck(L_25);
			String_t* L_26 = L_25->get_id_0();
			ProductCatalogItem_t977711995 * L_27 = V_0;
			NullCheck(L_27);
			int32_t L_28 = L_27->get_type_1();
			IDs_t3808979560 * L_29 = V_2;
			NullCheck((*((ConfigurationBuilder_t1298400415 **)L_24)));
			ConfigurationBuilder_AddProduct_m918244722((*((ConfigurationBuilder_t1298400415 **)L_24)), L_26, L_28, L_29, /*hidden argument*/NULL);
		}

IL_009b:
		{
			Il2CppObject* L_30 = V_1;
			NullCheck(L_30);
			bool L_31 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_30);
			if (L_31)
			{
				goto IL_0011;
			}
		}

IL_00a6:
		{
			IL2CPP_LEAVE(0xB8, FINALLY_00ab);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ab;
	}

FINALLY_00ab:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_32 = V_1;
			if (!L_32)
			{
				goto IL_00b7;
			}
		}

IL_00b1:
		{
			Il2CppObject* L_33 = V_1;
			NullCheck(L_33);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_33);
		}

IL_00b7:
		{
			IL2CPP_END_FINALLY(171)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(171)
	{
		IL2CPP_JUMP_TBL(0xB8, IL_00b8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b8:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPListener::.ctor()
extern "C"  void IAPListener__ctor_m236275320 (IAPListener_t3789552708 * __this, const MethodInfo* method)
{
	{
		__this->set_consumePurchase_2((bool)1);
		__this->set_dontDestroyOnLoad_3((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPListener::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const uint32_t IAPListener_OnEnable_m231334472_MetadataUsageId;
extern "C"  void IAPListener_OnEnable_m231334472 (IAPListener_t3789552708 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPListener_OnEnable_m231334472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_dontDestroyOnLoad_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_2 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		IAPButtonStoreManager_AddListener_m4120254135(L_2, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPListener::OnDisable()
extern Il2CppClass* IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var;
extern const uint32_t IAPListener_OnDisable_m4022413485_MetadataUsageId;
extern "C"  void IAPListener_OnDisable_m4022413485 (IAPListener_t3789552708 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPListener_OnDisable_m4022413485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IAPButtonStoreManager_t911589174_il2cpp_TypeInfo_var);
		IAPButtonStoreManager_t911589174 * L_0 = IAPButtonStoreManager_get_Instance_m3910054168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		IAPButtonStoreManager_RemoveListener_m1366820074(L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult UnityEngine.Purchasing.IAPListener::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityEvent_1_Invoke_m3308421134_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4005125553;
extern const uint32_t IAPListener_ProcessPurchase_m2550675376_MetadataUsageId;
extern "C"  int32_t IAPListener_ProcessPurchase_m2550675376 (IAPListener_t3789552708 * __this, PurchaseEventArgs_t547992434 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPListener_ProcessPurchase_m2550675376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		PurchaseEventArgs_t547992434 * L_0 = ___e0;
		PurchaseEventArgs_t547992434 * L_1 = ___e0;
		NullCheck(L_1);
		Product_t1203687971 * L_2 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		ProductDefinition_t1942475268 * L_3 = Product_get_definition_m2035415516(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = ProductDefinition_get_id_m264072292(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral4005125553, L_0, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		OnPurchaseCompletedEvent_t3384326983 * L_6 = __this->get_onPurchaseComplete_4();
		PurchaseEventArgs_t547992434 * L_7 = ___e0;
		NullCheck(L_7);
		Product_t1203687971 * L_8 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		UnityEvent_1_Invoke_m3308421134(L_6, L_8, /*hidden argument*/UnityEvent_1_Invoke_m3308421134_MethodInfo_var);
		bool L_9 = __this->get_consumePurchase_2();
		if (!L_9)
		{
			goto IL_0042;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0043;
	}

IL_0042:
	{
		G_B3_0 = 1;
	}

IL_0043:
	{
		return (int32_t)(G_B3_0);
	}
}
// System.Void UnityEngine.Purchasing.IAPListener::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityEvent_2_Invoke_m3835768897_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4192015245;
extern const uint32_t IAPListener_OnPurchaseFailed_m1842770977_MetadataUsageId;
extern "C"  void IAPListener_OnPurchaseFailed_m1842770977 (IAPListener_t3789552708 * __this, Product_t1203687971 * ___product0, int32_t ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IAPListener_OnPurchaseFailed_m1842770977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Product_t1203687971 * L_0 = ___product0;
		int32_t L_1 = ___reason1;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral4192015245, L_0, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		OnPurchaseFailedEvent_t3694879381 * L_5 = __this->get_onPurchaseFailed_5();
		Product_t1203687971 * L_6 = ___product0;
		int32_t L_7 = ___reason1;
		NullCheck(L_5);
		UnityEvent_2_Invoke_m3835768897(L_5, L_6, L_7, /*hidden argument*/UnityEvent_2_Invoke_m3835768897_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent::.ctor()
extern const MethodInfo* UnityEvent_1__ctor_m4041951455_MethodInfo_var;
extern const uint32_t OnPurchaseCompletedEvent__ctor_m2960164866_MetadataUsageId;
extern "C"  void OnPurchaseCompletedEvent__ctor_m2960164866 (OnPurchaseCompletedEvent_t3384326983 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnPurchaseCompletedEvent__ctor_m2960164866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m4041951455(__this, /*hidden argument*/UnityEvent_1__ctor_m4041951455_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent::.ctor()
extern const MethodInfo* UnityEvent_2__ctor_m2157306554_MethodInfo_var;
extern const uint32_t OnPurchaseFailedEvent__ctor_m3253910082_MetadataUsageId;
extern "C"  void OnPurchaseFailedEvent__ctor_m3253910082 (OnPurchaseFailedEvent_t3694879381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OnPurchaseFailedEvent__ctor_m3253910082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_2__ctor_m2157306554(__this, /*hidden argument*/UnityEvent_2__ctor_m2157306554_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleTangle::.ctor()
extern "C"  void AppleTangle__ctor_m1226795583 (AppleTangle_t53875121 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] UnityEngine.Purchasing.Security.AppleTangle::Data()
extern Il2CppClass* AppleTangle_t53875121_il2cpp_TypeInfo_var;
extern const uint32_t AppleTangle_Data_m3366264679_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* AppleTangle_Data_m3366264679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleTangle_Data_m3366264679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AppleTangle_t53875121_il2cpp_TypeInfo_var);
		bool L_0 = ((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->get_IsPopulated_3();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return (ByteU5BU5D_t3397334013*)NULL;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AppleTangle_t53875121_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = ((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->get_data_0();
		Int32U5BU5D_t3030399641* L_2 = ((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->get_order_1();
		int32_t L_3 = ((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->get_key_2();
		ByteU5BU5D_t3397334013* L_4 = Obfuscator_DeObfuscate_m4253116256(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleTangle::.cctor()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* AppleTangle_t53875121_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305144____U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral173406109;
extern const uint32_t AppleTangle__cctor_m3030347248_MetadataUsageId;
extern "C"  void AppleTangle__cctor_m3030347248 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleTangle__cctor_m3030347248_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, _stringLiteral173406109, /*hidden argument*/NULL);
		((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->set_data_0(L_0);
		Int32U5BU5D_t3030399641* L_1 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)61)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305144____U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0_FieldInfo_var), /*hidden argument*/NULL);
		((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->set_order_1(L_1);
		((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->set_key_2(((int32_t)44));
		((AppleTangle_t53875121_StaticFields*)AppleTangle_t53875121_il2cpp_TypeInfo_var->static_fields)->set_IsPopulated_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayTangle::.ctor()
extern "C"  void GooglePlayTangle__ctor_m1761951308 (GooglePlayTangle_t2749524914 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] UnityEngine.Purchasing.Security.GooglePlayTangle::Data()
extern Il2CppClass* GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var;
extern const uint32_t GooglePlayTangle_Data_m2557246410_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* GooglePlayTangle_Data_m2557246410 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GooglePlayTangle_Data_m2557246410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var);
		bool L_0 = ((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->get_IsPopulated_3();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return (ByteU5BU5D_t3397334013*)NULL;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = ((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->get_data_0();
		Int32U5BU5D_t3030399641* L_2 = ((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->get_order_1();
		int32_t L_3 = ((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->get_key_2();
		ByteU5BU5D_t3397334013* L_4 = Obfuscator_DeObfuscate_m4253116256(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayTangle::.cctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const uint32_t GooglePlayTangle__cctor_m4030200525_MetadataUsageId;
extern "C"  void GooglePlayTangle__cctor_m4030200525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GooglePlayTangle__cctor_m4030200525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->set_data_0(L_1);
		((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->set_order_1(((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->set_key_2(0);
		((GooglePlayTangle_t2749524914_StaticFields*)GooglePlayTangle_t2749524914_il2cpp_TypeInfo_var->static_fields)->set_IsPopulated_3((bool)0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.UnityChannelTangle::.ctor()
extern "C"  void UnityChannelTangle__ctor_m2291896147 (UnityChannelTangle_t1696213691 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] UnityEngine.Purchasing.Security.UnityChannelTangle::Data()
extern Il2CppClass* UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var;
extern const uint32_t UnityChannelTangle_Data_m1711076387_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* UnityChannelTangle_Data_m1711076387 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelTangle_Data_m1711076387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var);
		bool L_0 = ((UnityChannelTangle_t1696213691_StaticFields*)UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var->static_fields)->get_IsPopulated_3();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return (ByteU5BU5D_t3397334013*)NULL;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = ((UnityChannelTangle_t1696213691_StaticFields*)UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var->static_fields)->get_data_0();
		Int32U5BU5D_t3030399641* L_2 = ((UnityChannelTangle_t1696213691_StaticFields*)UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var->static_fields)->get_order_1();
		int32_t L_3 = ((UnityChannelTangle_t1696213691_StaticFields*)UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var->static_fields)->get_key_2();
		ByteU5BU5D_t3397334013* L_4 = Obfuscator_DeObfuscate_m4253116256(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Purchasing.Security.UnityChannelTangle::.cctor()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4123081918;
extern const uint32_t UnityChannelTangle__cctor_m2915893586_MetadataUsageId;
extern "C"  void UnityChannelTangle__cctor_m2915893586 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelTangle__cctor_m2915893586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, _stringLiteral4123081918, /*hidden argument*/NULL);
		((UnityChannelTangle_t1696213691_StaticFields*)UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var->static_fields)->set_data_0(L_0);
		Int32U5BU5D_t3030399641* L_1 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)1);
		((UnityChannelTangle_t1696213691_StaticFields*)UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var->static_fields)->set_order_1(L_1);
		((UnityChannelTangle_t1696213691_StaticFields*)UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var->static_fields)->set_key_2(((int32_t)107));
		((UnityChannelTangle_t1696213691_StaticFields*)UnityChannelTangle_t1696213691_il2cpp_TypeInfo_var->static_fields)->set_IsPopulated_3((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
