﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.UnityChannelTangle
struct UnityChannelTangle_t1696213691;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.UnityChannelTangle::.ctor()
extern "C"  void UnityChannelTangle__ctor_m2291896147 (UnityChannelTangle_t1696213691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Purchasing.Security.UnityChannelTangle::Data()
extern "C"  ByteU5BU5D_t3397334013* UnityChannelTangle_Data_m1711076387 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.UnityChannelTangle::.cctor()
extern "C"  void UnityChannelTangle__cctor_m2915893586 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
