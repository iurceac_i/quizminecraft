﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainGame
struct MainGame_t3800664731;
// Question[]
struct QuestionU5BU5D_t1028533817;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void MainGame::.ctor()
extern "C"  void MainGame__ctor_m2380176706 (MainGame_t3800664731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::Awake()
extern "C"  void MainGame_Awake_m496679287 (MainGame_t3800664731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::Start()
extern "C"  void MainGame_Start_m520251270 (MainGame_t3800664731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Question[] MainGame::ReadJson()
extern "C"  QuestionU5BU5D_t1028533817* MainGame_ReadJson_m2628908067 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::GameDescription()
extern "C"  void MainGame_GameDescription_m643713462 (MainGame_t3800664731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Question[] MainGame::getQuestions()
extern "C"  QuestionU5BU5D_t1028533817* MainGame_getQuestions_m2295499002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::ActivePanel(UnityEngine.GameObject)
extern "C"  void MainGame_ActivePanel_m1042157020 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::EnablePanel(UnityEngine.GameObject)
extern "C"  void MainGame_EnablePanel_m2776962149 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::BackButton(UnityEngine.GameObject)
extern "C"  void MainGame_BackButton_m1576195507 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::DisablePan(UnityEngine.GameObject)
extern "C"  void MainGame_DisablePan_m558909205 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::LegasiInfoBack(UnityEngine.GameObject)
extern "C"  void MainGame_LegasiInfoBack_m3527212594 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::InactiveButtons()
extern "C"  void MainGame_InactiveButtons_m1486052864 (MainGame_t3800664731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::ActiveButtons()
extern "C"  void MainGame_ActiveButtons_m3331183351 (MainGame_t3800664731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::BackOnBtn()
extern "C"  void MainGame_BackOnBtn_m2583837872 (MainGame_t3800664731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::BackOffBtn()
extern "C"  void MainGame_BackOffBtn_m2255805544 (MainGame_t3800664731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
