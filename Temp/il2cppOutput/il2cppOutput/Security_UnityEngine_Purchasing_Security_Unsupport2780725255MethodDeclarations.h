﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.UnsupportedSignerInfoVersion
struct UnsupportedSignerInfoVersion_t2780725255;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.UnsupportedSignerInfoVersion::.ctor()
extern "C"  void UnsupportedSignerInfoVersion__ctor_m1609509651 (UnsupportedSignerInfoVersion_t2780725255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
