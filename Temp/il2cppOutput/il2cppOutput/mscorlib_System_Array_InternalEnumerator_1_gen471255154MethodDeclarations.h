﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen471255154.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23907470188.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m391600021_gshared (InternalEnumerator_1_t471255154 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m391600021(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t471255154 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m391600021_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m321450633_gshared (InternalEnumerator_1_t471255154 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m321450633(__this, method) ((  void (*) (InternalEnumerator_1_t471255154 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m321450633_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m503099009_gshared (InternalEnumerator_1_t471255154 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m503099009(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t471255154 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m503099009_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m963633748_gshared (InternalEnumerator_1_t471255154 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m963633748(__this, method) ((  void (*) (InternalEnumerator_1_t471255154 *, const MethodInfo*))InternalEnumerator_1_Dispose_m963633748_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1386686921_gshared (InternalEnumerator_1_t471255154 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1386686921(__this, method) ((  bool (*) (InternalEnumerator_1_t471255154 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1386686921_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t3907470188  InternalEnumerator_1_get_Current_m1716177574_gshared (InternalEnumerator_1_t471255154 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1716177574(__this, method) ((  KeyValuePair_2_t3907470188  (*) (InternalEnumerator_1_t471255154 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1716177574_gshared)(__this, method)
