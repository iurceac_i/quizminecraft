﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Store.LoginForwardCallback
struct LoginForwardCallback_t1105422505;
// UnityEngine.Store.ILoginListener
struct ILoginListener_t3164042774;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Store.LoginForwardCallback::.ctor(UnityEngine.Store.ILoginListener)
extern "C"  void LoginForwardCallback__ctor_m1842789250 (LoginForwardCallback_t1105422505 * __this, Il2CppObject * ___loginListener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
