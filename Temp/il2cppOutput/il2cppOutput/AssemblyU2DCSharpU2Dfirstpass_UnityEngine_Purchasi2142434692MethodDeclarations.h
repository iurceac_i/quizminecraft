﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.ConfigurationBuilder
struct ConfigurationBuilder_t1298400415;
// UnityEngine.Purchasing.ProductCatalog
struct ProductCatalog_t2667590766;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalog2667590766.h"

// System.Void UnityEngine.Purchasing.IAPConfigurationHelper::PopulateConfigurationBuilder(UnityEngine.Purchasing.ConfigurationBuilder&,UnityEngine.Purchasing.ProductCatalog)
extern "C"  void IAPConfigurationHelper_PopulateConfigurationBuilder_m3523216658 (Il2CppObject * __this /* static, unused */, ConfigurationBuilder_t1298400415 ** ___builder0, ProductCatalog_t2667590766 * ___catalog1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
