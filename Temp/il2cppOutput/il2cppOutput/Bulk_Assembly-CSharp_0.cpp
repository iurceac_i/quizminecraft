﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Adapter
struct Adapter_t814751345;
// Levels[]
struct LevelsU5BU5D_t2050899114;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.Button
struct Button_t2872111280;
// Adapter/OnItemClickListener
struct OnItemClickListener_t418219798;
// Adapter/<setAdapter>c__AnonStorey0
struct U3CsetAdapterU3Ec__AnonStorey0_t1050088512;
// AssemblyCSharp.Purchase
struct Purchase_t1644043085;
// Main
struct Main_t2809994845;
// System.String
struct String_t;
// UnityEngine.Purchasing.IAppleExtensions
struct IAppleExtensions_t1627764765;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_t547992434;
// UnityEngine.Purchasing.IAppleConfiguration
struct IAppleConfiguration_t3277762425;
// UnityEngine.Purchasing.Product
struct Product_t1203687971;
// ButtonAnimation
struct ButtonAnimation_t1426385366;
// FPSDisplay
struct FPSDisplay_t3952194245;
// Handler
struct Handler_t2658839040;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// Handler/Execute
struct Execute_t1389284496;
// Handler/ExecuteWithGameObjcet
struct ExecuteWithGameObjcet_t1191175011;
// Handler/ExecuteWithButton
struct ExecuteWithButton_t3718107720;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Handler/<WaitForSeconds>c__Iterator0
struct U3CWaitForSecondsU3Ec__Iterator0_t4015752965;
// Handler/<WaitForSeconds>c__Iterator1
struct U3CWaitForSecondsU3Ec__Iterator1_t2449669024;
// Handler/<WaitForSeconds>c__Iterator2
struct U3CWaitForSecondsU3Ec__Iterator2_t2852953551;
// Handler/<WaitForSeconds>c__Iterator3
struct U3CWaitForSecondsU3Ec__Iterator3_t1286869610;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// Kakera.PickerController
struct PickerController_t3670494704;
// UnityEngine.UI.Image
struct Image_t2042527209;
// Kakera.PickerController/<LoadImage>c__Iterator0
struct U3CLoadImageU3Ec__Iterator0_t2450652310;
// Kakera.PickeriOS
struct PickeriOS_t3652150543;
// Kakera.PickerUnsupported
struct PickerUnsupported_t1738158813;
// Kakera.Rotator
struct Rotator_t3287300421;
// Kakera.Unimgpicker
struct Unimgpicker_t2332148304;
// Kakera.Unimgpicker/ImageDelegate
struct ImageDelegate_t3548206662;
// Kakera.Unimgpicker/ErrorDelegate
struct ErrorDelegate_t402150177;
// LevelController
struct LevelController_t2717835266;
// Question[]
struct QuestionU5BU5D_t1028533817;
// Levels
struct Levels_t748035019;
// System.Collections.Generic.List`1<Question>
struct List_1_t2297069972;
// Question
struct Question_t2927948840;
// MainGame
struct MainGame_t3800664731;
// Root
struct Root_t2702590648;
// UnityEngine.Animator
struct Animator_t69676727;
// PlayerPrefsX
struct PlayerPrefsX_t1687815431;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Action`3<System.Int32[],System.Byte[],System.Int32>
struct Action_3_t676603244;
// System.Action`3<System.Object,System.Byte[],System.Int32>
struct Action_3_t1896272050;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Action`3<System.Single[],System.Byte[],System.Int32>
struct Action_3_t2127361240;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// System.Action`3<UnityEngine.Vector2[],System.Byte[],System.Int32>
struct Action_3_t3961902885;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Action`3<UnityEngine.Vector3[],System.Byte[],System.Int32>
struct Action_3_t3958575688;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t1854387467;
// System.Action`3<UnityEngine.Quaternion[],System.Byte[],System.Int32>
struct Action_3_t1730450702;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// System.Action`3<UnityEngine.Color[],System.Byte[],System.Int32>
struct Action_3_t2179559061;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Action`2<System.Collections.Generic.List`1<System.Int32>,System.Byte[]>
struct Action_2_t2760079778;
// System.Action`2<System.Object,System.Byte[]>
struct Action_2_t3279936571;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// System.Action`2<System.Collections.Generic.List`1<System.Single>,System.Byte[]>
struct Action_2_t306807534;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Byte[]>
struct Action_2_t415804163;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Byte[]>
struct Action_2_t901991902;
// System.Collections.Generic.List`1<UnityEngine.Quaternion>
struct List_1_t3399195050;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Quaternion>,System.Byte[]>
struct Action_2_t1584067604;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1389513207;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Color>,System.Byte[]>
struct Action_2_t402030579;
// QuestionController
struct QuestionController_t445239244;
// QuestionController/UpdateListener
struct UpdateListener_t3189012468;
// readHTML
struct readHTML_t503794377;
// ScreenOrientation
struct ScreenOrientation_t2584746402;
// SoundController
struct SoundController_t1686593041;
// Stats
struct Stats_t967880071;
// Stats/<LoadImageStats>c__Iterator0
struct U3CLoadImageStatsU3Ec__Iterator0_t1390744875;
// Util
struct Util_t4006552276;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_Adapter814751345.h"
#include "AssemblyU2DCSharp_Adapter814751345MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_Levels748035019.h"
#include "AssemblyU2DCSharp_Adapter_U3CsetAdapterU3Ec__AnonS1050088512MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "AssemblyU2DCSharp_Levels748035019MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_Adapter_U3CsetAdapterU3Ec__AnonS1050088512.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_AssemblyCSharp_Purchase1644043085.h"
#include "AssemblyU2DCSharp_AssemblyCSharp_Purchase1644043085MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"
#include "UnityEngine_UnityEngine_NetworkReachability1092747145.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Nullable_1_gen3251239280MethodDeclarations.h"
#include "mscorlib_System_IO_File1930543328MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JsonUtility653638946MethodDeclarations.h"
#include "AssemblyU2DCSharp_Main2809994845.h"
#include "UnityEngine_UnityEngine_JsonUtility653638946.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Unit3301441281MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Conf1298400415.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591.h"
#include "UnityEngine.Purchasing_ArrayTypes.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod2754455291.h"
#include "AssemblyU2DCSharp_Handler_ExecuteWithButton3718107720MethodDeclarations.h"
#include "AssemblyU2DCSharp_Handler2658839040MethodDeclarations.h"
#include "AssemblyU2DCSharp_Handler_ExecuteWithButton3718107720.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod3600019299.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434MethodDeclarations.h"
#include "mscorlib_System_StringComparison2376310518.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasing53875121MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_AppleVali3837389912MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_AppleInAp3271698749MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "Security_UnityEngine_Purchasing_Security_AppleRece3991411794.h"
#include "Security_UnityEngine_Purchasing_Security_AppleInAp3271698749.h"
#include "Security_ArrayTypes.h"
#include "Security_UnityEngine_Purchasing_Security_AppleVali3837389912.h"
#include "AssemblyU2DCSharp_ButtonAnimation1426385366.h"
#include "AssemblyU2DCSharp_ButtonAnimation1426385366MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_FPSDisplay3952194245.h"
#include "AssemblyU2DCSharp_FPSDisplay3952194245MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "AssemblyU2DCSharp_Handler2658839040.h"
#include "AssemblyU2DCSharp_Handler_Execute1389284496.h"
#include "AssemblyU2DCSharp_Handler_ExecuteWithGameObjcet1191175011.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I4015752965MethodDeclarations.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I4015752965.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I2449669024MethodDeclarations.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I2449669024.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I2852953551MethodDeclarations.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I2852953551.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I1286869610MethodDeclarations.h"
#include "AssemblyU2DCSharp_Handler_U3CWaitForSecondsU3Ec__I1286869610.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_Handler_Execute1389284496MethodDeclarations.h"
#include "AssemblyU2DCSharp_Handler_ExecuteWithGameObjcet1191175011MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharp_Kakera_PickerController3670494704.h"
#include "AssemblyU2DCSharp_Kakera_PickerController3670494704MethodDeclarations.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker_ImageDelegate3548206662MethodDeclarations.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker2332148304MethodDeclarations.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker2332148304.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker_ImageDelegate3548206662.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "AssemblyU2DCSharp_Kakera_PickerController_U3CLoadI2450652310MethodDeclarations.h"
#include "AssemblyU2DCSharp_Kakera_PickerController_U3CLoadI2450652310.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite309593783MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "AssemblyU2DCSharp_Kakera_PickeriOS3652150543.h"
#include "AssemblyU2DCSharp_Kakera_PickeriOS3652150543MethodDeclarations.h"
#include "AssemblyU2DCSharp_Kakera_PickerUnsupported1738158813.h"
#include "AssemblyU2DCSharp_Kakera_PickerUnsupported1738158813MethodDeclarations.h"
#include "AssemblyU2DCSharp_Kakera_Rotator3287300421.h"
#include "AssemblyU2DCSharp_Kakera_Rotator3287300421MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker_ErrorDelegate402150177.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker_ErrorDelegate402150177MethodDeclarations.h"
#include "AssemblyU2DCSharp_LevelController2717835266.h"
#include "AssemblyU2DCSharp_LevelController2717835266MethodDeclarations.h"
#include "AssemblyU2DCSharp_MainGame3800664731MethodDeclarations.h"
#include "AssemblyU2DCSharp_QuestionController445239244MethodDeclarations.h"
#include "AssemblyU2DCSharp_Question2927948840.h"
#include "AssemblyU2DCSharp_QuestionController445239244.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2297069972.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2297069972MethodDeclarations.h"
#include "AssemblyU2DCSharp_Main2809994845MethodDeclarations.h"
#include "AssemblyU2DCSharp_MainGame3800664731.h"
#include "UnityEngine_UnityEngine_QualitySettings3238033062MethodDeclarations.h"
#include "AssemblyU2DCSharp_Root2702590648MethodDeclarations.h"
#include "AssemblyU2DCSharp_Root2702590648.h"
#include "AssemblyU2DCSharp_SoundController1686593041.h"
#include "AssemblyU2DCSharp_SoundController1686593041MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "AssemblyU2DCSharp_PlayerPrefsX1687815431.h"
#include "AssemblyU2DCSharp_PlayerPrefsX1687815431MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_UInt642909196914.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "mscorlib_System_Collections_BitArray4180138994MethodDeclarations.h"
#include "mscorlib_System_Collections_BitArray4180138994.h"
#include "AssemblyU2DCSharp_PlayerPrefsX_ArrayType77146353.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "System_Core_System_Action_3_gen676603244MethodDeclarations.h"
#include "System_Core_System_Action_3_gen676603244.h"
#include "System_Core_System_Action_3_gen2127361240MethodDeclarations.h"
#include "System_Core_System_Action_3_gen2127361240.h"
#include "UnityEngine_ArrayTypes.h"
#include "System_Core_System_Action_3_gen3961902885MethodDeclarations.h"
#include "System_Core_System_Action_3_gen3961902885.h"
#include "System_Core_System_Action_3_gen3958575688MethodDeclarations.h"
#include "System_Core_System_Action_3_gen3958575688.h"
#include "System_Core_System_Action_3_gen1730450702MethodDeclarations.h"
#include "System_Core_System_Action_3_gen1730450702.h"
#include "System_Core_System_Action_3_gen2179559061MethodDeclarations.h"
#include "System_Core_System_Action_3_gen2179559061.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2760079778MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "System_Core_System_Action_2_gen2760079778.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064MethodDeclarations.h"
#include "System_Core_System_Action_2_gen306807534MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064.h"
#include "System_Core_System_Action_2_gen306807534.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711MethodDeclarations.h"
#include "System_Core_System_Action_2_gen415804163MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711.h"
#include "System_Core_System_Action_2_gen415804163.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712MethodDeclarations.h"
#include "System_Core_System_Action_2_gen901991902MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712.h"
#include "System_Core_System_Action_2_gen901991902.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3399195050MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1584067604MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3399195050.h"
#include "System_Core_System_Action_2_gen1584067604.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1389513207MethodDeclarations.h"
#include "System_Core_System_Action_2_gen402030579MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1389513207.h"
#include "System_Core_System_Action_2_gen402030579.h"
#include "mscorlib_System_BitConverter3195628829.h"
#include "mscorlib_System_BitConverter3195628829MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayerPrefsX_ArrayType77146353MethodDeclarations.h"
#include "AssemblyU2DCSharp_Question2927948840MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "AssemblyU2DCSharp_Util4006552276MethodDeclarations.h"
#include "AssemblyU2DCSharp_readHTML503794377.h"
#include "AssemblyU2DCSharp_readHTML503794377MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScreenOrientation2584746402.h"
#include "AssemblyU2DCSharp_ScreenOrientation2584746402MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DeviceOrientation895964084.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stats967880071.h"
#include "AssemblyU2DCSharp_Stats967880071MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stats_U3CLoadImageStatsU3Ec__Ite1390744875MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stats_U3CLoadImageStatsU3Ec__Ite1390744875.h"
#include "AssemblyU2DCSharp_Util4006552276.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m4280536079(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m681991875_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Transform_t3275118058 * p1, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m681991875(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m681991875_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
#define Object_Instantiate_TisGameObject_t1756533147_m3066053529(__this /* static, unused */, p0, p1, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m681991875_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t2872111280_m3862106414(__this, method) ((  Button_t2872111280 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern "C"  Il2CppObject * JsonUtility_FromJson_TisIl2CppObject_m1534990071_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define JsonUtility_FromJson_TisIl2CppObject_m1534990071(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m1534990071_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<Main>(System.String)
#define JsonUtility_FromJson_TisMain_t2809994845_m2505641288(__this /* static, unused */, p0, method) ((  Main_t2809994845 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m1534990071_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<System.Object>()
extern "C"  Il2CppObject * IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IExtensionProvider_GetExtension_TisIl2CppObject_m925367407(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.IExtensionProvider::GetExtension<UnityEngine.Purchasing.IAppleExtensions>()
#define IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IExtensionProvider_GetExtension_TisIl2CppObject_m925367407_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<System.Object>()
extern "C"  Il2CppObject * ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared (ConfigurationBuilder_t1298400415 * __this, const MethodInfo* method);
#define ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Purchasing.ConfigurationBuilder::Configure<UnityEngine.Purchasing.IAppleConfiguration>()
#define ConfigurationBuilder_Configure_TisIAppleConfiguration_t3277762425_m4232047636(__this, method) ((  Il2CppObject * (*) (ConfigurationBuilder_t1298400415 *, const MethodInfo*))ConfigurationBuilder_Configure_TisIl2CppObject_m4102053310_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
#define Component_GetComponent_TisButton_t2872111280_m3197608264(__this, method) ((  Button_t2872111280 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Kakera.Unimgpicker/ImageDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisImageDelegate_t3548206662_m4137337953(__this /* static, unused */, p0, p1, p2, method) ((  ImageDelegate_t3548206662 * (*) (Il2CppObject * /* static, unused */, ImageDelegate_t3548206662 **, ImageDelegate_t3548206662 *, ImageDelegate_t3548206662 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Kakera.Unimgpicker/ErrorDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisErrorDelegate_t402150177_m1167077550(__this /* static, unused */, p0, p1, p2, method) ((  ErrorDelegate_t402150177 * (*) (Il2CppObject * /* static, unused */, ErrorDelegate_t402150177 **, ErrorDelegate_t402150177 *, ErrorDelegate_t402150177 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.JsonUtility::FromJson<Root>(System.String)
#define JsonUtility_FromJson_TisRoot_t2702590648_m2285480907(__this /* static, unused */, p0, method) ((  Root_t2702590648 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m1534990071_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t69676727_m2717502299(__this, method) ((  Animator_t69676727 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Boolean PlayerPrefsX::SetValue<System.Object>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
extern "C"  bool PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared (Il2CppObject * __this /* static, unused */, String_t* ___key0, Il2CppObject * ___array1, int32_t ___arrayType2, int32_t ___vectorNumber3, Action_3_t1896272050 * ___convert4, const MethodInfo* method);
#define PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, Il2CppObject *, int32_t, int32_t, Action_3_t1896272050 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Boolean PlayerPrefsX::SetValue<System.Int32[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisInt32U5BU5D_t3030399641_m1762095552(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, Int32U5BU5D_t3030399641*, int32_t, int32_t, Action_3_t676603244 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Boolean PlayerPrefsX::SetValue<System.Single[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisSingleU5BU5D_t577127397_m105798686(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, SingleU5BU5D_t577127397*, int32_t, int32_t, Action_3_t2127361240 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Boolean PlayerPrefsX::SetValue<UnityEngine.Vector2[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisVector2U5BU5D_t686124026_m4218454869(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, Vector2U5BU5D_t686124026*, int32_t, int32_t, Action_3_t3961902885 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Boolean PlayerPrefsX::SetValue<UnityEngine.Vector3[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisVector3U5BU5D_t1172311765_m2587447478(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, Vector3U5BU5D_t1172311765*, int32_t, int32_t, Action_3_t3958575688 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Boolean PlayerPrefsX::SetValue<UnityEngine.Quaternion[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisQuaternionU5BU5D_t1854387467_m3934838360(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, QuaternionU5BU5D_t1854387467*, int32_t, int32_t, Action_3_t1730450702 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Boolean PlayerPrefsX::SetValue<UnityEngine.Color[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisColorU5BU5D_t672350442_m2570053159(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, ColorU5BU5D_t672350442*, int32_t, int32_t, Action_3_t2179559061 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void PlayerPrefsX::GetValue<System.Object>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
extern "C"  void PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared (Il2CppObject * __this /* static, unused */, String_t* ___key0, Il2CppObject * ___list1, int32_t ___arrayType2, int32_t ___vectorNumber3, Action_2_t3279936571 * ___convert4, const MethodInfo* method);
#define PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, Il2CppObject *, int32_t, int32_t, Action_2_t3279936571 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<System.Int32>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t1440998580_m3818794476(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t1440998580 *, int32_t, int32_t, Action_2_t2760079778 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<System.Single>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t1445631064_m2582544058(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t1445631064 *, int32_t, int32_t, Action_2_t306807534 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<UnityEngine.Vector2>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t1612828711_m1723016453(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t1612828711 *, int32_t, int32_t, Action_2_t415804163 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<UnityEngine.Vector3>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t1612828712_m1874995114(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t1612828712 *, int32_t, int32_t, Action_2_t901991902 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<UnityEngine.Quaternion>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t3399195050_m2139544680(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t3399195050 *, int32_t, int32_t, Action_2_t1584067604 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<UnityEngine.Color>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t1389513207_m3175850115(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t1389513207 *, int32_t, int32_t, Action_2_t402030579 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2042527209_m4162535761(__this, method) ((  Image_t2042527209 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Adapter::.ctor()
extern "C"  void Adapter__ctor_m1806919526 (Adapter_t814751345 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Adapter::setAdapter(Levels[])
extern Il2CppClass* U3CsetAdapterU3Ec__AnonStorey0_t1050088512_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern const MethodInfo* U3CsetAdapterU3Ec__AnonStorey0_U3CU3Em__0_m3611385842_MethodInfo_var;
extern const uint32_t Adapter_setAdapter_m3137295986_MetadataUsageId;
extern "C"  void Adapter_setAdapter_m3137295986 (Adapter_t814751345 * __this, LevelsU5BU5D_t2050899114* ___levels0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Adapter_setAdapter_m3137295986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	Text_t356221433 * V_3 = NULL;
	int32_t V_4 = 0;
	GameObject_t1756533147 * V_5 = NULL;
	GameObject_t1756533147 * V_6 = NULL;
	GameObject_t1756533147 * V_7 = NULL;
	GameObject_t1756533147 * V_8 = NULL;
	GameObject_t1756533147 * V_9 = NULL;
	int32_t V_10 = 0;
	GameObject_t1756533147 * V_11 = NULL;
	Button_t2872111280 * V_12 = NULL;
	{
		V_0 = 0;
		goto IL_01b2;
	}

IL_0007:
	{
		U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * L_0 = (U3CsetAdapterU3Ec__AnonStorey0_t1050088512 *)il2cpp_codegen_object_new(U3CsetAdapterU3Ec__AnonStorey0_t1050088512_il2cpp_TypeInfo_var);
		U3CsetAdapterU3Ec__AnonStorey0__ctor_m2813505567(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		GameObject_t1756533147 * L_2 = __this->get_gameObjects_2();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Transform_GetChild_m3838588184(L_3, 0, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		GameObject_t1756533147 * L_6 = V_2;
		NullCheck(L_6);
		Text_t356221433 * L_7 = GameObject_GetComponent_TisText_t356221433_m4280536079(L_6, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var);
		V_3 = L_7;
		Text_t356221433 * L_8 = V_3;
		LevelsU5BU5D_t2050899114* L_9 = ___levels0;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Levels_t748035019 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		int32_t L_13 = Levels_get_NrLevel_m2233528047(L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		String_t* L_14 = Int32_ToString_m2960866144((&V_4), /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_14);
		GameObject_t1756533147 * L_15 = __this->get_gameObjects_2();
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = Transform_GetChild_m3838588184(L_16, 1, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t3275118058 * L_18 = Transform_GetChild_m3838588184(L_17, 3, /*hidden argument*/NULL);
		NullCheck(L_18);
		GameObject_t1756533147 * L_19 = Component_get_gameObject_m3105766835(L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		GameObject_t1756533147 * L_20 = __this->get_gameObjects_2();
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = Transform_GetChild_m3838588184(L_21, 1, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = Transform_GetChild_m3838588184(L_22, 4, /*hidden argument*/NULL);
		NullCheck(L_23);
		GameObject_t1756533147 * L_24 = Component_get_gameObject_m3105766835(L_23, /*hidden argument*/NULL);
		V_6 = L_24;
		GameObject_t1756533147 * L_25 = __this->get_gameObjects_2();
		NullCheck(L_25);
		Transform_t3275118058 * L_26 = GameObject_get_transform_m909382139(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = Transform_GetChild_m3838588184(L_26, 1, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = Transform_GetChild_m3838588184(L_27, 5, /*hidden argument*/NULL);
		NullCheck(L_28);
		GameObject_t1756533147 * L_29 = Component_get_gameObject_m3105766835(L_28, /*hidden argument*/NULL);
		V_7 = L_29;
		GameObject_t1756533147 * L_30 = __this->get_gameObjects_2();
		NullCheck(L_30);
		Transform_t3275118058 * L_31 = GameObject_get_transform_m909382139(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = Transform_GetChild_m3838588184(L_31, 3, /*hidden argument*/NULL);
		NullCheck(L_32);
		GameObject_t1756533147 * L_33 = Component_get_gameObject_m3105766835(L_32, /*hidden argument*/NULL);
		V_8 = L_33;
		GameObject_t1756533147 * L_34 = V_8;
		LevelsU5BU5D_t2050899114* L_35 = ___levels0;
		int32_t L_36 = V_0;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		Levels_t748035019 * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_38);
		bool L_39 = Levels_get_IsLocked_m626983591(L_38, /*hidden argument*/NULL);
		NullCheck(L_34);
		GameObject_SetActive_m2887581199(L_34, L_39, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_40 = __this->get_gameObjects_2();
		NullCheck(L_40);
		Transform_t3275118058 * L_41 = GameObject_get_transform_m909382139(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_t3275118058 * L_42 = Transform_GetChild_m3838588184(L_41, 1, /*hidden argument*/NULL);
		NullCheck(L_42);
		GameObject_t1756533147 * L_43 = Component_get_gameObject_m3105766835(L_42, /*hidden argument*/NULL);
		V_9 = L_43;
		GameObject_t1756533147 * L_44 = V_9;
		LevelsU5BU5D_t2050899114* L_45 = ___levels0;
		int32_t L_46 = V_0;
		NullCheck(L_45);
		int32_t L_47 = L_46;
		Levels_t748035019 * L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_48);
		bool L_49 = Levels_get_IsLocked_m626983591(L_48, /*hidden argument*/NULL);
		NullCheck(L_44);
		GameObject_SetActive_m2887581199(L_44, (bool)((((int32_t)L_49) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		LevelsU5BU5D_t2050899114* L_50 = ___levels0;
		int32_t L_51 = V_0;
		NullCheck(L_50);
		int32_t L_52 = L_51;
		Levels_t748035019 * L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		NullCheck(L_53);
		int32_t L_54 = Levels_getStarNumber_m1660958789(L_53, /*hidden argument*/NULL);
		V_10 = L_54;
		GameObject_t1756533147 * L_55 = V_7;
		int32_t L_56 = V_10;
		NullCheck(L_55);
		GameObject_SetActive_m2887581199(L_55, (bool)((((int32_t)L_56) == ((int32_t)3))? 1 : 0), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_57 = V_6;
		int32_t L_58 = V_10;
		NullCheck(L_57);
		GameObject_SetActive_m2887581199(L_57, (bool)((((int32_t)L_58) > ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_59 = V_5;
		int32_t L_60 = V_10;
		NullCheck(L_59);
		GameObject_SetActive_m2887581199(L_59, (bool)((((int32_t)L_60) > ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * L_61 = V_1;
		GameObject_t1756533147 * L_62 = __this->get_gameObjects_2();
		Transform_t3275118058 * L_63 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_64 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_62, L_63, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		NullCheck(L_61);
		L_61->set_finalObj_0(L_64);
		U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * L_65 = V_1;
		NullCheck(L_65);
		GameObject_t1756533147 * L_66 = L_65->get_finalObj_0();
		NullCheck(L_66);
		GameObject_SetActive_m2887581199(L_66, (bool)1, /*hidden argument*/NULL);
		U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * L_67 = V_1;
		NullCheck(L_67);
		GameObject_t1756533147 * L_68 = L_67->get_finalObj_0();
		String_t* L_69 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		NullCheck(L_68);
		Object_set_name_m4157836998(L_68, L_69, /*hidden argument*/NULL);
		U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * L_70 = V_1;
		NullCheck(L_70);
		GameObject_t1756533147 * L_71 = L_70->get_finalObj_0();
		NullCheck(L_71);
		Transform_t3275118058 * L_72 = GameObject_get_transform_m909382139(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		Transform_t3275118058 * L_73 = Transform_GetChild_m3838588184(L_72, 2, /*hidden argument*/NULL);
		NullCheck(L_73);
		GameObject_t1756533147 * L_74 = Component_get_gameObject_m3105766835(L_73, /*hidden argument*/NULL);
		V_11 = L_74;
		GameObject_t1756533147 * L_75 = V_11;
		NullCheck(L_75);
		Button_t2872111280 * L_76 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_75, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		V_12 = L_76;
		Button_t2872111280 * L_77 = V_12;
		NullCheck(L_77);
		ButtonClickedEvent_t2455055323 * L_78 = Button_get_onClick_m1595880935(L_77, /*hidden argument*/NULL);
		U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * L_79 = V_1;
		IntPtr_t L_80;
		L_80.set_m_value_0((void*)(void*)U3CsetAdapterU3Ec__AnonStorey0_U3CU3Em__0_m3611385842_MethodInfo_var);
		UnityAction_t4025899511 * L_81 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_81, L_79, L_80, /*hidden argument*/NULL);
		NullCheck(L_78);
		UnityEvent_AddListener_m1596810379(L_78, L_81, /*hidden argument*/NULL);
		Button_t2872111280 * L_82 = V_12;
		LevelsU5BU5D_t2050899114* L_83 = ___levels0;
		int32_t L_84 = V_0;
		NullCheck(L_83);
		int32_t L_85 = L_84;
		Levels_t748035019 * L_86 = (L_83)->GetAt(static_cast<il2cpp_array_size_t>(L_85));
		NullCheck(L_86);
		bool L_87 = Levels_get_IsLocked_m626983591(L_86, /*hidden argument*/NULL);
		NullCheck(L_82);
		Selectable_set_interactable_m63718297(L_82, (bool)((((int32_t)L_87) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_88 = V_0;
		V_0 = ((int32_t)((int32_t)L_88+(int32_t)1));
	}

IL_01b2:
	{
		int32_t L_89 = V_0;
		LevelsU5BU5D_t2050899114* L_90 = ___levels0;
		NullCheck(L_90);
		if ((((int32_t)L_89) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_90)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Adapter::UpdateAdapter(Levels[])
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern const uint32_t Adapter_UpdateAdapter_m349117033_MetadataUsageId;
extern "C"  void Adapter_UpdateAdapter_m349117033 (Adapter_t814751345 * __this, LevelsU5BU5D_t2050899114* ___levels0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Adapter_UpdateAdapter_m349117033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	GameObject_t1756533147 * V_3 = NULL;
	int32_t V_4 = 0;
	GameObject_t1756533147 * V_5 = NULL;
	GameObject_t1756533147 * V_6 = NULL;
	Button_t2872111280 * V_7 = NULL;
	GameObject_t1756533147 * V_8 = NULL;
	{
		V_0 = 0;
		goto IL_0117;
	}

IL_0007:
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Transform_t3275118058 * L_2 = Transform_GetChild_m3838588184(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Transform_GetChild_m3838588184(L_2, 1, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Transform_GetChild_m3838588184(L_3, 3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Transform_t3275118058 * L_8 = Transform_GetChild_m3838588184(L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Transform_GetChild_m3838588184(L_8, 1, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = Transform_GetChild_m3838588184(L_9, 4, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		NullCheck(L_12);
		Transform_t3275118058 * L_14 = Transform_GetChild_m3838588184(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Transform_GetChild_m3838588184(L_14, 1, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = Transform_GetChild_m3838588184(L_15, 5, /*hidden argument*/NULL);
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = Component_get_gameObject_m3105766835(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		LevelsU5BU5D_t2050899114* L_18 = ___levels0;
		int32_t L_19 = V_0;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		Levels_t748035019 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		int32_t L_22 = Levels_getStarNumber_m1660958789(L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		GameObject_t1756533147 * L_23 = V_3;
		int32_t L_24 = V_4;
		NullCheck(L_23);
		GameObject_SetActive_m2887581199(L_23, (bool)((((int32_t)L_24) == ((int32_t)3))? 1 : 0), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_25 = V_2;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		GameObject_SetActive_m2887581199(L_25, (bool)((((int32_t)L_26) > ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = V_1;
		int32_t L_28 = V_4;
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)((((int32_t)L_28) > ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_30 = V_0;
		NullCheck(L_29);
		Transform_t3275118058 * L_31 = Transform_GetChild_m3838588184(L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = Transform_GetChild_m3838588184(L_31, 3, /*hidden argument*/NULL);
		NullCheck(L_32);
		GameObject_t1756533147 * L_33 = Component_get_gameObject_m3105766835(L_32, /*hidden argument*/NULL);
		V_5 = L_33;
		GameObject_t1756533147 * L_34 = V_5;
		LevelsU5BU5D_t2050899114* L_35 = ___levels0;
		int32_t L_36 = V_0;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		Levels_t748035019 * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_38);
		bool L_39 = Levels_get_IsLocked_m626983591(L_38, /*hidden argument*/NULL);
		NullCheck(L_34);
		GameObject_SetActive_m2887581199(L_34, L_39, /*hidden argument*/NULL);
		Transform_t3275118058 * L_40 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_41 = V_0;
		NullCheck(L_40);
		Transform_t3275118058 * L_42 = Transform_GetChild_m3838588184(L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		Transform_t3275118058 * L_43 = Transform_GetChild_m3838588184(L_42, 2, /*hidden argument*/NULL);
		NullCheck(L_43);
		GameObject_t1756533147 * L_44 = Component_get_gameObject_m3105766835(L_43, /*hidden argument*/NULL);
		V_6 = L_44;
		GameObject_t1756533147 * L_45 = V_6;
		NullCheck(L_45);
		Button_t2872111280 * L_46 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_45, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		V_7 = L_46;
		Button_t2872111280 * L_47 = V_7;
		LevelsU5BU5D_t2050899114* L_48 = ___levels0;
		int32_t L_49 = V_0;
		NullCheck(L_48);
		int32_t L_50 = L_49;
		Levels_t748035019 * L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		NullCheck(L_51);
		bool L_52 = Levels_get_IsLocked_m626983591(L_51, /*hidden argument*/NULL);
		NullCheck(L_47);
		Selectable_set_interactable_m63718297(L_47, (bool)((((int32_t)L_52) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Transform_t3275118058 * L_53 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_54 = V_0;
		NullCheck(L_53);
		Transform_t3275118058 * L_55 = Transform_GetChild_m3838588184(L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_55);
		Transform_t3275118058 * L_56 = Transform_GetChild_m3838588184(L_55, 1, /*hidden argument*/NULL);
		NullCheck(L_56);
		GameObject_t1756533147 * L_57 = Component_get_gameObject_m3105766835(L_56, /*hidden argument*/NULL);
		V_8 = L_57;
		GameObject_t1756533147 * L_58 = V_8;
		LevelsU5BU5D_t2050899114* L_59 = ___levels0;
		int32_t L_60 = V_0;
		NullCheck(L_59);
		int32_t L_61 = L_60;
		Levels_t748035019 * L_62 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		NullCheck(L_62);
		bool L_63 = Levels_get_IsLocked_m626983591(L_62, /*hidden argument*/NULL);
		NullCheck(L_58);
		GameObject_SetActive_m2887581199(L_58, (bool)((((int32_t)L_63) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_64 = V_0;
		V_0 = ((int32_t)((int32_t)L_64+(int32_t)1));
	}

IL_0117:
	{
		int32_t L_65 = V_0;
		LevelsU5BU5D_t2050899114* L_66 = ___levels0;
		NullCheck(L_66);
		if ((((int32_t)L_65) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_66)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Adapter::OnItemClick(UnityEngine.GameObject)
extern Il2CppClass* OnItemClickListener_t418219798_il2cpp_TypeInfo_var;
extern const uint32_t Adapter_OnItemClick_m3700375114_MetadataUsageId;
extern "C"  void Adapter_OnItemClick_m3700375114 (Adapter_t814751345 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Adapter_OnItemClick_m3700375114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m2079638459(L_0, /*hidden argument*/NULL);
		int32_t L_2 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Il2CppObject * L_3 = __this->get_adapterCallBack_4();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		Il2CppObject * L_4 = __this->get_adapterCallBack_4();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		InterfaceActionInvoker1< int32_t >::Invoke(0 /* System.Void Adapter/OnItemClickListener::onItemClicked(System.Int32) */, OnItemClickListener_t418219798_il2cpp_TypeInfo_var, L_4, L_5);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Adapter::registerListener(Adapter/OnItemClickListener)
extern "C"  void Adapter_registerListener_m1706720079 (Adapter_t814751345 * __this, Il2CppObject * ___callback0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___callback0;
		__this->set_adapterCallBack_4(L_0);
		return;
	}
}
// System.Void Adapter/<setAdapter>c__AnonStorey0::.ctor()
extern "C"  void U3CsetAdapterU3Ec__AnonStorey0__ctor_m2813505567 (U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Adapter/<setAdapter>c__AnonStorey0::<>m__0()
extern "C"  void U3CsetAdapterU3Ec__AnonStorey0_U3CU3Em__0_m3611385842 (U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * __this, const MethodInfo* method)
{
	{
		Adapter_t814751345 * L_0 = __this->get_U24this_1();
		GameObject_t1756533147 * L_1 = __this->get_finalObj_0();
		NullCheck(L_0);
		Adapter_OnItemClick_m3700375114(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::.ctor()
extern "C"  void Purchase__ctor_m123144463 (Purchase_t1644043085 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::Start()
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4027593417_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m3795771450_MethodInfo_var;
extern const MethodInfo* Nullable_1_GetValueOrDefault_m706662709_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4001649840;
extern Il2CppCodeGenString* _stringLiteral2328218522;
extern Il2CppCodeGenString* _stringLiteral1576077039;
extern Il2CppCodeGenString* _stringLiteral2484002866;
extern Il2CppCodeGenString* _stringLiteral2946594395;
extern const uint32_t Purchase_Start_m402233827_MetadataUsageId;
extern "C"  void Purchase_Start_m402233827 (Purchase_t1644043085 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_Start_m402233827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	Nullable_1_t3251239280  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t G_B8_0 = 0;
	{
		Purchase_readPurchaseJson_m1913674994(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_kProductIDSubscription_9();
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_m_StoreController_5();
		if (L_1)
		{
			goto IL_00d6;
		}
	}
	{
		int32_t L_2 = Application_get_internetReachability_m1191733175(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_00d0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral4001649840, /*hidden argument*/NULL);
		String_t* L_3 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, _stringLiteral2328218522, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		V_1 = (bool)((((Il2CppObject*)(String_t*)L_4) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		String_t* L_5 = Boolean_ToString_m1253164328((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1576077039, L_5, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		String_t* L_7 = V_0;
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2484002866, L_7, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		String_t* L_9 = V_0;
		if (!L_9)
		{
			goto IL_0080;
		}
	}
	{
		String_t* L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0081;
		}
	}

IL_0080:
	{
		return;
	}

IL_0081:
	{
		String_t* L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_14 = DateTime_Parse_m1142721566(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		Nullable_1_t3251239280  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Nullable_1__ctor_m4027593417(&L_15, L_14, /*hidden argument*/Nullable_1__ctor_m4027593417_MethodInfo_var);
		V_2 = L_15;
		bool L_16 = Nullable_1_get_HasValue_m3795771450((&V_2), /*hidden argument*/Nullable_1_get_HasValue_m3795771450_MethodInfo_var);
		if (!L_16)
		{
			goto IL_00ac;
		}
	}
	{
		DateTime_t693205669  L_17 = Nullable_1_GetValueOrDefault_m706662709((&V_2), /*hidden argument*/Nullable_1_GetValueOrDefault_m706662709_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_18 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_19 = DateTime_op_GreaterThan_m2730200039(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		G_B8_0 = ((int32_t)(L_19));
		goto IL_00ad;
	}

IL_00ac:
	{
		G_B8_0 = 0;
	}

IL_00ad:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->set_isPurchased_2((bool)G_B8_0);
		bool L_20 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_isPurchased_2();
		bool L_21 = L_20;
		Il2CppObject * L_22 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2946594395, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_00d0:
	{
		Purchase_InitializePurchasing_m1496585717(__this, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::readPurchaseJson()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisMain_t2809994845_m2505641288_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2995538678;
extern const uint32_t Purchase_readPurchaseJson_m1913674994_MetadataUsageId;
extern "C"  void Purchase_readPurchaseJson_m1913674994 (Purchase_t1644043085 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_readPurchaseJson_m1913674994_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		V_0 = _stringLiteral2995538678;
		String_t* L_0 = Application_get_dataPath_m371940330(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_3 = File_ReadAllText_m1018286608(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		Main_t2809994845 * L_5 = JsonUtility_FromJson_TisMain_t2809994845_m2505641288(NULL /*static, unused*/, L_4, /*hidden argument*/JsonUtility_FromJson_TisMain_t2809994845_m2505641288_MethodInfo_var);
		__this->set_itemsContent_10(L_5);
		Main_t2809994845 * L_6 = __this->get_itemsContent_10();
		NullCheck(L_6);
		String_t* L_7 = L_6->get_product_id_0();
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->set_kProductIDSubscription_9(L_7);
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::InitializePurchasing()
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern Il2CppClass* IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var;
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern const uint32_t Purchase_InitializePurchasing_m1496585717_MetadataUsageId;
extern "C"  void Purchase_InitializePurchasing_m1496585717 (Purchase_t1644043085 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_InitializePurchasing_m1496585717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConfigurationBuilder_t1298400415 * V_0 = NULL;
	{
		bool L_0 = Purchase_IsInitialized_m2743679681(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_1 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_2 = ConfigurationBuilder_Instance_m4243979412(NULL /*static, unused*/, L_1, ((IPurchasingModuleU5BU5D_t4128245854*)SZArrayNew(IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_2;
		ConfigurationBuilder_t1298400415 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		String_t* L_4 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_kProductIDSubscription_9();
		NullCheck(L_3);
		ConfigurationBuilder_AddProduct_m3779153393(L_3, L_4, 2, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_5 = V_0;
		UnityPurchasing_Initialize_m1012234273(NULL /*static, unused*/, __this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AssemblyCSharp.Purchase::IsInitialized()
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern const uint32_t Purchase_IsInitialized_m2743679681_MetadataUsageId;
extern "C"  bool Purchase_IsInitialized_m2743679681 (Purchase_t1644043085 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_IsInitialized_m2743679681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_m_StoreController_5();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_m_StoreExtensionProvider_6();
		G_B3_0 = ((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Void AssemblyCSharp.Purchase::BuyConsumable()
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern const uint32_t Purchase_BuyConsumable_m2197570716_MetadataUsageId;
extern "C"  void Purchase_BuyConsumable_m2197570716 (Purchase_t1644043085 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_BuyConsumable_m2197570716_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_kProductIDConsumable_7();
		Purchase_BuyProductID_m4244666769(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::BuyNonConsumable()
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern const uint32_t Purchase_BuyNonConsumable_m2013515493_MetadataUsageId;
extern "C"  void Purchase_BuyNonConsumable_m2013515493 (Purchase_t1644043085 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_BuyNonConsumable_m2013515493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_kProductIDNonConsumable_8();
		Purchase_BuyProductID_m4244666769(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::BuySubscription(UnityEngine.GameObject)
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteWithButton_t3718107720_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern const MethodInfo* Purchase_BlockInteractableButton_m2639323695_MethodInfo_var;
extern const uint32_t Purchase_BuySubscription_m937582440_MetadataUsageId;
extern "C"  void Purchase_BuySubscription_m937582440 (Purchase_t1644043085 * __this, GameObject_t1756533147 * ___button0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_BuySubscription_m937582440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_kProductIDSubscription_9();
		Purchase_BuyProductID_m4244666769(__this, L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = ___button0;
		NullCheck(L_1);
		Button_t2872111280 * L_2 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_1, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		V_0 = L_2;
		Button_t2872111280 * L_3 = V_0;
		NullCheck(L_3);
		Selectable_set_interactable_m63718297(L_3, (bool)0, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)Purchase_BlockInteractableButton_m2639323695_MethodInfo_var);
		ExecuteWithButton_t3718107720 * L_5 = (ExecuteWithButton_t3718107720 *)il2cpp_codegen_object_new(ExecuteWithButton_t3718107720_il2cpp_TypeInfo_var);
		ExecuteWithButton__ctor_m1880605207(L_5, __this, L_4, /*hidden argument*/NULL);
		Button_t2872111280 * L_6 = V_0;
		Handler_StartDelayed_m3476731120(NULL /*static, unused*/, (7.0f), L_5, L_6, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::BlockInteractableButton(UnityEngine.UI.Button)
extern "C"  void Purchase_BlockInteractableButton_m2639323695 (Purchase_t1644043085 * __this, Button_t2872111280 * ___btn0, const MethodInfo* method)
{
	{
		Button_t2872111280 * L_0 = ___btn0;
		NullCheck(L_0);
		Selectable_set_interactable_m63718297(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::BuyProductID(System.String)
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern Il2CppClass* IStoreController_t92554892_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1534276110;
extern Il2CppCodeGenString* _stringLiteral2501933543;
extern Il2CppCodeGenString* _stringLiteral3573183659;
extern const uint32_t Purchase_BuyProductID_m4244666769_MetadataUsageId;
extern "C"  void Purchase_BuyProductID_m4244666769 (Purchase_t1644043085 * __this, String_t* ___productId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_BuyProductID_m4244666769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Product_t1203687971 * V_0 = NULL;
	{
		bool L_0 = Purchase_IsInitialized_m2743679681(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_m_StoreController_5();
		NullCheck(L_1);
		ProductCollection_t3600019299 * L_2 = InterfaceFuncInvoker0< ProductCollection_t3600019299 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_1);
		String_t* L_3 = ___productId0;
		NullCheck(L_2);
		Product_t1203687971 * L_4 = ProductCollection_WithID_m3999574440(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Product_t1203687971 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0057;
		}
	}
	{
		Product_t1203687971 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = Product_get_availableToPurchase_m3285924692(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0057;
		}
	}
	{
		Product_t1203687971 * L_8 = V_0;
		NullCheck(L_8);
		ProductDefinition_t1942475268 * L_9 = Product_get_definition_m2035415516(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = ProductDefinition_get_id_m264072292(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1534276110, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_m_StoreController_5();
		Product_t1203687971 * L_13 = V_0;
		NullCheck(L_12);
		InterfaceActionInvoker1< Product_t1203687971 * >::Invoke(2 /* System.Void UnityEngine.Purchasing.IStoreController::InitiatePurchase(UnityEngine.Purchasing.Product) */, IStoreController_t92554892_il2cpp_TypeInfo_var, L_12, L_13);
		goto IL_0061;
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2501933543, /*hidden argument*/NULL);
	}

IL_0061:
	{
		goto IL_0076;
	}

IL_0066:
	{
		Purchase_InitializePurchasing_m1496585717(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3573183659, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::RestorePurchases(UnityEngine.GameObject)
extern Il2CppClass* ExecuteWithButton_t3718107720_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern Il2CppClass* IAppleExtensions_t1627764765_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimePlatform_t1869584967_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern const MethodInfo* Purchase_BlockInteractableButton_m2639323695_MethodInfo_var;
extern const MethodInfo* IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var;
extern const MethodInfo* Purchase_U3CRestorePurchasesU3Em__0_m3309735971_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m309821356_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3592252195;
extern Il2CppCodeGenString* _stringLiteral3927888663;
extern Il2CppCodeGenString* _stringLiteral1309169529;
extern const uint32_t Purchase_RestorePurchases_m2342185403_MetadataUsageId;
extern "C"  void Purchase_RestorePurchases_m2342185403 (Purchase_t1644043085 * __this, GameObject_t1756533147 * ___button0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_RestorePurchases_m2342185403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___button0;
		NullCheck(L_0);
		Button_t2872111280 * L_1 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_0, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		V_0 = L_1;
		Button_t2872111280 * L_2 = V_0;
		NullCheck(L_2);
		Selectable_set_interactable_m63718297(L_2, (bool)0, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)Purchase_BlockInteractableButton_m2639323695_MethodInfo_var);
		ExecuteWithButton_t3718107720 * L_4 = (ExecuteWithButton_t3718107720 *)il2cpp_codegen_object_new(ExecuteWithButton_t3718107720_il2cpp_TypeInfo_var);
		ExecuteWithButton__ctor_m1880605207(L_4, __this, L_3, /*hidden argument*/NULL);
		Button_t2872111280 * L_5 = V_0;
		Handler_StartDelayed_m3476731120(NULL /*static, unused*/, (7.0f), L_4, L_5, __this, /*hidden argument*/NULL);
		bool L_6 = Purchase_IsInitialized_m2743679681(__this, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0043;
		}
	}
	{
		Purchase_InitializePurchasing_m1496585717(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3592252195, /*hidden argument*/NULL);
		return;
	}

IL_0043:
	{
		int32_t L_7 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)8)))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_8 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)1))))
		{
			goto IL_0085;
		}
	}

IL_0059:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3927888663, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		Il2CppObject * L_9 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_m_StoreExtensionProvider_6();
		NullCheck(L_9);
		Il2CppObject * L_10 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IExtensionProvider_GetExtension_TisIAppleExtensions_t1627764765_m3353400141_MethodInfo_var, L_9);
		V_1 = L_10;
		Il2CppObject * L_11 = V_1;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)Purchase_U3CRestorePurchasesU3Em__0_m3309735971_MethodInfo_var);
		Action_1_t3627374100 * L_13 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m309821356(L_13, __this, L_12, /*hidden argument*/Action_1__ctor_m309821356_MethodInfo_var);
		NullCheck(L_11);
		InterfaceActionInvoker1< Action_1_t3627374100 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>) */, IAppleExtensions_t1627764765_il2cpp_TypeInfo_var, L_11, L_13);
		goto IL_009e;
	}

IL_0085:
	{
		int32_t L_14 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(RuntimePlatform_t1869584967_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1309169529, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
	}

IL_009e:
	{
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2765083868;
extern const uint32_t Purchase_OnInitialized_m973436933_MetadataUsageId;
extern "C"  void Purchase_OnInitialized_m973436933 (Purchase_t1644043085 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_OnInitialized_m973436933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2765083868, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___controller0;
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->set_m_StoreController_5(L_0);
		Il2CppObject * L_1 = ___extensions1;
		((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->set_m_StoreExtensionProvider_6(L_1);
		Purchase_verifyOnStart_m1690669461(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern Il2CppClass* InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1730743872;
extern const uint32_t Purchase_OnInitializeFailed_m2837146904_MetadataUsageId;
extern "C"  void Purchase_OnInitializeFailed_m2837146904 (Purchase_t1644043085 * __this, int32_t ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_OnInitializeFailed_m2837146904_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___error0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1730743872, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult AssemblyCSharp.Purchase::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern Il2CppClass* IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var;
extern Il2CppClass* IAppleConfiguration_t3277762425_il2cpp_TypeInfo_var;
extern Il2CppClass* Nullable_1_t3251239280_il2cpp_TypeInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIAppleConfiguration_t3277762425_m4232047636_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m3795771450_MethodInfo_var;
extern const MethodInfo* Nullable_1_ToString_m1419821888_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2618156685;
extern Il2CppCodeGenString* _stringLiteral76365574;
extern Il2CppCodeGenString* _stringLiteral2391886964;
extern Il2CppCodeGenString* _stringLiteral2422062668;
extern Il2CppCodeGenString* _stringLiteral2328218522;
extern Il2CppCodeGenString* _stringLiteral3924017192;
extern const uint32_t Purchase_ProcessPurchase_m2859853793_MetadataUsageId;
extern "C"  int32_t Purchase_ProcessPurchase_m2859853793 (Purchase_t1644043085 * __this, PurchaseEventArgs_t547992434 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_ProcessPurchase_m2859853793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConfigurationBuilder_t1298400415 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Nullable_1_t3251239280  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		PurchaseEventArgs_t547992434 * L_0 = ___args0;
		NullCheck(L_0);
		Product_t1203687971 * L_1 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		ProductDefinition_t1942475268 * L_2 = Product_get_definition_m2035415516(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ProductDefinition_get_id_m264072292(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2618156685, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		PurchaseEventArgs_t547992434 * L_5 = ___args0;
		NullCheck(L_5);
		Product_t1203687971 * L_6 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		ProductDefinition_t1942475268 * L_7 = Product_get_definition_m2035415516(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = ProductDefinition_get_id_m264072292(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		String_t* L_9 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_kProductIDSubscription_9();
		bool L_10 = String_Equals_m2950069882(NULL /*static, unused*/, L_8, L_9, 4, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00fe;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_11 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_12 = ConfigurationBuilder_Instance_m4243979412(NULL /*static, unused*/, L_11, ((IPurchasingModuleU5BU5D_t4128245854*)SZArrayNew(IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_12;
		ConfigurationBuilder_t1298400415 * L_13 = V_0;
		NullCheck(L_13);
		Il2CppObject * L_14 = ConfigurationBuilder_Configure_TisIAppleConfiguration_t3277762425_m4232047636(L_13, /*hidden argument*/ConfigurationBuilder_Configure_TisIAppleConfiguration_t3277762425_m4232047636_MethodInfo_var);
		V_1 = L_14;
		Il2CppObject * L_15 = V_1;
		NullCheck(L_15);
		String_t* L_16 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.Purchasing.IAppleConfiguration::get_appReceipt() */, IAppleConfiguration_t3277762425_il2cpp_TypeInfo_var, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		String_t* L_17 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_kProductIDSubscription_9();
		Nullable_1_t3251239280  L_18 = Purchase_validate_m3617248933(__this, L_16, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		bool L_19 = Nullable_1_get_HasValue_m3795771450((&V_2), /*hidden argument*/Nullable_1_get_HasValue_m3795771450_MethodInfo_var);
		if (!((((int32_t)L_19) == ((int32_t)0))? 1 : 0))
		{
			goto IL_0087;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral76365574, /*hidden argument*/NULL);
		goto IL_009c;
	}

IL_0087:
	{
		Nullable_1_t3251239280  L_20 = V_2;
		Nullable_1_t3251239280  L_21 = L_20;
		Il2CppObject * L_22 = Box(Nullable_1_t3251239280_il2cpp_TypeInfo_var, &L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2391886964, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
	}

IL_009c:
	{
		bool L_24 = Nullable_1_get_HasValue_m3795771450((&V_2), /*hidden argument*/Nullable_1_get_HasValue_m3795771450_MethodInfo_var);
		if (!L_24)
		{
			goto IL_00f9;
		}
	}
	{
		GameObject_t1756533147 * L_25 = __this->get_alertPurchase_3();
		NullCheck(L_25);
		GameObject_SetActive_m2887581199(L_25, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_26 = __this->get_gamePanel_4();
		NullCheck(L_26);
		GameObject_SetActive_m2887581199(L_26, (bool)0, /*hidden argument*/NULL);
		String_t* L_27 = Nullable_1_ToString_m1419821888((&V_2), /*hidden argument*/Nullable_1_ToString_m1419821888_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2422062668, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		String_t* L_29 = Nullable_1_ToString_m1419821888((&V_2), /*hidden argument*/Nullable_1_ToString_m1419821888_MethodInfo_var);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral2328218522, L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->set_isPurchased_2((bool)1);
	}

IL_00f9:
	{
		goto IL_011d;
	}

IL_00fe:
	{
		PurchaseEventArgs_t547992434 * L_30 = ___args0;
		NullCheck(L_30);
		Product_t1203687971 * L_31 = PurchaseEventArgs_get_purchasedProduct_m3327797079(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		ProductDefinition_t1942475268 * L_32 = Product_get_definition_m2035415516(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		String_t* L_33 = ProductDefinition_get_id_m264072292(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral3924017192, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
	}

IL_011d:
	{
		return (int32_t)(0);
	}
}
// System.Void AssemblyCSharp.Purchase::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3089522628;
extern const uint32_t Purchase_OnPurchaseFailed_m645576138_MetadataUsageId;
extern "C"  void Purchase_OnPurchaseFailed_m645576138 (Purchase_t1644043085 * __this, Product_t1203687971 * ___product0, int32_t ___failureReason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_OnPurchaseFailed_m645576138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Product_t1203687971 * L_0 = ___product0;
		NullCheck(L_0);
		ProductDefinition_t1942475268 * L_1 = Product_get_definition_m2035415516(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_storeSpecificId_m2251287741(L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___failureReason1;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3089522628, L_2, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<System.DateTime> AssemblyCSharp.Purchase::validate(System.String,System.String)
extern Il2CppClass* Nullable_1_t3251239280_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* AppleTangle_t53875121_il2cpp_TypeInfo_var;
extern Il2CppClass* AppleValidator_t3837389912_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4027593417_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4272844521;
extern const uint32_t Purchase_validate_m3617248933_MetadataUsageId;
extern "C"  Nullable_1_t3251239280  Purchase_validate_m3617248933 (Purchase_t1644043085 * __this, String_t* ___receiptStr0, String_t* ___myProduct1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_validate_m3617248933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t3251239280  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Nullable_1_t3251239280  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ByteU5BU5D_t3397334013* V_2 = NULL;
	AppleReceipt_t3991411794 * V_3 = NULL;
	AppleInAppPurchaseReceipt_t3271698749 * V_4 = NULL;
	AppleInAppPurchaseReceiptU5BU5D_t579068208* V_5 = NULL;
	int32_t V_6 = 0;
	DateTime_t693205669  V_7;
	memset(&V_7, 0, sizeof(V_7));
	String_t* V_8 = NULL;
	{
		Initobj (Nullable_1_t3251239280_il2cpp_TypeInfo_var, (&V_1));
		Nullable_1_t3251239280  L_0 = V_1;
		V_0 = L_0;
		String_t* L_1 = ___receiptStr0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_2 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(AppleTangle_t53875121_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_3 = AppleTangle_Data_m3366264679(NULL /*static, unused*/, /*hidden argument*/NULL);
		AppleValidator_t3837389912 * L_4 = (AppleValidator_t3837389912 *)il2cpp_codegen_object_new(AppleValidator_t3837389912_il2cpp_TypeInfo_var);
		AppleValidator__ctor_m1455195731(L_4, L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = V_2;
		NullCheck(L_4);
		AppleReceipt_t3991411794 * L_6 = AppleValidator_Validate_m6433925(L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		AppleReceipt_t3991411794 * L_7 = V_3;
		if (!L_7)
		{
			goto IL_009f;
		}
	}
	{
		AppleReceipt_t3991411794 * L_8 = V_3;
		NullCheck(L_8);
		AppleInAppPurchaseReceiptU5BU5D_t579068208* L_9 = L_8->get_inAppPurchaseReceipts_6();
		V_5 = L_9;
		V_6 = 0;
		goto IL_0094;
	}

IL_0038:
	{
		AppleInAppPurchaseReceiptU5BU5D_t579068208* L_10 = V_5;
		int32_t L_11 = V_6;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		AppleInAppPurchaseReceipt_t3271698749 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_4 = L_13;
		AppleInAppPurchaseReceipt_t3271698749 * L_14 = V_4;
		NullCheck(L_14);
		DateTime_t693205669  L_15 = AppleInAppPurchaseReceipt_get_subscriptionExpirationDate_m3742016707(L_14, /*hidden argument*/NULL);
		V_7 = L_15;
		AppleInAppPurchaseReceipt_t3271698749 * L_16 = V_4;
		NullCheck(L_16);
		String_t* L_17 = AppleInAppPurchaseReceipt_get_productID_m152993489(L_16, /*hidden argument*/NULL);
		V_8 = L_17;
		String_t* L_18 = ___myProduct1;
		String_t* L_19 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_Equals_m2950069882(NULL /*static, unused*/, L_18, L_19, 4, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_008e;
		}
	}
	{
		DateTime_t693205669  L_21 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_22 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_23 = DateTime_op_GreaterThan_m2730200039(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_008e;
		}
	}
	{
		DateTime_t693205669  L_24 = V_7;
		Nullable_1_t3251239280  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Nullable_1__ctor_m4027593417(&L_25, L_24, /*hidden argument*/Nullable_1__ctor_m4027593417_MethodInfo_var);
		V_0 = L_25;
		DateTime_t693205669  L_26 = V_7;
		DateTime_t693205669  L_27 = L_26;
		Il2CppObject * L_28 = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral4272844521, L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_008e:
	{
		int32_t L_30 = V_6;
		V_6 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_0094:
	{
		int32_t L_31 = V_6;
		AppleInAppPurchaseReceiptU5BU5D_t579068208* L_32 = V_5;
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length)))))))
		{
			goto IL_0038;
		}
	}

IL_009f:
	{
		Nullable_1_t3251239280  L_33 = V_0;
		return L_33;
	}
}
// System.Boolean AssemblyCSharp.Purchase::verifyOnStart()
extern Il2CppClass* StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var;
extern Il2CppClass* IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var;
extern Il2CppClass* IAppleConfiguration_t3277762425_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Nullable_1_t3251239280_il2cpp_TypeInfo_var;
extern const MethodInfo* ConfigurationBuilder_Configure_TisIAppleConfiguration_t3277762425_m4232047636_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m4027593417_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m3795771450_MethodInfo_var;
extern const MethodInfo* Nullable_1_GetValueOrDefault_m706662709_MethodInfo_var;
extern const MethodInfo* Nullable_1_ToString_m1419821888_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2752298055;
extern Il2CppCodeGenString* _stringLiteral2328218522;
extern Il2CppCodeGenString* _stringLiteral2946594395;
extern Il2CppCodeGenString* _stringLiteral4220331123;
extern Il2CppCodeGenString* _stringLiteral543987621;
extern Il2CppCodeGenString* _stringLiteral4006860343;
extern const uint32_t Purchase_verifyOnStart_m1690669461_MetadataUsageId;
extern "C"  bool Purchase_verifyOnStart_m1690669461 (Purchase_t1644043085 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_verifyOnStart_m1690669461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConfigurationBuilder_t1298400415 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	String_t* V_2 = NULL;
	Nullable_1_t3251239280  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Nullable_1_t3251239280  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t4003664591_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t4003664591 * L_0 = StandardPurchasingModule_Instance_m2889845773(NULL /*static, unused*/, /*hidden argument*/NULL);
		ConfigurationBuilder_t1298400415 * L_1 = ConfigurationBuilder_Instance_m4243979412(NULL /*static, unused*/, L_0, ((IPurchasingModuleU5BU5D_t4128245854*)SZArrayNew(IPurchasingModuleU5BU5D_t4128245854_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_1;
		ConfigurationBuilder_t1298400415 * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_3 = ConfigurationBuilder_Configure_TisIAppleConfiguration_t3277762425_m4232047636(L_2, /*hidden argument*/ConfigurationBuilder_Configure_TisIAppleConfiguration_t3277762425_m4232047636_MethodInfo_var);
		V_1 = L_3;
		Il2CppObject * L_4 = V_1;
		NullCheck(L_4);
		String_t* L_5 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.Purchasing.IAppleConfiguration::get_appReceipt() */, IAppleConfiguration_t3277762425_il2cpp_TypeInfo_var, L_4);
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		Il2CppObject * L_6 = V_1;
		NullCheck(L_6);
		String_t* L_7 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.Purchasing.IAppleConfiguration::get_appReceipt() */, IAppleConfiguration_t3277762425_il2cpp_TypeInfo_var, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00b4;
		}
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2752298055, /*hidden argument*/NULL);
		String_t* L_10 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, _stringLiteral2328218522, /*hidden argument*/NULL);
		V_2 = L_10;
		String_t* L_11 = V_2;
		if (!L_11)
		{
			goto IL_0063;
		}
	}
	{
		String_t* L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_14 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0065;
		}
	}

IL_0063:
	{
		return (bool)0;
	}

IL_0065:
	{
		String_t* L_15 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_16 = DateTime_Parse_m1142721566(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Nullable_1_t3251239280  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Nullable_1__ctor_m4027593417(&L_17, L_16, /*hidden argument*/Nullable_1__ctor_m4027593417_MethodInfo_var);
		V_3 = L_17;
		bool L_18 = Nullable_1_get_HasValue_m3795771450((&V_3), /*hidden argument*/Nullable_1_get_HasValue_m3795771450_MethodInfo_var);
		if (!L_18)
		{
			goto IL_0090;
		}
	}
	{
		DateTime_t693205669  L_19 = Nullable_1_GetValueOrDefault_m706662709((&V_3), /*hidden argument*/Nullable_1_GetValueOrDefault_m706662709_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_20 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_21 = DateTime_op_GreaterThan_m2730200039(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		G_B8_0 = ((int32_t)(L_21));
		goto IL_0091;
	}

IL_0090:
	{
		G_B8_0 = 0;
	}

IL_0091:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->set_isPurchased_2((bool)G_B8_0);
		bool L_22 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_isPurchased_2();
		bool L_23 = L_22;
		Il2CppObject * L_24 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_23);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2946594395, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_00b4:
	{
		Il2CppObject * L_26 = V_1;
		NullCheck(L_26);
		String_t* L_27 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.Purchasing.IAppleConfiguration::get_appReceipt() */, IAppleConfiguration_t3277762425_il2cpp_TypeInfo_var, L_26);
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		String_t* L_28 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_kProductIDSubscription_9();
		Nullable_1_t3251239280  L_29 = Purchase_validate_m3617248933(__this, L_27, L_28, /*hidden argument*/NULL);
		V_4 = L_29;
		bool L_30 = Nullable_1_get_HasValue_m3795771450((&V_4), /*hidden argument*/Nullable_1_get_HasValue_m3795771450_MethodInfo_var);
		if (!((((int32_t)L_30) == ((int32_t)0))? 1 : 0))
		{
			goto IL_00fa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral4220331123, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->set_isPurchased_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral2328218522, L_31, /*hidden argument*/NULL);
		goto IL_0149;
	}

IL_00fa:
	{
		String_t* L_32 = Nullable_1_ToString_m1419821888((&V_4), /*hidden argument*/Nullable_1_ToString_m1419821888_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral543987621, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->set_isPurchased_2((bool)1);
		String_t* L_34 = Nullable_1_ToString_m1419821888((&V_4), /*hidden argument*/Nullable_1_ToString_m1419821888_MethodInfo_var);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral2328218522, L_34, /*hidden argument*/NULL);
		Nullable_1_t3251239280  L_35 = V_4;
		Nullable_1_t3251239280  L_36 = L_35;
		Il2CppObject * L_37 = Box(Nullable_1_t3251239280_il2cpp_TypeInfo_var, &L_36);
		String_t* L_38 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral4006860343, L_37, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
	}

IL_0149:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		bool L_39 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_isPurchased_2();
		bool L_40 = L_39;
		Il2CppObject * L_41 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_40);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2946594395, L_41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0164:
	{
		return (bool)0;
	}
}
// System.Void AssemblyCSharp.Purchase::.cctor()
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral383851855;
extern Il2CppCodeGenString* _stringLiteral4118854190;
extern const uint32_t Purchase__cctor_m3135322714_MetadataUsageId;
extern "C"  void Purchase__cctor_m3135322714 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase__cctor_m3135322714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->set_kProductIDConsumable_7(_stringLiteral383851855);
		((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->set_kProductIDNonConsumable_8(_stringLiteral4118854190);
		return;
	}
}
// System.Void AssemblyCSharp.Purchase::<RestorePurchases>m__0(System.Boolean)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral814425683;
extern Il2CppCodeGenString* _stringLiteral3902255188;
extern Il2CppCodeGenString* _stringLiteral2933046699;
extern const uint32_t Purchase_U3CRestorePurchasesU3Em__0_m3309735971_MetadataUsageId;
extern "C"  void Purchase_U3CRestorePurchasesU3Em__0_m3309735971 (Purchase_t1644043085 * __this, bool ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchase_U3CRestorePurchasesU3Em__0_m3309735971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Purchase_verifyOnStart_m1690669461(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral814425683, /*hidden argument*/NULL);
	}

IL_0015:
	{
		bool L_1 = ___result0;
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral3902255188, L_3, _stringLiteral2933046699, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonAnimation::.ctor()
extern "C"  void ButtonAnimation__ctor_m484333939 (ButtonAnimation_t1426385366 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonAnimation::Start()
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisButton_t2872111280_m3197608264_MethodInfo_var;
extern const MethodInfo* ButtonAnimation_hideButton_m857075365_MethodInfo_var;
extern const uint32_t ButtonAnimation_Start_m1237470215_MetadataUsageId;
extern "C"  void ButtonAnimation_Start_m1237470215 (ButtonAnimation_t1426385366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonAnimation_Start_m1237470215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	{
		Button_t2872111280 * L_0 = Component_GetComponent_TisButton_t2872111280_m3197608264(__this, /*hidden argument*/Component_GetComponent_TisButton_t2872111280_m3197608264_MethodInfo_var);
		V_0 = L_0;
		Button_t2872111280 * L_1 = V_0;
		NullCheck(L_1);
		ButtonClickedEvent_t2455055323 * L_2 = Button_get_onClick_m1595880935(L_1, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)ButtonAnimation_hideButton_m857075365_MethodInfo_var);
		UnityAction_t4025899511 * L_4 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		UnityEvent_AddListener_m1596810379(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonAnimation::Update()
extern "C"  void ButtonAnimation_Update_m2507586176 (ButtonAnimation_t1426385366 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ButtonAnimation::hideButton()
extern "C"  void ButtonAnimation_hideButton_m857075365 (ButtonAnimation_t1426385366 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ButtonAnimation::showButton()
extern "C"  void ButtonAnimation_showButton_m143411492 (ButtonAnimation_t1426385366 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FPSDisplay::.ctor()
extern "C"  void FPSDisplay__ctor_m1019612258 (FPSDisplay_t3952194245 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FPSDisplay::Update()
extern "C"  void FPSDisplay_Update_m3681504265 (FPSDisplay_t3952194245 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_deltaTime_2();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_deltaTime_2();
		__this->set_deltaTime_2(((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)(0.1f))))));
		return;
	}
}
// System.Void FPSDisplay::OnGUI()
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3263054191;
extern const uint32_t FPSDisplay_OnGUI_m4029818262_MetadataUsageId;
extern "C"  void FPSDisplay_OnGUI_m4029818262 (FPSDisplay_t3952194245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FPSDisplay_OnGUI_m4029818262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GUIStyle_t1799908754 * V_2 = NULL;
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	String_t* V_6 = NULL;
	{
		int32_t L_0 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		GUIStyle_t1799908754 * L_2 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_2, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_0;
		int32_t L_4 = V_1;
		Rect__ctor_m1220545469((&V_3), (0.0f), (0.0f), (((float)((float)L_3))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4*(int32_t)2))/(int32_t)((int32_t)100)))))), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_5 = V_2;
		NullCheck(L_5);
		GUIStyle_set_alignment_m1024943876(L_5, 0, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_6 = V_2;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		GUIStyle_set_fontSize_m4015341543(L_6, ((int32_t)((int32_t)((int32_t)((int32_t)L_7*(int32_t)2))/(int32_t)((int32_t)100))), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_8 = V_2;
		NullCheck(L_8);
		GUIStyleState_t3801000545 * L_9 = GUIStyle_get_normal_m2789468942(L_8, /*hidden argument*/NULL);
		Color_t2020392075  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color__ctor_m1909920690(&L_10, (0.0f), (0.0f), (0.5f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		GUIStyleState_set_textColor_m3970174237(L_9, L_10, /*hidden argument*/NULL);
		float L_11 = __this->get_deltaTime_2();
		V_4 = ((float)((float)L_11*(float)(1000.0f)));
		float L_12 = __this->get_deltaTime_2();
		V_5 = ((float)((float)(1.0f)/(float)L_12));
		float L_13 = V_4;
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		float L_16 = V_5;
		float L_17 = L_16;
		Il2CppObject * L_18 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3263054191, L_15, L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		Rect_t3681755626  L_20 = V_3;
		String_t* L_21 = V_6;
		GUIStyle_t1799908754 * L_22 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2231582000(NULL /*static, unused*/, L_20, L_21, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Handler::.ctor()
extern "C"  void Handler__ctor_m3548521471 (Handler_t2658839040 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Coroutine Handler::StartDelayed(System.Single,System.Boolean,UnityEngine.MonoBehaviour)
extern "C"  Coroutine_t2299508840 * Handler_StartDelayed_m880710460 (Il2CppObject * __this /* static, unused */, float ___waitSeconds0, bool ___isValable1, MonoBehaviour_t1158329972 * ___behaviour2, const MethodInfo* method)
{
	{
		MonoBehaviour_t1158329972 * L_0 = ___behaviour2;
		float L_1 = ___waitSeconds0;
		bool L_2 = ___isValable1;
		Il2CppObject * L_3 = Handler_WaitForSeconds_m3157568414(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Coroutine_t2299508840 * L_4 = MonoBehaviour_StartCoroutine_m2470621050(L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Coroutine Handler::StartDelayed(System.Single,Handler/Execute,UnityEngine.MonoBehaviour)
extern "C"  Coroutine_t2299508840 * Handler_StartDelayed_m4016449873 (Il2CppObject * __this /* static, unused */, float ___waitSeconds0, Execute_t1389284496 * ___execute1, MonoBehaviour_t1158329972 * ___behaviour2, const MethodInfo* method)
{
	{
		MonoBehaviour_t1158329972 * L_0 = ___behaviour2;
		float L_1 = ___waitSeconds0;
		Execute_t1389284496 * L_2 = ___execute1;
		Il2CppObject * L_3 = Handler_WaitForSeconds_m2283772139(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Coroutine_t2299508840 * L_4 = MonoBehaviour_StartCoroutine_m2470621050(L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Coroutine Handler::StartDelayed(System.Single,Handler/ExecuteWithGameObjcet,UnityEngine.GameObject,UnityEngine.MonoBehaviour)
extern "C"  Coroutine_t2299508840 * Handler_StartDelayed_m1932308614 (Il2CppObject * __this /* static, unused */, float ___waitSeconds0, ExecuteWithGameObjcet_t1191175011 * ___execute1, GameObject_t1756533147 * ___GO2, MonoBehaviour_t1158329972 * ___behaviour3, const MethodInfo* method)
{
	{
		MonoBehaviour_t1158329972 * L_0 = ___behaviour3;
		float L_1 = ___waitSeconds0;
		ExecuteWithGameObjcet_t1191175011 * L_2 = ___execute1;
		GameObject_t1756533147 * L_3 = ___GO2;
		Il2CppObject * L_4 = Handler_WaitForSeconds_m1128382948(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		Coroutine_t2299508840 * L_5 = MonoBehaviour_StartCoroutine_m2470621050(L_0, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Coroutine Handler::StartDelayed(System.Single,Handler/ExecuteWithButton,UnityEngine.UI.Button,UnityEngine.MonoBehaviour)
extern "C"  Coroutine_t2299508840 * Handler_StartDelayed_m3476731120 (Il2CppObject * __this /* static, unused */, float ___waitSeconds0, ExecuteWithButton_t3718107720 * ___execute1, Button_t2872111280 * ___btn2, MonoBehaviour_t1158329972 * ___behaviour3, const MethodInfo* method)
{
	{
		MonoBehaviour_t1158329972 * L_0 = ___behaviour3;
		float L_1 = ___waitSeconds0;
		ExecuteWithButton_t3718107720 * L_2 = ___execute1;
		Button_t2872111280 * L_3 = ___btn2;
		Il2CppObject * L_4 = Handler_WaitForSeconds_m2136273796(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		Coroutine_t2299508840 * L_5 = MonoBehaviour_StartCoroutine_m2470621050(L_0, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void Handler::StopDelayed(UnityEngine.Coroutine,UnityEngine.MonoBehaviour)
extern "C"  void Handler_StopDelayed_m2765900203 (Il2CppObject * __this /* static, unused */, Coroutine_t2299508840 * ___coroutine0, MonoBehaviour_t1158329972 * ___behaviour1, const MethodInfo* method)
{
	{
		Coroutine_t2299508840 * L_0 = ___coroutine0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		MonoBehaviour_t1158329972 * L_1 = ___behaviour1;
		Coroutine_t2299508840 * L_2 = ___coroutine0;
		NullCheck(L_1);
		MonoBehaviour_StopCoroutine_m1668572632(L_1, L_2, /*hidden argument*/NULL);
	}

IL_000d:
	{
		return;
	}
}
// System.Collections.IEnumerator Handler::WaitForSeconds(System.Single,System.Boolean)
extern Il2CppClass* U3CWaitForSecondsU3Ec__Iterator0_t4015752965_il2cpp_TypeInfo_var;
extern const uint32_t Handler_WaitForSeconds_m3157568414_MetadataUsageId;
extern "C"  Il2CppObject * Handler_WaitForSeconds_m3157568414 (Il2CppObject * __this /* static, unused */, float ___time0, bool ___isValable1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Handler_WaitForSeconds_m3157568414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * V_0 = NULL;
	{
		U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * L_0 = (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 *)il2cpp_codegen_object_new(U3CWaitForSecondsU3Ec__Iterator0_t4015752965_il2cpp_TypeInfo_var);
		U3CWaitForSecondsU3Ec__Iterator0__ctor_m1894433998(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * L_1 = V_0;
		float L_2 = ___time0;
		NullCheck(L_1);
		L_1->set_time_0(L_2);
		U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * L_3 = V_0;
		bool L_4 = ___isValable1;
		NullCheck(L_3);
		L_3->set_isValable_1(L_4);
		U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * L_5 = V_0;
		return L_5;
	}
}
// System.Collections.IEnumerator Handler::WaitForSeconds(System.Single,Handler/Execute)
extern Il2CppClass* U3CWaitForSecondsU3Ec__Iterator1_t2449669024_il2cpp_TypeInfo_var;
extern const uint32_t Handler_WaitForSeconds_m2283772139_MetadataUsageId;
extern "C"  Il2CppObject * Handler_WaitForSeconds_m2283772139 (Il2CppObject * __this /* static, unused */, float ___time0, Execute_t1389284496 * ___method1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Handler_WaitForSeconds_m2283772139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * V_0 = NULL;
	{
		U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * L_0 = (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 *)il2cpp_codegen_object_new(U3CWaitForSecondsU3Ec__Iterator1_t2449669024_il2cpp_TypeInfo_var);
		U3CWaitForSecondsU3Ec__Iterator1__ctor_m2602417577(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * L_1 = V_0;
		float L_2 = ___time0;
		NullCheck(L_1);
		L_1->set_time_0(L_2);
		U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * L_3 = V_0;
		Execute_t1389284496 * L_4 = ___method1;
		NullCheck(L_3);
		L_3->set_method_1(L_4);
		U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * L_5 = V_0;
		return L_5;
	}
}
// System.Collections.IEnumerator Handler::WaitForSeconds(System.Single,Handler/ExecuteWithGameObjcet,UnityEngine.GameObject)
extern Il2CppClass* U3CWaitForSecondsU3Ec__Iterator2_t2852953551_il2cpp_TypeInfo_var;
extern const uint32_t Handler_WaitForSeconds_m1128382948_MetadataUsageId;
extern "C"  Il2CppObject * Handler_WaitForSeconds_m1128382948 (Il2CppObject * __this /* static, unused */, float ___time0, ExecuteWithGameObjcet_t1191175011 * ___method1, GameObject_t1756533147 * ___GO2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Handler_WaitForSeconds_m1128382948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * V_0 = NULL;
	{
		U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * L_0 = (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 *)il2cpp_codegen_object_new(U3CWaitForSecondsU3Ec__Iterator2_t2852953551_il2cpp_TypeInfo_var);
		U3CWaitForSecondsU3Ec__Iterator2__ctor_m3934226712(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * L_1 = V_0;
		float L_2 = ___time0;
		NullCheck(L_1);
		L_1->set_time_0(L_2);
		U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * L_3 = V_0;
		ExecuteWithGameObjcet_t1191175011 * L_4 = ___method1;
		NullCheck(L_3);
		L_3->set_method_1(L_4);
		U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * L_5 = V_0;
		GameObject_t1756533147 * L_6 = ___GO2;
		NullCheck(L_5);
		L_5->set_GO_2(L_6);
		U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * L_7 = V_0;
		return L_7;
	}
}
// System.Collections.IEnumerator Handler::WaitForSeconds(System.Single,Handler/ExecuteWithButton,UnityEngine.UI.Button)
extern Il2CppClass* U3CWaitForSecondsU3Ec__Iterator3_t1286869610_il2cpp_TypeInfo_var;
extern const uint32_t Handler_WaitForSeconds_m2136273796_MetadataUsageId;
extern "C"  Il2CppObject * Handler_WaitForSeconds_m2136273796 (Il2CppObject * __this /* static, unused */, float ___time0, ExecuteWithButton_t3718107720 * ___method1, Button_t2872111280 * ___btn2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Handler_WaitForSeconds_m2136273796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * V_0 = NULL;
	{
		U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * L_0 = (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 *)il2cpp_codegen_object_new(U3CWaitForSecondsU3Ec__Iterator3_t1286869610_il2cpp_TypeInfo_var);
		U3CWaitForSecondsU3Ec__Iterator3__ctor_m2125104883(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * L_1 = V_0;
		float L_2 = ___time0;
		NullCheck(L_1);
		L_1->set_time_0(L_2);
		U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * L_3 = V_0;
		ExecuteWithButton_t3718107720 * L_4 = ___method1;
		NullCheck(L_3);
		L_3->set_method_1(L_4);
		U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * L_5 = V_0;
		Button_t2872111280 * L_6 = ___btn2;
		NullCheck(L_5);
		L_5->set_btn_2(L_6);
		U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * L_7 = V_0;
		return L_7;
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator0::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0__ctor_m1894433998 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Handler/<WaitForSeconds>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForSecondsU3Ec__Iterator0_MoveNext_m2095135530_MetadataUsageId;
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator0_MoveNext_m2095135530 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForSecondsU3Ec__Iterator0_MoveNext_m2095135530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_0054;
	}

IL_0021:
	{
		float L_2 = __this->get_time_0();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_2(L_3);
		bool L_4 = __this->get_U24disposing_3();
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0041:
	{
		goto IL_0056;
	}

IL_0046:
	{
		__this->set_isValable_1((bool)1);
		__this->set_U24PC_4((-1));
	}

IL_0054:
	{
		return (bool)0;
	}

IL_0056:
	{
		return (bool)1;
	}
}
// System.Object Handler/<WaitForSeconds>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2981468158 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Handler/<WaitForSeconds>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2172460022 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator0::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Dispose_m46014511 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForSecondsU3Ec__Iterator0_Reset_m3335393237_MetadataUsageId;
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Reset_m3335393237 (U3CWaitForSecondsU3Ec__Iterator0_t4015752965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForSecondsU3Ec__Iterator0_Reset_m3335393237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator1::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator1__ctor_m2602417577 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Handler/<WaitForSeconds>c__Iterator1::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForSecondsU3Ec__Iterator1_MoveNext_m882073451_MetadataUsageId;
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator1_MoveNext_m882073451 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForSecondsU3Ec__Iterator1_MoveNext_m882073451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_0058;
	}

IL_0021:
	{
		float L_2 = __this->get_time_0();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_2(L_3);
		bool L_4 = __this->get_U24disposing_3();
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0041:
	{
		goto IL_005a;
	}

IL_0046:
	{
		Execute_t1389284496 * L_5 = __this->get_method_1();
		NullCheck(L_5);
		Execute_Invoke_m2096048023(L_5, /*hidden argument*/NULL);
		__this->set_U24PC_4((-1));
	}

IL_0058:
	{
		return (bool)0;
	}

IL_005a:
	{
		return (bool)1;
	}
}
// System.Object Handler/<WaitForSeconds>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m762440515 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Handler/<WaitForSeconds>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2115095867 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator1::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator1_Dispose_m3768844650 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForSecondsU3Ec__Iterator1_Reset_m1526271408_MetadataUsageId;
extern "C"  void U3CWaitForSecondsU3Ec__Iterator1_Reset_m1526271408 (U3CWaitForSecondsU3Ec__Iterator1_t2449669024 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForSecondsU3Ec__Iterator1_Reset_m1526271408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator2::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator2__ctor_m3934226712 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Handler/<WaitForSeconds>c__Iterator2::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForSecondsU3Ec__Iterator2_MoveNext_m226710572_MetadataUsageId;
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator2_MoveNext_m226710572 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForSecondsU3Ec__Iterator2_MoveNext_m226710572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_005e;
	}

IL_0021:
	{
		float L_2 = __this->get_time_0();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_3(L_3);
		bool L_4 = __this->get_U24disposing_4();
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0041:
	{
		goto IL_0060;
	}

IL_0046:
	{
		ExecuteWithGameObjcet_t1191175011 * L_5 = __this->get_method_1();
		GameObject_t1756533147 * L_6 = __this->get_GO_2();
		NullCheck(L_5);
		ExecuteWithGameObjcet_Invoke_m2016891188(L_5, L_6, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_005e:
	{
		return (bool)0;
	}

IL_0060:
	{
		return (bool)1;
	}
}
// System.Object Handler/<WaitForSeconds>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m585009908 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Handler/<WaitForSeconds>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3960121836 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator2::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator2_Dispose_m179864869 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForSecondsU3Ec__Iterator2_Reset_m3747011487_MetadataUsageId;
extern "C"  void U3CWaitForSecondsU3Ec__Iterator2_Reset_m3747011487 (U3CWaitForSecondsU3Ec__Iterator2_t2852953551 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForSecondsU3Ec__Iterator2_Reset_m3747011487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator3::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator3__ctor_m2125104883 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Handler/<WaitForSeconds>c__Iterator3::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForSecondsU3Ec__Iterator3_MoveNext_m3308615789_MetadataUsageId;
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator3_MoveNext_m3308615789 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForSecondsU3Ec__Iterator3_MoveNext_m3308615789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_005e;
	}

IL_0021:
	{
		float L_2 = __this->get_time_0();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_3(L_3);
		bool L_4 = __this->get_U24disposing_4();
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0041:
	{
		goto IL_0060;
	}

IL_0046:
	{
		ExecuteWithButton_t3718107720 * L_5 = __this->get_method_1();
		Button_t2872111280 * L_6 = __this->get_btn_2();
		NullCheck(L_5);
		ExecuteWithButton_Invoke_m2824180992(L_5, L_6, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_005e:
	{
		return (bool)0;
	}

IL_0060:
	{
		return (bool)1;
	}
}
// System.Object Handler/<WaitForSeconds>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3184991801 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Handler/<WaitForSeconds>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3737969969 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator3::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator3_Dispose_m1067827552 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Handler/<WaitForSeconds>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForSecondsU3Ec__Iterator3_Reset_m160027770_MetadataUsageId;
extern "C"  void U3CWaitForSecondsU3Ec__Iterator3_Reset_m160027770 (U3CWaitForSecondsU3Ec__Iterator3_t1286869610 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForSecondsU3Ec__Iterator3_Reset_m160027770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Handler/Execute::.ctor(System.Object,System.IntPtr)
extern "C"  void Execute__ctor_m2841091363 (Execute_t1389284496 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Handler/Execute::Invoke()
extern "C"  void Execute_Invoke_m2096048023 (Execute_t1389284496 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Execute_Invoke_m2096048023((Execute_t1389284496 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_Execute_t1389284496 (Execute_t1389284496 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult Handler/Execute::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Execute_BeginInvoke_m1349574186 (Execute_t1389284496 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Handler/Execute::EndInvoke(System.IAsyncResult)
extern "C"  void Execute_EndInvoke_m3596557593 (Execute_t1389284496 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Handler/ExecuteWithButton::.ctor(System.Object,System.IntPtr)
extern "C"  void ExecuteWithButton__ctor_m1880605207 (ExecuteWithButton_t3718107720 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Handler/ExecuteWithButton::Invoke(UnityEngine.UI.Button)
extern "C"  void ExecuteWithButton_Invoke_m2824180992 (ExecuteWithButton_t3718107720 * __this, Button_t2872111280 * ___btns0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExecuteWithButton_Invoke_m2824180992((ExecuteWithButton_t3718107720 *)__this->get_prev_9(),___btns0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Button_t2872111280 * ___btns0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___btns0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Button_t2872111280 * ___btns0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___btns0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___btns0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Handler/ExecuteWithButton::BeginInvoke(UnityEngine.UI.Button,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExecuteWithButton_BeginInvoke_m2797783651 (ExecuteWithButton_t3718107720 * __this, Button_t2872111280 * ___btns0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___btns0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Handler/ExecuteWithButton::EndInvoke(System.IAsyncResult)
extern "C"  void ExecuteWithButton_EndInvoke_m1258134841 (ExecuteWithButton_t3718107720 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Handler/ExecuteWithGameObjcet::.ctor(System.Object,System.IntPtr)
extern "C"  void ExecuteWithGameObjcet__ctor_m342781680 (ExecuteWithGameObjcet_t1191175011 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Handler/ExecuteWithGameObjcet::Invoke(UnityEngine.GameObject)
extern "C"  void ExecuteWithGameObjcet_Invoke_m2016891188 (ExecuteWithGameObjcet_t1191175011 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExecuteWithGameObjcet_Invoke_m2016891188((ExecuteWithGameObjcet_t1191175011 *)__this->get_prev_9(),___go0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, GameObject_t1756533147 * ___go0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___go0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, GameObject_t1756533147 * ___go0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___go0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___go0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Handler/ExecuteWithGameObjcet::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExecuteWithGameObjcet_BeginInvoke_m3662776725 (ExecuteWithGameObjcet_t1191175011 * __this, GameObject_t1756533147 * ___go0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___go0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Handler/ExecuteWithGameObjcet::EndInvoke(System.IAsyncResult)
extern "C"  void ExecuteWithGameObjcet_EndInvoke_m1737415974 (ExecuteWithGameObjcet_t1191175011 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Kakera.PickerController::.ctor()
extern "C"  void PickerController__ctor_m2804163018 (PickerController_t3670494704 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kakera.PickerController::Awake()
extern Il2CppClass* ImageDelegate_t3548206662_il2cpp_TypeInfo_var;
extern const MethodInfo* PickerController_U3CAwakeU3Em__0_m1932660688_MethodInfo_var;
extern const uint32_t PickerController_Awake_m238727193_MetadataUsageId;
extern "C"  void PickerController_Awake_m238727193 (PickerController_t3670494704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PickerController_Awake_m238727193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Unimgpicker_t2332148304 * L_0 = __this->get_imagePicker_2();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)PickerController_U3CAwakeU3Em__0_m1932660688_MethodInfo_var);
		ImageDelegate_t3548206662 * L_2 = (ImageDelegate_t3548206662 *)il2cpp_codegen_object_new(ImageDelegate_t3548206662_il2cpp_TypeInfo_var);
		ImageDelegate__ctor_m1094922347(L_2, __this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Unimgpicker_add_Completed_m169132615(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kakera.PickerController::OnPressShowPicker()
extern Il2CppCodeGenString* _stringLiteral2529633983;
extern Il2CppCodeGenString* _stringLiteral3804262974;
extern const uint32_t PickerController_OnPressShowPicker_m1265903567_MetadataUsageId;
extern "C"  void PickerController_OnPressShowPicker_m1265903567 (PickerController_t3670494704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PickerController_OnPressShowPicker_m1265903567_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Unimgpicker_t2332148304 * L_0 = __this->get_imagePicker_2();
		NullCheck(L_0);
		Unimgpicker_Show_m3309216668(L_0, _stringLiteral2529633983, _stringLiteral3804262974, ((int32_t)1024), /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Kakera.PickerController::LoadImage(System.String,UnityEngine.UI.Image)
extern Il2CppClass* U3CLoadImageU3Ec__Iterator0_t2450652310_il2cpp_TypeInfo_var;
extern const uint32_t PickerController_LoadImage_m2557733243_MetadataUsageId;
extern "C"  Il2CppObject * PickerController_LoadImage_m2557733243 (PickerController_t3670494704 * __this, String_t* ___path0, Image_t2042527209 * ___output1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PickerController_LoadImage_m2557733243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadImageU3Ec__Iterator0_t2450652310 * V_0 = NULL;
	{
		U3CLoadImageU3Ec__Iterator0_t2450652310 * L_0 = (U3CLoadImageU3Ec__Iterator0_t2450652310 *)il2cpp_codegen_object_new(U3CLoadImageU3Ec__Iterator0_t2450652310_il2cpp_TypeInfo_var);
		U3CLoadImageU3Ec__Iterator0__ctor_m1214713917(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadImageU3Ec__Iterator0_t2450652310 * L_1 = V_0;
		String_t* L_2 = ___path0;
		NullCheck(L_1);
		L_1->set_path_0(L_2);
		U3CLoadImageU3Ec__Iterator0_t2450652310 * L_3 = V_0;
		Image_t2042527209 * L_4 = ___output1;
		NullCheck(L_3);
		L_3->set_output_4(L_4);
		U3CLoadImageU3Ec__Iterator0_t2450652310 * L_5 = V_0;
		return L_5;
	}
}
// System.Void Kakera.PickerController::<Awake>m__0(System.String)
extern "C"  void PickerController_U3CAwakeU3Em__0_m1932660688 (PickerController_t3670494704 * __this, String_t* ___path0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___path0;
		Image_t2042527209 * L_1 = __this->get_imageRenderer_3();
		Il2CppObject * L_2 = PickerController_LoadImage_m2557733243(__this, L_0, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___path0;
		Image_t2042527209 * L_4 = __this->get_imagePortretRenderer_4();
		Il2CppObject * L_5 = PickerController_LoadImage_m2557733243(__this, L_3, L_4, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kakera.PickerController/<LoadImage>c__Iterator0::.ctor()
extern "C"  void U3CLoadImageU3Ec__Iterator0__ctor_m1214713917 (U3CLoadImageU3Ec__Iterator0_t2450652310 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Kakera.PickerController/<LoadImage>c__Iterator0::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3083327535;
extern Il2CppCodeGenString* _stringLiteral2562837341;
extern Il2CppCodeGenString* _stringLiteral2494949592;
extern Il2CppCodeGenString* _stringLiteral1857935903;
extern Il2CppCodeGenString* _stringLiteral33571700;
extern const uint32_t U3CLoadImageU3Ec__Iterator0_MoveNext_m1921802115_MetadataUsageId;
extern "C"  bool U3CLoadImageU3Ec__Iterator0_MoveNext_m1921802115 (U3CLoadImageU3Ec__Iterator0_t2450652310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadImageU3Ec__Iterator0_MoveNext_m1921802115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_008d;
		}
	}
	{
		goto IL_013b;
	}

IL_0021:
	{
		String_t* L_2 = __this->get_path_0();
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3083327535, L_2, /*hidden argument*/NULL);
		String_t* L_3 = __this->get_path_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2562837341, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_path_0();
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2494949592, L_5, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_1(L_6);
		String_t* L_7 = __this->get_U3CurlU3E__0_1();
		WWW_t2919945039 * L_8 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_8, L_7, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__1_2(L_8);
		WWW_t2919945039 * L_9 = __this->get_U3CwwwU3E__1_2();
		__this->set_U24current_5(L_9);
		bool L_10 = __this->get_U24disposing_6();
		if (L_10)
		{
			goto IL_0088;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0088:
	{
		goto IL_013d;
	}

IL_008d:
	{
		WWW_t2919945039 * L_11 = __this->get_U3CwwwU3E__1_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1857935903, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		WWW_t2919945039 * L_13 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_13);
		Texture2D_t3542995729 * L_14 = WWW_get_texture_m1121178301(L_13, /*hidden argument*/NULL);
		__this->set_U3CtextureU3E__2_3(L_14);
		Texture2D_t3542995729 * L_15 = __this->get_U3CtextureU3E__2_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_15, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00d9;
		}
	}
	{
		String_t* L_17 = __this->get_U3CurlU3E__0_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral33571700, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_00d9:
	{
		Image_t2042527209 * L_19 = __this->get_output_4();
		WWW_t2919945039 * L_20 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_20);
		Texture2D_t3542995729 * L_21 = WWW_get_texture_m1121178301(L_20, /*hidden argument*/NULL);
		WWW_t2919945039 * L_22 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_22);
		Texture2D_t3542995729 * L_23 = WWW_get_texture_m1121178301(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		int32_t L_24 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_23);
		WWW_t2919945039 * L_25 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_25);
		Texture2D_t3542995729 * L_26 = WWW_get_texture_m1121178301(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_26);
		Rect_t3681755626  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Rect__ctor_m1220545469(&L_28, (0.0f), (0.0f), (((float)((float)L_24))), (((float)((float)L_27))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Vector2__ctor_m3067419446(&L_29, (0.0f), (0.0f), /*hidden argument*/NULL);
		Sprite_t309593783 * L_30 = Sprite_Create_m3262956430(NULL /*static, unused*/, L_21, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_19);
		Image_set_sprite_m1800056820(L_19, L_30, /*hidden argument*/NULL);
		__this->set_U24PC_7((-1));
	}

IL_013b:
	{
		return (bool)0;
	}

IL_013d:
	{
		return (bool)1;
	}
}
// System.Object Kakera.PickerController/<LoadImage>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadImageU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3547872455 (U3CLoadImageU3Ec__Iterator0_t2450652310 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object Kakera.PickerController/<LoadImage>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadImageU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1803205551 (U3CLoadImageU3Ec__Iterator0_t2450652310 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void Kakera.PickerController/<LoadImage>c__Iterator0::Dispose()
extern "C"  void U3CLoadImageU3Ec__Iterator0_Dispose_m806514424 (U3CLoadImageU3Ec__Iterator0_t2450652310 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void Kakera.PickerController/<LoadImage>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadImageU3Ec__Iterator0_Reset_m1802674170_MetadataUsageId;
extern "C"  void U3CLoadImageU3Ec__Iterator0_Reset_m1802674170 (U3CLoadImageU3Ec__Iterator0_t2450652310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadImageU3Ec__Iterator0_Reset_m1802674170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Kakera.PickeriOS::.ctor()
extern "C"  void PickeriOS__ctor_m2158368285 (PickeriOS_t3652150543 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL Unimgpicker_show(char*, char*, int32_t);
// System.Void Kakera.PickeriOS::Unimgpicker_show(System.String,System.String,System.Int32)
extern "C"  void PickeriOS_Unimgpicker_show_m114728490 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___outputFileName1, int32_t ___maxSize2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, int32_t);

	// Marshaling of parameter '___title0' to native representation
	char* ____title0_marshaled = NULL;
	____title0_marshaled = il2cpp_codegen_marshal_string(___title0);

	// Marshaling of parameter '___outputFileName1' to native representation
	char* ____outputFileName1_marshaled = NULL;
	____outputFileName1_marshaled = il2cpp_codegen_marshal_string(___outputFileName1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Unimgpicker_show)(____title0_marshaled, ____outputFileName1_marshaled, ___maxSize2);

	// Marshaling cleanup of parameter '___title0' native representation
	il2cpp_codegen_marshal_free(____title0_marshaled);
	____title0_marshaled = NULL;

	// Marshaling cleanup of parameter '___outputFileName1' native representation
	il2cpp_codegen_marshal_free(____outputFileName1_marshaled);
	____outputFileName1_marshaled = NULL;

}
// System.Void Kakera.PickeriOS::Show(System.String,System.String,System.Int32)
extern "C"  void PickeriOS_Show_m4287527539 (PickeriOS_t3652150543 * __this, String_t* ___title0, String_t* ___outputFileName1, int32_t ___maxSize2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title0;
		String_t* L_1 = ___outputFileName1;
		int32_t L_2 = ___maxSize2;
		PickeriOS_Unimgpicker_show_m114728490(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kakera.PickerUnsupported::.ctor()
extern "C"  void PickerUnsupported__ctor_m1506088397 (PickerUnsupported_t1738158813 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kakera.PickerUnsupported::Show(System.String,System.String,System.Int32)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral205876309;
extern Il2CppCodeGenString* _stringLiteral688240414;
extern Il2CppCodeGenString* _stringLiteral3506746329;
extern const uint32_t PickerUnsupported_Show_m1565248927_MetadataUsageId;
extern "C"  void PickerUnsupported_Show_m1565248927 (PickerUnsupported_t1738158813 * __this, String_t* ___title0, String_t* ___outputFileName1, int32_t ___maxSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PickerUnsupported_Show_m1565248927_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	{
		V_0 = _stringLiteral205876309;
		String_t* L_0 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral688240414, /*hidden argument*/NULL);
		V_1 = L_1;
		GameObject_t1756533147 * L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		GameObject_t1756533147 * L_4 = V_1;
		String_t* L_5 = V_0;
		NullCheck(L_4);
		GameObject_SendMessage_m2115020133(L_4, _stringLiteral3506746329, L_5, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void Kakera.Rotator::.ctor()
extern "C"  void Rotator__ctor_m1196604511 (Rotator_t3287300421 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kakera.Rotator::Update()
extern "C"  void Rotator_Update_m3475716304 (Rotator_t3287300421 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Kakera.Unimgpicker::.ctor()
extern Il2CppClass* PickeriOS_t3652150543_il2cpp_TypeInfo_var;
extern const uint32_t Unimgpicker__ctor_m4196344082_MetadataUsageId;
extern "C"  void Unimgpicker__ctor_m4196344082 (Unimgpicker_t2332148304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Unimgpicker__ctor_m4196344082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PickeriOS_t3652150543 * L_0 = (PickeriOS_t3652150543 *)il2cpp_codegen_object_new(PickeriOS_t3652150543_il2cpp_TypeInfo_var);
		PickeriOS__ctor_m2158368285(L_0, /*hidden argument*/NULL);
		__this->set_picker_4(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Kakera.Unimgpicker::add_Completed(Kakera.Unimgpicker/ImageDelegate)
extern Il2CppClass* ImageDelegate_t3548206662_il2cpp_TypeInfo_var;
extern const uint32_t Unimgpicker_add_Completed_m169132615_MetadataUsageId;
extern "C"  void Unimgpicker_add_Completed_m169132615 (Unimgpicker_t2332148304 * __this, ImageDelegate_t3548206662 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Unimgpicker_add_Completed_m169132615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ImageDelegate_t3548206662 * V_0 = NULL;
	ImageDelegate_t3548206662 * V_1 = NULL;
	{
		ImageDelegate_t3548206662 * L_0 = __this->get_Completed_2();
		V_0 = L_0;
	}

IL_0007:
	{
		ImageDelegate_t3548206662 * L_1 = V_0;
		V_1 = L_1;
		ImageDelegate_t3548206662 ** L_2 = __this->get_address_of_Completed_2();
		ImageDelegate_t3548206662 * L_3 = V_1;
		ImageDelegate_t3548206662 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		ImageDelegate_t3548206662 * L_6 = V_0;
		ImageDelegate_t3548206662 * L_7 = InterlockedCompareExchangeImpl<ImageDelegate_t3548206662 *>(L_2, ((ImageDelegate_t3548206662 *)CastclassSealed(L_5, ImageDelegate_t3548206662_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		ImageDelegate_t3548206662 * L_8 = V_0;
		ImageDelegate_t3548206662 * L_9 = V_1;
		if ((!(((Il2CppObject*)(ImageDelegate_t3548206662 *)L_8) == ((Il2CppObject*)(ImageDelegate_t3548206662 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Kakera.Unimgpicker::remove_Completed(Kakera.Unimgpicker/ImageDelegate)
extern Il2CppClass* ImageDelegate_t3548206662_il2cpp_TypeInfo_var;
extern const uint32_t Unimgpicker_remove_Completed_m1384417240_MetadataUsageId;
extern "C"  void Unimgpicker_remove_Completed_m1384417240 (Unimgpicker_t2332148304 * __this, ImageDelegate_t3548206662 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Unimgpicker_remove_Completed_m1384417240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ImageDelegate_t3548206662 * V_0 = NULL;
	ImageDelegate_t3548206662 * V_1 = NULL;
	{
		ImageDelegate_t3548206662 * L_0 = __this->get_Completed_2();
		V_0 = L_0;
	}

IL_0007:
	{
		ImageDelegate_t3548206662 * L_1 = V_0;
		V_1 = L_1;
		ImageDelegate_t3548206662 ** L_2 = __this->get_address_of_Completed_2();
		ImageDelegate_t3548206662 * L_3 = V_1;
		ImageDelegate_t3548206662 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		ImageDelegate_t3548206662 * L_6 = V_0;
		ImageDelegate_t3548206662 * L_7 = InterlockedCompareExchangeImpl<ImageDelegate_t3548206662 *>(L_2, ((ImageDelegate_t3548206662 *)CastclassSealed(L_5, ImageDelegate_t3548206662_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		ImageDelegate_t3548206662 * L_8 = V_0;
		ImageDelegate_t3548206662 * L_9 = V_1;
		if ((!(((Il2CppObject*)(ImageDelegate_t3548206662 *)L_8) == ((Il2CppObject*)(ImageDelegate_t3548206662 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Kakera.Unimgpicker::add_Failed(Kakera.Unimgpicker/ErrorDelegate)
extern Il2CppClass* ErrorDelegate_t402150177_il2cpp_TypeInfo_var;
extern const uint32_t Unimgpicker_add_Failed_m613040790_MetadataUsageId;
extern "C"  void Unimgpicker_add_Failed_m613040790 (Unimgpicker_t2332148304 * __this, ErrorDelegate_t402150177 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Unimgpicker_add_Failed_m613040790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ErrorDelegate_t402150177 * V_0 = NULL;
	ErrorDelegate_t402150177 * V_1 = NULL;
	{
		ErrorDelegate_t402150177 * L_0 = __this->get_Failed_3();
		V_0 = L_0;
	}

IL_0007:
	{
		ErrorDelegate_t402150177 * L_1 = V_0;
		V_1 = L_1;
		ErrorDelegate_t402150177 ** L_2 = __this->get_address_of_Failed_3();
		ErrorDelegate_t402150177 * L_3 = V_1;
		ErrorDelegate_t402150177 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		ErrorDelegate_t402150177 * L_6 = V_0;
		ErrorDelegate_t402150177 * L_7 = InterlockedCompareExchangeImpl<ErrorDelegate_t402150177 *>(L_2, ((ErrorDelegate_t402150177 *)CastclassSealed(L_5, ErrorDelegate_t402150177_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		ErrorDelegate_t402150177 * L_8 = V_0;
		ErrorDelegate_t402150177 * L_9 = V_1;
		if ((!(((Il2CppObject*)(ErrorDelegate_t402150177 *)L_8) == ((Il2CppObject*)(ErrorDelegate_t402150177 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Kakera.Unimgpicker::remove_Failed(Kakera.Unimgpicker/ErrorDelegate)
extern Il2CppClass* ErrorDelegate_t402150177_il2cpp_TypeInfo_var;
extern const uint32_t Unimgpicker_remove_Failed_m1445021837_MetadataUsageId;
extern "C"  void Unimgpicker_remove_Failed_m1445021837 (Unimgpicker_t2332148304 * __this, ErrorDelegate_t402150177 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Unimgpicker_remove_Failed_m1445021837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ErrorDelegate_t402150177 * V_0 = NULL;
	ErrorDelegate_t402150177 * V_1 = NULL;
	{
		ErrorDelegate_t402150177 * L_0 = __this->get_Failed_3();
		V_0 = L_0;
	}

IL_0007:
	{
		ErrorDelegate_t402150177 * L_1 = V_0;
		V_1 = L_1;
		ErrorDelegate_t402150177 ** L_2 = __this->get_address_of_Failed_3();
		ErrorDelegate_t402150177 * L_3 = V_1;
		ErrorDelegate_t402150177 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		ErrorDelegate_t402150177 * L_6 = V_0;
		ErrorDelegate_t402150177 * L_7 = InterlockedCompareExchangeImpl<ErrorDelegate_t402150177 *>(L_2, ((ErrorDelegate_t402150177 *)CastclassSealed(L_5, ErrorDelegate_t402150177_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		ErrorDelegate_t402150177 * L_8 = V_0;
		ErrorDelegate_t402150177 * L_9 = V_1;
		if ((!(((Il2CppObject*)(ErrorDelegate_t402150177 *)L_8) == ((Il2CppObject*)(ErrorDelegate_t402150177 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Kakera.Unimgpicker::Show(System.String,System.String,System.Int32)
extern Il2CppClass* IPicker_t4111000441_il2cpp_TypeInfo_var;
extern const uint32_t Unimgpicker_Show_m3309216668_MetadataUsageId;
extern "C"  void Unimgpicker_Show_m3309216668 (Unimgpicker_t2332148304 * __this, String_t* ___title0, String_t* ___outputFileName1, int32_t ___maxSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Unimgpicker_Show_m3309216668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_picker_4();
		String_t* L_1 = ___title0;
		String_t* L_2 = ___outputFileName1;
		int32_t L_3 = ___maxSize2;
		NullCheck(L_0);
		InterfaceActionInvoker3< String_t*, String_t*, int32_t >::Invoke(0 /* System.Void Kakera.IPicker::Show(System.String,System.String,System.Int32) */, IPicker_t4111000441_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3);
		return;
	}
}
// System.Void Kakera.Unimgpicker::OnComplete(System.String)
extern "C"  void Unimgpicker_OnComplete_m1188031576 (Unimgpicker_t2332148304 * __this, String_t* ___path0, const MethodInfo* method)
{
	ImageDelegate_t3548206662 * V_0 = NULL;
	{
		ImageDelegate_t3548206662 * L_0 = __this->get_Completed_2();
		V_0 = L_0;
		ImageDelegate_t3548206662 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		ImageDelegate_t3548206662 * L_2 = V_0;
		String_t* L_3 = ___path0;
		NullCheck(L_2);
		ImageDelegate_Invoke_m2935186509(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Kakera.Unimgpicker::OnFailure(System.String)
extern "C"  void Unimgpicker_OnFailure_m2180883323 (Unimgpicker_t2332148304 * __this, String_t* ___message0, const MethodInfo* method)
{
	ErrorDelegate_t402150177 * V_0 = NULL;
	{
		ErrorDelegate_t402150177 * L_0 = __this->get_Failed_3();
		V_0 = L_0;
		ErrorDelegate_t402150177 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		ErrorDelegate_t402150177 * L_2 = V_0;
		String_t* L_3 = ___message0;
		NullCheck(L_2);
		ErrorDelegate_Invoke_m2011956720(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Kakera.Unimgpicker/ErrorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ErrorDelegate__ctor_m331128292 (ErrorDelegate_t402150177 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Kakera.Unimgpicker/ErrorDelegate::Invoke(System.String)
extern "C"  void ErrorDelegate_Invoke_m2011956720 (ErrorDelegate_t402150177 * __this, String_t* ___message0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ErrorDelegate_Invoke_m2011956720((ErrorDelegate_t402150177 *)__this->get_prev_9(),___message0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ErrorDelegate_t402150177 (ErrorDelegate_t402150177 * __this, String_t* ___message0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.IAsyncResult Kakera.Unimgpicker/ErrorDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ErrorDelegate_BeginInvoke_m4179556173 (ErrorDelegate_t402150177 * __this, String_t* ___message0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Kakera.Unimgpicker/ErrorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ErrorDelegate_EndInvoke_m40295598 (ErrorDelegate_t402150177 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Kakera.Unimgpicker/ImageDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ImageDelegate__ctor_m1094922347 (ImageDelegate_t3548206662 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Kakera.Unimgpicker/ImageDelegate::Invoke(System.String)
extern "C"  void ImageDelegate_Invoke_m2935186509 (ImageDelegate_t3548206662 * __this, String_t* ___path0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ImageDelegate_Invoke_m2935186509((ImageDelegate_t3548206662 *)__this->get_prev_9(),___path0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___path0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___path0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___path0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___path0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___path0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ImageDelegate_t3548206662 (ImageDelegate_t3548206662 * __this, String_t* ___path0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Native function invocation
	il2cppPInvokeFunc(____path0_marshaled);

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

}
// System.IAsyncResult Kakera.Unimgpicker/ImageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ImageDelegate_BeginInvoke_m662071400 (ImageDelegate_t3548206662 * __this, String_t* ___path0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___path0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Kakera.Unimgpicker/ImageDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ImageDelegate_EndInvoke_m1638926857 (ImageDelegate_t3548206662 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LevelController::.ctor()
extern "C"  void LevelController__ctor_m3784088425 (LevelController_t2717835266 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LevelController::Start()
extern "C"  void LevelController_Start_m1756945929 (LevelController_t2717835266 * __this, const MethodInfo* method)
{
	{
		QuestionU5BU5D_t1028533817* L_0 = MainGame_getQuestions_m2295499002(NULL /*static, unused*/, /*hidden argument*/NULL);
		LevelsU5BU5D_t2050899114* L_1 = LevelController_GetLevels_m3045393308(__this, L_0, /*hidden argument*/NULL);
		__this->set_levels_3(L_1);
		LevelsU5BU5D_t2050899114* L_2 = __this->get_levels_3();
		LevelController_SetUpAnswers_m4148261728(__this, L_2, /*hidden argument*/NULL);
		LevelsU5BU5D_t2050899114* L_3 = __this->get_levels_3();
		LevelController_DetectOnEnableLevel_m1065078623(__this, L_3, /*hidden argument*/NULL);
		Adapter_t814751345 * L_4 = __this->get_adapter_2();
		LevelsU5BU5D_t2050899114* L_5 = __this->get_levels_3();
		NullCheck(L_4);
		Adapter_setAdapter_m3137295986(L_4, L_5, /*hidden argument*/NULL);
		Adapter_t814751345 * L_6 = __this->get_adapter_2();
		NullCheck(L_6);
		Adapter_registerListener_m1706720079(L_6, __this, /*hidden argument*/NULL);
		QuestionController_t445239244 * L_7 = __this->get_questController_4();
		NullCheck(L_7);
		QuestionController_registerListener_m712129072(L_7, __this, /*hidden argument*/NULL);
		return;
	}
}
// Levels[] LevelController::GetLevels(Question[])
extern Il2CppClass* LevelsU5BU5D_t2050899114_il2cpp_TypeInfo_var;
extern Il2CppClass* Levels_t748035019_il2cpp_TypeInfo_var;
extern const uint32_t LevelController_GetLevels_m3045393308_MetadataUsageId;
extern "C"  LevelsU5BU5D_t2050899114* LevelController_GetLevels_m3045393308 (LevelController_t2717835266 * __this, QuestionU5BU5D_t1028533817* ___quests0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelController_GetLevels_m3045393308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LevelsU5BU5D_t2050899114* V_0 = NULL;
	int32_t V_1 = 0;
	Levels_t748035019 * V_2 = NULL;
	int32_t V_3 = 0;
	{
		QuestionU5BU5D_t1028533817* L_0 = ___quests0;
		NullCheck(L_0);
		V_0 = ((LevelsU5BU5D_t2050899114*)SZArrayNew(LevelsU5BU5D_t2050899114_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))/(int32_t)((int32_t)11)))));
		V_1 = 0;
		Levels_t748035019 * L_1 = (Levels_t748035019 *)il2cpp_codegen_object_new(Levels_t748035019_il2cpp_TypeInfo_var);
		Levels__ctor_m1598716242(L_1, /*hidden argument*/NULL);
		V_2 = L_1;
		Levels_t748035019 * L_2 = V_2;
		NullCheck(L_2);
		Levels_set_IsLocked_m4000392438(L_2, (bool)0, /*hidden argument*/NULL);
		Levels_t748035019 * L_3 = V_2;
		NullCheck(L_3);
		Levels_set_NrLevel_m914816550(L_3, 1, /*hidden argument*/NULL);
		LevelsU5BU5D_t2050899114* L_4 = V_0;
		int32_t L_5 = V_1;
		Levels_t748035019 * L_6 = V_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Levels_t748035019 *)L_6);
		V_3 = 0;
		goto IL_005f;
	}

IL_002d:
	{
		int32_t L_7 = V_3;
		int32_t L_8 = V_1;
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)((int32_t)((int32_t)11)*(int32_t)((int32_t)((int32_t)L_8+(int32_t)1))))))))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
		Levels_t748035019 * L_10 = (Levels_t748035019 *)il2cpp_codegen_object_new(Levels_t748035019_il2cpp_TypeInfo_var);
		Levels__ctor_m1598716242(L_10, /*hidden argument*/NULL);
		V_2 = L_10;
		Levels_t748035019 * L_11 = V_2;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		Levels_set_NrLevel_m914816550(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)), /*hidden argument*/NULL);
		LevelsU5BU5D_t2050899114* L_13 = V_0;
		int32_t L_14 = V_1;
		Levels_t748035019 * L_15 = V_2;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Levels_t748035019 *)L_15);
	}

IL_0050:
	{
		LevelsU5BU5D_t2050899114* L_16 = V_0;
		int32_t L_17 = V_1;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		Levels_t748035019 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		QuestionU5BU5D_t1028533817* L_20 = ___quests0;
		int32_t L_21 = V_3;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		Question_t2927948840 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_19);
		Levels_addElement_m1871995915(L_19, L_23, /*hidden argument*/NULL);
		int32_t L_24 = V_3;
		V_3 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_005f:
	{
		int32_t L_25 = V_3;
		QuestionU5BU5D_t1028533817* L_26 = ___quests0;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_27 = V_0;
		return L_27;
	}
}
// System.Void LevelController::SetUpAnswers(Levels[])
extern "C"  void LevelController_SetUpAnswers_m4148261728 (LevelController_t2717835266 * __this, LevelsU5BU5D_t2050899114* ___levels0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		goto IL_0030;
	}

IL_0007:
	{
		LevelsU5BU5D_t2050899114* L_0 = ___levels0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Levels_t748035019 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		int32_t L_4 = Levels_get_NrLevel_m2233528047(L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		String_t* L_5 = Int32_ToString_m2960866144((&V_2), /*hidden argument*/NULL);
		int32_t L_6 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		LevelsU5BU5D_t2050899114* L_7 = ___levels0;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Levels_t748035019 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		int32_t L_11 = V_1;
		NullCheck(L_10);
		Levels_set_CorectAnswers_m2211099053(L_10, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_13 = V_0;
		LevelsU5BU5D_t2050899114* L_14 = ___levels0;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void LevelController::DetectOnEnableLevel(Levels[])
extern "C"  void LevelController_DetectOnEnableLevel_m1065078623 (LevelController_t2717835266 * __this, LevelsU5BU5D_t2050899114* ___levels0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_003b;
	}

IL_0007:
	{
		LevelsU5BU5D_t2050899114* L_0 = ___levels0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Levels_t748035019 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		bool L_4 = Levels_get_IsLocked_m626983591(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_5 = ___levels0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Levels_t748035019 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		int32_t L_9 = Levels_getStarNumber_m1660958789(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_10 = ___levels0;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		int32_t L_12 = ((int32_t)((int32_t)L_11+(int32_t)1));
		Levels_t748035019 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		Levels_set_IsLocked_m4000392438(L_13, (bool)0, /*hidden argument*/NULL);
		goto IL_0037;
	}

IL_0032:
	{
		goto IL_0046;
	}

IL_0037:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_15 = V_0;
		LevelsU5BU5D_t2050899114* L_16 = ___levels0;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length))))-(int32_t)1)))))
		{
			goto IL_0007;
		}
	}

IL_0046:
	{
		return;
	}
}
// System.Void LevelController::onItemClicked(System.Int32)
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern const uint32_t LevelController_onItemClicked_m3049872075_MetadataUsageId;
extern "C"  void LevelController_onItemClicked_m3049872075 (LevelController_t2717835266 * __this, int32_t ___position0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelController_onItemClicked_m3049872075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___position0;
		if ((((int32_t)L_0) > ((int32_t)5)))
		{
			goto IL_003c;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_1 = __this->get_levels_3();
		int32_t L_2 = ___position0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Levels_t748035019 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		bool L_5 = Levels_get_IsLocked_m626983591(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0037;
		}
	}
	{
		QuestionController_t445239244 * L_6 = __this->get_questController_4();
		LevelsU5BU5D_t2050899114* L_7 = __this->get_levels_3();
		int32_t L_8 = ___position0;
		NullCheck(L_6);
		QuestionController_SetLevels_m2193348496(L_6, L_7, L_8, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = __this->get_gamePanel_5();
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)1, /*hidden argument*/NULL);
	}

IL_0037:
	{
		goto IL_0093;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		bool L_10 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_isPurchased_2();
		if (!L_10)
		{
			goto IL_007b;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_11 = __this->get_levels_3();
		int32_t L_12 = ___position0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		Levels_t748035019 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		bool L_15 = Levels_get_IsLocked_m626983591(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0076;
		}
	}
	{
		QuestionController_t445239244 * L_16 = __this->get_questController_4();
		LevelsU5BU5D_t2050899114* L_17 = __this->get_levels_3();
		int32_t L_18 = ___position0;
		NullCheck(L_16);
		QuestionController_SetLevels_m2193348496(L_16, L_17, L_18, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_19 = __this->get_gamePanel_5();
		NullCheck(L_19);
		GameObject_SetActive_m2887581199(L_19, (bool)1, /*hidden argument*/NULL);
	}

IL_0076:
	{
		goto IL_0093;
	}

IL_007b:
	{
		GameObject_t1756533147 * L_20 = __this->get_gamePanel_5();
		NullCheck(L_20);
		GameObject_SetActive_m2887581199(L_20, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_21 = __this->get_buyPanel_6();
		NullCheck(L_21);
		GameObject_SetActive_m2887581199(L_21, (bool)1, /*hidden argument*/NULL);
	}

IL_0093:
	{
		return;
	}
}
// System.Void LevelController::onUpdateAdapter()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t LevelController_onUpdateAdapter_m1165209206_MetadataUsageId;
extern "C"  void LevelController_onUpdateAdapter_m1165209206 (LevelController_t2717835266 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelController_onUpdateAdapter_m1165209206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Adapter_t814751345 * L_0 = __this->get_adapter_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0045;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_2 = __this->get_levels_3();
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_3 = __this->get_levels_3();
		LevelController_SetUpAnswers_m4148261728(__this, L_3, /*hidden argument*/NULL);
		LevelsU5BU5D_t2050899114* L_4 = __this->get_levels_3();
		LevelController_DetectOnEnableLevel_m1065078623(__this, L_4, /*hidden argument*/NULL);
		Adapter_t814751345 * L_5 = __this->get_adapter_2();
		LevelsU5BU5D_t2050899114* L_6 = __this->get_levels_3();
		NullCheck(L_5);
		Adapter_UpdateAdapter_m349117033(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0045:
	{
		return;
	}
}
// System.Void Levels::.ctor()
extern "C"  void Levels__ctor_m1598716242 (Levels_t748035019 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_isLocked_3((bool)1);
		return;
	}
}
// System.Int32 Levels::get_NrLevel()
extern "C"  int32_t Levels_get_NrLevel_m2233528047 (Levels_t748035019 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_nrLevel_0();
		return L_0;
	}
}
// System.Void Levels::set_NrLevel(System.Int32)
extern "C"  void Levels_set_NrLevel_m914816550 (Levels_t748035019 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_nrLevel_0(L_0);
		return;
	}
}
// System.Boolean Levels::get_IsLocked()
extern "C"  bool Levels_get_IsLocked_m626983591 (Levels_t748035019 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isLocked_3();
		return L_0;
	}
}
// System.Void Levels::set_IsLocked(System.Boolean)
extern "C"  void Levels_set_IsLocked_m4000392438 (Levels_t748035019 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isLocked_3(L_0);
		return;
	}
}
// System.Int32 Levels::get_CorectAnswers()
extern "C"  int32_t Levels_get_CorectAnswers_m2038854904 (Levels_t748035019 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_corectAnswers_1();
		return L_0;
	}
}
// System.Void Levels::set_CorectAnswers(System.Int32)
extern "C"  void Levels_set_CorectAnswers_m2211099053 (Levels_t748035019 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_corectAnswers_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<Question> Levels::get_Quests()
extern "C"  List_1_t2297069972 * Levels_get_Quests_m1321190565 (Levels_t748035019 * __this, const MethodInfo* method)
{
	{
		List_1_t2297069972 * L_0 = __this->get_quests_2();
		return L_0;
	}
}
// System.Void Levels::set_Quests(System.Collections.Generic.List`1<Question>)
extern "C"  void Levels_set_Quests_m838198532 (Levels_t748035019 * __this, List_1_t2297069972 * ___value0, const MethodInfo* method)
{
	{
		List_1_t2297069972 * L_0 = ___value0;
		__this->set_quests_2(L_0);
		return;
	}
}
// System.Void Levels::addElement(Question)
extern Il2CppClass* List_1_t2297069972_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m659779495_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4092702619_MethodInfo_var;
extern const uint32_t Levels_addElement_m1871995915_MetadataUsageId;
extern "C"  void Levels_addElement_m1871995915 (Levels_t748035019 * __this, Question_t2927948840 * ___question0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Levels_addElement_m1871995915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2297069972 * L_0 = __this->get_quests_2();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		List_1_t2297069972 * L_1 = (List_1_t2297069972 *)il2cpp_codegen_object_new(List_1_t2297069972_il2cpp_TypeInfo_var);
		List_1__ctor_m659779495(L_1, /*hidden argument*/List_1__ctor_m659779495_MethodInfo_var);
		__this->set_quests_2(L_1);
	}

IL_0016:
	{
		List_1_t2297069972 * L_2 = __this->get_quests_2();
		Question_t2927948840 * L_3 = ___question0;
		NullCheck(L_2);
		List_1_Add_m4092702619(L_2, L_3, /*hidden argument*/List_1_Add_m4092702619_MethodInfo_var);
		return;
	}
}
// System.Int32 Levels::getStarNumber()
extern "C"  int32_t Levels_getStarNumber_m1660958789 (Levels_t748035019 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_corectAnswers_1();
		if ((((int32_t)L_0) <= ((int32_t)8)))
		{
			goto IL_000e;
		}
	}
	{
		return 3;
	}

IL_000e:
	{
		int32_t L_1 = __this->get_corectAnswers_1();
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)9))))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = __this->get_corectAnswers_1();
		if ((((int32_t)L_2) <= ((int32_t)6)))
		{
			goto IL_0029;
		}
	}
	{
		return 2;
	}

IL_0029:
	{
		int32_t L_3 = __this->get_corectAnswers_1();
		if ((((int32_t)L_3) >= ((int32_t)7)))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = __this->get_corectAnswers_1();
		if ((((int32_t)L_4) <= ((int32_t)3)))
		{
			goto IL_0043;
		}
	}
	{
		return 1;
	}

IL_0043:
	{
		return 0;
	}
}
// System.Void Main::.ctor()
extern "C"  void Main__ctor_m325986520 (Main_t2809994845 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Main::get_store_id()
extern "C"  String_t* Main_get_store_id_m2078185357 (Main_t2809994845 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_store_id_1();
		return L_0;
	}
}
// System.String Main::get_product_id()
extern "C"  String_t* Main_get_product_id_m3829812457 (Main_t2809994845 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_product_id_0();
		return L_0;
	}
}
// System.String Main::get_game_title()
extern "C"  String_t* Main_get_game_title_m3768709001 (Main_t2809994845 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_game_title_2();
		return L_0;
	}
}
// System.String Main::get_game_description()
extern "C"  String_t* Main_get_game_description_m3767261251 (Main_t2809994845 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_game_description_3();
		return L_0;
	}
}
// System.Void MainGame::.ctor()
extern "C"  void MainGame__ctor_m2380176706 (MainGame_t3800664731 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGame::Awake()
extern "C"  void MainGame_Awake_m496679287 (MainGame_t3800664731 * __this, const MethodInfo* method)
{
	{
		QualitySettings_set_vSyncCount_m1045847619(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		Application_set_targetFrameRate_m2941880625(NULL /*static, unused*/, ((int32_t)45), /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGame::Start()
extern Il2CppClass* MainGame_t3800664731_il2cpp_TypeInfo_var;
extern const uint32_t MainGame_Start_m520251270_MetadataUsageId;
extern "C"  void MainGame_Start_m520251270 (MainGame_t3800664731 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_Start_m520251270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MainGame_GameDescription_m643713462(__this, /*hidden argument*/NULL);
		QuestionU5BU5D_t1028533817* L_0 = MainGame_ReadJson_m2628908067(NULL /*static, unused*/, /*hidden argument*/NULL);
		((MainGame_t3800664731_StaticFields*)MainGame_t3800664731_il2cpp_TypeInfo_var->static_fields)->set_questions_2(L_0);
		MainGame_ActiveButtons_m3331183351(__this, /*hidden argument*/NULL);
		return;
	}
}
// Question[] MainGame::ReadJson()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisRoot_t2702590648_m2285480907_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2169816720;
extern const uint32_t MainGame_ReadJson_m2628908067_MetadataUsageId;
extern "C"  QuestionU5BU5D_t1028533817* MainGame_ReadJson_m2628908067 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_ReadJson_m2628908067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		V_0 = _stringLiteral2169816720;
		String_t* L_0 = Application_get_dataPath_m371940330(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_3 = File_ReadAllText_m1018286608(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		Root_t2702590648 * L_5 = JsonUtility_FromJson_TisRoot_t2702590648_m2285480907(NULL /*static, unused*/, L_4, /*hidden argument*/JsonUtility_FromJson_TisRoot_t2702590648_m2285480907_MethodInfo_var);
		NullCheck(L_5);
		QuestionU5BU5D_t1028533817* L_6 = Root_get_questions_m2291853548(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void MainGame::GameDescription()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisMain_t2809994845_m2505641288_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2995538678;
extern const uint32_t MainGame_GameDescription_m643713462_MetadataUsageId;
extern "C"  void MainGame_GameDescription_m643713462 (MainGame_t3800664731 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_GameDescription_m643713462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		V_0 = _stringLiteral2995538678;
		String_t* L_0 = Application_get_dataPath_m371940330(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_3 = File_ReadAllText_m1018286608(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		Main_t2809994845 * L_5 = JsonUtility_FromJson_TisMain_t2809994845_m2505641288(NULL /*static, unused*/, L_4, /*hidden argument*/JsonUtility_FromJson_TisMain_t2809994845_m2505641288_MethodInfo_var);
		__this->set_itemsContent_19(L_5);
		Main_t2809994845 * L_6 = __this->get_itemsContent_19();
		NullCheck(L_6);
		String_t* L_7 = Main_get_game_title_m3768709001(L_6, /*hidden argument*/NULL);
		__this->set_valueTitle_15(L_7);
		Main_t2809994845 * L_8 = __this->get_itemsContent_19();
		NullCheck(L_8);
		String_t* L_9 = Main_get_game_description_m3767261251(L_8, /*hidden argument*/NULL);
		__this->set_valueDescription_14(L_9);
		Text_t356221433 * L_10 = __this->get_titleGame_12();
		String_t* L_11 = __this->get_valueTitle_15();
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_11);
		Text_t356221433 * L_12 = __this->get_gameDescription_13();
		String_t* L_13 = __this->get_valueDescription_14();
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_13);
		return;
	}
}
// Question[] MainGame::getQuestions()
extern Il2CppClass* MainGame_t3800664731_il2cpp_TypeInfo_var;
extern const uint32_t MainGame_getQuestions_m2295499002_MetadataUsageId;
extern "C"  QuestionU5BU5D_t1028533817* MainGame_getQuestions_m2295499002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_getQuestions_m2295499002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		QuestionU5BU5D_t1028533817* L_0 = ((MainGame_t3800664731_StaticFields*)MainGame_t3800664731_il2cpp_TypeInfo_var->static_fields)->get_questions_2();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		QuestionU5BU5D_t1028533817* L_1 = MainGame_ReadJson_m2628908067(NULL /*static, unused*/, /*hidden argument*/NULL);
		((MainGame_t3800664731_StaticFields*)MainGame_t3800664731_il2cpp_TypeInfo_var->static_fields)->set_questions_2(L_1);
		QuestionU5BU5D_t1028533817* L_2 = ((MainGame_t3800664731_StaticFields*)MainGame_t3800664731_il2cpp_TypeInfo_var->static_fields)->get_questions_2();
		return L_2;
	}

IL_001a:
	{
		QuestionU5BU5D_t1028533817* L_3 = ((MainGame_t3800664731_StaticFields*)MainGame_t3800664731_il2cpp_TypeInfo_var->static_fields)->get_questions_2();
		return L_3;
	}
}
// System.Void MainGame::ActivePanel(UnityEngine.GameObject)
extern Il2CppClass* Execute_t1389284496_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteWithGameObjcet_t1191175011_il2cpp_TypeInfo_var;
extern const MethodInfo* SoundController_OpenWindow_m1923503136_MethodInfo_var;
extern const MethodInfo* MainGame_EnablePanel_m2776962149_MethodInfo_var;
extern const uint32_t MainGame_ActivePanel_m1042157020_MetadataUsageId;
extern "C"  void MainGame_ActivePanel_m1042157020 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_ActivePanel_m1042157020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SoundController_t1686593041 * L_0 = __this->get_soundController_18();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)SoundController_OpenWindow_m1923503136_MethodInfo_var);
		Execute_t1389284496 * L_2 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_2, L_0, L_1, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (0.5f), L_2, __this, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)MainGame_EnablePanel_m2776962149_MethodInfo_var);
		ExecuteWithGameObjcet_t1191175011 * L_4 = (ExecuteWithGameObjcet_t1191175011 *)il2cpp_codegen_object_new(ExecuteWithGameObjcet_t1191175011_il2cpp_TypeInfo_var);
		ExecuteWithGameObjcet__ctor_m342781680(L_4, __this, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = ___gamePanel0;
		Handler_StartDelayed_m1932308614(NULL /*static, unused*/, (0.3f), L_4, L_5, __this, /*hidden argument*/NULL);
		MainGame_InactiveButtons_m1486052864(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGame::EnablePanel(UnityEngine.GameObject)
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1441097755;
extern const uint32_t MainGame_EnablePanel_m2776962149_MetadataUsageId;
extern "C"  void MainGame_EnablePanel_m2776962149 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_EnablePanel_m2776962149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Animator_t69676727 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck(L_0);
		Animator_t69676727 * L_1 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		V_0 = L_1;
		Animator_t69676727 * L_2 = V_0;
		NullCheck(L_2);
		Animator_SetTrigger_m3418492570(L_2, _stringLiteral1441097755, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = ___go0;
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGame::BackButton(UnityEngine.GameObject)
extern Il2CppClass* Execute_t1389284496_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteWithGameObjcet_t1191175011_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* SoundController_CloseWindow_m1597160790_MethodInfo_var;
extern const MethodInfo* MainGame_DisablePan_m558909205_MethodInfo_var;
extern const MethodInfo* MainGame_BackOffBtn_m2255805544_MethodInfo_var;
extern const MethodInfo* MainGame_BackOnBtn_m2583837872_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3457520150;
extern const uint32_t MainGame_BackButton_m1576195507_MetadataUsageId;
extern "C"  void MainGame_BackButton_m1576195507 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_BackButton_m1576195507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Animator_t69676727 * V_0 = NULL;
	{
		MainGame_ActiveButtons_m3331183351(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = ___gamePanel0;
		NullCheck(L_0);
		Animator_t69676727 * L_1 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		V_0 = L_1;
		Animator_t69676727 * L_2 = V_0;
		NullCheck(L_2);
		Animator_SetTrigger_m3418492570(L_2, _stringLiteral3457520150, /*hidden argument*/NULL);
		SoundController_t1686593041 * L_3 = __this->get_soundController_18();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)SoundController_CloseWindow_m1597160790_MethodInfo_var);
		Execute_t1389284496 * L_5 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_5, L_3, L_4, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (0.4f), L_5, __this, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)MainGame_DisablePan_m558909205_MethodInfo_var);
		ExecuteWithGameObjcet_t1191175011 * L_7 = (ExecuteWithGameObjcet_t1191175011 *)il2cpp_codegen_object_new(ExecuteWithGameObjcet_t1191175011_il2cpp_TypeInfo_var);
		ExecuteWithGameObjcet__ctor_m342781680(L_7, __this, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = ___gamePanel0;
		Handler_StartDelayed_m1932308614(NULL /*static, unused*/, (0.8f), L_7, L_8, __this, /*hidden argument*/NULL);
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)MainGame_BackOffBtn_m2255805544_MethodInfo_var);
		Execute_t1389284496 * L_10 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_10, __this, L_9, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (0.1f), L_10, __this, /*hidden argument*/NULL);
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)MainGame_BackOnBtn_m2583837872_MethodInfo_var);
		Execute_t1389284496 * L_12 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_12, __this, L_11, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (0.8f), L_12, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGame::DisablePan(UnityEngine.GameObject)
extern "C"  void MainGame_DisablePan_m558909205 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGame::LegasiInfoBack(UnityEngine.GameObject)
extern Il2CppClass* Execute_t1389284496_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteWithGameObjcet_t1191175011_il2cpp_TypeInfo_var;
extern const MethodInfo* SoundController_CloseWindow_m1597160790_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* MainGame_DisablePan_m558909205_MethodInfo_var;
extern const MethodInfo* MainGame_BackOnBtn_m2583837872_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3457520150;
extern const uint32_t MainGame_LegasiInfoBack_m3527212594_MetadataUsageId;
extern "C"  void MainGame_LegasiInfoBack_m3527212594 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainGame_LegasiInfoBack_m3527212594_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Animator_t69676727 * V_0 = NULL;
	{
		MainGame_BackOffBtn_m2255805544(__this, /*hidden argument*/NULL);
		SoundController_t1686593041 * L_0 = __this->get_soundController_18();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)SoundController_CloseWindow_m1597160790_MethodInfo_var);
		Execute_t1389284496 * L_2 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_2, L_0, L_1, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (0.4f), L_2, __this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = ___gamePanel0;
		NullCheck(L_3);
		Animator_t69676727 * L_4 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_3, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		V_0 = L_4;
		Animator_t69676727 * L_5 = V_0;
		NullCheck(L_5);
		Animator_SetTrigger_m3418492570(L_5, _stringLiteral3457520150, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)MainGame_DisablePan_m558909205_MethodInfo_var);
		ExecuteWithGameObjcet_t1191175011 * L_7 = (ExecuteWithGameObjcet_t1191175011 *)il2cpp_codegen_object_new(ExecuteWithGameObjcet_t1191175011_il2cpp_TypeInfo_var);
		ExecuteWithGameObjcet__ctor_m342781680(L_7, __this, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = ___gamePanel0;
		Handler_StartDelayed_m1932308614(NULL /*static, unused*/, (1.2f), L_7, L_8, __this, /*hidden argument*/NULL);
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)MainGame_BackOnBtn_m2583837872_MethodInfo_var);
		Execute_t1389284496 * L_10 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_10, __this, L_9, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (1.0f), L_10, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGame::InactiveButtons()
extern "C"  void MainGame_InactiveButtons_m1486052864 (MainGame_t3800664731 * __this, const MethodInfo* method)
{
	{
		Button_t2872111280 * L_0 = __this->get_play_btn_5();
		NullCheck(L_0);
		Selectable_set_interactable_m63718297(L_0, (bool)0, /*hidden argument*/NULL);
		Button_t2872111280 * L_1 = __this->get_store_btn_6();
		NullCheck(L_1);
		Selectable_set_interactable_m63718297(L_1, (bool)0, /*hidden argument*/NULL);
		Button_t2872111280 * L_2 = __this->get_stats_btn_7();
		NullCheck(L_2);
		Selectable_set_interactable_m63718297(L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGame::ActiveButtons()
extern "C"  void MainGame_ActiveButtons_m3331183351 (MainGame_t3800664731 * __this, const MethodInfo* method)
{
	{
		Button_t2872111280 * L_0 = __this->get_play_btn_5();
		NullCheck(L_0);
		Selectable_set_interactable_m63718297(L_0, (bool)1, /*hidden argument*/NULL);
		Button_t2872111280 * L_1 = __this->get_store_btn_6();
		NullCheck(L_1);
		Selectable_set_interactable_m63718297(L_1, (bool)1, /*hidden argument*/NULL);
		Button_t2872111280 * L_2 = __this->get_stats_btn_7();
		NullCheck(L_2);
		Selectable_set_interactable_m63718297(L_2, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGame::BackOnBtn()
extern "C"  void MainGame_BackOnBtn_m2583837872 (MainGame_t3800664731 * __this, const MethodInfo* method)
{
	{
		Button_t2872111280 * L_0 = __this->get_back_1_8();
		NullCheck(L_0);
		Selectable_set_interactable_m63718297(L_0, (bool)1, /*hidden argument*/NULL);
		Button_t2872111280 * L_1 = __this->get_back_2_9();
		NullCheck(L_1);
		Selectable_set_interactable_m63718297(L_1, (bool)1, /*hidden argument*/NULL);
		Button_t2872111280 * L_2 = __this->get_back_3_10();
		NullCheck(L_2);
		Selectable_set_interactable_m63718297(L_2, (bool)1, /*hidden argument*/NULL);
		Button_t2872111280 * L_3 = __this->get_back_4_11();
		NullCheck(L_3);
		Selectable_set_interactable_m63718297(L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGame::BackOffBtn()
extern "C"  void MainGame_BackOffBtn_m2255805544 (MainGame_t3800664731 * __this, const MethodInfo* method)
{
	{
		Button_t2872111280 * L_0 = __this->get_back_1_8();
		NullCheck(L_0);
		Selectable_set_interactable_m63718297(L_0, (bool)0, /*hidden argument*/NULL);
		Button_t2872111280 * L_1 = __this->get_back_2_9();
		NullCheck(L_1);
		Selectable_set_interactable_m63718297(L_1, (bool)0, /*hidden argument*/NULL);
		Button_t2872111280 * L_2 = __this->get_back_3_10();
		NullCheck(L_2);
		Selectable_set_interactable_m63718297(L_2, (bool)0, /*hidden argument*/NULL);
		Button_t2872111280 * L_3 = __this->get_back_4_11();
		NullCheck(L_3);
		Selectable_set_interactable_m63718297(L_3, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPrefsX::.ctor()
extern "C"  void PlayerPrefsX__ctor_m743890102 (PlayerPrefsX_t1687815431 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayerPrefsX::SetBool(System.String,System.Boolean)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_SetBool_m2483950367_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetBool_m2483950367 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetBool_m2483950367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___name0;
			bool L_1 = ___value1;
			G_B1_0 = L_0;
			if (!L_1)
			{
				G_B2_0 = L_0;
				goto IL_000d;
			}
		}

IL_0007:
		{
			G_B3_0 = 1;
			G_B3_1 = G_B1_0;
			goto IL_000e;
		}

IL_000d:
		{
			G_B3_0 = 0;
			G_B3_1 = G_B2_0;
		}

IL_000e:
		{
			PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
			goto IL_0020;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0018;
		throw e;
	}

CATCH_0018:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0022;
	} // end catch (depth: 1)

IL_0020:
	{
		return (bool)1;
	}

IL_0022:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean PlayerPrefsX::GetBool(System.String)
extern "C"  bool PlayerPrefsX_GetBool_m1433478398 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean PlayerPrefsX::GetBool(System.String,System.Boolean)
extern "C"  bool PlayerPrefsX_GetBool_m563309883 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___defaultValue1, const MethodInfo* method)
{
	String_t* G_B2_0 = NULL;
	int32_t G_B2_1 = 0;
	String_t* G_B1_0 = NULL;
	int32_t G_B1_1 = 0;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	int32_t G_B3_2 = 0;
	{
		String_t* L_0 = ___name0;
		bool L_1 = ___defaultValue1;
		G_B1_0 = L_0;
		G_B1_1 = 1;
		if (!L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = 1;
			goto IL_000e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_000f:
	{
		int32_t L_2 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)G_B3_2) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Int64 PlayerPrefsX::GetLong(System.String,System.Int64)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2995165015;
extern Il2CppCodeGenString* _stringLiteral3164798925;
extern const uint32_t PlayerPrefsX_GetLong_m431868007_MetadataUsageId;
extern "C"  int64_t PlayerPrefsX_GetLong_m431868007 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int64_t ___defaultValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetLong_m431868007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint64_t V_2 = 0;
	{
		int64_t L_0 = ___defaultValue1;
		PlayerPrefsX_SplitLong_m2682948856(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		String_t* L_1 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_1, _stringLiteral2995165015, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		int32_t L_4 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = ___key0;
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, L_5, _stringLiteral3164798925, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		int32_t L_8 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = V_1;
		V_2 = (((int64_t)((uint64_t)(((uint32_t)((uint32_t)L_9))))));
		uint64_t L_10 = V_2;
		V_2 = ((int64_t)((int64_t)L_10<<(int32_t)((int32_t)32)));
		uint64_t L_11 = V_2;
		int32_t L_12 = V_0;
		return ((int64_t)((int64_t)L_11|(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)L_12))))))));
	}
}
// System.Int64 PlayerPrefsX::GetLong(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2995165015;
extern Il2CppCodeGenString* _stringLiteral3164798925;
extern const uint32_t PlayerPrefsX_GetLong_m3735091293_MetadataUsageId;
extern "C"  int64_t PlayerPrefsX_GetLong_m3735091293 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetLong_m3735091293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint64_t V_2 = 0;
	{
		String_t* L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral2995165015, /*hidden argument*/NULL);
		int32_t L_2 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___key0;
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, L_3, _stringLiteral3164798925, /*hidden argument*/NULL);
		int32_t L_5 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		V_2 = (((int64_t)((uint64_t)(((uint32_t)((uint32_t)L_6))))));
		uint64_t L_7 = V_2;
		V_2 = ((int64_t)((int64_t)L_7<<(int32_t)((int32_t)32)));
		uint64_t L_8 = V_2;
		int32_t L_9 = V_0;
		return ((int64_t)((int64_t)L_8|(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)L_9))))))));
	}
}
// System.Void PlayerPrefsX::SplitLong(System.Int64,System.Int32&,System.Int32&)
extern "C"  void PlayerPrefsX_SplitLong_m2682948856 (Il2CppObject * __this /* static, unused */, int64_t ___input0, int32_t* ___lowBits1, int32_t* ___highBits2, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___lowBits1;
		int64_t L_1 = ___input0;
		*((int32_t*)(L_0)) = (int32_t)(((int32_t)((uint32_t)L_1)));
		int32_t* L_2 = ___highBits2;
		int64_t L_3 = ___input0;
		*((int32_t*)(L_2)) = (int32_t)(((int32_t)((uint32_t)((int64_t)((int64_t)L_3>>(int32_t)((int32_t)32))))));
		return;
	}
}
// System.Void PlayerPrefsX::SetLong(System.String,System.Int64)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2995165015;
extern Il2CppCodeGenString* _stringLiteral3164798925;
extern const uint32_t PlayerPrefsX_SetLong_m1312103776_MetadataUsageId;
extern "C"  void PlayerPrefsX_SetLong_m1312103776 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int64_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetLong_m1312103776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int64_t L_0 = ___value1;
		PlayerPrefsX_SplitLong_m2682948856(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		String_t* L_1 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_1, _stringLiteral2995165015, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ___key0;
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, L_4, _stringLiteral3164798925, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayerPrefsX::SetVector2(System.String,UnityEngine.Vector2)
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_SetVector2_m1470654325_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetVector2_m1470654325 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2_t2243707579  ___vector1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetVector2_m1470654325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)2));
		float L_2 = (&___vector1)->get_x_0();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_2);
		SingleU5BU5D_t577127397* L_3 = L_1;
		float L_4 = (&___vector1)->get_y_1();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_4);
		bool L_5 = PlayerPrefsX_SetFloatArray_m1305280912(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Vector2 PlayerPrefsX::GetVector2(System.String)
extern "C"  Vector2_t2243707579  PlayerPrefsX_GetVector2_m3651951690 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = PlayerPrefsX_GetFloatArray_m1814773467(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		SingleU5BU5D_t577127397* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) >= ((int32_t)2)))
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t2243707579  L_3 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0016:
	{
		SingleU5BU5D_t577127397* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t577127397* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = 1;
		float L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		Vector2_t2243707579  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m3067419446(&L_10, L_6, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// UnityEngine.Vector2 PlayerPrefsX::GetVector2(System.String,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  PlayerPrefsX_GetVector2_m2771394184 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2_t2243707579  ___defaultValue1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Vector2_t2243707579  L_3 = PlayerPrefsX_GetVector2_m3651951690(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		Vector2_t2243707579  L_4 = ___defaultValue1;
		return L_4;
	}
}
// System.Boolean PlayerPrefsX::SetVector3(System.String,UnityEngine.Vector3)
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_SetVector3_m378186261_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetVector3_m378186261 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3_t2243707580  ___vector1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetVector3_m378186261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)3));
		float L_2 = (&___vector1)->get_x_1();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_2);
		SingleU5BU5D_t577127397* L_3 = L_1;
		float L_4 = (&___vector1)->get_y_2();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_4);
		SingleU5BU5D_t577127397* L_5 = L_3;
		float L_6 = (&___vector1)->get_z_3();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_6);
		bool L_7 = PlayerPrefsX_SetFloatArray_m1305280912(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.Vector3 PlayerPrefsX::GetVector3(System.String)
extern "C"  Vector3_t2243707580  PlayerPrefsX_GetVector3_m2471918672 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = PlayerPrefsX_GetFloatArray_m1814773467(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		SingleU5BU5D_t577127397* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) >= ((int32_t)3)))
		{
			goto IL_0016;
		}
	}
	{
		Vector3_t2243707580  L_3 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0016:
	{
		SingleU5BU5D_t577127397* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t577127397* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = 1;
		float L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		SingleU5BU5D_t577127397* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = 2;
		float L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Vector3_t2243707580  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2638739322(&L_13, L_6, L_9, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// UnityEngine.Vector3 PlayerPrefsX::GetVector3(System.String,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  PlayerPrefsX_GetVector3_m707181159 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3_t2243707580  ___defaultValue1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Vector3_t2243707580  L_3 = PlayerPrefsX_GetVector3_m2471918672(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		Vector3_t2243707580  L_4 = ___defaultValue1;
		return L_4;
	}
}
// System.Boolean PlayerPrefsX::SetQuaternion(System.String,UnityEngine.Quaternion)
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_SetQuaternion_m1970153377_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetQuaternion_m1970153377 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Quaternion_t4030073918  ___vector1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetQuaternion_m1970153377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_2 = (&___vector1)->get_x_0();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_2);
		SingleU5BU5D_t577127397* L_3 = L_1;
		float L_4 = (&___vector1)->get_y_1();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_4);
		SingleU5BU5D_t577127397* L_5 = L_3;
		float L_6 = (&___vector1)->get_z_2();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_6);
		SingleU5BU5D_t577127397* L_7 = L_5;
		float L_8 = (&___vector1)->get_w_3();
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)L_8);
		bool L_9 = PlayerPrefsX_SetFloatArray_m1305280912(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Quaternion PlayerPrefsX::GetQuaternion(System.String)
extern "C"  Quaternion_t4030073918  PlayerPrefsX_GetQuaternion_m887856706 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = PlayerPrefsX_GetFloatArray_m1814773467(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		SingleU5BU5D_t577127397* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0016;
		}
	}
	{
		Quaternion_t4030073918  L_3 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0016:
	{
		SingleU5BU5D_t577127397* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t577127397* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = 1;
		float L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		SingleU5BU5D_t577127397* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = 2;
		float L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		SingleU5BU5D_t577127397* L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = 3;
		float L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		Quaternion_t4030073918  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Quaternion__ctor_m3196903881(&L_16, L_6, L_9, L_12, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Quaternion PlayerPrefsX::GetQuaternion(System.String,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  PlayerPrefsX_GetQuaternion_m2630933209 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Quaternion_t4030073918  ___defaultValue1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Quaternion_t4030073918  L_3 = PlayerPrefsX_GetQuaternion_m887856706(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		Quaternion_t4030073918  L_4 = ___defaultValue1;
		return L_4;
	}
}
// System.Boolean PlayerPrefsX::SetColor(System.String,UnityEngine.Color)
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_SetColor_m3641687733_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetColor_m3641687733 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Color_t2020392075  ___color1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetColor_m3641687733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_2 = (&___color1)->get_r_0();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_2);
		SingleU5BU5D_t577127397* L_3 = L_1;
		float L_4 = (&___color1)->get_g_1();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_4);
		SingleU5BU5D_t577127397* L_5 = L_3;
		float L_6 = (&___color1)->get_b_2();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_6);
		SingleU5BU5D_t577127397* L_7 = L_5;
		float L_8 = (&___color1)->get_a_3();
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)L_8);
		bool L_9 = PlayerPrefsX_SetFloatArray_m1305280912(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Color PlayerPrefsX::GetColor(System.String)
extern "C"  Color_t2020392075  PlayerPrefsX_GetColor_m2868168762 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = PlayerPrefsX_GetFloatArray_m1814773467(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		SingleU5BU5D_t577127397* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_002a;
		}
	}
	{
		Color_t2020392075  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m1909920690(&L_3, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_3;
	}

IL_002a:
	{
		SingleU5BU5D_t577127397* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t577127397* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = 1;
		float L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		SingleU5BU5D_t577127397* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = 2;
		float L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		SingleU5BU5D_t577127397* L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = 3;
		float L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		Color_t2020392075  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Color__ctor_m1909920690(&L_16, L_6, L_9, L_12, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Color PlayerPrefsX::GetColor(System.String,UnityEngine.Color)
extern "C"  Color_t2020392075  PlayerPrefsX_GetColor_m3306356312 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Color_t2020392075  ___defaultValue1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Color_t2020392075  L_3 = PlayerPrefsX_GetColor_m2868168762(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		Color_t2020392075  L_4 = ___defaultValue1;
		return L_4;
	}
}
// System.Boolean PlayerPrefsX::SetBoolArray(System.String,System.Boolean[])
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayType_t77146353_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* BitArray_t4180138994_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_SetBoolArray_m2699961658_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetBoolArray_m2699961658 (Il2CppObject * __this /* static, unused */, String_t* ___key0, BooleanU5BU5D_t3568034315* ___boolArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetBoolArray_m2699961658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	BitArray_t4180138994 * V_1 = NULL;
	{
		BooleanU5BU5D_t3568034315* L_0 = ___boolArray1;
		NullCheck(L_0);
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))+(int32_t)7))/(int32_t)8))+(int32_t)5))));
		ByteU5BU5D_t3397334013* L_1 = V_0;
		int32_t L_2 = ((int32_t)2);
		Il2CppObject * L_3 = Box(ArrayType_t77146353_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint8_t L_4 = Convert_ToByte_m3829002889(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_4);
		BooleanU5BU5D_t3568034315* L_5 = ___boolArray1;
		BitArray_t4180138994 * L_6 = (BitArray_t4180138994 *)il2cpp_codegen_object_new(BitArray_t4180138994_il2cpp_TypeInfo_var);
		BitArray__ctor_m3242483683(L_6, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		BitArray_t4180138994 * L_7 = V_1;
		ByteU5BU5D_t3397334013* L_8 = V_0;
		NullCheck(L_7);
		BitArray_CopyTo_m2910588211(L_7, (Il2CppArray *)(Il2CppArray *)L_8, 5, /*hidden argument*/NULL);
		PlayerPrefsX_Initialize_m3123284730(NULL /*static, unused*/, /*hidden argument*/NULL);
		BooleanU5BU5D_t3568034315* L_9 = ___boolArray1;
		NullCheck(L_9);
		ByteU5BU5D_t3397334013* L_10 = V_0;
		PlayerPrefsX_ConvertInt32ToBytes_m990830831(NULL /*static, unused*/, (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))), L_10, /*hidden argument*/NULL);
		String_t* L_11 = ___key0;
		ByteU5BU5D_t3397334013* L_12 = V_0;
		bool L_13 = PlayerPrefsX_SaveBytes_m2961073563(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Boolean[] PlayerPrefsX::GetBoolArray(System.String)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* BitArray_t4180138994_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2405073339;
extern Il2CppCodeGenString* _stringLiteral2092780187;
extern const uint32_t PlayerPrefsX_GetBoolArray_m2134041115_MetadataUsageId;
extern "C"  BooleanU5BU5D_t3568034315* PlayerPrefsX_GetBoolArray_m2134041115 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetBoolArray_m2134041115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	BitArray_t4180138994 * V_2 = NULL;
	BooleanU5BU5D_t3568034315* V_3 = NULL;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_009c;
		}
	}
	{
		String_t* L_2 = ___key0;
		String_t* L_3 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_4 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t3397334013* L_5 = V_0;
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) >= ((int32_t)5)))
		{
			goto IL_0037;
		}
	}
	{
		String_t* L_6 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2405073339, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return ((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0037:
	{
		ByteU5BU5D_t3397334013* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		uint8_t L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		if ((((int32_t)L_10) == ((int32_t)2)))
		{
			goto IL_0057;
		}
	}
	{
		String_t* L_11 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2596409543(NULL /*static, unused*/, L_11, _stringLiteral2092780187, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return ((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0057:
	{
		PlayerPrefsX_Initialize_m3123284730(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_13 = V_0;
		NullCheck(L_13);
		V_1 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))))-(int32_t)5))));
		ByteU5BU5D_t3397334013* L_14 = V_0;
		ByteU5BU5D_t3397334013* L_15 = V_1;
		ByteU5BU5D_t3397334013* L_16 = V_1;
		NullCheck(L_16);
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_14, 5, (Il2CppArray *)(Il2CppArray *)L_15, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_17 = V_1;
		BitArray_t4180138994 * L_18 = (BitArray_t4180138994 *)il2cpp_codegen_object_new(BitArray_t4180138994_il2cpp_TypeInfo_var);
		BitArray__ctor_m3712470753(L_18, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		BitArray_t4180138994 * L_19 = V_2;
		ByteU5BU5D_t3397334013* L_20 = V_0;
		int32_t L_21 = PlayerPrefsX_ConvertBytesToInt32_m1019863976(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		BitArray_set_Length_m2083275088(L_19, L_21, /*hidden argument*/NULL);
		BitArray_t4180138994 * L_22 = V_2;
		NullCheck(L_22);
		int32_t L_23 = BitArray_get_Count_m2234414662(L_22, /*hidden argument*/NULL);
		V_3 = ((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)L_23));
		BitArray_t4180138994 * L_24 = V_2;
		BooleanU5BU5D_t3568034315* L_25 = V_3;
		NullCheck(L_24);
		BitArray_CopyTo_m2910588211(L_24, (Il2CppArray *)(Il2CppArray *)L_25, 0, /*hidden argument*/NULL);
		BooleanU5BU5D_t3568034315* L_26 = V_3;
		return L_26;
	}

IL_009c:
	{
		return ((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)0));
	}
}
// System.Boolean[] PlayerPrefsX::GetBoolArray(System.String,System.Boolean,System.Int32)
extern Il2CppClass* BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetBoolArray_m806442737_MetadataUsageId;
extern "C"  BooleanU5BU5D_t3568034315* PlayerPrefsX_GetBoolArray_m806442737 (Il2CppObject * __this /* static, unused */, String_t* ___key0, bool ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetBoolArray_m806442737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BooleanU5BU5D_t3568034315* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		BooleanU5BU5D_t3568034315* L_3 = PlayerPrefsX_GetBoolArray_m2134041115(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0028;
	}

IL_0020:
	{
		BooleanU5BU5D_t3568034315* L_5 = V_0;
		int32_t L_6 = V_1;
		bool L_7 = ___defaultValue1;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (bool)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		BooleanU5BU5D_t3568034315* L_11 = V_0;
		return L_11;
	}
}
// System.Boolean PlayerPrefsX::SetStringArray(System.String,System.String[])
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayType_t77146353_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2587746055;
extern Il2CppCodeGenString* _stringLiteral3145710920;
extern Il2CppCodeGenString* _stringLiteral372029394;
extern const uint32_t PlayerPrefsX_SetStringArray_m3559579766_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetStringArray_m3559579766 (Il2CppObject * __this /* static, unused */, String_t* ___key0, StringU5BU5D_t1642385972* ___stringArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetStringArray_m3559579766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringU5BU5D_t1642385972* L_0 = ___stringArray1;
		NullCheck(L_0);
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))+(int32_t)1))));
		ByteU5BU5D_t3397334013* L_1 = V_0;
		int32_t L_2 = ((int32_t)3);
		Il2CppObject * L_3 = Box(ArrayType_t77146353_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint8_t L_4 = Convert_ToByte_m3829002889(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_4);
		PlayerPrefsX_Initialize_m3123284730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_007f;
	}

IL_0025:
	{
		StringU5BU5D_t1642385972* L_5 = ___stringArray1;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if (L_8)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_9 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2587746055, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_003f:
	{
		StringU5BU5D_t1642385972* L_11 = ___stringArray1;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		String_t* L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1606060069(L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_15) <= ((int32_t)((int32_t)255))))
		{
			goto IL_0063;
		}
	}
	{
		String_t* L_16 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3145710920, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0063:
	{
		ByteU5BU5D_t3397334013* L_18 = V_0;
		int32_t L_19 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		int32_t L_20 = L_19;
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_idx_2(((int32_t)((int32_t)L_20+(int32_t)1)));
		StringU5BU5D_t1642385972* L_21 = ___stringArray1;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		String_t* L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_24);
		int32_t L_25 = String_get_Length_m1606060069(L_24, /*hidden argument*/NULL);
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (uint8_t)(((int32_t)((uint8_t)L_25))));
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_007f:
	{
		int32_t L_27 = V_1;
		StringU5BU5D_t1642385972* L_28 = ___stringArray1;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_0025;
		}
	}

IL_0088:
	try
	{ // begin try (depth: 1)
		String_t* L_29 = ___key0;
		ByteU5BU5D_t3397334013* L_30 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_31 = Convert_ToBase64String_m1936815455(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		StringU5BU5D_t1642385972* L_33 = ___stringArray1;
		String_t* L_34 = String_Join_m1966872927(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		String_t* L_35 = String_Concat_m612901809(NULL /*static, unused*/, L_31, _stringLiteral372029394, L_34, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, L_29, L_35, /*hidden argument*/NULL);
		goto IL_00b6;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00ae;
		throw e;
	}

CATCH_00ae:
	{ // begin catch(System.Object)
		V_2 = (bool)0;
		goto IL_00b8;
	} // end catch (depth: 1)

IL_00b6:
	{
		return (bool)1;
	}

IL_00b8:
	{
		bool L_36 = V_2;
		return L_36;
	}
}
// System.String[] PlayerPrefsX::GetStringArray(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029394;
extern Il2CppCodeGenString* _stringLiteral2405073339;
extern Il2CppCodeGenString* _stringLiteral1098059376;
extern const uint32_t PlayerPrefsX_GetStringArray_m1821490265_MetadataUsageId;
extern "C"  StringU5BU5D_t1642385972* PlayerPrefsX_GetStringArray_m1821490265 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetStringArray_m1821490265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	int32_t V_3 = 0;
	StringU5BU5D_t1642385972* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ef;
		}
	}
	{
		String_t* L_2 = ___key0;
		String_t* L_3 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		NullCheck(_stringLiteral372029394);
		Il2CppChar L_5 = String_get_Chars_m4230566705(_stringLiteral372029394, 0, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_6 = String_IndexOf_m2358239236(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) >= ((int32_t)4)))
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_8 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2405073339, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0042:
	{
		String_t* L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck(L_10);
		String_t* L_12 = String_Substring_m12482732(L_10, 0, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_13 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		ByteU5BU5D_t3397334013* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = 0;
		uint8_t L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		if ((((int32_t)L_16) == ((int32_t)3)))
		{
			goto IL_0070;
		}
	}
	{
		String_t* L_17 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2596409543(NULL /*static, unused*/, L_17, _stringLiteral1098059376, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0070:
	{
		PlayerPrefsX_Initialize_m3123284730(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_19 = V_2;
		NullCheck(L_19);
		V_3 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length))))-(int32_t)1));
		int32_t L_20 = V_3;
		V_4 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_20));
		int32_t L_21 = V_1;
		V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
		V_6 = 0;
		goto IL_00e4;
	}

IL_0090:
	{
		ByteU5BU5D_t3397334013* L_22 = V_2;
		int32_t L_23 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		int32_t L_24 = L_23;
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_idx_2(((int32_t)((int32_t)L_24+(int32_t)1)));
		NullCheck(L_22);
		int32_t L_25 = L_24;
		uint8_t L_26 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		V_7 = L_26;
		int32_t L_27 = V_5;
		int32_t L_28 = V_7;
		String_t* L_29 = V_0;
		NullCheck(L_29);
		int32_t L_30 = String_get_Length_m1606060069(L_29, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_27+(int32_t)L_28))) <= ((int32_t)L_30)))
		{
			goto IL_00c8;
		}
	}
	{
		String_t* L_31 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2405073339, L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_00c8:
	{
		StringU5BU5D_t1642385972* L_33 = V_4;
		int32_t L_34 = V_6;
		String_t* L_35 = V_0;
		int32_t L_36 = V_5;
		int32_t L_37 = V_7;
		NullCheck(L_35);
		String_t* L_38 = String_Substring_m12482732(L_35, L_36, L_37, /*hidden argument*/NULL);
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_38);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(L_34), (String_t*)L_38);
		int32_t L_39 = V_5;
		int32_t L_40 = V_7;
		V_5 = ((int32_t)((int32_t)L_39+(int32_t)L_40));
		int32_t L_41 = V_6;
		V_6 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00e4:
	{
		int32_t L_42 = V_6;
		int32_t L_43 = V_3;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0090;
		}
	}
	{
		StringU5BU5D_t1642385972* L_44 = V_4;
		return L_44;
	}

IL_00ef:
	{
		return ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0));
	}
}
// System.String[] PlayerPrefsX::GetStringArray(System.String,System.String,System.Int32)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetStringArray_m335614902_MetadataUsageId;
extern "C"  StringU5BU5D_t1642385972* PlayerPrefsX_GetStringArray_m335614902 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetStringArray_m335614902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		StringU5BU5D_t1642385972* L_3 = PlayerPrefsX_GetStringArray_m1821490265(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0028;
	}

IL_0020:
	{
		StringU5BU5D_t1642385972* L_5 = V_0;
		int32_t L_6 = V_1;
		String_t* L_7 = ___defaultValue1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (String_t*)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		StringU5BU5D_t1642385972* L_11 = V_0;
		return L_11;
	}
}
// System.Boolean PlayerPrefsX::SetIntArray(System.String,System.Int32[])
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t676603244_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromInt_m961274613_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1561331108_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisInt32U5BU5D_t3030399641_m1762095552_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetIntArray_m4032022271_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetIntArray_m4032022271 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Int32U5BU5D_t3030399641* ___intArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetIntArray_m4032022271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	Int32U5BU5D_t3030399641* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	Int32U5BU5D_t3030399641* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		Int32U5BU5D_t3030399641* L_1 = ___intArray1;
		Action_3_t676603244 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_4();
		G_B1_0 = 1;
		G_B1_1 = 1;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = 1;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromInt_m961274613_MethodInfo_var);
		Action_3_t676603244 * L_4 = (Action_3_t676603244 *)il2cpp_codegen_object_new(Action_3_t676603244_il2cpp_TypeInfo_var);
		Action_3__ctor_m1561331108(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m1561331108_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_4(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t676603244 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_4();
		bool L_6 = PlayerPrefsX_SetValue_TisInt32U5BU5D_t3030399641_m1762095552(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisInt32U5BU5D_t3030399641_m1762095552_MethodInfo_var);
		return L_6;
	}
}
// System.Boolean PlayerPrefsX::SetFloatArray(System.String,System.Single[])
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t2127361240_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromFloat_m2802631456_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m2823532598_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisSingleU5BU5D_t577127397_m105798686_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetFloatArray_m1305280912_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetFloatArray_m1305280912 (Il2CppObject * __this /* static, unused */, String_t* ___key0, SingleU5BU5D_t577127397* ___floatArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetFloatArray_m1305280912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	SingleU5BU5D_t577127397* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	SingleU5BU5D_t577127397* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = ___floatArray1;
		Action_3_t2127361240 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_5();
		G_B1_0 = 1;
		G_B1_1 = 0;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = 0;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromFloat_m2802631456_MethodInfo_var);
		Action_3_t2127361240 * L_4 = (Action_3_t2127361240 *)il2cpp_codegen_object_new(Action_3_t2127361240_il2cpp_TypeInfo_var);
		Action_3__ctor_m2823532598(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m2823532598_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache1_5(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t2127361240 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_5();
		bool L_6 = PlayerPrefsX_SetValue_TisSingleU5BU5D_t577127397_m105798686(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisSingleU5BU5D_t577127397_m105798686_MethodInfo_var);
		return L_6;
	}
}
// System.Boolean PlayerPrefsX::SetVector2Array(System.String,UnityEngine.Vector2[])
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3961902885_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromVector2_m3403065726_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m3364057547_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisVector2U5BU5D_t686124026_m4218454869_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetVector2Array_m2335509664_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetVector2Array_m2335509664 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2U5BU5D_t686124026* ___vector2Array1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetVector2Array_m2335509664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	Vector2U5BU5D_t686124026* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	Vector2U5BU5D_t686124026* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		Vector2U5BU5D_t686124026* L_1 = ___vector2Array1;
		Action_3_t3961902885 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_6();
		G_B1_0 = 2;
		G_B1_1 = 4;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 2;
			G_B2_1 = 4;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromVector2_m3403065726_MethodInfo_var);
		Action_3_t3961902885 * L_4 = (Action_3_t3961902885 *)il2cpp_codegen_object_new(Action_3_t3961902885_il2cpp_TypeInfo_var);
		Action_3__ctor_m3364057547(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m3364057547_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache2_6(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t3961902885 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_6();
		bool L_6 = PlayerPrefsX_SetValue_TisVector2U5BU5D_t686124026_m4218454869(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisVector2U5BU5D_t686124026_m4218454869_MethodInfo_var);
		return L_6;
	}
}
// System.Boolean PlayerPrefsX::SetVector3Array(System.String,UnityEngine.Vector3[])
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3958575688_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromVector3_m1055003390_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m3671507690_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisVector3U5BU5D_t1172311765_m2587447478_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetVector3Array_m2015741178_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetVector3Array_m2015741178 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3U5BU5D_t1172311765* ___vector3Array1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetVector3Array_m2015741178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	Vector3U5BU5D_t1172311765* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	Vector3U5BU5D_t1172311765* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		Vector3U5BU5D_t1172311765* L_1 = ___vector3Array1;
		Action_3_t3958575688 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache3_7();
		G_B1_0 = 3;
		G_B1_1 = 5;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 3;
			G_B2_1 = 5;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromVector3_m1055003390_MethodInfo_var);
		Action_3_t3958575688 * L_4 = (Action_3_t3958575688 *)il2cpp_codegen_object_new(Action_3_t3958575688_il2cpp_TypeInfo_var);
		Action_3__ctor_m3671507690(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m3671507690_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache3_7(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t3958575688 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache3_7();
		bool L_6 = PlayerPrefsX_SetValue_TisVector3U5BU5D_t1172311765_m2587447478(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisVector3U5BU5D_t1172311765_m2587447478_MethodInfo_var);
		return L_6;
	}
}
// System.Boolean PlayerPrefsX::SetQuaternionArray(System.String,UnityEngine.Quaternion[])
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t1730450702_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromQuaternion_m3332436508_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m140702148_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisQuaternionU5BU5D_t1854387467_m3934838360_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetQuaternionArray_m3670241050_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetQuaternionArray_m3670241050 (Il2CppObject * __this /* static, unused */, String_t* ___key0, QuaternionU5BU5D_t1854387467* ___quaternionArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetQuaternionArray_m3670241050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	QuaternionU5BU5D_t1854387467* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	QuaternionU5BU5D_t1854387467* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		QuaternionU5BU5D_t1854387467* L_1 = ___quaternionArray1;
		Action_3_t1730450702 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache4_8();
		G_B1_0 = 4;
		G_B1_1 = 6;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 4;
			G_B2_1 = 6;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromQuaternion_m3332436508_MethodInfo_var);
		Action_3_t1730450702 * L_4 = (Action_3_t1730450702 *)il2cpp_codegen_object_new(Action_3_t1730450702_il2cpp_TypeInfo_var);
		Action_3__ctor_m140702148(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m140702148_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache4_8(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t1730450702 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache4_8();
		bool L_6 = PlayerPrefsX_SetValue_TisQuaternionU5BU5D_t1854387467_m3934838360(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisQuaternionU5BU5D_t1854387467_m3934838360_MethodInfo_var);
		return L_6;
	}
}
// System.Boolean PlayerPrefsX::SetColorArray(System.String,UnityEngine.Color[])
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t2179559061_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromColor_m1007003518_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m313799129_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisColorU5BU5D_t672350442_m2570053159_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetColorArray_m848705280_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SetColorArray_m848705280 (Il2CppObject * __this /* static, unused */, String_t* ___key0, ColorU5BU5D_t672350442* ___colorArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetColorArray_m848705280_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	ColorU5BU5D_t672350442* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	ColorU5BU5D_t672350442* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		ColorU5BU5D_t672350442* L_1 = ___colorArray1;
		Action_3_t2179559061 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache5_9();
		G_B1_0 = 4;
		G_B1_1 = 7;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 4;
			G_B2_1 = 7;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromColor_m1007003518_MethodInfo_var);
		Action_3_t2179559061 * L_4 = (Action_3_t2179559061 *)il2cpp_codegen_object_new(Action_3_t2179559061_il2cpp_TypeInfo_var);
		Action_3__ctor_m313799129(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m313799129_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache5_9(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t2179559061 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache5_9();
		bool L_6 = PlayerPrefsX_SetValue_TisColorU5BU5D_t672350442_m2570053159(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisColorU5BU5D_t672350442_m2570053159_MethodInfo_var);
		return L_6;
	}
}
// System.Void PlayerPrefsX::ConvertFromInt(System.Int32[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromInt_m961274613 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		ByteU5BU5D_t3397334013* L_4 = ___bytes1;
		PlayerPrefsX_ConvertInt32ToBytes_m990830831(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFromFloat(System.Single[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromFloat_m2802631456 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		SingleU5BU5D_t577127397* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		float L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		ByteU5BU5D_t3397334013* L_4 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFromVector2(UnityEngine.Vector2[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromVector2_m3403065726 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t686124026* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		Vector2U5BU5D_t686124026* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		float L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_x_0();
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2U5BU5D_t686124026* L_4 = ___array0;
		int32_t L_5 = ___i2;
		NullCheck(L_4);
		float L_6 = ((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->get_y_1();
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFromVector3(UnityEngine.Vector3[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromVector3_m1055003390 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		Vector3U5BU5D_t1172311765* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		float L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_x_1();
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector3U5BU5D_t1172311765* L_4 = ___array0;
		int32_t L_5 = ___i2;
		NullCheck(L_4);
		float L_6 = ((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->get_y_2();
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3U5BU5D_t1172311765* L_8 = ___array0;
		int32_t L_9 = ___i2;
		NullCheck(L_8);
		float L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->get_z_3();
		ByteU5BU5D_t3397334013* L_11 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFromQuaternion(UnityEngine.Quaternion[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromQuaternion_m3332436508 (Il2CppObject * __this /* static, unused */, QuaternionU5BU5D_t1854387467* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		QuaternionU5BU5D_t1854387467* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		float L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_x_0();
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		QuaternionU5BU5D_t1854387467* L_4 = ___array0;
		int32_t L_5 = ___i2;
		NullCheck(L_4);
		float L_6 = ((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->get_y_1();
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		QuaternionU5BU5D_t1854387467* L_8 = ___array0;
		int32_t L_9 = ___i2;
		NullCheck(L_8);
		float L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->get_z_2();
		ByteU5BU5D_t3397334013* L_11 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		QuaternionU5BU5D_t1854387467* L_12 = ___array0;
		int32_t L_13 = ___i2;
		NullCheck(L_12);
		float L_14 = ((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_w_3();
		ByteU5BU5D_t3397334013* L_15 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFromColor(UnityEngine.Color[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromColor_m1007003518 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		ColorU5BU5D_t672350442* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		float L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_r_0();
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ColorU5BU5D_t672350442* L_4 = ___array0;
		int32_t L_5 = ___i2;
		NullCheck(L_4);
		float L_6 = ((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->get_g_1();
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		ColorU5BU5D_t672350442* L_8 = ___array0;
		int32_t L_9 = ___i2;
		NullCheck(L_8);
		float L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->get_b_2();
		ByteU5BU5D_t3397334013* L_11 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		ColorU5BU5D_t672350442* L_12 = ___array0;
		int32_t L_13 = ___i2;
		NullCheck(L_12);
		float L_14 = ((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_a_3();
		ByteU5BU5D_t3397334013* L_15 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32[] PlayerPrefsX::GetIntArray(System.String)
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t2760079778_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3311112068_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToInt_m644963337_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m1702489330_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t1440998580_m3818794476_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m3453833174_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetIntArray_m1386818360_MetadataUsageId;
extern "C"  Int32U5BU5D_t3030399641* PlayerPrefsX_GetIntArray_m1386818360 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetIntArray_m1386818360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1440998580 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t1440998580 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t1440998580 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m3311112068(L_0, /*hidden argument*/List_1__ctor_m3311112068_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t1440998580 * L_2 = V_0;
		Action_2_t2760079778 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache6_10();
		G_B1_0 = 1;
		G_B1_1 = 1;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 1;
			G_B2_1 = 1;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToInt_m644963337_MethodInfo_var);
		Action_2_t2760079778 * L_5 = (Action_2_t2760079778 *)il2cpp_codegen_object_new(Action_2_t2760079778_il2cpp_TypeInfo_var);
		Action_2__ctor_m1702489330(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m1702489330_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache6_10(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t2760079778 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache6_10();
		PlayerPrefsX_GetValue_TisList_1_t1440998580_m3818794476(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t1440998580_m3818794476_MethodInfo_var);
		List_1_t1440998580 * L_7 = V_0;
		NullCheck(L_7);
		Int32U5BU5D_t3030399641* L_8 = List_1_ToArray_m3453833174(L_7, /*hidden argument*/List_1_ToArray_m3453833174_MethodInfo_var);
		return L_8;
	}
}
// System.Int32[] PlayerPrefsX::GetIntArray(System.String,System.Int32,System.Int32)
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetIntArray_m1834526262_MetadataUsageId;
extern "C"  Int32U5BU5D_t3030399641* PlayerPrefsX_GetIntArray_m1834526262 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetIntArray_m1834526262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Int32U5BU5D_t3030399641* L_3 = PlayerPrefsX_GetIntArray_m1386818360(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0028;
	}

IL_0020:
	{
		Int32U5BU5D_t3030399641* L_5 = V_0;
		int32_t L_6 = V_1;
		int32_t L_7 = ___defaultValue1;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (int32_t)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_11 = V_0;
		return L_11;
	}
}
// System.Single[] PlayerPrefsX::GetFloatArray(System.String)
extern Il2CppClass* List_1_t1445631064_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t306807534_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1509370154_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToFloat_m2617188600_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m357631906_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t1445631064_m2582544058_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m4175139116_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetFloatArray_m1814773467_MetadataUsageId;
extern "C"  SingleU5BU5D_t577127397* PlayerPrefsX_GetFloatArray_m1814773467 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetFloatArray_m1814773467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1445631064 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t1445631064 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t1445631064 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)il2cpp_codegen_object_new(List_1_t1445631064_il2cpp_TypeInfo_var);
		List_1__ctor_m1509370154(L_0, /*hidden argument*/List_1__ctor_m1509370154_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t1445631064 * L_2 = V_0;
		Action_2_t306807534 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache7_11();
		G_B1_0 = 1;
		G_B1_1 = 0;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 1;
			G_B2_1 = 0;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToFloat_m2617188600_MethodInfo_var);
		Action_2_t306807534 * L_5 = (Action_2_t306807534 *)il2cpp_codegen_object_new(Action_2_t306807534_il2cpp_TypeInfo_var);
		Action_2__ctor_m357631906(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m357631906_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache7_11(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t306807534 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache7_11();
		PlayerPrefsX_GetValue_TisList_1_t1445631064_m2582544058(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t1445631064_m2582544058_MethodInfo_var);
		List_1_t1445631064 * L_7 = V_0;
		NullCheck(L_7);
		SingleU5BU5D_t577127397* L_8 = List_1_ToArray_m4175139116(L_7, /*hidden argument*/List_1_ToArray_m4175139116_MethodInfo_var);
		return L_8;
	}
}
// System.Single[] PlayerPrefsX::GetFloatArray(System.String,System.Single,System.Int32)
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetFloatArray_m2057507575_MetadataUsageId;
extern "C"  SingleU5BU5D_t577127397* PlayerPrefsX_GetFloatArray_m2057507575 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetFloatArray_m2057507575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t577127397* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		SingleU5BU5D_t577127397* L_3 = PlayerPrefsX_GetFloatArray_m1814773467(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0028;
	}

IL_0020:
	{
		SingleU5BU5D_t577127397* L_5 = V_0;
		int32_t L_6 = V_1;
		float L_7 = ___defaultValue1;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (float)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		SingleU5BU5D_t577127397* L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Vector2[] PlayerPrefsX::GetVector2Array(System.String)
extern Il2CppClass* List_1_t1612828711_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t415804163_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m310628129_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToVector2_m3795235550_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m697927655_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t1612828711_m1723016453_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m4024240619_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetVector2Array_m2652766529_MetadataUsageId;
extern "C"  Vector2U5BU5D_t686124026* PlayerPrefsX_GetVector2Array_m2652766529 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetVector2Array_m2652766529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1612828711 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t1612828711 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t1612828711 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t1612828711 * L_0 = (List_1_t1612828711 *)il2cpp_codegen_object_new(List_1_t1612828711_il2cpp_TypeInfo_var);
		List_1__ctor_m310628129(L_0, /*hidden argument*/List_1__ctor_m310628129_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t1612828711 * L_2 = V_0;
		Action_2_t415804163 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache8_12();
		G_B1_0 = 2;
		G_B1_1 = 4;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = 4;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToVector2_m3795235550_MethodInfo_var);
		Action_2_t415804163 * L_5 = (Action_2_t415804163 *)il2cpp_codegen_object_new(Action_2_t415804163_il2cpp_TypeInfo_var);
		Action_2__ctor_m697927655(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m697927655_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache8_12(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t415804163 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache8_12();
		PlayerPrefsX_GetValue_TisList_1_t1612828711_m1723016453(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t1612828711_m1723016453_MethodInfo_var);
		List_1_t1612828711 * L_7 = V_0;
		NullCheck(L_7);
		Vector2U5BU5D_t686124026* L_8 = List_1_ToArray_m4024240619(L_7, /*hidden argument*/List_1_ToArray_m4024240619_MethodInfo_var);
		return L_8;
	}
}
// UnityEngine.Vector2[] PlayerPrefsX::GetVector2Array(System.String,UnityEngine.Vector2,System.Int32)
extern Il2CppClass* Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetVector2Array_m2920103712_MetadataUsageId;
extern "C"  Vector2U5BU5D_t686124026* PlayerPrefsX_GetVector2Array_m2920103712 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2_t2243707579  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetVector2Array_m2920103712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t686124026* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Vector2U5BU5D_t686124026* L_3 = PlayerPrefsX_GetVector2Array_m2652766529(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0031;
	}

IL_0020:
	{
		Vector2U5BU5D_t686124026* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Vector2_t2243707579  L_7 = ___defaultValue1;
		(*(Vector2_t2243707579 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		Vector2U5BU5D_t686124026* L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Vector3[] PlayerPrefsX::GetVector3Array(System.String)
extern Il2CppClass* List_1_t1612828712_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t901991902_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m347461442_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToVector3_m3000137214_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m4286091750_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t1612828712_m1874995114_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m2543904144_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetVector3Array_m3968718013_MetadataUsageId;
extern "C"  Vector3U5BU5D_t1172311765* PlayerPrefsX_GetVector3Array_m3968718013 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetVector3Array_m3968718013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1612828712 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t1612828712 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t1612828712 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t1612828712 * L_0 = (List_1_t1612828712 *)il2cpp_codegen_object_new(List_1_t1612828712_il2cpp_TypeInfo_var);
		List_1__ctor_m347461442(L_0, /*hidden argument*/List_1__ctor_m347461442_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t1612828712 * L_2 = V_0;
		Action_2_t901991902 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache9_13();
		G_B1_0 = 3;
		G_B1_1 = 5;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 3;
			G_B2_1 = 5;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToVector3_m3000137214_MethodInfo_var);
		Action_2_t901991902 * L_5 = (Action_2_t901991902 *)il2cpp_codegen_object_new(Action_2_t901991902_il2cpp_TypeInfo_var);
		Action_2__ctor_m4286091750(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m4286091750_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache9_13(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t901991902 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache9_13();
		PlayerPrefsX_GetValue_TisList_1_t1612828712_m1874995114(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t1612828712_m1874995114_MethodInfo_var);
		List_1_t1612828712 * L_7 = V_0;
		NullCheck(L_7);
		Vector3U5BU5D_t1172311765* L_8 = List_1_ToArray_m2543904144(L_7, /*hidden argument*/List_1_ToArray_m2543904144_MethodInfo_var);
		return L_8;
	}
}
// UnityEngine.Vector3[] PlayerPrefsX::GetVector3Array(System.String,UnityEngine.Vector3,System.Int32)
extern Il2CppClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetVector3Array_m2590474277_MetadataUsageId;
extern "C"  Vector3U5BU5D_t1172311765* PlayerPrefsX_GetVector3Array_m2590474277 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3_t2243707580  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetVector3Array_m2590474277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1172311765* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Vector3U5BU5D_t1172311765* L_3 = PlayerPrefsX_GetVector3Array_m3968718013(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0031;
	}

IL_0020:
	{
		Vector3U5BU5D_t1172311765* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Vector3_t2243707580  L_7 = ___defaultValue1;
		(*(Vector3_t2243707580 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Quaternion[] PlayerPrefsX::GetQuaternionArray(System.String)
extern Il2CppClass* List_1_t3399195050_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t1584067604_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2815122524_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToQuaternion_m4252035516_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m3760184070_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t3399195050_m2139544680_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m3316098218_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetQuaternionArray_m3914704109_MetadataUsageId;
extern "C"  QuaternionU5BU5D_t1854387467* PlayerPrefsX_GetQuaternionArray_m3914704109 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetQuaternionArray_m3914704109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3399195050 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t3399195050 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t3399195050 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t3399195050 * L_0 = (List_1_t3399195050 *)il2cpp_codegen_object_new(List_1_t3399195050_il2cpp_TypeInfo_var);
		List_1__ctor_m2815122524(L_0, /*hidden argument*/List_1__ctor_m2815122524_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t3399195050 * L_2 = V_0;
		Action_2_t1584067604 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheA_14();
		G_B1_0 = 4;
		G_B1_1 = 6;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 4;
			G_B2_1 = 6;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToQuaternion_m4252035516_MethodInfo_var);
		Action_2_t1584067604 * L_5 = (Action_2_t1584067604 *)il2cpp_codegen_object_new(Action_2_t1584067604_il2cpp_TypeInfo_var);
		Action_2__ctor_m3760184070(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m3760184070_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheA_14(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t1584067604 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheA_14();
		PlayerPrefsX_GetValue_TisList_1_t3399195050_m2139544680(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t3399195050_m2139544680_MethodInfo_var);
		List_1_t3399195050 * L_7 = V_0;
		NullCheck(L_7);
		QuaternionU5BU5D_t1854387467* L_8 = List_1_ToArray_m3316098218(L_7, /*hidden argument*/List_1_ToArray_m3316098218_MethodInfo_var);
		return L_8;
	}
}
// UnityEngine.Quaternion[] PlayerPrefsX::GetQuaternionArray(System.String,UnityEngine.Quaternion,System.Int32)
extern Il2CppClass* QuaternionU5BU5D_t1854387467_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetQuaternionArray_m3503864975_MetadataUsageId;
extern "C"  QuaternionU5BU5D_t1854387467* PlayerPrefsX_GetQuaternionArray_m3503864975 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Quaternion_t4030073918  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetQuaternionArray_m3503864975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	QuaternionU5BU5D_t1854387467* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		QuaternionU5BU5D_t1854387467* L_3 = PlayerPrefsX_GetQuaternionArray_m3914704109(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((QuaternionU5BU5D_t1854387467*)SZArrayNew(QuaternionU5BU5D_t1854387467_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0031;
	}

IL_0020:
	{
		QuaternionU5BU5D_t1854387467* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Quaternion_t4030073918  L_7 = ___defaultValue1;
		(*(Quaternion_t4030073918 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		QuaternionU5BU5D_t1854387467* L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Color[] PlayerPrefsX::GetColorArray(System.String)
extern Il2CppClass* List_1_t1389513207_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t402030579_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2982146419_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToColor_m3889997982_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m2525621589_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t1389513207_m3175850115_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1181907605_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetColorArray_m366961213_MetadataUsageId;
extern "C"  ColorU5BU5D_t672350442* PlayerPrefsX_GetColorArray_m366961213 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetColorArray_m366961213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1389513207 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t1389513207 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t1389513207 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t1389513207 * L_0 = (List_1_t1389513207 *)il2cpp_codegen_object_new(List_1_t1389513207_il2cpp_TypeInfo_var);
		List_1__ctor_m2982146419(L_0, /*hidden argument*/List_1__ctor_m2982146419_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t1389513207 * L_2 = V_0;
		Action_2_t402030579 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheB_15();
		G_B1_0 = 4;
		G_B1_1 = 7;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 4;
			G_B2_1 = 7;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToColor_m3889997982_MethodInfo_var);
		Action_2_t402030579 * L_5 = (Action_2_t402030579 *)il2cpp_codegen_object_new(Action_2_t402030579_il2cpp_TypeInfo_var);
		Action_2__ctor_m2525621589(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m2525621589_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheB_15(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t402030579 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheB_15();
		PlayerPrefsX_GetValue_TisList_1_t1389513207_m3175850115(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t1389513207_m3175850115_MethodInfo_var);
		List_1_t1389513207 * L_7 = V_0;
		NullCheck(L_7);
		ColorU5BU5D_t672350442* L_8 = List_1_ToArray_m1181907605(L_7, /*hidden argument*/List_1_ToArray_m1181907605_MethodInfo_var);
		return L_8;
	}
}
// UnityEngine.Color[] PlayerPrefsX::GetColorArray(System.String,UnityEngine.Color,System.Int32)
extern Il2CppClass* ColorU5BU5D_t672350442_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetColorArray_m1825582668_MetadataUsageId;
extern "C"  ColorU5BU5D_t672350442* PlayerPrefsX_GetColorArray_m1825582668 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Color_t2020392075  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetColorArray_m1825582668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColorU5BU5D_t672350442* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		ColorU5BU5D_t672350442* L_3 = PlayerPrefsX_GetColorArray_m366961213(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0031;
	}

IL_0020:
	{
		ColorU5BU5D_t672350442* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Color_t2020392075  L_7 = ___defaultValue1;
		(*(Color_t2020392075 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		ColorU5BU5D_t672350442* L_11 = V_0;
		return L_11;
	}
}
// System.Void PlayerPrefsX::ConvertToInt(System.Collections.Generic.List`1<System.Int32>,System.Byte[])
extern const MethodInfo* List_1_Add_m2828939739_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToInt_m644963337_MetadataUsageId;
extern "C"  void PlayerPrefsX_ConvertToInt_m644963337 (Il2CppObject * __this /* static, unused */, List_1_t1440998580 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToInt_m644963337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1440998580 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		int32_t L_2 = PlayerPrefsX_ConvertBytesToInt32_m1019863976(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m2828939739(L_0, L_2, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertToFloat(System.Collections.Generic.List`1<System.Single>,System.Byte[])
extern const MethodInfo* List_1_Add_m913687102_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToFloat_m2617188600_MetadataUsageId;
extern "C"  void PlayerPrefsX_ConvertToFloat_m2617188600 (Il2CppObject * __this /* static, unused */, List_1_t1445631064 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToFloat_m2617188600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1445631064 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		float L_2 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m913687102(L_0, L_2, /*hidden argument*/List_1_Add_m913687102_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertToVector2(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Byte[])
extern const MethodInfo* List_1_Add_m148291600_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToVector2_m3795235550_MetadataUsageId;
extern "C"  void PlayerPrefsX_ConvertToVector2_m3795235550 (Il2CppObject * __this /* static, unused */, List_1_t1612828711 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToVector2_m3795235550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1612828711 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		float L_2 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		float L_4 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m3067419446(&L_5, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m148291600(L_0, L_5, /*hidden argument*/List_1_Add_m148291600_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertToVector3(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Byte[])
extern const MethodInfo* List_1_Add_m2338641291_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToVector3_m3000137214_MetadataUsageId;
extern "C"  void PlayerPrefsX_ConvertToVector3_m3000137214 (Il2CppObject * __this /* static, unused */, List_1_t1612828712 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToVector3_m3000137214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1612828712 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		float L_2 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		float L_4 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = ___bytes1;
		float L_6 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2638739322(&L_7, L_2, L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m2338641291(L_0, L_7, /*hidden argument*/List_1_Add_m2338641291_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertToQuaternion(System.Collections.Generic.List`1<UnityEngine.Quaternion>,System.Byte[])
extern const MethodInfo* List_1_Add_m1729225192_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToQuaternion_m4252035516_MetadataUsageId;
extern "C"  void PlayerPrefsX_ConvertToQuaternion_m4252035516 (Il2CppObject * __this /* static, unused */, List_1_t3399195050 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToQuaternion_m4252035516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3399195050 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		float L_2 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		float L_4 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = ___bytes1;
		float L_6 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		float L_8 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Quaternion__ctor_m3196903881(&L_9, L_2, L_4, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m1729225192(L_0, L_9, /*hidden argument*/List_1_Add_m1729225192_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertToColor(System.Collections.Generic.List`1<UnityEngine.Color>,System.Byte[])
extern const MethodInfo* List_1_Add_m3224551367_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToColor_m3889997982_MetadataUsageId;
extern "C"  void PlayerPrefsX_ConvertToColor_m3889997982 (Il2CppObject * __this /* static, unused */, List_1_t1389513207 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToColor_m3889997982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1389513207 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		float L_2 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		float L_4 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = ___bytes1;
		float L_6 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		float L_8 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, L_2, L_4, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m3224551367(L_0, L_9, /*hidden argument*/List_1_Add_m3224551367_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ShowArrayType(System.String)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayType_t77146353_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral426807795;
extern Il2CppCodeGenString* _stringLiteral93295297;
extern const uint32_t PlayerPrefsX_ShowArrayType_m1550763734_MetadataUsageId;
extern "C"  void PlayerPrefsX_ShowArrayType_m1550763734 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ShowArrayType_m1550763734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_2 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ByteU5BU5D_t3397334013* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		uint8_t L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		String_t* L_7 = ___key0;
		Il2CppObject * L_8 = Box(ArrayType_t77146353_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1561703559(NULL /*static, unused*/, L_7, _stringLiteral426807795, L_9, _stringLiteral93295297, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void PlayerPrefsX::Initialize()
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_Initialize_m3123284730_MetadataUsageId;
extern "C"  void PlayerPrefsX_Initialize_m3123284730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_Initialize_m3123284730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		bool L_0 = ((BitConverter_t3195628829_StaticFields*)BitConverter_t3195628829_il2cpp_TypeInfo_var->static_fields)->get_IsLittleEndian_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_endianDiff1_0(0);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_endianDiff2_1(0);
		goto IL_0027;
	}

IL_001b:
	{
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_endianDiff1_0(3);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_endianDiff2_1(1);
	}

IL_0027:
	{
		ByteU5BU5D_t3397334013* L_1 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		if (L_1)
		{
			goto IL_003c;
		}
	}
	{
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_byteBlock_3(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)4)));
	}

IL_003c:
	{
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_idx_2(1);
		return;
	}
}
// System.Boolean PlayerPrefsX::SaveBytes(System.String,System.Byte[])
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_SaveBytes_m2961073563_MetadataUsageId;
extern "C"  bool PlayerPrefsX_SaveBytes_m2961073563 (Il2CppObject * __this /* static, unused */, String_t* ___key0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SaveBytes_m2961073563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		String_t* L_0 = ___key0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToBase64String_m1936815455(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		goto IL_0019;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0011;
		throw e;
	}

CATCH_0011:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_001b;
	} // end catch (depth: 1)

IL_0019:
	{
		return (bool)1;
	}

IL_001b:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void PlayerPrefsX::ConvertFloatToBytes(System.Single,System.Byte[])
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_ConvertFloatToBytes_m3413650209_MetadataUsageId;
extern "C"  void PlayerPrefsX_ConvertFloatToBytes_m3413650209 (Il2CppObject * __this /* static, unused */, float ___f0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertFloatToBytes_m3413650209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___f0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = BitConverter_GetBytes_m4095372044(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_byteBlock_3(L_1);
		ByteU5BU5D_t3397334013* L_2 = ___bytes1;
		PlayerPrefsX_ConvertTo4Bytes_m3604966018(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single PlayerPrefsX::ConvertBytesToFloat(System.Byte[])
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_ConvertBytesToFloat_m1424229334_MetadataUsageId;
extern "C"  float PlayerPrefsX_ConvertBytesToFloat_m1424229334 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertBytesToFloat_m1424229334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		PlayerPrefsX_ConvertFrom4Bytes_m9415465(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_1 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		float L_2 = BitConverter_ToSingle_m159411893(NULL /*static, unused*/, L_1, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PlayerPrefsX::ConvertInt32ToBytes(System.Int32,System.Byte[])
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_ConvertInt32ToBytes_m990830831_MetadataUsageId;
extern "C"  void PlayerPrefsX_ConvertInt32ToBytes_m990830831 (Il2CppObject * __this /* static, unused */, int32_t ___i0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertInt32ToBytes_m990830831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___i0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = BitConverter_GetBytes_m1300847478(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_byteBlock_3(L_1);
		ByteU5BU5D_t3397334013* L_2 = ___bytes1;
		PlayerPrefsX_ConvertTo4Bytes_m3604966018(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PlayerPrefsX::ConvertBytesToInt32(System.Byte[])
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_ConvertBytesToInt32_m1019863976_MetadataUsageId;
extern "C"  int32_t PlayerPrefsX_ConvertBytesToInt32_m1019863976 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertBytesToInt32_m1019863976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		PlayerPrefsX_ConvertFrom4Bytes_m9415465(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_1 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		int32_t L_2 = BitConverter_ToInt32_m2742027961(NULL /*static, unused*/, L_1, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PlayerPrefsX::ConvertTo4Bytes(System.Byte[])
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_ConvertTo4Bytes_m3604966018_MetadataUsageId;
extern "C"  void PlayerPrefsX_ConvertTo4Bytes_m3604966018 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertTo4Bytes_m3604966018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		int32_t L_1 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		ByteU5BU5D_t3397334013* L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff1_0();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (uint8_t)L_5);
		ByteU5BU5D_t3397334013* L_6 = ___bytes0;
		int32_t L_7 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		ByteU5BU5D_t3397334013* L_8 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_9 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff2_1();
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)((int32_t)1+(int32_t)L_9));
		uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_7+(int32_t)1))), (uint8_t)L_11);
		ByteU5BU5D_t3397334013* L_12 = ___bytes0;
		int32_t L_13 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		ByteU5BU5D_t3397334013* L_14 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_15 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff2_1();
		NullCheck(L_14);
		int32_t L_16 = ((int32_t)((int32_t)2-(int32_t)L_15));
		uint8_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_13+(int32_t)2))), (uint8_t)L_17);
		ByteU5BU5D_t3397334013* L_18 = ___bytes0;
		int32_t L_19 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		ByteU5BU5D_t3397334013* L_20 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_21 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff1_0();
		NullCheck(L_20);
		int32_t L_22 = ((int32_t)((int32_t)3-(int32_t)L_21));
		uint8_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)3))), (uint8_t)L_23);
		int32_t L_24 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_idx_2(((int32_t)((int32_t)L_24+(int32_t)4)));
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFrom4Bytes(System.Byte[])
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_ConvertFrom4Bytes_m9415465_MetadataUsageId;
extern "C"  void PlayerPrefsX_ConvertFrom4Bytes_m9415465 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertFrom4Bytes_m9415465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_1 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff1_0();
		ByteU5BU5D_t3397334013* L_2 = ___bytes0;
		int32_t L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (uint8_t)L_5);
		ByteU5BU5D_t3397334013* L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_7 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff2_1();
		ByteU5BU5D_t3397334013* L_8 = ___bytes0;
		int32_t L_9 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)((int32_t)L_9+(int32_t)1));
		uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)1+(int32_t)L_7))), (uint8_t)L_11);
		ByteU5BU5D_t3397334013* L_12 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_13 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff2_1();
		ByteU5BU5D_t3397334013* L_14 = ___bytes0;
		int32_t L_15 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		NullCheck(L_14);
		int32_t L_16 = ((int32_t)((int32_t)L_15+(int32_t)2));
		uint8_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)2-(int32_t)L_13))), (uint8_t)L_17);
		ByteU5BU5D_t3397334013* L_18 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_19 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff1_0();
		ByteU5BU5D_t3397334013* L_20 = ___bytes0;
		int32_t L_21 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		NullCheck(L_20);
		int32_t L_22 = ((int32_t)((int32_t)L_21+(int32_t)3));
		uint8_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)3-(int32_t)L_19))), (uint8_t)L_23);
		int32_t L_24 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_idx_2(((int32_t)((int32_t)L_24+(int32_t)4)));
		return;
	}
}
// System.Void Question::.ctor()
extern "C"  void Question__ctor_m1719258817 (Question_t2927948840 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuestionController::.ctor()
extern "C"  void QuestionController__ctor_m3917901329 (QuestionController_t445239244 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuestionController::VerifyQuestion(UnityEngine.GameObject)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteWithGameObjcet_t1191175011_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m4010202484_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern const MethodInfo* QuestionController_CounterQuestion_m3694873063_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1389210506;
extern Il2CppCodeGenString* _stringLiteral1119447084;
extern const uint32_t QuestionController_VerifyQuestion_m145838332_MetadataUsageId;
extern "C"  void QuestionController_VerifyQuestion_m145838332 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestionController_VerifyQuestion_m145838332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Text_t356221433 * V_1 = NULL;
	Image_t2042527209 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		LevelsU5BU5D_t2050899114* L_0 = __this->get_levelGlobal_25();
		int32_t L_1 = __this->get_positionLevel_26();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Levels_t748035019 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		List_1_t2297069972 * L_4 = Levels_get_Quests_m1321190565(L_3, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_numQuest_24();
		NullCheck(L_4);
		Question_t2927948840 * L_6 = List_1_get_Item_m4010202484(L_4, L_5, /*hidden argument*/List_1_get_Item_m4010202484_MethodInfo_var);
		NullCheck(L_6);
		String_t* L_7 = L_6->get_answer_0();
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		__this->set_correctAns_15(L_8);
		String_t* L_9 = __this->get_correctAns_15();
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = ___obj0;
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = GameObject_get_gameObject_m3662236595(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Transform_GetChild_m3838588184(L_12, 0, /*hidden argument*/NULL);
		NullCheck(L_13);
		GameObject_t1756533147 * L_14 = Component_get_gameObject_m3105766835(L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		GameObject_t1756533147 * L_15 = V_0;
		NullCheck(L_15);
		Text_t356221433 * L_16 = GameObject_GetComponent_TisText_t356221433_m4280536079(L_15, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var);
		V_1 = L_16;
		GameObject_t1756533147 * L_17 = ___obj0;
		NullCheck(L_17);
		Image_t2042527209 * L_18 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_17, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		V_2 = L_18;
		Text_t356221433 * L_19 = V_1;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_19);
		String_t* L_21 = __this->get_correctAns_15();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00ce;
		}
	}
	{
		int32_t L_23 = __this->get_correctAnswer_27();
		__this->set_correctAnswer_27(((int32_t)((int32_t)L_23+(int32_t)1)));
		SoundController_t1686593041 * L_24 = __this->get_soundControll_29();
		NullCheck(L_24);
		SoundController_OnSound_m4103739782(L_24, /*hidden argument*/NULL);
		Image_t2042527209 * L_25 = V_2;
		Color_t2020392075  L_26 = Color_get_green_m2671273823(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_25, L_26);
		int32_t L_27 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral1389210506, 0, /*hidden argument*/NULL);
		V_3 = ((int32_t)((int32_t)L_27+(int32_t)1));
		int32_t L_28 = V_3;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral1389210506, L_28, /*hidden argument*/NULL);
		IntPtr_t L_29;
		L_29.set_m_value_0((void*)(void*)QuestionController_CounterQuestion_m3694873063_MethodInfo_var);
		ExecuteWithGameObjcet_t1191175011 * L_30 = (ExecuteWithGameObjcet_t1191175011 *)il2cpp_codegen_object_new(ExecuteWithGameObjcet_t1191175011_il2cpp_TypeInfo_var);
		ExecuteWithGameObjcet__ctor_m342781680(L_30, __this, L_29, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_31 = ___obj0;
		Handler_StartDelayed_m1932308614(NULL /*static, unused*/, (1.0f), L_30, L_31, __this, /*hidden argument*/NULL);
		goto IL_0133;
	}

IL_00ce:
	{
		Text_t356221433 * L_32 = V_1;
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_32);
		String_t* L_34 = __this->get_correctAns_15();
		NullCheck(L_34);
		String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_36 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_33, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0133;
		}
	}
	{
		SoundController_t1686593041 * L_37 = __this->get_soundControll_29();
		NullCheck(L_37);
		SoundController_OffSound_m3094588068(L_37, /*hidden argument*/NULL);
		Image_t2042527209 * L_38 = V_2;
		Color_t2020392075  L_39 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_38);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_38, L_39);
		int32_t L_40 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral1119447084, 0, /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)L_40+(int32_t)1));
		int32_t L_41 = V_4;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral1119447084, L_41, /*hidden argument*/NULL);
		IntPtr_t L_42;
		L_42.set_m_value_0((void*)(void*)QuestionController_CounterQuestion_m3694873063_MethodInfo_var);
		ExecuteWithGameObjcet_t1191175011 * L_43 = (ExecuteWithGameObjcet_t1191175011 *)il2cpp_codegen_object_new(ExecuteWithGameObjcet_t1191175011_il2cpp_TypeInfo_var);
		ExecuteWithGameObjcet__ctor_m342781680(L_43, __this, L_42, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_44 = ___obj0;
		Handler_StartDelayed_m1932308614(NULL /*static, unused*/, (1.0f), L_43, L_44, __this, /*hidden argument*/NULL);
	}

IL_0133:
	{
		Button_t2872111280 * L_45 = __this->get_btn_1_7();
		NullCheck(L_45);
		Selectable_set_interactable_m63718297(L_45, (bool)0, /*hidden argument*/NULL);
		Button_t2872111280 * L_46 = __this->get_btn_2_8();
		NullCheck(L_46);
		Selectable_set_interactable_m63718297(L_46, (bool)0, /*hidden argument*/NULL);
		Button_t2872111280 * L_47 = __this->get_btn_3_9();
		NullCheck(L_47);
		Selectable_set_interactable_m63718297(L_47, (bool)0, /*hidden argument*/NULL);
		Button_t2872111280 * L_48 = __this->get_btn_4_10();
		NullCheck(L_48);
		Selectable_set_interactable_m63718297(L_48, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuestionController::SetLevels(Levels[],System.Int32)
extern const MethodInfo* List_1_get_Item_m4010202484_MethodInfo_var;
extern const uint32_t QuestionController_SetLevels_m2193348496_MetadataUsageId;
extern "C"  void QuestionController_SetLevels_m2193348496 (QuestionController_t445239244 * __this, LevelsU5BU5D_t2050899114* ___level0, int32_t ___position1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestionController_SetLevels_m2193348496_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_numQuest_24(0);
		LevelsU5BU5D_t2050899114* L_0 = ___level0;
		__this->set_levelGlobal_25(L_0);
		int32_t L_1 = ___position1;
		__this->set_positionLevel_26(L_1);
		LevelsU5BU5D_t2050899114* L_2 = __this->get_levelGlobal_25();
		int32_t L_3 = __this->get_positionLevel_26();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Levels_t748035019 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		List_1_t2297069972 * L_6 = Levels_get_Quests_m1321190565(L_5, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_numQuest_24();
		NullCheck(L_6);
		Question_t2927948840 * L_8 = List_1_get_Item_m4010202484(L_6, L_7, /*hidden argument*/List_1_get_Item_m4010202484_MethodInfo_var);
		QuestionController_SetQuestion_m3640094679(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuestionController::SetQuestion(Question)
extern "C"  void QuestionController_SetQuestion_m3640094679 (QuestionController_t445239244 * __this, Question_t2927948840 * ___quest0, const MethodInfo* method)
{
	{
		Question_t2927948840 * L_0 = ___quest0;
		NullCheck(L_0);
		StringU5BU5D_t1642385972* L_1 = L_0->get_choices_1();
		Util_reshuffle_m3930049179(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Text_t356221433 * L_2 = __this->get_question_2();
		Question_t2927948840 * L_3 = ___quest0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get_question_2();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_4);
		Text_t356221433 * L_5 = __this->get_answer_0_3();
		Question_t2927948840 * L_6 = ___quest0;
		NullCheck(L_6);
		StringU5BU5D_t1642385972* L_7 = L_6->get_choices_1();
		NullCheck(L_7);
		int32_t L_8 = 0;
		String_t* L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_9);
		Text_t356221433 * L_10 = __this->get_answer_1_4();
		Question_t2927948840 * L_11 = ___quest0;
		NullCheck(L_11);
		StringU5BU5D_t1642385972* L_12 = L_11->get_choices_1();
		NullCheck(L_12);
		int32_t L_13 = 1;
		String_t* L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_14);
		Text_t356221433 * L_15 = __this->get_answer_2_5();
		Question_t2927948840 * L_16 = ___quest0;
		NullCheck(L_16);
		StringU5BU5D_t1642385972* L_17 = L_16->get_choices_1();
		NullCheck(L_17);
		int32_t L_18 = 2;
		String_t* L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_15);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_15, L_19);
		Text_t356221433 * L_20 = __this->get_answer_3_6();
		Question_t2927948840 * L_21 = ___quest0;
		NullCheck(L_21);
		StringU5BU5D_t1642385972* L_22 = L_21->get_choices_1();
		NullCheck(L_22);
		int32_t L_23 = 3;
		String_t* L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_20);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_20, L_24);
		return;
	}
}
// System.Void QuestionController::CounterQuestion(UnityEngine.GameObject)
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4010202484_MethodInfo_var;
extern const uint32_t QuestionController_CounterQuestion_m3694873063_MetadataUsageId;
extern "C"  void QuestionController_CounterQuestion_m3694873063 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestionController_CounterQuestion_m3694873063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Image_t2042527209 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		GameObject_t1756533147 * L_0 = ___obj0;
		NullCheck(L_0);
		Image_t2042527209 * L_1 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_0, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		V_0 = L_1;
		Image_t2042527209 * L_2 = V_0;
		Color_t2020392075  L_3 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_2, L_3);
		Button_t2872111280 * L_4 = __this->get_btn_1_7();
		NullCheck(L_4);
		Selectable_set_interactable_m63718297(L_4, (bool)1, /*hidden argument*/NULL);
		Button_t2872111280 * L_5 = __this->get_btn_2_8();
		NullCheck(L_5);
		Selectable_set_interactable_m63718297(L_5, (bool)1, /*hidden argument*/NULL);
		Button_t2872111280 * L_6 = __this->get_btn_3_9();
		NullCheck(L_6);
		Selectable_set_interactable_m63718297(L_6, (bool)1, /*hidden argument*/NULL);
		Button_t2872111280 * L_7 = __this->get_btn_4_10();
		NullCheck(L_7);
		Selectable_set_interactable_m63718297(L_7, (bool)1, /*hidden argument*/NULL);
		int32_t L_8 = __this->get_numQuest_24();
		if ((((int32_t)L_8) > ((int32_t)((int32_t)9))))
		{
			goto IL_0085;
		}
	}
	{
		int32_t L_9 = __this->get_numQuest_24();
		__this->set_numQuest_24(((int32_t)((int32_t)L_9+(int32_t)1)));
		LevelsU5BU5D_t2050899114* L_10 = __this->get_levelGlobal_25();
		int32_t L_11 = __this->get_positionLevel_26();
		NullCheck(L_10);
		int32_t L_12 = L_11;
		Levels_t748035019 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		List_1_t2297069972 * L_14 = Levels_get_Quests_m1321190565(L_13, /*hidden argument*/NULL);
		int32_t L_15 = __this->get_numQuest_24();
		NullCheck(L_14);
		Question_t2927948840 * L_16 = List_1_get_Item_m4010202484(L_14, L_15, /*hidden argument*/List_1_get_Item_m4010202484_MethodInfo_var);
		QuestionController_SetQuestion_m3640094679(__this, L_16, /*hidden argument*/NULL);
		goto IL_01d2;
	}

IL_0085:
	{
		LevelsU5BU5D_t2050899114* L_17 = __this->get_levelGlobal_25();
		int32_t L_18 = __this->get_positionLevel_26();
		NullCheck(L_17);
		int32_t L_19 = L_18;
		Levels_t748035019 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		int32_t L_21 = __this->get_correctAnswer_27();
		NullCheck(L_20);
		Levels_set_CorectAnswers_m2211099053(L_20, L_21, /*hidden argument*/NULL);
		LevelsU5BU5D_t2050899114* L_22 = __this->get_levelGlobal_25();
		int32_t L_23 = __this->get_positionLevel_26();
		NullCheck(L_22);
		int32_t L_24 = L_23;
		Levels_t748035019 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		int32_t L_26 = Levels_getStarNumber_m1660958789(L_25, /*hidden argument*/NULL);
		V_1 = L_26;
		GameObject_t1756533147 * L_27 = __this->get_star3_13();
		int32_t L_28 = V_1;
		NullCheck(L_27);
		GameObject_SetActive_m2887581199(L_27, (bool)((((int32_t)L_28) == ((int32_t)3))? 1 : 0), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_29 = __this->get_star2_12();
		int32_t L_30 = V_1;
		NullCheck(L_29);
		GameObject_SetActive_m2887581199(L_29, (bool)((((int32_t)L_30) > ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_31 = __this->get_star1_11();
		int32_t L_32 = V_1;
		NullCheck(L_31);
		GameObject_SetActive_m2887581199(L_31, (bool)((((int32_t)L_32) > ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Text_t356221433 * L_33 = __this->get_numb_of_compl_mission_23();
		int32_t L_34 = __this->get_positionLevel_26();
		V_2 = ((int32_t)((int32_t)L_34+(int32_t)1));
		String_t* L_35 = Int32_ToString_m2960866144((&V_2), /*hidden argument*/NULL);
		NullCheck(L_33);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_33, L_35);
		GameObject_t1756533147 * L_36 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		GameObject_SetActive_m2887581199(L_36, (bool)0, /*hidden argument*/NULL);
		int32_t L_37 = __this->get_positionLevel_26();
		if ((((int32_t)L_37) > ((int32_t)((int32_t)38))))
		{
			goto IL_0197;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_38 = __this->get_levelGlobal_25();
		int32_t L_39 = __this->get_positionLevel_26();
		NullCheck(L_38);
		int32_t L_40 = L_39;
		Levels_t748035019 * L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		NullCheck(L_41);
		int32_t L_42 = Levels_getStarNumber_m1660958789(L_41, /*hidden argument*/NULL);
		if ((((int32_t)L_42) <= ((int32_t)0)))
		{
			goto IL_0163;
		}
	}
	{
		GameObject_t1756533147 * L_43 = __this->get_singleButtonPanel_19();
		NullCheck(L_43);
		GameObject_SetActive_m2887581199(L_43, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_44 = __this->get_twoButtons_18();
		NullCheck(L_44);
		GameObject_SetActive_m2887581199(L_44, (bool)1, /*hidden argument*/NULL);
		SoundController_t1686593041 * L_45 = __this->get_soundControll_29();
		NullCheck(L_45);
		SoundController_SuccesMission_m3861769026(L_45, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_46 = __this->get_notEnought_22();
		NullCheck(L_46);
		GameObject_SetActive_m2887581199(L_46, (bool)0, /*hidden argument*/NULL);
		goto IL_0192;
	}

IL_0163:
	{
		SoundController_t1686593041 * L_47 = __this->get_soundControll_29();
		NullCheck(L_47);
		SoundController_FailMission_m1630072376(L_47, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_48 = __this->get_twoButtons_18();
		NullCheck(L_48);
		GameObject_SetActive_m2887581199(L_48, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_49 = __this->get_notEnought_22();
		NullCheck(L_49);
		GameObject_SetActive_m2887581199(L_49, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_50 = __this->get_singleButtonPanel_19();
		NullCheck(L_50);
		GameObject_SetActive_m2887581199(L_50, (bool)1, /*hidden argument*/NULL);
	}

IL_0192:
	{
		goto IL_01d2;
	}

IL_0197:
	{
		SoundController_t1686593041 * L_51 = __this->get_soundControll_29();
		NullCheck(L_51);
		SoundController_SuccesMission_m3861769026(L_51, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_52 = __this->get_twoButtons_18();
		NullCheck(L_52);
		GameObject_SetActive_m2887581199(L_52, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_53 = __this->get_notEnought_22();
		NullCheck(L_53);
		GameObject_SetActive_m2887581199(L_53, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_54 = __this->get_finalAlertPanel_20();
		NullCheck(L_54);
		GameObject_SetActive_m2887581199(L_54, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_55 = __this->get_singleButtonPanel_19();
		NullCheck(L_55);
		GameObject_SetActive_m2887581199(L_55, (bool)1, /*hidden argument*/NULL);
	}

IL_01d2:
	{
		return;
	}
}
// System.Void QuestionController::NextLevel()
extern Il2CppClass* UpdateListener_t3189012468_il2cpp_TypeInfo_var;
extern Il2CppClass* Purchase_t1644043085_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m4010202484_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3833502378;
extern const uint32_t QuestionController_NextLevel_m2486282466_MetadataUsageId;
extern "C"  void QuestionController_NextLevel_m2486282466 (QuestionController_t445239244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestionController_NextLevel_m2486282466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		LevelsU5BU5D_t2050899114* L_0 = __this->get_levelGlobal_25();
		int32_t L_1 = __this->get_positionLevel_26();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Levels_t748035019 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		int32_t L_4 = Levels_get_NrLevel_m2233528047(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) > ((int32_t)5)))
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_5 = __this->get_correctAnswer_27();
		LevelsU5BU5D_t2050899114* L_6 = __this->get_levelGlobal_25();
		int32_t L_7 = __this->get_positionLevel_26();
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Levels_t748035019 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		int32_t L_10 = Levels_get_NrLevel_m2233528047(L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		String_t* L_11 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		int32_t L_12 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)L_12)))
		{
			goto IL_0089;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_13 = __this->get_levelGlobal_25();
		int32_t L_14 = __this->get_positionLevel_26();
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Levels_t748035019 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		int32_t L_17 = Levels_get_NrLevel_m2233528047(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		String_t* L_18 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		int32_t L_19 = __this->get_correctAnswer_27();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		Il2CppObject * L_20 = __this->get_callBack_14();
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		Il2CppObject * L_21 = __this->get_callBack_14();
		NullCheck(L_21);
		InterfaceActionInvoker0::Invoke(0 /* System.Void QuestionController/UpdateListener::onUpdateAdapter() */, UpdateListener_t3189012468_il2cpp_TypeInfo_var, L_21);
	}

IL_0089:
	{
		__this->set_correctAnswer_27(0);
		__this->set_numQuest_24(0);
		LevelsU5BU5D_t2050899114* L_22 = __this->get_levelGlobal_25();
		int32_t L_23 = __this->get_positionLevel_26();
		NullCheck(L_22);
		int32_t L_24 = L_23;
		Levels_t748035019 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		bool L_26 = Levels_get_IsLocked_m626983591(L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_0104;
		}
	}
	{
		int32_t L_27 = __this->get_positionLevel_26();
		if ((((int32_t)L_27) > ((int32_t)((int32_t)39))))
		{
			goto IL_0104;
		}
	}
	{
		int32_t L_28 = __this->get_positionLevel_26();
		__this->set_positionLevel_26(((int32_t)((int32_t)L_28+(int32_t)1)));
		LevelsU5BU5D_t2050899114* L_29 = __this->get_levelGlobal_25();
		int32_t L_30 = __this->get_positionLevel_26();
		NullCheck(L_29);
		int32_t L_31 = L_30;
		Levels_t748035019 * L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		List_1_t2297069972 * L_33 = Levels_get_Quests_m1321190565(L_32, /*hidden argument*/NULL);
		int32_t L_34 = __this->get_numQuest_24();
		NullCheck(L_33);
		Question_t2927948840 * L_35 = List_1_get_Item_m4010202484(L_33, L_34, /*hidden argument*/List_1_get_Item_m4010202484_MethodInfo_var);
		QuestionController_SetQuestion_m3640094679(__this, L_35, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_36 = __this->get_singleButtonPanel_19();
		NullCheck(L_36);
		GameObject_SetActive_m2887581199(L_36, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_37 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		GameObject_SetActive_m2887581199(L_37, (bool)1, /*hidden argument*/NULL);
	}

IL_0104:
	{
		goto IL_021a;
	}

IL_0109:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchase_t1644043085_il2cpp_TypeInfo_var);
		bool L_38 = ((Purchase_t1644043085_StaticFields*)Purchase_t1644043085_il2cpp_TypeInfo_var->static_fields)->get_isPurchased_2();
		if (!L_38)
		{
			goto IL_0204;
		}
	}
	{
		int32_t L_39 = __this->get_correctAnswer_27();
		LevelsU5BU5D_t2050899114* L_40 = __this->get_levelGlobal_25();
		int32_t L_41 = __this->get_positionLevel_26();
		NullCheck(L_40);
		int32_t L_42 = L_41;
		Levels_t748035019 * L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_43);
		int32_t L_44 = Levels_get_NrLevel_m2233528047(L_43, /*hidden argument*/NULL);
		V_2 = L_44;
		String_t* L_45 = Int32_ToString_m2960866144((&V_2), /*hidden argument*/NULL);
		int32_t L_46 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		if ((((int32_t)L_39) <= ((int32_t)L_46)))
		{
			goto IL_0184;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_47 = __this->get_levelGlobal_25();
		int32_t L_48 = __this->get_positionLevel_26();
		NullCheck(L_47);
		int32_t L_49 = L_48;
		Levels_t748035019 * L_50 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		NullCheck(L_50);
		int32_t L_51 = Levels_get_NrLevel_m2233528047(L_50, /*hidden argument*/NULL);
		V_3 = L_51;
		String_t* L_52 = Int32_ToString_m2960866144((&V_3), /*hidden argument*/NULL);
		int32_t L_53 = __this->get_correctAnswer_27();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		Il2CppObject * L_54 = __this->get_callBack_14();
		if (!L_54)
		{
			goto IL_0184;
		}
	}
	{
		Il2CppObject * L_55 = __this->get_callBack_14();
		NullCheck(L_55);
		InterfaceActionInvoker0::Invoke(0 /* System.Void QuestionController/UpdateListener::onUpdateAdapter() */, UpdateListener_t3189012468_il2cpp_TypeInfo_var, L_55);
	}

IL_0184:
	{
		__this->set_correctAnswer_27(0);
		__this->set_numQuest_24(0);
		LevelsU5BU5D_t2050899114* L_56 = __this->get_levelGlobal_25();
		int32_t L_57 = __this->get_positionLevel_26();
		NullCheck(L_56);
		int32_t L_58 = L_57;
		Levels_t748035019 * L_59 = (L_56)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		NullCheck(L_59);
		bool L_60 = Levels_get_IsLocked_m626983591(L_59, /*hidden argument*/NULL);
		if (L_60)
		{
			goto IL_01ff;
		}
	}
	{
		int32_t L_61 = __this->get_positionLevel_26();
		if ((((int32_t)L_61) > ((int32_t)((int32_t)39))))
		{
			goto IL_01ff;
		}
	}
	{
		int32_t L_62 = __this->get_positionLevel_26();
		__this->set_positionLevel_26(((int32_t)((int32_t)L_62+(int32_t)1)));
		LevelsU5BU5D_t2050899114* L_63 = __this->get_levelGlobal_25();
		int32_t L_64 = __this->get_positionLevel_26();
		NullCheck(L_63);
		int32_t L_65 = L_64;
		Levels_t748035019 * L_66 = (L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		NullCheck(L_66);
		List_1_t2297069972 * L_67 = Levels_get_Quests_m1321190565(L_66, /*hidden argument*/NULL);
		int32_t L_68 = __this->get_numQuest_24();
		NullCheck(L_67);
		Question_t2927948840 * L_69 = List_1_get_Item_m4010202484(L_67, L_68, /*hidden argument*/List_1_get_Item_m4010202484_MethodInfo_var);
		QuestionController_SetQuestion_m3640094679(__this, L_69, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_70 = __this->get_singleButtonPanel_19();
		NullCheck(L_70);
		GameObject_SetActive_m2887581199(L_70, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_71 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_71);
		GameObject_SetActive_m2887581199(L_71, (bool)1, /*hidden argument*/NULL);
	}

IL_01ff:
	{
		goto IL_021a;
	}

IL_0204:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3833502378, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_72 = __this->get_buyPanel_16();
		NullCheck(L_72);
		GameObject_SetActive_m2887581199(L_72, (bool)1, /*hidden argument*/NULL);
	}

IL_021a:
	{
		return;
	}
}
// System.Void QuestionController::RetryLevel(UnityEngine.GameObject)
extern Il2CppClass* UpdateListener_t3189012468_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m4010202484_MethodInfo_var;
extern const uint32_t QuestionController_RetryLevel_m2188939663_MetadataUsageId;
extern "C"  void QuestionController_RetryLevel_m2188939663 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestionController_RetryLevel_m2188939663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_correctAnswer_27();
		LevelsU5BU5D_t2050899114* L_1 = __this->get_levelGlobal_25();
		int32_t L_2 = __this->get_positionLevel_26();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Levels_t748035019 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		int32_t L_5 = Levels_get_NrLevel_m2233528047(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		int32_t L_7 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_7)))
		{
			goto IL_0071;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_8 = __this->get_levelGlobal_25();
		int32_t L_9 = __this->get_positionLevel_26();
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Levels_t748035019 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		int32_t L_12 = Levels_get_NrLevel_m2233528047(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		String_t* L_13 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		int32_t L_14 = __this->get_correctAnswer_27();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Il2CppObject * L_15 = __this->get_callBack_14();
		if (!L_15)
		{
			goto IL_0071;
		}
	}
	{
		Il2CppObject * L_16 = __this->get_callBack_14();
		NullCheck(L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void QuestionController/UpdateListener::onUpdateAdapter() */, UpdateListener_t3189012468_il2cpp_TypeInfo_var, L_16);
	}

IL_0071:
	{
		GameObject_t1756533147 * L_17 = ___gamePanel0;
		NullCheck(L_17);
		GameObject_SetActive_m2887581199(L_17, (bool)0, /*hidden argument*/NULL);
		__this->set_correctAnswer_27(0);
		__this->set_numQuest_24(0);
		LevelsU5BU5D_t2050899114* L_18 = __this->get_levelGlobal_25();
		int32_t L_19 = __this->get_positionLevel_26();
		NullCheck(L_18);
		int32_t L_20 = L_19;
		Levels_t748035019 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		bool L_22 = Levels_get_IsLocked_m626983591(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00e5;
		}
	}
	{
		int32_t L_23 = __this->get_positionLevel_26();
		if ((((int32_t)L_23) > ((int32_t)((int32_t)39))))
		{
			goto IL_00e5;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_24 = __this->get_levelGlobal_25();
		int32_t L_25 = __this->get_positionLevel_26();
		NullCheck(L_24);
		int32_t L_26 = L_25;
		Levels_t748035019 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		List_1_t2297069972 * L_28 = Levels_get_Quests_m1321190565(L_27, /*hidden argument*/NULL);
		int32_t L_29 = __this->get_numQuest_24();
		NullCheck(L_28);
		Question_t2927948840 * L_30 = List_1_get_Item_m4010202484(L_28, L_29, /*hidden argument*/List_1_get_Item_m4010202484_MethodInfo_var);
		QuestionController_SetQuestion_m3640094679(__this, L_30, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_31 = __this->get_singleButtonPanel_19();
		NullCheck(L_31);
		GameObject_SetActive_m2887581199(L_31, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_32 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		GameObject_SetActive_m2887581199(L_32, (bool)1, /*hidden argument*/NULL);
	}

IL_00e5:
	{
		GameObject_t1756533147 * L_33 = __this->get_finalAlertPanel_20();
		NullCheck(L_33);
		GameObject_SetActive_m2887581199(L_33, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuestionController::BackButton(UnityEngine.GameObject)
extern Il2CppClass* UpdateListener_t3189012468_il2cpp_TypeInfo_var;
extern const uint32_t QuestionController_BackButton_m2933698246_MetadataUsageId;
extern "C"  void QuestionController_BackButton_m2933698246 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestionController_BackButton_m2933698246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_correctAnswer_27();
		LevelsU5BU5D_t2050899114* L_1 = __this->get_levelGlobal_25();
		int32_t L_2 = __this->get_positionLevel_26();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Levels_t748035019 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		int32_t L_5 = Levels_get_NrLevel_m2233528047(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		int32_t L_7 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_7)))
		{
			goto IL_0078;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_8 = __this->get_levelGlobal_25();
		int32_t L_9 = __this->get_positionLevel_26();
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Levels_t748035019 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		int32_t L_12 = Levels_get_NrLevel_m2233528047(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		String_t* L_13 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		int32_t L_14 = __this->get_correctAnswer_27();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Il2CppObject * L_15 = __this->get_callBack_14();
		if (!L_15)
		{
			goto IL_0071;
		}
	}
	{
		Il2CppObject * L_16 = __this->get_callBack_14();
		NullCheck(L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void QuestionController/UpdateListener::onUpdateAdapter() */, UpdateListener_t3189012468_il2cpp_TypeInfo_var, L_16);
	}

IL_0071:
	{
		GameObject_t1756533147 * L_17 = ___gamePanel0;
		NullCheck(L_17);
		GameObject_SetActive_m2887581199(L_17, (bool)0, /*hidden argument*/NULL);
	}

IL_0078:
	{
		__this->set_correctAnswer_27(0);
		GameObject_t1756533147 * L_18 = ___gamePanel0;
		NullCheck(L_18);
		GameObject_SetActive_m2887581199(L_18, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuestionController::ClosePopup(UnityEngine.GameObject)
extern "C"  void QuestionController_ClosePopup_m4255358609 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method)
{
	{
		SoundController_t1686593041 * L_0 = __this->get_soundControll_29();
		NullCheck(L_0);
		SoundController_CloseWindow_m1597160790(L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_GamePanel_17();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = ___gamePanel0;
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuestionController::GoHome(UnityEngine.GameObject)
extern Il2CppClass* UpdateListener_t3189012468_il2cpp_TypeInfo_var;
extern const uint32_t QuestionController_GoHome_m7599600_MetadataUsageId;
extern "C"  void QuestionController_GoHome_m7599600 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestionController_GoHome_m7599600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_correctAnswer_27();
		LevelsU5BU5D_t2050899114* L_1 = __this->get_levelGlobal_25();
		int32_t L_2 = __this->get_positionLevel_26();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Levels_t748035019 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		int32_t L_5 = Levels_get_NrLevel_m2233528047(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		int32_t L_7 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_7)))
		{
			goto IL_0071;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_8 = __this->get_levelGlobal_25();
		int32_t L_9 = __this->get_positionLevel_26();
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Levels_t748035019 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		int32_t L_12 = Levels_get_NrLevel_m2233528047(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		String_t* L_13 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		int32_t L_14 = __this->get_correctAnswer_27();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Il2CppObject * L_15 = __this->get_callBack_14();
		if (!L_15)
		{
			goto IL_0071;
		}
	}
	{
		Il2CppObject * L_16 = __this->get_callBack_14();
		NullCheck(L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void QuestionController/UpdateListener::onUpdateAdapter() */, UpdateListener_t3189012468_il2cpp_TypeInfo_var, L_16);
	}

IL_0071:
	{
		GameObject_t1756533147 * L_17 = ___go0;
		NullCheck(L_17);
		GameObject_SetActive_m2887581199(L_17, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = __this->get_GamePanel_17();
		NullCheck(L_18);
		GameObject_SetActive_m2887581199(L_18, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuestionController::registerListener(QuestionController/UpdateListener)
extern "C"  void QuestionController_registerListener_m712129072 (QuestionController_t445239244 * __this, Il2CppObject * ___callback0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___callback0;
		__this->set_callBack_14(L_0);
		return;
	}
}
// System.Void QuestionController::RefreshQuestionController()
extern Il2CppClass* UpdateListener_t3189012468_il2cpp_TypeInfo_var;
extern const uint32_t QuestionController_RefreshQuestionController_m1860514236_MetadataUsageId;
extern "C"  void QuestionController_RefreshQuestionController_m1860514236 (QuestionController_t445239244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuestionController_RefreshQuestionController_m1860514236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_correctAnswer_27();
		LevelsU5BU5D_t2050899114* L_1 = __this->get_levelGlobal_25();
		int32_t L_2 = __this->get_positionLevel_26();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Levels_t748035019 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		int32_t L_5 = Levels_get_NrLevel_m2233528047(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		int32_t L_7 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_7)))
		{
			goto IL_0071;
		}
	}
	{
		LevelsU5BU5D_t2050899114* L_8 = __this->get_levelGlobal_25();
		int32_t L_9 = __this->get_positionLevel_26();
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Levels_t748035019 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		int32_t L_12 = Levels_get_NrLevel_m2233528047(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		String_t* L_13 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		int32_t L_14 = __this->get_correctAnswer_27();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Il2CppObject * L_15 = __this->get_callBack_14();
		if (!L_15)
		{
			goto IL_0071;
		}
	}
	{
		Il2CppObject * L_16 = __this->get_callBack_14();
		NullCheck(L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void QuestionController/UpdateListener::onUpdateAdapter() */, UpdateListener_t3189012468_il2cpp_TypeInfo_var, L_16);
	}

IL_0071:
	{
		return;
	}
}
// System.Void readHTML::.ctor()
extern Il2CppCodeGenString* _stringLiteral1820665699;
extern Il2CppCodeGenString* _stringLiteral711387765;
extern Il2CppCodeGenString* _stringLiteral3871804602;
extern const uint32_t readHTML__ctor_m2799608704_MetadataUsageId;
extern "C"  void readHTML__ctor_m2799608704 (readHTML_t503794377 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (readHTML__ctor_m2799608704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_path_terms_4(_stringLiteral1820665699);
		__this->set_path_subscription_5(_stringLiteral711387765);
		__this->set_path_privacy_6(_stringLiteral3871804602);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void readHTML::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern const uint32_t readHTML_Start_m3205736136_MetadataUsageId;
extern "C"  void readHTML_Start_m3205736136 (readHTML_t503794377 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (readHTML_Start_m3205736136_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GUIStyle_t1799908754 * V_0 = NULL;
	{
		String_t* L_0 = Application_get_dataPath_m371940330(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = __this->get_path_subscription_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_3 = File_ReadAllText_m1018286608(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_readTextFileSubscription_8(L_3);
		String_t* L_4 = Application_get_dataPath_m371940330(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_path_terms_4();
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		String_t* L_7 = File_ReadAllText_m1018286608(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_readTextFileTerms_9(L_7);
		String_t* L_8 = Application_get_dataPath_m371940330(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_9 = __this->get_path_privacy_6();
		String_t* L_10 = String_Concat_m2596409543(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		String_t* L_11 = File_ReadAllText_m1018286608(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->set_readTextFilePrivacy_10(L_11);
		GUIStyle_t1799908754 * L_12 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_12, /*hidden argument*/NULL);
		V_0 = L_12;
		GUIStyle_t1799908754 * L_13 = V_0;
		NullCheck(L_13);
		GUIStyle_set_richText_m1853532836(L_13, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_14 = __this->get_contentURL_2();
		String_t* L_15 = __this->get_readTextFileSubscription_8();
		NullCheck(L_14);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, L_15);
		return;
	}
}
// System.Void readHTML::EnableLegasyInfo(UnityEngine.GameObject)
extern Il2CppClass* Execute_t1389284496_il2cpp_TypeInfo_var;
extern const MethodInfo* readHTML_ActiveBtn_m2177739882_MethodInfo_var;
extern const uint32_t readHTML_EnableLegasyInfo_m2020288232_MetadataUsageId;
extern "C"  void readHTML_EnableLegasyInfo_m2020288232 (readHTML_t503794377 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (readHTML_EnableLegasyInfo_m2020288232_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Button_t2872111280 * L_0 = __this->get_btn_1_14();
		NullCheck(L_0);
		Selectable_set_interactable_m63718297(L_0, (bool)0, /*hidden argument*/NULL);
		SoundController_t1686593041 * L_1 = __this->get_soundControll_15();
		NullCheck(L_1);
		SoundController_OpenWindow_m1923503136(L_1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = ___go0;
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_backButton_3();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)readHTML_ActiveBtn_m2177739882_MethodInfo_var);
		Execute_t1389284496 * L_5 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_5, __this, L_4, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (1.5f), L_5, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void readHTML::StartAnimation(System.Int32)
extern Il2CppClass* Execute_t1389284496_il2cpp_TypeInfo_var;
extern const MethodInfo* readHTML_SelectSubscription_m442030927_MethodInfo_var;
extern const MethodInfo* readHTML_SelectPrivacy_m996667200_MethodInfo_var;
extern const MethodInfo* readHTML_SelectTerms_m2844936593_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2081139971;
extern const uint32_t readHTML_StartAnimation_m4241922343_MetadataUsageId;
extern "C"  void readHTML_StartAnimation_m4241922343 (readHTML_t503794377 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (readHTML_StartAnimation_m4241922343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___i0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0040;
		}
	}
	{
		Button_t2872111280 * L_1 = __this->get_SubscriptionBtn_11();
		NullCheck(L_1);
		Selectable_set_interactable_m63718297(L_1, (bool)0, /*hidden argument*/NULL);
		Animator_t69676727 * L_2 = __this->get_anim_7();
		NullCheck(L_2);
		Animator_SetTrigger_m3418492570(L_2, _stringLiteral2081139971, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)readHTML_SelectSubscription_m442030927_MethodInfo_var);
		Execute_t1389284496 * L_4 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_4, __this, L_3, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (0.3f), L_4, __this, /*hidden argument*/NULL);
		goto IL_00bb;
	}

IL_0040:
	{
		int32_t L_5 = ___i0;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0080;
		}
	}
	{
		Button_t2872111280 * L_6 = __this->get_privacyBtn_12();
		NullCheck(L_6);
		Selectable_set_interactable_m63718297(L_6, (bool)0, /*hidden argument*/NULL);
		Animator_t69676727 * L_7 = __this->get_anim_7();
		NullCheck(L_7);
		Animator_SetTrigger_m3418492570(L_7, _stringLiteral2081139971, /*hidden argument*/NULL);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)readHTML_SelectPrivacy_m996667200_MethodInfo_var);
		Execute_t1389284496 * L_9 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_9, __this, L_8, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (0.3f), L_9, __this, /*hidden argument*/NULL);
		goto IL_00bb;
	}

IL_0080:
	{
		int32_t L_10 = ___i0;
		if ((!(((uint32_t)L_10) == ((uint32_t)3))))
		{
			goto IL_00bb;
		}
	}
	{
		Button_t2872111280 * L_11 = __this->get_termsBtn_13();
		NullCheck(L_11);
		Selectable_set_interactable_m63718297(L_11, (bool)0, /*hidden argument*/NULL);
		Animator_t69676727 * L_12 = __this->get_anim_7();
		NullCheck(L_12);
		Animator_SetTrigger_m3418492570(L_12, _stringLiteral2081139971, /*hidden argument*/NULL);
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)readHTML_SelectTerms_m2844936593_MethodInfo_var);
		Execute_t1389284496 * L_14 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_14, __this, L_13, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (0.3f), L_14, __this, /*hidden argument*/NULL);
	}

IL_00bb:
	{
		return;
	}
}
// System.Void readHTML::SelectSubscription()
extern "C"  void readHTML_SelectSubscription_m442030927 (readHTML_t503794377 * __this, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_contentURL_2();
		String_t* L_1 = __this->get_readTextFileSubscription_8();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		Button_t2872111280 * L_2 = __this->get_SubscriptionBtn_11();
		NullCheck(L_2);
		Selectable_set_interactable_m63718297(L_2, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void readHTML::SelectPrivacy()
extern "C"  void readHTML_SelectPrivacy_m996667200 (readHTML_t503794377 * __this, const MethodInfo* method)
{
	{
		Button_t2872111280 * L_0 = __this->get_privacyBtn_12();
		NullCheck(L_0);
		Selectable_set_interactable_m63718297(L_0, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_1 = __this->get_contentURL_2();
		String_t* L_2 = __this->get_readTextFilePrivacy_10();
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		return;
	}
}
// System.Void readHTML::SelectTerms()
extern "C"  void readHTML_SelectTerms_m2844936593 (readHTML_t503794377 * __this, const MethodInfo* method)
{
	{
		Button_t2872111280 * L_0 = __this->get_termsBtn_13();
		NullCheck(L_0);
		Selectable_set_interactable_m63718297(L_0, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_1 = __this->get_contentURL_2();
		String_t* L_2 = __this->get_readTextFileTerms_9();
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		return;
	}
}
// System.Void readHTML::ActiveBtn()
extern "C"  void readHTML_ActiveBtn_m2177739882 (readHTML_t503794377 * __this, const MethodInfo* method)
{
	{
		Button_t2872111280 * L_0 = __this->get_btn_1_14();
		NullCheck(L_0);
		Selectable_set_interactable_m63718297(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Root::.ctor()
extern "C"  void Root__ctor_m3913752709 (Root_t2702590648 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Question[] Root::get_questions()
extern "C"  QuestionU5BU5D_t1028533817* Root_get_questions_m2291853548 (Root_t2702590648 * __this, const MethodInfo* method)
{
	{
		QuestionU5BU5D_t1028533817* L_0 = __this->get_questions_0();
		return L_0;
	}
}
// System.Void Root::set_question(Question[])
extern "C"  void Root_set_question_m1612369518 (Root_t2702590648 * __this, QuestionU5BU5D_t1028533817* ___items0, const MethodInfo* method)
{
	{
		QuestionU5BU5D_t1028533817* L_0 = ___items0;
		__this->set_questions_0(L_0);
		return;
	}
}
// System.Void ScreenOrientation::.ctor()
extern "C"  void ScreenOrientation__ctor_m1408293085 (ScreenOrientation_t2584746402 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScreenOrientation::Start()
extern "C"  void ScreenOrientation_Start_m3374179005 (ScreenOrientation_t2584746402 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ScreenOrientation::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t ScreenOrientation_Update_m3072168224_MetadataUsageId;
extern "C"  void ScreenOrientation_Update_m3072168224 (ScreenOrientation_t2584746402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScreenOrientation_Update_m3072168224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_deviceOrientation_m2415424840(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0028;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_landscapeStatsPanel_2();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_PortraitStatsPanel_3();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_3 = Input_get_deviceOrientation_m2415424840(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)3)))
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_4 = Input_get_deviceOrientation_m2415424840(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)4))))
		{
			goto IL_0056;
		}
	}

IL_003e:
	{
		GameObject_t1756533147 * L_5 = __this->get_PortraitStatsPanel_3();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = __this->get_landscapeStatsPanel_2();
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)1, /*hidden argument*/NULL);
	}

IL_0056:
	{
		int32_t L_7 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)L_8)))
		{
			goto IL_0082;
		}
	}
	{
		GameObject_t1756533147 * L_9 = __this->get_PortraitStatsPanel_3();
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = __this->get_landscapeStatsPanel_2();
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)1, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0082:
	{
		GameObject_t1756533147 * L_11 = __this->get_landscapeStatsPanel_2();
		NullCheck(L_11);
		GameObject_SetActive_m2887581199(L_11, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = __this->get_PortraitStatsPanel_3();
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)1, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void SoundController::.ctor()
extern "C"  void SoundController__ctor_m1375591970 (SoundController_t1686593041 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundController::Start()
extern "C"  void SoundController_Start_m2439355714 (SoundController_t1686593041 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_backgroundSound_10();
		NullCheck(L_0);
		AudioSource_Play_m353744792(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundController::Update()
extern "C"  void SoundController_Update_m1015243901 (SoundController_t1686593041 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SoundController::OnSound()
extern "C"  void SoundController_OnSound_m4103739782 (SoundController_t1686593041 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_soundControll_9();
		AudioClip_t1932558630 * L_1 = __this->get_correctSound_2();
		NullCheck(L_0);
		AudioSource_set_clip_m738814682(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_2 = __this->get_soundControll_9();
		NullCheck(L_2);
		AudioSource_set_loop_m313035616(L_2, (bool)0, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_3 = __this->get_soundControll_9();
		NullCheck(L_3);
		AudioSource_Play_m353744792(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundController::OffSound()
extern "C"  void SoundController_OffSound_m3094588068 (SoundController_t1686593041 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_soundControll_9();
		AudioClip_t1932558630 * L_1 = __this->get_incorrectSound_3();
		NullCheck(L_0);
		AudioSource_set_clip_m738814682(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_2 = __this->get_soundControll_9();
		NullCheck(L_2);
		AudioSource_set_loop_m313035616(L_2, (bool)0, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_3 = __this->get_soundControll_9();
		NullCheck(L_3);
		AudioSource_Play_m353744792(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundController::FailMission()
extern "C"  void SoundController_FailMission_m1630072376 (SoundController_t1686593041 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_soundControll_9();
		AudioClip_t1932558630 * L_1 = __this->get_failMission_5();
		NullCheck(L_0);
		AudioSource_set_clip_m738814682(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_2 = __this->get_soundControll_9();
		NullCheck(L_2);
		AudioSource_set_loop_m313035616(L_2, (bool)0, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_3 = __this->get_soundControll_9();
		NullCheck(L_3);
		AudioSource_Play_m353744792(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundController::CloseWindow()
extern "C"  void SoundController_CloseWindow_m1597160790 (SoundController_t1686593041 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_soundControll_9();
		AudioClip_t1932558630 * L_1 = __this->get_closeWindow_4();
		NullCheck(L_0);
		AudioSource_set_clip_m738814682(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_2 = __this->get_soundControll_9();
		NullCheck(L_2);
		AudioSource_set_loop_m313035616(L_2, (bool)0, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_3 = __this->get_soundControll_9();
		NullCheck(L_3);
		AudioSource_Play_m353744792(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundController::OpenWindow()
extern "C"  void SoundController_OpenWindow_m1923503136 (SoundController_t1686593041 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_soundControll_9();
		AudioClip_t1932558630 * L_1 = __this->get_menuSong_6();
		NullCheck(L_0);
		AudioSource_set_clip_m738814682(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_2 = __this->get_soundControll_9();
		NullCheck(L_2);
		AudioSource_set_loop_m313035616(L_2, (bool)0, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_3 = __this->get_soundControll_9();
		NullCheck(L_3);
		AudioSource_Play_m353744792(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundController::SuccesMission()
extern "C"  void SoundController_SuccesMission_m3861769026 (SoundController_t1686593041 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_soundControll_9();
		AudioClip_t1932558630 * L_1 = __this->get_succesMission_7();
		NullCheck(L_0);
		AudioSource_set_clip_m738814682(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_2 = __this->get_soundControll_9();
		NullCheck(L_2);
		AudioSource_set_loop_m313035616(L_2, (bool)0, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_3 = __this->get_soundControll_9();
		NullCheck(L_3);
		AudioSource_Play_m353744792(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundController::soundSettings()
extern "C"  void SoundController_soundSettings_m608092896 (SoundController_t1686593041 * __this, const MethodInfo* method)
{
	{
		SoundController_OpenWindow_m1923503136(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = __this->get_settingsMenu_8();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundController::DisableEffectSound(UnityEngine.GameObject)
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern const uint32_t SoundController_DisableEffectSound_m2674684258_MetadataUsageId;
extern "C"  void SoundController_DisableEffectSound_m2674684258 (SoundController_t1686593041 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundController_DisableEffectSound_m2674684258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Image_t2042527209 * V_0 = NULL;
	{
		AudioSource_t1135106623 * L_0 = __this->get_soundControll_9();
		AudioSource_t1135106623 * L_1 = __this->get_soundControll_9();
		NullCheck(L_1);
		bool L_2 = Behaviour_get_enabled_m4079055610(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Behaviour_set_enabled_m1796096907(L_0, (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		SoundController_CloseWindow_m1597160790(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = ___go0;
		NullCheck(L_3);
		Image_t2042527209 * L_4 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		V_0 = L_4;
		AudioSource_t1135106623 * L_5 = __this->get_soundControll_9();
		NullCheck(L_5);
		bool L_6 = Behaviour_get_enabled_m4079055610(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0047;
		}
	}
	{
		Image_t2042527209 * L_7 = V_0;
		Sprite_t309593783 * L_8 = __this->get_onButton_11();
		NullCheck(L_7);
		Image_set_sprite_m1800056820(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0053;
	}

IL_0047:
	{
		Image_t2042527209 * L_9 = V_0;
		Sprite_t309593783 * L_10 = __this->get_offButton_12();
		NullCheck(L_9);
		Image_set_sprite_m1800056820(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void SoundController::DisableBackgroundSound(UnityEngine.GameObject)
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern const uint32_t SoundController_DisableBackgroundSound_m178036229_MetadataUsageId;
extern "C"  void SoundController_DisableBackgroundSound_m178036229 (SoundController_t1686593041 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundController_DisableBackgroundSound_m178036229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Image_t2042527209 * V_0 = NULL;
	{
		AudioSource_t1135106623 * L_0 = __this->get_backgroundSound_10();
		AudioSource_t1135106623 * L_1 = __this->get_backgroundSound_10();
		NullCheck(L_1);
		bool L_2 = Behaviour_get_enabled_m4079055610(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Behaviour_set_enabled_m1796096907(L_0, (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		SoundController_CloseWindow_m1597160790(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = ___go0;
		NullCheck(L_3);
		Image_t2042527209 * L_4 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		V_0 = L_4;
		AudioSource_t1135106623 * L_5 = __this->get_backgroundSound_10();
		NullCheck(L_5);
		bool L_6 = Behaviour_get_enabled_m4079055610(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0047;
		}
	}
	{
		Image_t2042527209 * L_7 = V_0;
		Sprite_t309593783 * L_8 = __this->get_onButton_11();
		NullCheck(L_7);
		Image_set_sprite_m1800056820(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0053;
	}

IL_0047:
	{
		Image_t2042527209 * L_9 = V_0;
		Sprite_t309593783 * L_10 = __this->get_offButton_12();
		NullCheck(L_9);
		Image_set_sprite_m1800056820(L_9, L_10, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void Stats::.ctor()
extern "C"  void Stats__ctor_m3441957910 (Stats_t967880071 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stats::StoreMenu()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Execute_t1389284496_il2cpp_TypeInfo_var;
extern const MethodInfo* Stats_PortretEnable_m2884510581_MethodInfo_var;
extern const MethodInfo* Stats_LanscapeEnable_m3462121364_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3083327535;
extern Il2CppCodeGenString* _stringLiteral1389210506;
extern Il2CppCodeGenString* _stringLiteral1119447084;
extern const uint32_t Stats_StoreMenu_m3465421044_MetadataUsageId;
extern "C"  void Stats_StoreMenu_m3465421044 (Stats_t967880071 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stats_StoreMenu_m3465421044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		SoundController_t1686593041 * L_0 = __this->get_soundController_12();
		NullCheck(L_0);
		SoundController_OpenWindow_m1923503136(L_0, /*hidden argument*/NULL);
		String_t* L_1 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, _stringLiteral3083327535, /*hidden argument*/NULL);
		__this->set_pathImageStats_16(L_1);
		String_t* L_2 = __this->get_pathImageStats_16();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		String_t* L_5 = __this->get_pathImageStats_16();
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1606060069(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0067;
		}
	}

IL_0040:
	{
		Image_t2042527209 * L_7 = __this->get_imageStats_17();
		Sprite_t309593783 * L_8 = __this->get_defaultImage_19();
		NullCheck(L_7);
		Image_set_sprite_m1800056820(L_7, L_8, /*hidden argument*/NULL);
		Image_t2042527209 * L_9 = __this->get_imagePortraitStats_18();
		Sprite_t309593783 * L_10 = __this->get_defaultImage_19();
		NullCheck(L_9);
		Image_set_sprite_m1800056820(L_9, L_10, /*hidden argument*/NULL);
		goto IL_0099;
	}

IL_0067:
	{
		String_t* L_11 = __this->get_pathImageStats_16();
		Image_t2042527209 * L_12 = __this->get_imageStats_17();
		Il2CppObject * L_13 = Stats_LoadImageStats_m535395134(__this, L_11, L_12, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_13, /*hidden argument*/NULL);
		String_t* L_14 = __this->get_pathImageStats_16();
		Image_t2042527209 * L_15 = __this->get_imagePortraitStats_18();
		Il2CppObject * L_16 = Stats_LoadImageStats_m535395134(__this, L_14, L_15, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_16, /*hidden argument*/NULL);
	}

IL_0099:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_17 = Input_get_deviceOrientation_m2415424840(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)1))))
		{
			goto IL_00cd;
		}
	}
	{
		GameObject_t1756533147 * L_18 = __this->get_landscapeStatsPanel_10();
		NullCheck(L_18);
		GameObject_SetActive_m2887581199(L_18, (bool)0, /*hidden argument*/NULL);
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)(void*)Stats_PortretEnable_m2884510581_MethodInfo_var);
		Execute_t1389284496 * L_20 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_20, __this, L_19, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (1.0f), L_20, __this, /*hidden argument*/NULL);
		goto IL_00f1;
	}

IL_00cd:
	{
		GameObject_t1756533147 * L_21 = __this->get_PortraitStatsPanel_11();
		NullCheck(L_21);
		GameObject_SetActive_m2887581199(L_21, (bool)0, /*hidden argument*/NULL);
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)Stats_LanscapeEnable_m3462121364_MethodInfo_var);
		Execute_t1389284496 * L_23 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_23, __this, L_22, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (1.0f), L_23, __this, /*hidden argument*/NULL);
	}

IL_00f1:
	{
		int32_t L_24 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_25 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_24) <= ((int32_t)L_25)))
		{
			goto IL_0129;
		}
	}
	{
		GameObject_t1756533147 * L_26 = __this->get_PortraitStatsPanel_11();
		NullCheck(L_26);
		GameObject_SetActive_m2887581199(L_26, (bool)0, /*hidden argument*/NULL);
		IntPtr_t L_27;
		L_27.set_m_value_0((void*)(void*)Stats_LanscapeEnable_m3462121364_MethodInfo_var);
		Execute_t1389284496 * L_28 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_28, __this, L_27, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (1.0f), L_28, __this, /*hidden argument*/NULL);
		goto IL_014d;
	}

IL_0129:
	{
		GameObject_t1756533147 * L_29 = __this->get_landscapeStatsPanel_10();
		NullCheck(L_29);
		GameObject_SetActive_m2887581199(L_29, (bool)0, /*hidden argument*/NULL);
		IntPtr_t L_30;
		L_30.set_m_value_0((void*)(void*)Stats_PortretEnable_m2884510581_MethodInfo_var);
		Execute_t1389284496 * L_31 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_31, __this, L_30, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (1.0f), L_31, __this, /*hidden argument*/NULL);
	}

IL_014d:
	{
		MainGame_t3800664731 * L_32 = __this->get_mainGame_13();
		NullCheck(L_32);
		MainGame_InactiveButtons_m1486052864(L_32, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_33 = __this->get_backButton_3();
		NullCheck(L_33);
		GameObject_SetActive_m2887581199(L_33, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_34 = __this->get_statsPanel_2();
		NullCheck(L_34);
		GameObject_SetActive_m2887581199(L_34, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_35 = __this->get_totalFollowers_5();
		int32_t L_36 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral1389210506, 0, /*hidden argument*/NULL);
		V_0 = L_36;
		String_t* L_37 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		NullCheck(L_35);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_35, L_37);
		Text_t356221433 * L_38 = __this->get_totalLikes_4();
		int32_t L_39 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral1119447084, 0, /*hidden argument*/NULL);
		V_1 = L_39;
		String_t* L_40 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		NullCheck(L_38);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_38, L_40);
		Text_t356221433 * L_41 = __this->get_totalLikesPortret_6();
		int32_t L_42 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral1389210506, 0, /*hidden argument*/NULL);
		V_2 = L_42;
		String_t* L_43 = Int32_ToString_m2960866144((&V_2), /*hidden argument*/NULL);
		NullCheck(L_41);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_41, L_43);
		Text_t356221433 * L_44 = __this->get_totalFollowersPortret_7();
		int32_t L_45 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral1119447084, 0, /*hidden argument*/NULL);
		V_3 = L_45;
		String_t* L_46 = Int32_ToString_m2960866144((&V_3), /*hidden argument*/NULL);
		NullCheck(L_44);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_44, L_46);
		return;
	}
}
// System.Void Stats::activeAlert(UnityEngine.GameObject)
extern "C"  void Stats_activeAlert_m406229684 (Stats_t967880071 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		SoundController_t1686593041 * L_1 = __this->get_soundController_12();
		NullCheck(L_1);
		SoundController_OpenWindow_m1923503136(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stats::CloseAlert(UnityEngine.GameObject)
extern "C"  void Stats_CloseAlert_m3751967524 (Stats_t967880071 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		SoundController_t1686593041 * L_1 = __this->get_soundController_12();
		NullCheck(L_1);
		SoundController_CloseWindow_m1597160790(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stats::BackButtStats(UnityEngine.GameObject)
extern Il2CppClass* Execute_t1389284496_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteWithGameObjcet_t1191175011_il2cpp_TypeInfo_var;
extern const MethodInfo* Stats_onBackButt_m2446812067_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* MainGame_ActiveButtons_m3331183351_MethodInfo_var;
extern const MethodInfo* SoundController_CloseWindow_m1597160790_MethodInfo_var;
extern const MethodInfo* Stats_DisablePanel_m3629909344_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3457520150;
extern const uint32_t Stats_BackButtStats_m1146948631_MetadataUsageId;
extern "C"  void Stats_BackButtStats_m1146948631 (Stats_t967880071 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stats_BackButtStats_m1146948631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Animator_t69676727 * V_0 = NULL;
	{
		Stats_offBackButt_m4112918333(__this, /*hidden argument*/NULL);
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)Stats_onBackButt_m2446812067_MethodInfo_var);
		Execute_t1389284496 * L_1 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_1, __this, L_0, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (1.0f), L_1, __this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = ___gamePanel0;
		NullCheck(L_2);
		Animator_t69676727 * L_3 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		V_0 = L_3;
		Animator_t69676727 * L_4 = V_0;
		NullCheck(L_4);
		Animator_SetTrigger_m3418492570(L_4, _stringLiteral3457520150, /*hidden argument*/NULL);
		MainGame_t3800664731 * L_5 = __this->get_mainGame_13();
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)MainGame_ActiveButtons_m3331183351_MethodInfo_var);
		Execute_t1389284496 * L_7 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_7, L_5, L_6, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (0.1f), L_7, __this, /*hidden argument*/NULL);
		SoundController_t1686593041 * L_8 = __this->get_soundController_12();
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)SoundController_CloseWindow_m1597160790_MethodInfo_var);
		Execute_t1389284496 * L_10 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_10, L_8, L_9, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (0.4f), L_10, __this, /*hidden argument*/NULL);
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)Stats_DisablePanel_m3629909344_MethodInfo_var);
		ExecuteWithGameObjcet_t1191175011 * L_12 = (ExecuteWithGameObjcet_t1191175011 *)il2cpp_codegen_object_new(ExecuteWithGameObjcet_t1191175011_il2cpp_TypeInfo_var);
		ExecuteWithGameObjcet__ctor_m342781680(L_12, __this, L_11, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_13 = ___gamePanel0;
		Handler_StartDelayed_m1932308614(NULL /*static, unused*/, (1.0f), L_12, L_13, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stats::DisablePanel(UnityEngine.GameObject)
extern "C"  void Stats_DisablePanel_m3629909344 (Stats_t967880071 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stats::LanscapeEnable()
extern "C"  void Stats_LanscapeEnable_m3462121364 (Stats_t967880071 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_landscapeStatsPanel_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stats::PortretEnable()
extern "C"  void Stats_PortretEnable_m2884510581 (Stats_t967880071 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_PortraitStatsPanel_11();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stats::onBackButt()
extern "C"  void Stats_onBackButt_m2446812067 (Stats_t967880071 * __this, const MethodInfo* method)
{
	{
		Button_t2872111280 * L_0 = __this->get_btn_back_2_15();
		NullCheck(L_0);
		Selectable_set_interactable_m63718297(L_0, (bool)1, /*hidden argument*/NULL);
		Button_t2872111280 * L_1 = __this->get_btn_back_1_14();
		NullCheck(L_1);
		Selectable_set_interactable_m63718297(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stats::offBackButt()
extern "C"  void Stats_offBackButt_m4112918333 (Stats_t967880071 * __this, const MethodInfo* method)
{
	{
		Button_t2872111280 * L_0 = __this->get_btn_back_2_15();
		NullCheck(L_0);
		Selectable_set_interactable_m63718297(L_0, (bool)0, /*hidden argument*/NULL);
		Button_t2872111280 * L_1 = __this->get_btn_back_1_14();
		NullCheck(L_1);
		Selectable_set_interactable_m63718297(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stats::ActiveMyStatsSong()
extern Il2CppClass* Execute_t1389284496_il2cpp_TypeInfo_var;
extern const MethodInfo* SoundController_OpenWindow_m1923503136_MethodInfo_var;
extern const uint32_t Stats_ActiveMyStatsSong_m751971532_MetadataUsageId;
extern "C"  void Stats_ActiveMyStatsSong_m751971532 (Stats_t967880071 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stats_ActiveMyStatsSong_m751971532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SoundController_t1686593041 * L_0 = __this->get_soundController_12();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)SoundController_OpenWindow_m1923503136_MethodInfo_var);
		Execute_t1389284496 * L_2 = (Execute_t1389284496 *)il2cpp_codegen_object_new(Execute_t1389284496_il2cpp_TypeInfo_var);
		Execute__ctor_m2841091363(L_2, L_0, L_1, /*hidden argument*/NULL);
		Handler_StartDelayed_m4016449873(NULL /*static, unused*/, (0.6f), L_2, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Stats::LoadImageStats(System.String,UnityEngine.UI.Image)
extern Il2CppClass* U3CLoadImageStatsU3Ec__Iterator0_t1390744875_il2cpp_TypeInfo_var;
extern const uint32_t Stats_LoadImageStats_m535395134_MetadataUsageId;
extern "C"  Il2CppObject * Stats_LoadImageStats_m535395134 (Stats_t967880071 * __this, String_t* ___path0, Image_t2042527209 * ___output1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stats_LoadImageStats_m535395134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * V_0 = NULL;
	{
		U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * L_0 = (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 *)il2cpp_codegen_object_new(U3CLoadImageStatsU3Ec__Iterator0_t1390744875_il2cpp_TypeInfo_var);
		U3CLoadImageStatsU3Ec__Iterator0__ctor_m3820255872(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * L_1 = V_0;
		String_t* L_2 = ___path0;
		NullCheck(L_1);
		L_1->set_path_0(L_2);
		U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * L_3 = V_0;
		Image_t2042527209 * L_4 = ___output1;
		NullCheck(L_3);
		L_3->set_output_4(L_4);
		U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * L_5 = V_0;
		return L_5;
	}
}
// System.Void Stats/<LoadImageStats>c__Iterator0::.ctor()
extern "C"  void U3CLoadImageStatsU3Ec__Iterator0__ctor_m3820255872 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Stats/<LoadImageStats>c__Iterator0::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2494949592;
extern Il2CppCodeGenString* _stringLiteral33571700;
extern const uint32_t U3CLoadImageStatsU3Ec__Iterator0_MoveNext_m3115053136_MetadataUsageId;
extern "C"  bool U3CLoadImageStatsU3Ec__Iterator0_MoveNext_m3115053136 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadImageStatsU3Ec__Iterator0_MoveNext_m3115053136_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_0101;
	}

IL_0021:
	{
		String_t* L_2 = __this->get_path_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2494949592, L_2, /*hidden argument*/NULL);
		__this->set_U3CurlU3E__0_1(L_3);
		String_t* L_4 = __this->get_U3CurlU3E__0_1();
		WWW_t2919945039 * L_5 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_5, L_4, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__1_2(L_5);
		WWW_t2919945039 * L_6 = __this->get_U3CwwwU3E__1_2();
		__this->set_U24current_5(L_6);
		bool L_7 = __this->get_U24disposing_6();
		if (L_7)
		{
			goto IL_0063;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_0063:
	{
		goto IL_0103;
	}

IL_0068:
	{
		WWW_t2919945039 * L_8 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_8);
		Texture2D_t3542995729 * L_9 = WWW_get_texture_m1121178301(L_8, /*hidden argument*/NULL);
		__this->set_U3CtextureU3E__2_3(L_9);
		Texture2D_t3542995729 * L_10 = __this->get_U3CtextureU3E__2_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_009f;
		}
	}
	{
		String_t* L_12 = __this->get_U3CurlU3E__0_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral33571700, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_009f:
	{
		Image_t2042527209 * L_14 = __this->get_output_4();
		WWW_t2919945039 * L_15 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_15);
		Texture2D_t3542995729 * L_16 = WWW_get_texture_m1121178301(L_15, /*hidden argument*/NULL);
		WWW_t2919945039 * L_17 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_17);
		Texture2D_t3542995729 * L_18 = WWW_get_texture_m1121178301(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_18);
		WWW_t2919945039 * L_20 = __this->get_U3CwwwU3E__1_2();
		NullCheck(L_20);
		Texture2D_t3542995729 * L_21 = WWW_get_texture_m1121178301(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_21);
		Rect_t3681755626  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Rect__ctor_m1220545469(&L_23, (0.0f), (0.0f), (((float)((float)L_19))), (((float)((float)L_22))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector2__ctor_m3067419446(&L_24, (0.0f), (0.0f), /*hidden argument*/NULL);
		Sprite_t309593783 * L_25 = Sprite_Create_m3262956430(NULL /*static, unused*/, L_16, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_14);
		Image_set_sprite_m1800056820(L_14, L_25, /*hidden argument*/NULL);
		__this->set_U24PC_7((-1));
	}

IL_0101:
	{
		return (bool)0;
	}

IL_0103:
	{
		return (bool)1;
	}
}
// System.Object Stats/<LoadImageStats>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadImageStatsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1270063688 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object Stats/<LoadImageStats>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadImageStatsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m16369936 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void Stats/<LoadImageStats>c__Iterator0::Dispose()
extern "C"  void U3CLoadImageStatsU3Ec__Iterator0_Dispose_m2104620369 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void Stats/<LoadImageStats>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadImageStatsU3Ec__Iterator0_Reset_m318265383_MetadataUsageId;
extern "C"  void U3CLoadImageStatsU3Ec__Iterator0_Reset_m318265383 (U3CLoadImageStatsU3Ec__Iterator0_t1390744875 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadImageStatsU3Ec__Iterator0_Reset_m318265383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Util::.ctor()
extern "C"  void Util__ctor_m4057728895 (Util_t4006552276 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Util::reshuffle(System.String[])
extern "C"  void Util_reshuffle_m3930049179 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___texts0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		goto IL_0023;
	}

IL_0007:
	{
		StringU5BU5D_t1642385972* L_0 = ___texts0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		String_t* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		int32_t L_4 = V_0;
		StringU5BU5D_t1642385972* L_5 = ___texts0;
		NullCheck(L_5);
		int32_t L_6 = Random_Range_m694320887(NULL /*static, unused*/, L_4, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		V_2 = L_6;
		StringU5BU5D_t1642385972* L_7 = ___texts0;
		int32_t L_8 = V_0;
		StringU5BU5D_t1642385972* L_9 = ___texts0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		String_t* L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_12);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (String_t*)L_12);
		StringU5BU5D_t1642385972* L_13 = ___texts0;
		int32_t L_14 = V_2;
		String_t* L_15 = V_1;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (String_t*)L_15);
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_17 = V_0;
		StringU5BU5D_t1642385972* L_18 = ___texts0;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
