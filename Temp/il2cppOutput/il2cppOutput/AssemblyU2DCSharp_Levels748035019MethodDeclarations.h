﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Levels
struct Levels_t748035019;
// System.Collections.Generic.List`1<Question>
struct List_1_t2297069972;
// Question
struct Question_t2927948840;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Question2927948840.h"

// System.Void Levels::.ctor()
extern "C"  void Levels__ctor_m1598716242 (Levels_t748035019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Levels::get_NrLevel()
extern "C"  int32_t Levels_get_NrLevel_m2233528047 (Levels_t748035019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Levels::set_NrLevel(System.Int32)
extern "C"  void Levels_set_NrLevel_m914816550 (Levels_t748035019 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Levels::get_IsLocked()
extern "C"  bool Levels_get_IsLocked_m626983591 (Levels_t748035019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Levels::set_IsLocked(System.Boolean)
extern "C"  void Levels_set_IsLocked_m4000392438 (Levels_t748035019 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Levels::get_CorectAnswers()
extern "C"  int32_t Levels_get_CorectAnswers_m2038854904 (Levels_t748035019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Levels::set_CorectAnswers(System.Int32)
extern "C"  void Levels_set_CorectAnswers_m2211099053 (Levels_t748035019 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Question> Levels::get_Quests()
extern "C"  List_1_t2297069972 * Levels_get_Quests_m1321190565 (Levels_t748035019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Levels::set_Quests(System.Collections.Generic.List`1<Question>)
extern "C"  void Levels_set_Quests_m838198532 (Levels_t748035019 * __this, List_1_t2297069972 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Levels::addElement(Question)
extern "C"  void Levels_addElement_m1871995915 (Levels_t748035019 * __this, Question_t2927948840 * ___question0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Levels::getStarNumber()
extern "C"  int32_t Levels_getStarNumber_m1660958789 (Levels_t748035019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
