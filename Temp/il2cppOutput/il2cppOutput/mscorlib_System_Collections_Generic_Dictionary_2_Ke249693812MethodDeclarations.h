﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>
struct Dictionary_2_t1855157670;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke249693812.h"
#include "Stores_UnityEngine_Purchasing_AppStore379104228.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2375089462_gshared (Enumerator_t249693812 * __this, Dictionary_2_t1855157670 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2375089462(__this, ___host0, method) ((  void (*) (Enumerator_t249693812 *, Dictionary_2_t1855157670 *, const MethodInfo*))Enumerator__ctor_m2375089462_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m746983207_gshared (Enumerator_t249693812 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m746983207(__this, method) ((  Il2CppObject * (*) (Enumerator_t249693812 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m746983207_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1973072123_gshared (Enumerator_t249693812 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1973072123(__this, method) ((  void (*) (Enumerator_t249693812 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1973072123_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3868076458_gshared (Enumerator_t249693812 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3868076458(__this, method) ((  void (*) (Enumerator_t249693812 *, const MethodInfo*))Enumerator_Dispose_m3868076458_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1791504463_gshared (Enumerator_t249693812 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1791504463(__this, method) ((  bool (*) (Enumerator_t249693812 *, const MethodInfo*))Enumerator_MoveNext_m1791504463_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Purchasing.AppStore,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2405695813_gshared (Enumerator_t249693812 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2405695813(__this, method) ((  int32_t (*) (Enumerator_t249693812 *, const MethodInfo*))Enumerator_get_Current_m2405695813_gshared)(__this, method)
