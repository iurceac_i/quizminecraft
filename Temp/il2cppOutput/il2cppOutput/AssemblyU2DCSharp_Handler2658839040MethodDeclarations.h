﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Handler
struct Handler_t2658839040;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// Handler/Execute
struct Execute_t1389284496;
// Handler/ExecuteWithGameObjcet
struct ExecuteWithGameObjcet_t1191175011;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Handler/ExecuteWithButton
struct ExecuteWithButton_t3718107720;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Handler_Execute1389284496.h"
#include "AssemblyU2DCSharp_Handler_ExecuteWithGameObjcet1191175011.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_Handler_ExecuteWithButton3718107720.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"

// System.Void Handler::.ctor()
extern "C"  void Handler__ctor_m3548521471 (Handler_t2658839040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine Handler::StartDelayed(System.Single,System.Boolean,UnityEngine.MonoBehaviour)
extern "C"  Coroutine_t2299508840 * Handler_StartDelayed_m880710460 (Il2CppObject * __this /* static, unused */, float ___waitSeconds0, bool ___isValable1, MonoBehaviour_t1158329972 * ___behaviour2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine Handler::StartDelayed(System.Single,Handler/Execute,UnityEngine.MonoBehaviour)
extern "C"  Coroutine_t2299508840 * Handler_StartDelayed_m4016449873 (Il2CppObject * __this /* static, unused */, float ___waitSeconds0, Execute_t1389284496 * ___execute1, MonoBehaviour_t1158329972 * ___behaviour2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine Handler::StartDelayed(System.Single,Handler/ExecuteWithGameObjcet,UnityEngine.GameObject,UnityEngine.MonoBehaviour)
extern "C"  Coroutine_t2299508840 * Handler_StartDelayed_m1932308614 (Il2CppObject * __this /* static, unused */, float ___waitSeconds0, ExecuteWithGameObjcet_t1191175011 * ___execute1, GameObject_t1756533147 * ___GO2, MonoBehaviour_t1158329972 * ___behaviour3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine Handler::StartDelayed(System.Single,Handler/ExecuteWithButton,UnityEngine.UI.Button,UnityEngine.MonoBehaviour)
extern "C"  Coroutine_t2299508840 * Handler_StartDelayed_m3476731120 (Il2CppObject * __this /* static, unused */, float ___waitSeconds0, ExecuteWithButton_t3718107720 * ___execute1, Button_t2872111280 * ___btn2, MonoBehaviour_t1158329972 * ___behaviour3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Handler::StopDelayed(UnityEngine.Coroutine,UnityEngine.MonoBehaviour)
extern "C"  void Handler_StopDelayed_m2765900203 (Il2CppObject * __this /* static, unused */, Coroutine_t2299508840 * ___coroutine0, MonoBehaviour_t1158329972 * ___behaviour1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Handler::WaitForSeconds(System.Single,System.Boolean)
extern "C"  Il2CppObject * Handler_WaitForSeconds_m3157568414 (Il2CppObject * __this /* static, unused */, float ___time0, bool ___isValable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Handler::WaitForSeconds(System.Single,Handler/Execute)
extern "C"  Il2CppObject * Handler_WaitForSeconds_m2283772139 (Il2CppObject * __this /* static, unused */, float ___time0, Execute_t1389284496 * ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Handler::WaitForSeconds(System.Single,Handler/ExecuteWithGameObjcet,UnityEngine.GameObject)
extern "C"  Il2CppObject * Handler_WaitForSeconds_m1128382948 (Il2CppObject * __this /* static, unused */, float ___time0, ExecuteWithGameObjcet_t1191175011 * ___method1, GameObject_t1756533147 * ___GO2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Handler::WaitForSeconds(System.Single,Handler/ExecuteWithButton,UnityEngine.UI.Button)
extern "C"  Il2CppObject * Handler_WaitForSeconds_m2136273796 (Il2CppObject * __this /* static, unused */, float ___time0, ExecuteWithButton_t3718107720 * ___method1, Button_t2872111280 * ___btn2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
