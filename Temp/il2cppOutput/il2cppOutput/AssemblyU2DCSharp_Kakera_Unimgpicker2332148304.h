﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Kakera.Unimgpicker/ImageDelegate
struct ImageDelegate_t3548206662;
// Kakera.Unimgpicker/ErrorDelegate
struct ErrorDelegate_t402150177;
// Kakera.IPicker
struct IPicker_t4111000441;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kakera.Unimgpicker
struct  Unimgpicker_t2332148304  : public MonoBehaviour_t1158329972
{
public:
	// Kakera.Unimgpicker/ImageDelegate Kakera.Unimgpicker::Completed
	ImageDelegate_t3548206662 * ___Completed_2;
	// Kakera.Unimgpicker/ErrorDelegate Kakera.Unimgpicker::Failed
	ErrorDelegate_t402150177 * ___Failed_3;
	// Kakera.IPicker Kakera.Unimgpicker::picker
	Il2CppObject * ___picker_4;

public:
	inline static int32_t get_offset_of_Completed_2() { return static_cast<int32_t>(offsetof(Unimgpicker_t2332148304, ___Completed_2)); }
	inline ImageDelegate_t3548206662 * get_Completed_2() const { return ___Completed_2; }
	inline ImageDelegate_t3548206662 ** get_address_of_Completed_2() { return &___Completed_2; }
	inline void set_Completed_2(ImageDelegate_t3548206662 * value)
	{
		___Completed_2 = value;
		Il2CppCodeGenWriteBarrier(&___Completed_2, value);
	}

	inline static int32_t get_offset_of_Failed_3() { return static_cast<int32_t>(offsetof(Unimgpicker_t2332148304, ___Failed_3)); }
	inline ErrorDelegate_t402150177 * get_Failed_3() const { return ___Failed_3; }
	inline ErrorDelegate_t402150177 ** get_address_of_Failed_3() { return &___Failed_3; }
	inline void set_Failed_3(ErrorDelegate_t402150177 * value)
	{
		___Failed_3 = value;
		Il2CppCodeGenWriteBarrier(&___Failed_3, value);
	}

	inline static int32_t get_offset_of_picker_4() { return static_cast<int32_t>(offsetof(Unimgpicker_t2332148304, ___picker_4)); }
	inline Il2CppObject * get_picker_4() const { return ___picker_4; }
	inline Il2CppObject ** get_address_of_picker_4() { return &___picker_4; }
	inline void set_picker_4(Il2CppObject * value)
	{
		___picker_4 = value;
		Il2CppCodeGenWriteBarrier(&___picker_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
