﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonAnimation
struct ButtonAnimation_t1426385366;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonAnimation::.ctor()
extern "C"  void ButtonAnimation__ctor_m484333939 (ButtonAnimation_t1426385366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnimation::Start()
extern "C"  void ButtonAnimation_Start_m1237470215 (ButtonAnimation_t1426385366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnimation::Update()
extern "C"  void ButtonAnimation_Update_m2507586176 (ButtonAnimation_t1426385366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnimation::hideButton()
extern "C"  void ButtonAnimation_hideButton_m857075365 (ButtonAnimation_t1426385366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonAnimation::showButton()
extern "C"  void ButtonAnimation_showButton_m143411492 (ButtonAnimation_t1426385366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
