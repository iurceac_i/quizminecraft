﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IAPDemo/<InitUI>c__AnonStorey1
struct U3CInitUIU3Ec__AnonStorey1_t996539604;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IAPDemo/<InitUI>c__AnonStorey1::.ctor()
extern "C"  void U3CInitUIU3Ec__AnonStorey1__ctor_m1248594915 (U3CInitUIU3Ec__AnonStorey1_t996539604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo/<InitUI>c__AnonStorey1::<>m__0(System.Boolean,System.String,System.String)
extern "C"  void U3CInitUIU3Ec__AnonStorey1_U3CU3Em__0_m1768299555 (U3CInitUIU3Ec__AnonStorey1_t996539604 * __this, bool ___success0, String_t* ___signData1, String_t* ___signature2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
