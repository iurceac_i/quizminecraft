﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl1301617341MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_FacebookStoreImpl1362794587MethodDeclarations.h"
#include "Stores_UnityEngine_Purchasing_TizenStoreImpl274247241MethodDeclarations.h"

extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[3] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AppleStoreImpl_MessageCallback_m2420291589),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_FacebookStoreImpl_MessageCallback_m4247126525),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_TizenStoreImpl_MessageCallback_m2338259177),
};
