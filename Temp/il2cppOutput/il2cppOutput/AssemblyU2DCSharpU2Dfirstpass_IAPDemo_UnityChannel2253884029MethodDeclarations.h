﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IAPDemo/UnityChannelLoginHandler
struct UnityChannelLoginHandler_t2253884029;
// System.String
struct String_t;
// UnityEngine.Store.UserInfo
struct UserInfo_t741955747;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityStore_UnityEngine_Store_UserInfo741955747.h"

// System.Void IAPDemo/UnityChannelLoginHandler::.ctor()
extern "C"  void UnityChannelLoginHandler__ctor_m3480826282 (UnityChannelLoginHandler_t2253884029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo/UnityChannelLoginHandler::OnInitialized()
extern "C"  void UnityChannelLoginHandler_OnInitialized_m541166015 (UnityChannelLoginHandler_t2253884029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo/UnityChannelLoginHandler::OnInitializeFailed(System.String)
extern "C"  void UnityChannelLoginHandler_OnInitializeFailed_m1588211160 (UnityChannelLoginHandler_t2253884029 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo/UnityChannelLoginHandler::OnLogin(UnityEngine.Store.UserInfo)
extern "C"  void UnityChannelLoginHandler_OnLogin_m2000746577 (UnityChannelLoginHandler_t2253884029 * __this, UserInfo_t741955747 * ___userInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAPDemo/UnityChannelLoginHandler::OnLoginFailed(System.String)
extern "C"  void UnityChannelLoginHandler_OnLoginFailed_m2261121525 (UnityChannelLoginHandler_t2253884029 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
