﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.UI.Button
struct Button_t2872111280;
// SoundController
struct SoundController_t1686593041;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// readHTML
struct  readHTML_t503794377  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text readHTML::contentURL
	Text_t356221433 * ___contentURL_2;
	// UnityEngine.GameObject readHTML::backButton
	GameObject_t1756533147 * ___backButton_3;
	// System.String readHTML::path_terms
	String_t* ___path_terms_4;
	// System.String readHTML::path_subscription
	String_t* ___path_subscription_5;
	// System.String readHTML::path_privacy
	String_t* ___path_privacy_6;
	// UnityEngine.Animator readHTML::anim
	Animator_t69676727 * ___anim_7;
	// System.String readHTML::readTextFileSubscription
	String_t* ___readTextFileSubscription_8;
	// System.String readHTML::readTextFileTerms
	String_t* ___readTextFileTerms_9;
	// System.String readHTML::readTextFilePrivacy
	String_t* ___readTextFilePrivacy_10;
	// UnityEngine.UI.Button readHTML::SubscriptionBtn
	Button_t2872111280 * ___SubscriptionBtn_11;
	// UnityEngine.UI.Button readHTML::privacyBtn
	Button_t2872111280 * ___privacyBtn_12;
	// UnityEngine.UI.Button readHTML::termsBtn
	Button_t2872111280 * ___termsBtn_13;
	// UnityEngine.UI.Button readHTML::btn_1
	Button_t2872111280 * ___btn_1_14;
	// SoundController readHTML::soundControll
	SoundController_t1686593041 * ___soundControll_15;

public:
	inline static int32_t get_offset_of_contentURL_2() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___contentURL_2)); }
	inline Text_t356221433 * get_contentURL_2() const { return ___contentURL_2; }
	inline Text_t356221433 ** get_address_of_contentURL_2() { return &___contentURL_2; }
	inline void set_contentURL_2(Text_t356221433 * value)
	{
		___contentURL_2 = value;
		Il2CppCodeGenWriteBarrier(&___contentURL_2, value);
	}

	inline static int32_t get_offset_of_backButton_3() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___backButton_3)); }
	inline GameObject_t1756533147 * get_backButton_3() const { return ___backButton_3; }
	inline GameObject_t1756533147 ** get_address_of_backButton_3() { return &___backButton_3; }
	inline void set_backButton_3(GameObject_t1756533147 * value)
	{
		___backButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___backButton_3, value);
	}

	inline static int32_t get_offset_of_path_terms_4() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___path_terms_4)); }
	inline String_t* get_path_terms_4() const { return ___path_terms_4; }
	inline String_t** get_address_of_path_terms_4() { return &___path_terms_4; }
	inline void set_path_terms_4(String_t* value)
	{
		___path_terms_4 = value;
		Il2CppCodeGenWriteBarrier(&___path_terms_4, value);
	}

	inline static int32_t get_offset_of_path_subscription_5() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___path_subscription_5)); }
	inline String_t* get_path_subscription_5() const { return ___path_subscription_5; }
	inline String_t** get_address_of_path_subscription_5() { return &___path_subscription_5; }
	inline void set_path_subscription_5(String_t* value)
	{
		___path_subscription_5 = value;
		Il2CppCodeGenWriteBarrier(&___path_subscription_5, value);
	}

	inline static int32_t get_offset_of_path_privacy_6() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___path_privacy_6)); }
	inline String_t* get_path_privacy_6() const { return ___path_privacy_6; }
	inline String_t** get_address_of_path_privacy_6() { return &___path_privacy_6; }
	inline void set_path_privacy_6(String_t* value)
	{
		___path_privacy_6 = value;
		Il2CppCodeGenWriteBarrier(&___path_privacy_6, value);
	}

	inline static int32_t get_offset_of_anim_7() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___anim_7)); }
	inline Animator_t69676727 * get_anim_7() const { return ___anim_7; }
	inline Animator_t69676727 ** get_address_of_anim_7() { return &___anim_7; }
	inline void set_anim_7(Animator_t69676727 * value)
	{
		___anim_7 = value;
		Il2CppCodeGenWriteBarrier(&___anim_7, value);
	}

	inline static int32_t get_offset_of_readTextFileSubscription_8() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___readTextFileSubscription_8)); }
	inline String_t* get_readTextFileSubscription_8() const { return ___readTextFileSubscription_8; }
	inline String_t** get_address_of_readTextFileSubscription_8() { return &___readTextFileSubscription_8; }
	inline void set_readTextFileSubscription_8(String_t* value)
	{
		___readTextFileSubscription_8 = value;
		Il2CppCodeGenWriteBarrier(&___readTextFileSubscription_8, value);
	}

	inline static int32_t get_offset_of_readTextFileTerms_9() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___readTextFileTerms_9)); }
	inline String_t* get_readTextFileTerms_9() const { return ___readTextFileTerms_9; }
	inline String_t** get_address_of_readTextFileTerms_9() { return &___readTextFileTerms_9; }
	inline void set_readTextFileTerms_9(String_t* value)
	{
		___readTextFileTerms_9 = value;
		Il2CppCodeGenWriteBarrier(&___readTextFileTerms_9, value);
	}

	inline static int32_t get_offset_of_readTextFilePrivacy_10() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___readTextFilePrivacy_10)); }
	inline String_t* get_readTextFilePrivacy_10() const { return ___readTextFilePrivacy_10; }
	inline String_t** get_address_of_readTextFilePrivacy_10() { return &___readTextFilePrivacy_10; }
	inline void set_readTextFilePrivacy_10(String_t* value)
	{
		___readTextFilePrivacy_10 = value;
		Il2CppCodeGenWriteBarrier(&___readTextFilePrivacy_10, value);
	}

	inline static int32_t get_offset_of_SubscriptionBtn_11() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___SubscriptionBtn_11)); }
	inline Button_t2872111280 * get_SubscriptionBtn_11() const { return ___SubscriptionBtn_11; }
	inline Button_t2872111280 ** get_address_of_SubscriptionBtn_11() { return &___SubscriptionBtn_11; }
	inline void set_SubscriptionBtn_11(Button_t2872111280 * value)
	{
		___SubscriptionBtn_11 = value;
		Il2CppCodeGenWriteBarrier(&___SubscriptionBtn_11, value);
	}

	inline static int32_t get_offset_of_privacyBtn_12() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___privacyBtn_12)); }
	inline Button_t2872111280 * get_privacyBtn_12() const { return ___privacyBtn_12; }
	inline Button_t2872111280 ** get_address_of_privacyBtn_12() { return &___privacyBtn_12; }
	inline void set_privacyBtn_12(Button_t2872111280 * value)
	{
		___privacyBtn_12 = value;
		Il2CppCodeGenWriteBarrier(&___privacyBtn_12, value);
	}

	inline static int32_t get_offset_of_termsBtn_13() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___termsBtn_13)); }
	inline Button_t2872111280 * get_termsBtn_13() const { return ___termsBtn_13; }
	inline Button_t2872111280 ** get_address_of_termsBtn_13() { return &___termsBtn_13; }
	inline void set_termsBtn_13(Button_t2872111280 * value)
	{
		___termsBtn_13 = value;
		Il2CppCodeGenWriteBarrier(&___termsBtn_13, value);
	}

	inline static int32_t get_offset_of_btn_1_14() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___btn_1_14)); }
	inline Button_t2872111280 * get_btn_1_14() const { return ___btn_1_14; }
	inline Button_t2872111280 ** get_address_of_btn_1_14() { return &___btn_1_14; }
	inline void set_btn_1_14(Button_t2872111280 * value)
	{
		___btn_1_14 = value;
		Il2CppCodeGenWriteBarrier(&___btn_1_14, value);
	}

	inline static int32_t get_offset_of_soundControll_15() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___soundControll_15)); }
	inline SoundController_t1686593041 * get_soundControll_15() const { return ___soundControll_15; }
	inline SoundController_t1686593041 ** get_address_of_soundControll_15() { return &___soundControll_15; }
	inline void set_soundControll_15(SoundController_t1686593041 * value)
	{
		___soundControll_15 = value;
		Il2CppCodeGenWriteBarrier(&___soundControll_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
