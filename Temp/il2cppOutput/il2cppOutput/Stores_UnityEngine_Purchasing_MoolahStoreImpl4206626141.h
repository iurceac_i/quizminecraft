﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Purchasing.Extension.IStoreCallback
struct IStoreCallback_t2691517565;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "Stores_UnityEngine_Purchasing_CloudMoolahMode206291964.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl
struct  MoolahStoreImpl_t4206626141  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.MoolahStoreImpl::m_callback
	Il2CppObject * ___m_callback_7;
	// System.Boolean UnityEngine.Purchasing.MoolahStoreImpl::isNeedPolling
	bool ___isNeedPolling_8;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_CurrentStoreProductID
	String_t* ___m_CurrentStoreProductID_9;
	// System.Boolean UnityEngine.Purchasing.MoolahStoreImpl::isRequestAuthCodeing
	bool ___isRequestAuthCodeing_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_appKey
	String_t* ___m_appKey_11;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_hashKey
	String_t* ___m_hashKey_12;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_notificationURL
	String_t* ___m_notificationURL_13;
	// UnityEngine.Purchasing.CloudMoolahMode UnityEngine.Purchasing.MoolahStoreImpl::m_mode
	int32_t ___m_mode_14;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_CustomerID
	String_t* ___m_CustomerID_15;

public:
	inline static int32_t get_offset_of_m_callback_7() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141, ___m_callback_7)); }
	inline Il2CppObject * get_m_callback_7() const { return ___m_callback_7; }
	inline Il2CppObject ** get_address_of_m_callback_7() { return &___m_callback_7; }
	inline void set_m_callback_7(Il2CppObject * value)
	{
		___m_callback_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_callback_7, value);
	}

	inline static int32_t get_offset_of_isNeedPolling_8() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141, ___isNeedPolling_8)); }
	inline bool get_isNeedPolling_8() const { return ___isNeedPolling_8; }
	inline bool* get_address_of_isNeedPolling_8() { return &___isNeedPolling_8; }
	inline void set_isNeedPolling_8(bool value)
	{
		___isNeedPolling_8 = value;
	}

	inline static int32_t get_offset_of_m_CurrentStoreProductID_9() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141, ___m_CurrentStoreProductID_9)); }
	inline String_t* get_m_CurrentStoreProductID_9() const { return ___m_CurrentStoreProductID_9; }
	inline String_t** get_address_of_m_CurrentStoreProductID_9() { return &___m_CurrentStoreProductID_9; }
	inline void set_m_CurrentStoreProductID_9(String_t* value)
	{
		___m_CurrentStoreProductID_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_CurrentStoreProductID_9, value);
	}

	inline static int32_t get_offset_of_isRequestAuthCodeing_10() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141, ___isRequestAuthCodeing_10)); }
	inline bool get_isRequestAuthCodeing_10() const { return ___isRequestAuthCodeing_10; }
	inline bool* get_address_of_isRequestAuthCodeing_10() { return &___isRequestAuthCodeing_10; }
	inline void set_isRequestAuthCodeing_10(bool value)
	{
		___isRequestAuthCodeing_10 = value;
	}

	inline static int32_t get_offset_of_m_appKey_11() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141, ___m_appKey_11)); }
	inline String_t* get_m_appKey_11() const { return ___m_appKey_11; }
	inline String_t** get_address_of_m_appKey_11() { return &___m_appKey_11; }
	inline void set_m_appKey_11(String_t* value)
	{
		___m_appKey_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_appKey_11, value);
	}

	inline static int32_t get_offset_of_m_hashKey_12() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141, ___m_hashKey_12)); }
	inline String_t* get_m_hashKey_12() const { return ___m_hashKey_12; }
	inline String_t** get_address_of_m_hashKey_12() { return &___m_hashKey_12; }
	inline void set_m_hashKey_12(String_t* value)
	{
		___m_hashKey_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_hashKey_12, value);
	}

	inline static int32_t get_offset_of_m_notificationURL_13() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141, ___m_notificationURL_13)); }
	inline String_t* get_m_notificationURL_13() const { return ___m_notificationURL_13; }
	inline String_t** get_address_of_m_notificationURL_13() { return &___m_notificationURL_13; }
	inline void set_m_notificationURL_13(String_t* value)
	{
		___m_notificationURL_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_notificationURL_13, value);
	}

	inline static int32_t get_offset_of_m_mode_14() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141, ___m_mode_14)); }
	inline int32_t get_m_mode_14() const { return ___m_mode_14; }
	inline int32_t* get_address_of_m_mode_14() { return &___m_mode_14; }
	inline void set_m_mode_14(int32_t value)
	{
		___m_mode_14 = value;
	}

	inline static int32_t get_offset_of_m_CustomerID_15() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141, ___m_CustomerID_15)); }
	inline String_t* get_m_CustomerID_15() const { return ___m_CustomerID_15; }
	inline String_t** get_address_of_m_CustomerID_15() { return &___m_CustomerID_15; }
	inline void set_m_CustomerID_15(String_t* value)
	{
		___m_CustomerID_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_CustomerID_15, value);
	}
};

struct MoolahStoreImpl_t4206626141_StaticFields
{
public:
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::pollingPath
	String_t* ___pollingPath_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestAuthCodePath
	String_t* ___requestAuthCodePath_3;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestRestoreTransactionUrl
	String_t* ___requestRestoreTransactionUrl_4;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestValidateReceiptUrl
	String_t* ___requestValidateReceiptUrl_5;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestProductValidateUrl
	String_t* ___requestProductValidateUrl_6;

public:
	inline static int32_t get_offset_of_pollingPath_2() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141_StaticFields, ___pollingPath_2)); }
	inline String_t* get_pollingPath_2() const { return ___pollingPath_2; }
	inline String_t** get_address_of_pollingPath_2() { return &___pollingPath_2; }
	inline void set_pollingPath_2(String_t* value)
	{
		___pollingPath_2 = value;
		Il2CppCodeGenWriteBarrier(&___pollingPath_2, value);
	}

	inline static int32_t get_offset_of_requestAuthCodePath_3() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141_StaticFields, ___requestAuthCodePath_3)); }
	inline String_t* get_requestAuthCodePath_3() const { return ___requestAuthCodePath_3; }
	inline String_t** get_address_of_requestAuthCodePath_3() { return &___requestAuthCodePath_3; }
	inline void set_requestAuthCodePath_3(String_t* value)
	{
		___requestAuthCodePath_3 = value;
		Il2CppCodeGenWriteBarrier(&___requestAuthCodePath_3, value);
	}

	inline static int32_t get_offset_of_requestRestoreTransactionUrl_4() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141_StaticFields, ___requestRestoreTransactionUrl_4)); }
	inline String_t* get_requestRestoreTransactionUrl_4() const { return ___requestRestoreTransactionUrl_4; }
	inline String_t** get_address_of_requestRestoreTransactionUrl_4() { return &___requestRestoreTransactionUrl_4; }
	inline void set_requestRestoreTransactionUrl_4(String_t* value)
	{
		___requestRestoreTransactionUrl_4 = value;
		Il2CppCodeGenWriteBarrier(&___requestRestoreTransactionUrl_4, value);
	}

	inline static int32_t get_offset_of_requestValidateReceiptUrl_5() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141_StaticFields, ___requestValidateReceiptUrl_5)); }
	inline String_t* get_requestValidateReceiptUrl_5() const { return ___requestValidateReceiptUrl_5; }
	inline String_t** get_address_of_requestValidateReceiptUrl_5() { return &___requestValidateReceiptUrl_5; }
	inline void set_requestValidateReceiptUrl_5(String_t* value)
	{
		___requestValidateReceiptUrl_5 = value;
		Il2CppCodeGenWriteBarrier(&___requestValidateReceiptUrl_5, value);
	}

	inline static int32_t get_offset_of_requestProductValidateUrl_6() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4206626141_StaticFields, ___requestProductValidateUrl_6)); }
	inline String_t* get_requestProductValidateUrl_6() const { return ___requestProductValidateUrl_6; }
	inline String_t** get_address_of_requestProductValidateUrl_6() { return &___requestProductValidateUrl_6; }
	inline void set_requestProductValidateUrl_6(String_t* value)
	{
		___requestProductValidateUrl_6 = value;
		Il2CppCodeGenWriteBarrier(&___requestProductValidateUrl_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
