﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Store.ILoginListener
struct ILoginListener_t3164042774;

#include "UnityEngine_UnityEngine_AndroidJavaProxy4274989947.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Store.LoginForwardCallback
struct  LoginForwardCallback_t1105422505  : public AndroidJavaProxy_t4274989947
{
public:
	// UnityEngine.Store.ILoginListener UnityEngine.Store.LoginForwardCallback::loginListener
	Il2CppObject * ___loginListener_1;

public:
	inline static int32_t get_offset_of_loginListener_1() { return static_cast<int32_t>(offsetof(LoginForwardCallback_t1105422505, ___loginListener_1)); }
	inline Il2CppObject * get_loginListener_1() const { return ___loginListener_1; }
	inline Il2CppObject ** get_address_of_loginListener_1() { return &___loginListener_1; }
	inline void set_loginListener_1(Il2CppObject * value)
	{
		___loginListener_1 = value;
		Il2CppCodeGenWriteBarrier(&___loginListener_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
