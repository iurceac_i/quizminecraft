﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E3806193287MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Purchasing.Extension.ProductDescription>::.ctor(System.Collections.Generic.HashSet`1<T>)
#define Enumerator__ctor_m777160461(__this, ___hashset0, method) ((  void (*) (Enumerator_t140044219 *, HashSet_1_t1651728377 *, const MethodInfo*))Enumerator__ctor_m1279102766_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Purchasing.Extension.ProductDescription>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2492685451(__this, method) ((  Il2CppObject * (*) (Enumerator_t140044219 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2899861010_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Purchasing.Extension.ProductDescription>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2441325219(__this, method) ((  void (*) (Enumerator_t140044219 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2573763156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Purchasing.Extension.ProductDescription>::MoveNext()
#define Enumerator_MoveNext_m3592743331(__this, method) ((  bool (*) (Enumerator_t140044219 *, const MethodInfo*))Enumerator_MoveNext_m2097560514_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Purchasing.Extension.ProductDescription>::get_Current()
#define Enumerator_get_Current_m3928455624(__this, method) ((  ProductDescription_t3318267523 * (*) (Enumerator_t140044219 *, const MethodInfo*))Enumerator_get_Current_m3016104593_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Purchasing.Extension.ProductDescription>::Dispose()
#define Enumerator_Dispose_m934082852(__this, method) ((  void (*) (Enumerator_t140044219 *, const MethodInfo*))Enumerator_Dispose_m2585752265_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Purchasing.Extension.ProductDescription>::CheckState()
#define Enumerator_CheckState_m879499496(__this, method) ((  void (*) (Enumerator_t140044219 *, const MethodInfo*))Enumerator_CheckState_m1761755727_gshared)(__this, method)
