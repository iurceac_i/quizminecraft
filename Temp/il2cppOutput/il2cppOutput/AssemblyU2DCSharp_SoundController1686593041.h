﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundController
struct  SoundController_t1686593041  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioClip SoundController::correctSound
	AudioClip_t1932558630 * ___correctSound_2;
	// UnityEngine.AudioClip SoundController::incorrectSound
	AudioClip_t1932558630 * ___incorrectSound_3;
	// UnityEngine.AudioClip SoundController::closeWindow
	AudioClip_t1932558630 * ___closeWindow_4;
	// UnityEngine.AudioClip SoundController::failMission
	AudioClip_t1932558630 * ___failMission_5;
	// UnityEngine.AudioClip SoundController::menuSong
	AudioClip_t1932558630 * ___menuSong_6;
	// UnityEngine.AudioClip SoundController::succesMission
	AudioClip_t1932558630 * ___succesMission_7;
	// UnityEngine.GameObject SoundController::settingsMenu
	GameObject_t1756533147 * ___settingsMenu_8;
	// UnityEngine.AudioSource SoundController::soundControll
	AudioSource_t1135106623 * ___soundControll_9;
	// UnityEngine.AudioSource SoundController::backgroundSound
	AudioSource_t1135106623 * ___backgroundSound_10;
	// UnityEngine.Sprite SoundController::onButton
	Sprite_t309593783 * ___onButton_11;
	// UnityEngine.Sprite SoundController::offButton
	Sprite_t309593783 * ___offButton_12;

public:
	inline static int32_t get_offset_of_correctSound_2() { return static_cast<int32_t>(offsetof(SoundController_t1686593041, ___correctSound_2)); }
	inline AudioClip_t1932558630 * get_correctSound_2() const { return ___correctSound_2; }
	inline AudioClip_t1932558630 ** get_address_of_correctSound_2() { return &___correctSound_2; }
	inline void set_correctSound_2(AudioClip_t1932558630 * value)
	{
		___correctSound_2 = value;
		Il2CppCodeGenWriteBarrier(&___correctSound_2, value);
	}

	inline static int32_t get_offset_of_incorrectSound_3() { return static_cast<int32_t>(offsetof(SoundController_t1686593041, ___incorrectSound_3)); }
	inline AudioClip_t1932558630 * get_incorrectSound_3() const { return ___incorrectSound_3; }
	inline AudioClip_t1932558630 ** get_address_of_incorrectSound_3() { return &___incorrectSound_3; }
	inline void set_incorrectSound_3(AudioClip_t1932558630 * value)
	{
		___incorrectSound_3 = value;
		Il2CppCodeGenWriteBarrier(&___incorrectSound_3, value);
	}

	inline static int32_t get_offset_of_closeWindow_4() { return static_cast<int32_t>(offsetof(SoundController_t1686593041, ___closeWindow_4)); }
	inline AudioClip_t1932558630 * get_closeWindow_4() const { return ___closeWindow_4; }
	inline AudioClip_t1932558630 ** get_address_of_closeWindow_4() { return &___closeWindow_4; }
	inline void set_closeWindow_4(AudioClip_t1932558630 * value)
	{
		___closeWindow_4 = value;
		Il2CppCodeGenWriteBarrier(&___closeWindow_4, value);
	}

	inline static int32_t get_offset_of_failMission_5() { return static_cast<int32_t>(offsetof(SoundController_t1686593041, ___failMission_5)); }
	inline AudioClip_t1932558630 * get_failMission_5() const { return ___failMission_5; }
	inline AudioClip_t1932558630 ** get_address_of_failMission_5() { return &___failMission_5; }
	inline void set_failMission_5(AudioClip_t1932558630 * value)
	{
		___failMission_5 = value;
		Il2CppCodeGenWriteBarrier(&___failMission_5, value);
	}

	inline static int32_t get_offset_of_menuSong_6() { return static_cast<int32_t>(offsetof(SoundController_t1686593041, ___menuSong_6)); }
	inline AudioClip_t1932558630 * get_menuSong_6() const { return ___menuSong_6; }
	inline AudioClip_t1932558630 ** get_address_of_menuSong_6() { return &___menuSong_6; }
	inline void set_menuSong_6(AudioClip_t1932558630 * value)
	{
		___menuSong_6 = value;
		Il2CppCodeGenWriteBarrier(&___menuSong_6, value);
	}

	inline static int32_t get_offset_of_succesMission_7() { return static_cast<int32_t>(offsetof(SoundController_t1686593041, ___succesMission_7)); }
	inline AudioClip_t1932558630 * get_succesMission_7() const { return ___succesMission_7; }
	inline AudioClip_t1932558630 ** get_address_of_succesMission_7() { return &___succesMission_7; }
	inline void set_succesMission_7(AudioClip_t1932558630 * value)
	{
		___succesMission_7 = value;
		Il2CppCodeGenWriteBarrier(&___succesMission_7, value);
	}

	inline static int32_t get_offset_of_settingsMenu_8() { return static_cast<int32_t>(offsetof(SoundController_t1686593041, ___settingsMenu_8)); }
	inline GameObject_t1756533147 * get_settingsMenu_8() const { return ___settingsMenu_8; }
	inline GameObject_t1756533147 ** get_address_of_settingsMenu_8() { return &___settingsMenu_8; }
	inline void set_settingsMenu_8(GameObject_t1756533147 * value)
	{
		___settingsMenu_8 = value;
		Il2CppCodeGenWriteBarrier(&___settingsMenu_8, value);
	}

	inline static int32_t get_offset_of_soundControll_9() { return static_cast<int32_t>(offsetof(SoundController_t1686593041, ___soundControll_9)); }
	inline AudioSource_t1135106623 * get_soundControll_9() const { return ___soundControll_9; }
	inline AudioSource_t1135106623 ** get_address_of_soundControll_9() { return &___soundControll_9; }
	inline void set_soundControll_9(AudioSource_t1135106623 * value)
	{
		___soundControll_9 = value;
		Il2CppCodeGenWriteBarrier(&___soundControll_9, value);
	}

	inline static int32_t get_offset_of_backgroundSound_10() { return static_cast<int32_t>(offsetof(SoundController_t1686593041, ___backgroundSound_10)); }
	inline AudioSource_t1135106623 * get_backgroundSound_10() const { return ___backgroundSound_10; }
	inline AudioSource_t1135106623 ** get_address_of_backgroundSound_10() { return &___backgroundSound_10; }
	inline void set_backgroundSound_10(AudioSource_t1135106623 * value)
	{
		___backgroundSound_10 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSound_10, value);
	}

	inline static int32_t get_offset_of_onButton_11() { return static_cast<int32_t>(offsetof(SoundController_t1686593041, ___onButton_11)); }
	inline Sprite_t309593783 * get_onButton_11() const { return ___onButton_11; }
	inline Sprite_t309593783 ** get_address_of_onButton_11() { return &___onButton_11; }
	inline void set_onButton_11(Sprite_t309593783 * value)
	{
		___onButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___onButton_11, value);
	}

	inline static int32_t get_offset_of_offButton_12() { return static_cast<int32_t>(offsetof(SoundController_t1686593041, ___offButton_12)); }
	inline Sprite_t309593783 * get_offButton_12() const { return ___offButton_12; }
	inline Sprite_t309593783 ** get_address_of_offButton_12() { return &___offButton_12; }
	inline void set_offButton_12(Sprite_t309593783 * value)
	{
		___offButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___offButton_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
