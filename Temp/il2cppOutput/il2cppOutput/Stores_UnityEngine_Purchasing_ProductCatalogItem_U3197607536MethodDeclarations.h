﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.ProductCatalogItem/<>c__DisplayClass21_0
struct U3CU3Ec__DisplayClass21_0_t3197607536;
// UnityEngine.Purchasing.LocalizedProductDescription
struct LocalizedProductDescription_t1525635964;

#include "codegen/il2cpp-codegen.h"
#include "Stores_UnityEngine_Purchasing_LocalizedProductDesc1525635964.h"

// System.Void UnityEngine.Purchasing.ProductCatalogItem/<>c__DisplayClass21_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass21_0__ctor_m1461517511 (U3CU3Ec__DisplayClass21_0_t3197607536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.ProductCatalogItem/<>c__DisplayClass21_0::<GetDescription>b__0(UnityEngine.Purchasing.LocalizedProductDescription)
extern "C"  bool U3CU3Ec__DisplayClass21_0_U3CGetDescriptionU3Eb__0_m2466483054 (U3CU3Ec__DisplayClass21_0_t3197607536 * __this, LocalizedProductDescription_t1525635964 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
