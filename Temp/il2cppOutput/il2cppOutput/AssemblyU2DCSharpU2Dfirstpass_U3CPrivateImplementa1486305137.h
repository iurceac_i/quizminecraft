﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1500295812.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305144  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=244 <PrivateImplementationDetails>::$field-3D85E4B24A66D114F17C805C94137114D36C012F
	U24ArrayTypeU3D244_t1500295812  ___U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields, ___U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0)); }
	inline U24ArrayTypeU3D244_t1500295812  get_U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0() const { return ___U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0; }
	inline U24ArrayTypeU3D244_t1500295812 * get_address_of_U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0() { return &___U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0; }
	inline void set_U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0(U24ArrayTypeU3D244_t1500295812  value)
	{
		___U24fieldU2D3D85E4B24A66D114F17C805C94137114D36C012F_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
