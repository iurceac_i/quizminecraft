﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Purchasing.AppStore,System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>
struct Transform_1_t1303859131;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23907470188.h"
#include "Stores_UnityEngine_Purchasing_AppStore379104228.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Purchasing.AppStore,System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3229191350_gshared (Transform_1_t1303859131 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m3229191350(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1303859131 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3229191350_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Purchasing.AppStore,System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3907470188  Transform_1_Invoke_m2651604150_gshared (Transform_1_t1303859131 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2651604150(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t3907470188  (*) (Transform_1_t1303859131 *, int32_t, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m2651604150_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Purchasing.AppStore,System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m408094047_gshared (Transform_1_t1303859131 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m408094047(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1303859131 *, int32_t, Il2CppObject *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m408094047_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Purchasing.AppStore,System.Object,System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3907470188  Transform_1_EndInvoke_m3915697292_gshared (Transform_1_t1303859131 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m3915697292(__this, ___result0, method) ((  KeyValuePair_2_t3907470188  (*) (Transform_1_t1303859131 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3915697292_gshared)(__this, ___result0, method)
