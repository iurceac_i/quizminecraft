﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22
struct U3CRequestAuthCodeU3Ed__22_t1220668507;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::.ctor(System.Int32)
extern "C"  void U3CRequestAuthCodeU3Ed__22__ctor_m3033810391 (U3CRequestAuthCodeU3Ed__22_t1220668507 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::System.IDisposable.Dispose()
extern "C"  void U3CRequestAuthCodeU3Ed__22_System_IDisposable_Dispose_m3269006233 (U3CRequestAuthCodeU3Ed__22_t1220668507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::MoveNext()
extern "C"  bool U3CRequestAuthCodeU3Ed__22_MoveNext_m2675358552 (U3CRequestAuthCodeU3Ed__22_t1220668507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  Il2CppObject * U3CRequestAuthCodeU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1401429307 (U3CRequestAuthCodeU3Ed__22_t1220668507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::System.Collections.IEnumerator.Reset()
extern "C"  void U3CRequestAuthCodeU3Ed__22_System_Collections_IEnumerator_Reset_m813359318 (U3CRequestAuthCodeU3Ed__22_t1220668507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRequestAuthCodeU3Ed__22_System_Collections_IEnumerator_get_Current_m570025662 (U3CRequestAuthCodeU3Ed__22_t1220668507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
