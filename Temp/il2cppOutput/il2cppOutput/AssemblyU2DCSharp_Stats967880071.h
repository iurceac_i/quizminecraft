﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// SoundController
struct SoundController_t1686593041;
// MainGame
struct MainGame_t3800664731;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.String
struct String_t;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stats
struct  Stats_t967880071  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Stats::statsPanel
	GameObject_t1756533147 * ___statsPanel_2;
	// UnityEngine.GameObject Stats::backButton
	GameObject_t1756533147 * ___backButton_3;
	// UnityEngine.UI.Text Stats::totalLikes
	Text_t356221433 * ___totalLikes_4;
	// UnityEngine.UI.Text Stats::totalFollowers
	Text_t356221433 * ___totalFollowers_5;
	// UnityEngine.UI.Text Stats::totalLikesPortret
	Text_t356221433 * ___totalLikesPortret_6;
	// UnityEngine.UI.Text Stats::totalFollowersPortret
	Text_t356221433 * ___totalFollowersPortret_7;
	// UnityEngine.GameObject Stats::alertFailPanel
	GameObject_t1756533147 * ___alertFailPanel_8;
	// UnityEngine.GameObject Stats::alertCorrectPanel
	GameObject_t1756533147 * ___alertCorrectPanel_9;
	// UnityEngine.GameObject Stats::landscapeStatsPanel
	GameObject_t1756533147 * ___landscapeStatsPanel_10;
	// UnityEngine.GameObject Stats::PortraitStatsPanel
	GameObject_t1756533147 * ___PortraitStatsPanel_11;
	// SoundController Stats::soundController
	SoundController_t1686593041 * ___soundController_12;
	// MainGame Stats::mainGame
	MainGame_t3800664731 * ___mainGame_13;
	// UnityEngine.UI.Button Stats::btn_back_1
	Button_t2872111280 * ___btn_back_1_14;
	// UnityEngine.UI.Button Stats::btn_back_2
	Button_t2872111280 * ___btn_back_2_15;
	// System.String Stats::pathImageStats
	String_t* ___pathImageStats_16;
	// UnityEngine.UI.Image Stats::imageStats
	Image_t2042527209 * ___imageStats_17;
	// UnityEngine.UI.Image Stats::imagePortraitStats
	Image_t2042527209 * ___imagePortraitStats_18;
	// UnityEngine.Sprite Stats::defaultImage
	Sprite_t309593783 * ___defaultImage_19;

public:
	inline static int32_t get_offset_of_statsPanel_2() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___statsPanel_2)); }
	inline GameObject_t1756533147 * get_statsPanel_2() const { return ___statsPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_statsPanel_2() { return &___statsPanel_2; }
	inline void set_statsPanel_2(GameObject_t1756533147 * value)
	{
		___statsPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___statsPanel_2, value);
	}

	inline static int32_t get_offset_of_backButton_3() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___backButton_3)); }
	inline GameObject_t1756533147 * get_backButton_3() const { return ___backButton_3; }
	inline GameObject_t1756533147 ** get_address_of_backButton_3() { return &___backButton_3; }
	inline void set_backButton_3(GameObject_t1756533147 * value)
	{
		___backButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___backButton_3, value);
	}

	inline static int32_t get_offset_of_totalLikes_4() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___totalLikes_4)); }
	inline Text_t356221433 * get_totalLikes_4() const { return ___totalLikes_4; }
	inline Text_t356221433 ** get_address_of_totalLikes_4() { return &___totalLikes_4; }
	inline void set_totalLikes_4(Text_t356221433 * value)
	{
		___totalLikes_4 = value;
		Il2CppCodeGenWriteBarrier(&___totalLikes_4, value);
	}

	inline static int32_t get_offset_of_totalFollowers_5() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___totalFollowers_5)); }
	inline Text_t356221433 * get_totalFollowers_5() const { return ___totalFollowers_5; }
	inline Text_t356221433 ** get_address_of_totalFollowers_5() { return &___totalFollowers_5; }
	inline void set_totalFollowers_5(Text_t356221433 * value)
	{
		___totalFollowers_5 = value;
		Il2CppCodeGenWriteBarrier(&___totalFollowers_5, value);
	}

	inline static int32_t get_offset_of_totalLikesPortret_6() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___totalLikesPortret_6)); }
	inline Text_t356221433 * get_totalLikesPortret_6() const { return ___totalLikesPortret_6; }
	inline Text_t356221433 ** get_address_of_totalLikesPortret_6() { return &___totalLikesPortret_6; }
	inline void set_totalLikesPortret_6(Text_t356221433 * value)
	{
		___totalLikesPortret_6 = value;
		Il2CppCodeGenWriteBarrier(&___totalLikesPortret_6, value);
	}

	inline static int32_t get_offset_of_totalFollowersPortret_7() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___totalFollowersPortret_7)); }
	inline Text_t356221433 * get_totalFollowersPortret_7() const { return ___totalFollowersPortret_7; }
	inline Text_t356221433 ** get_address_of_totalFollowersPortret_7() { return &___totalFollowersPortret_7; }
	inline void set_totalFollowersPortret_7(Text_t356221433 * value)
	{
		___totalFollowersPortret_7 = value;
		Il2CppCodeGenWriteBarrier(&___totalFollowersPortret_7, value);
	}

	inline static int32_t get_offset_of_alertFailPanel_8() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___alertFailPanel_8)); }
	inline GameObject_t1756533147 * get_alertFailPanel_8() const { return ___alertFailPanel_8; }
	inline GameObject_t1756533147 ** get_address_of_alertFailPanel_8() { return &___alertFailPanel_8; }
	inline void set_alertFailPanel_8(GameObject_t1756533147 * value)
	{
		___alertFailPanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___alertFailPanel_8, value);
	}

	inline static int32_t get_offset_of_alertCorrectPanel_9() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___alertCorrectPanel_9)); }
	inline GameObject_t1756533147 * get_alertCorrectPanel_9() const { return ___alertCorrectPanel_9; }
	inline GameObject_t1756533147 ** get_address_of_alertCorrectPanel_9() { return &___alertCorrectPanel_9; }
	inline void set_alertCorrectPanel_9(GameObject_t1756533147 * value)
	{
		___alertCorrectPanel_9 = value;
		Il2CppCodeGenWriteBarrier(&___alertCorrectPanel_9, value);
	}

	inline static int32_t get_offset_of_landscapeStatsPanel_10() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___landscapeStatsPanel_10)); }
	inline GameObject_t1756533147 * get_landscapeStatsPanel_10() const { return ___landscapeStatsPanel_10; }
	inline GameObject_t1756533147 ** get_address_of_landscapeStatsPanel_10() { return &___landscapeStatsPanel_10; }
	inline void set_landscapeStatsPanel_10(GameObject_t1756533147 * value)
	{
		___landscapeStatsPanel_10 = value;
		Il2CppCodeGenWriteBarrier(&___landscapeStatsPanel_10, value);
	}

	inline static int32_t get_offset_of_PortraitStatsPanel_11() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___PortraitStatsPanel_11)); }
	inline GameObject_t1756533147 * get_PortraitStatsPanel_11() const { return ___PortraitStatsPanel_11; }
	inline GameObject_t1756533147 ** get_address_of_PortraitStatsPanel_11() { return &___PortraitStatsPanel_11; }
	inline void set_PortraitStatsPanel_11(GameObject_t1756533147 * value)
	{
		___PortraitStatsPanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___PortraitStatsPanel_11, value);
	}

	inline static int32_t get_offset_of_soundController_12() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___soundController_12)); }
	inline SoundController_t1686593041 * get_soundController_12() const { return ___soundController_12; }
	inline SoundController_t1686593041 ** get_address_of_soundController_12() { return &___soundController_12; }
	inline void set_soundController_12(SoundController_t1686593041 * value)
	{
		___soundController_12 = value;
		Il2CppCodeGenWriteBarrier(&___soundController_12, value);
	}

	inline static int32_t get_offset_of_mainGame_13() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___mainGame_13)); }
	inline MainGame_t3800664731 * get_mainGame_13() const { return ___mainGame_13; }
	inline MainGame_t3800664731 ** get_address_of_mainGame_13() { return &___mainGame_13; }
	inline void set_mainGame_13(MainGame_t3800664731 * value)
	{
		___mainGame_13 = value;
		Il2CppCodeGenWriteBarrier(&___mainGame_13, value);
	}

	inline static int32_t get_offset_of_btn_back_1_14() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___btn_back_1_14)); }
	inline Button_t2872111280 * get_btn_back_1_14() const { return ___btn_back_1_14; }
	inline Button_t2872111280 ** get_address_of_btn_back_1_14() { return &___btn_back_1_14; }
	inline void set_btn_back_1_14(Button_t2872111280 * value)
	{
		___btn_back_1_14 = value;
		Il2CppCodeGenWriteBarrier(&___btn_back_1_14, value);
	}

	inline static int32_t get_offset_of_btn_back_2_15() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___btn_back_2_15)); }
	inline Button_t2872111280 * get_btn_back_2_15() const { return ___btn_back_2_15; }
	inline Button_t2872111280 ** get_address_of_btn_back_2_15() { return &___btn_back_2_15; }
	inline void set_btn_back_2_15(Button_t2872111280 * value)
	{
		___btn_back_2_15 = value;
		Il2CppCodeGenWriteBarrier(&___btn_back_2_15, value);
	}

	inline static int32_t get_offset_of_pathImageStats_16() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___pathImageStats_16)); }
	inline String_t* get_pathImageStats_16() const { return ___pathImageStats_16; }
	inline String_t** get_address_of_pathImageStats_16() { return &___pathImageStats_16; }
	inline void set_pathImageStats_16(String_t* value)
	{
		___pathImageStats_16 = value;
		Il2CppCodeGenWriteBarrier(&___pathImageStats_16, value);
	}

	inline static int32_t get_offset_of_imageStats_17() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___imageStats_17)); }
	inline Image_t2042527209 * get_imageStats_17() const { return ___imageStats_17; }
	inline Image_t2042527209 ** get_address_of_imageStats_17() { return &___imageStats_17; }
	inline void set_imageStats_17(Image_t2042527209 * value)
	{
		___imageStats_17 = value;
		Il2CppCodeGenWriteBarrier(&___imageStats_17, value);
	}

	inline static int32_t get_offset_of_imagePortraitStats_18() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___imagePortraitStats_18)); }
	inline Image_t2042527209 * get_imagePortraitStats_18() const { return ___imagePortraitStats_18; }
	inline Image_t2042527209 ** get_address_of_imagePortraitStats_18() { return &___imagePortraitStats_18; }
	inline void set_imagePortraitStats_18(Image_t2042527209 * value)
	{
		___imagePortraitStats_18 = value;
		Il2CppCodeGenWriteBarrier(&___imagePortraitStats_18, value);
	}

	inline static int32_t get_offset_of_defaultImage_19() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___defaultImage_19)); }
	inline Sprite_t309593783 * get_defaultImage_19() const { return ___defaultImage_19; }
	inline Sprite_t309593783 ** get_address_of_defaultImage_19() { return &___defaultImage_19; }
	inline void set_defaultImage_19(Sprite_t309593783 * value)
	{
		___defaultImage_19 = value;
		Il2CppCodeGenWriteBarrier(&___defaultImage_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
