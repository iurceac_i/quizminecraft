﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LevelController
struct LevelController_t2717835266;
// Levels[]
struct LevelsU5BU5D_t2050899114;
// Question[]
struct QuestionU5BU5D_t1028533817;

#include "codegen/il2cpp-codegen.h"

// System.Void LevelController::.ctor()
extern "C"  void LevelController__ctor_m3784088425 (LevelController_t2717835266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelController::Start()
extern "C"  void LevelController_Start_m1756945929 (LevelController_t2717835266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Levels[] LevelController::GetLevels(Question[])
extern "C"  LevelsU5BU5D_t2050899114* LevelController_GetLevels_m3045393308 (LevelController_t2717835266 * __this, QuestionU5BU5D_t1028533817* ___quests0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelController::SetUpAnswers(Levels[])
extern "C"  void LevelController_SetUpAnswers_m4148261728 (LevelController_t2717835266 * __this, LevelsU5BU5D_t2050899114* ___levels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelController::DetectOnEnableLevel(Levels[])
extern "C"  void LevelController_DetectOnEnableLevel_m1065078623 (LevelController_t2717835266 * __this, LevelsU5BU5D_t2050899114* ___levels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelController::onItemClicked(System.Int32)
extern "C"  void LevelController_onItemClicked_m3049872075 (LevelController_t2717835266 * __this, int32_t ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelController::onUpdateAdapter()
extern "C"  void LevelController_onUpdateAdapter_m1165209206 (LevelController_t2717835266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
