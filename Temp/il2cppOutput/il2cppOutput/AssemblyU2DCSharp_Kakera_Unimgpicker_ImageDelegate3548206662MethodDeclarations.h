﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Kakera.Unimgpicker/ImageDelegate
struct ImageDelegate_t3548206662;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Kakera.Unimgpicker/ImageDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ImageDelegate__ctor_m1094922347 (ImageDelegate_t3548206662 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Unimgpicker/ImageDelegate::Invoke(System.String)
extern "C"  void ImageDelegate_Invoke_m2935186509 (ImageDelegate_t3548206662 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Kakera.Unimgpicker/ImageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ImageDelegate_BeginInvoke_m662071400 (ImageDelegate_t3548206662 * __this, String_t* ___path0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Unimgpicker/ImageDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ImageDelegate_EndInvoke_m1638926857 (ImageDelegate_t3548206662 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
