﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.ConfigurationBuilder
struct ConfigurationBuilder_t1298400415;
// System.Action
struct Action_t3226471752;
// IAPDemo
struct IAPDemo_t823941185;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/<Awake>c__AnonStorey0
struct  U3CAwakeU3Ec__AnonStorey0_t2163001806  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.ConfigurationBuilder IAPDemo/<Awake>c__AnonStorey0::builder
	ConfigurationBuilder_t1298400415 * ___builder_0;
	// System.Action IAPDemo/<Awake>c__AnonStorey0::initializeUnityIap
	Action_t3226471752 * ___initializeUnityIap_1;
	// IAPDemo IAPDemo/<Awake>c__AnonStorey0::$this
	IAPDemo_t823941185 * ___U24this_2;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t2163001806, ___builder_0)); }
	inline ConfigurationBuilder_t1298400415 * get_builder_0() const { return ___builder_0; }
	inline ConfigurationBuilder_t1298400415 ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(ConfigurationBuilder_t1298400415 * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier(&___builder_0, value);
	}

	inline static int32_t get_offset_of_initializeUnityIap_1() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t2163001806, ___initializeUnityIap_1)); }
	inline Action_t3226471752 * get_initializeUnityIap_1() const { return ___initializeUnityIap_1; }
	inline Action_t3226471752 ** get_address_of_initializeUnityIap_1() { return &___initializeUnityIap_1; }
	inline void set_initializeUnityIap_1(Action_t3226471752 * value)
	{
		___initializeUnityIap_1 = value;
		Il2CppCodeGenWriteBarrier(&___initializeUnityIap_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t2163001806, ___U24this_2)); }
	inline IAPDemo_t823941185 * get_U24this_2() const { return ___U24this_2; }
	inline IAPDemo_t823941185 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(IAPDemo_t823941185 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
