﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t505099;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_0__ctor_m2464233274 (U3CU3Ec__DisplayClass7_0_t505099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0::<Purchase>b__0(System.Boolean,System.String)
extern "C"  void U3CU3Ec__DisplayClass7_0_U3CPurchaseU3Eb__0_m2251081206 (U3CU3Ec__DisplayClass7_0_t505099 * __this, bool ___purchaseSuccess0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
