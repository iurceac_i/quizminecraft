﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// readHTML
struct readHTML_t503794377;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void readHTML::.ctor()
extern "C"  void readHTML__ctor_m2799608704 (readHTML_t503794377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void readHTML::Start()
extern "C"  void readHTML_Start_m3205736136 (readHTML_t503794377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void readHTML::EnableLegasyInfo(UnityEngine.GameObject)
extern "C"  void readHTML_EnableLegasyInfo_m2020288232 (readHTML_t503794377 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void readHTML::StartAnimation(System.Int32)
extern "C"  void readHTML_StartAnimation_m4241922343 (readHTML_t503794377 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void readHTML::SelectSubscription()
extern "C"  void readHTML_SelectSubscription_m442030927 (readHTML_t503794377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void readHTML::SelectPrivacy()
extern "C"  void readHTML_SelectPrivacy_m996667200 (readHTML_t503794377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void readHTML::SelectTerms()
extern "C"  void readHTML_SelectTerms_m2844936593 (readHTML_t503794377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void readHTML::ActiveBtn()
extern "C"  void readHTML_ActiveBtn_m2177739882 (readHTML_t503794377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
