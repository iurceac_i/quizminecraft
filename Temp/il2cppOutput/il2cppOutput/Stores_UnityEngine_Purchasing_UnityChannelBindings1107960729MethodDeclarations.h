﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_t1107960729;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass16_0__ctor_m1806464238 (U3CU3Ec__DisplayClass16_0_t1107960729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0::<ValidateReceipt>b__0()
extern "C"  void U3CU3Ec__DisplayClass16_0_U3CValidateReceiptU3Eb__0_m573116822 (U3CU3Ec__DisplayClass16_0_t1107960729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
