﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>
struct KeyCollection_t43688145;
// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>
struct Dictionary_2_t1855157670;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.AppStore>
struct IEnumerator_1_t2149595351;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Purchasing.AppStore[]
struct AppStoreU5BU5D_t3188282381;

#include "codegen/il2cpp-codegen.h"
#include "Stores_UnityEngine_Purchasing_AppStore379104228.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke249693812.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m590569093_gshared (KeyCollection_t43688145 * __this, Dictionary_2_t1855157670 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m590569093(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t43688145 *, Dictionary_2_t1855157670 *, const MethodInfo*))KeyCollection__ctor_m590569093_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1156272491_gshared (KeyCollection_t43688145 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1156272491(__this, ___item0, method) ((  void (*) (KeyCollection_t43688145 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1156272491_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3197456066_gshared (KeyCollection_t43688145 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3197456066(__this, method) ((  void (*) (KeyCollection_t43688145 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3197456066_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1547453893_gshared (KeyCollection_t43688145 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1547453893(__this, ___item0, method) ((  bool (*) (KeyCollection_t43688145 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1547453893_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m111470242_gshared (KeyCollection_t43688145 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m111470242(__this, ___item0, method) ((  bool (*) (KeyCollection_t43688145 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m111470242_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2808543900_gshared (KeyCollection_t43688145 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2808543900(__this, method) ((  Il2CppObject* (*) (KeyCollection_t43688145 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2808543900_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3488596196_gshared (KeyCollection_t43688145 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3488596196(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t43688145 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3488596196_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1291148883_gshared (KeyCollection_t43688145 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1291148883(__this, method) ((  Il2CppObject * (*) (KeyCollection_t43688145 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1291148883_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m917965996_gshared (KeyCollection_t43688145 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m917965996(__this, method) ((  bool (*) (KeyCollection_t43688145 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m917965996_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1234865824_gshared (KeyCollection_t43688145 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1234865824(__this, method) ((  bool (*) (KeyCollection_t43688145 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1234865824_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1518127764_gshared (KeyCollection_t43688145 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1518127764(__this, method) ((  Il2CppObject * (*) (KeyCollection_t43688145 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1518127764_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m36123398_gshared (KeyCollection_t43688145 * __this, AppStoreU5BU5D_t3188282381* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m36123398(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t43688145 *, AppStoreU5BU5D_t3188282381*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m36123398_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::GetEnumerator()
extern "C"  Enumerator_t249693812  KeyCollection_GetEnumerator_m3405000929_gshared (KeyCollection_t43688145 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3405000929(__this, method) ((  Enumerator_t249693812  (*) (KeyCollection_t43688145 *, const MethodInfo*))KeyCollection_GetEnumerator_m3405000929_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m522341384_gshared (KeyCollection_t43688145 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m522341384(__this, method) ((  int32_t (*) (KeyCollection_t43688145 *, const MethodInfo*))KeyCollection_get_Count_m522341384_gshared)(__this, method)
