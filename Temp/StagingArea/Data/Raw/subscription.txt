
<b>Subscription Info</b>
	
This entry was written in English (UK). To the extent any translated version of this agreement conflicts with the English version, the English version controls.

Our company respect the privacy of our customers, and take our duty to protect your information very seriously. The following policy briefly outlines the personally identifiable information that we collect from our customers and prospective customers, and the steps that we take to safeguard their privacy.

<b>How We Collect and Make Use of Your Information</b>
Our app does not share or store any kind of personal information collected from the user account anywhere but on the device on which the app is installed and when signed into  the online control panel. By using this app, you consent to this privacy policy.

<b>Sharing and/or Disclosing Information</b>
Information about our customers is not an important part of our business. We DO NOT sell or share our customers information.

<b>Our Spam Policy</b>
We will not engage in spamming, and the use of our services for spamming will not be tolerated. We would encourage anyone receiving spam purporting to be from us, or anyone witnessing one of our customers using our service to spam others, to report the incident to us.

<b>Recording</b>
Call Recorder will store your recordings on your device, you can see al of them in the "Recordings" compartment from the app. We don't store them on our servers because you have the opportunity to share them directly from our app to your clouds or mail. You can do this by selecting the record you want to send and press the share button from the top right corner.

<b>Changes to This Policy</b>
Please be aware that our privacy policy may change from time to time. A current, time-stamped version will always be available on this page. Where changes are deemed significant we will endeavour to provide a more prominent notice or may contact our customers directly.


<b>Privacy Policy</b>
	
This entry was written in English (UK). To the extent any translated version of this agreement conflicts with the English version, the English version controls.

Our company respect the privacy of our customers, and take our duty to protect your information very seriously. The following policy briefly outlines the personally identifiable information that we collect from our customers and prospective customers, and the steps that we take to safeguard their privacy.

<b>How We Collect and Make Use of Your Information</b>
Our app does not share or store any kind of personal information collected from the user account anywhere but on the device on which the app is installed and when signed into  the online control panel. By using this app, you consent to this privacy policy.

<b>Sharing and/or Disclosing Information</b>
Information about our customers is not an important part of our business. We DO NOT sell or share our customers information.

<b>Our Spam Policy</b>
We will not engage in spamming, and the use of our services for spamming will not be tolerated. We would encourage anyone receiving spam purporting to be from us, or anyone witnessing one of our customers using our service to spam others, to report the incident to us.

<b>Recording</b>
Call Recorder will store your recordings on your device, you can see al of them in the "Recordings" compartment from the app. We don't store them on our servers because you have the opportunity to share them directly from our app to your clouds or mail. You can do this by selecting the record you want to send and press the share button from the top right corner.

<b>Changes to This Policy</b>
Please be aware that our privacy policy may change from time to time. A current, time-stamped version will always be available on this page. Where changes are deemed significant we will endeavour to provide a more prominent notice or may contact our customers directly.

