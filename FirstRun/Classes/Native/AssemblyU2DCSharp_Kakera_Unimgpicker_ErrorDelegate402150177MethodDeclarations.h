﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Kakera.Unimgpicker/ErrorDelegate
struct ErrorDelegate_t402150177;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Kakera.Unimgpicker/ErrorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ErrorDelegate__ctor_m331128292 (ErrorDelegate_t402150177 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Unimgpicker/ErrorDelegate::Invoke(System.String)
extern "C"  void ErrorDelegate_Invoke_m2011956720 (ErrorDelegate_t402150177 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Kakera.Unimgpicker/ErrorDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ErrorDelegate_BeginInvoke_m4179556173 (ErrorDelegate_t402150177 * __this, String_t* ___message0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Unimgpicker/ErrorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ErrorDelegate_EndInvoke_m40295598 (ErrorDelegate_t402150177 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
