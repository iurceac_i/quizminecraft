﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23
struct U3CStartPurchasePollingU3Ed__23_t2291090002;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::.ctor(System.Int32)
extern "C"  void U3CStartPurchasePollingU3Ed__23__ctor_m4123815566 (U3CStartPurchasePollingU3Ed__23_t2291090002 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::System.IDisposable.Dispose()
extern "C"  void U3CStartPurchasePollingU3Ed__23_System_IDisposable_Dispose_m324927154 (U3CStartPurchasePollingU3Ed__23_t2291090002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::MoveNext()
extern "C"  bool U3CStartPurchasePollingU3Ed__23_MoveNext_m2920364025 (U3CStartPurchasePollingU3Ed__23_t2291090002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  Il2CppObject * U3CStartPurchasePollingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4182791360 (U3CStartPurchasePollingU3Ed__23_t2291090002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::System.Collections.IEnumerator.Reset()
extern "C"  void U3CStartPurchasePollingU3Ed__23_System_Collections_IEnumerator_Reset_m2393313825 (U3CStartPurchasePollingU3Ed__23_t2291090002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartPurchasePollingU3Ed__23_System_Collections_IEnumerator_get_Current_m3453876085 (U3CStartPurchasePollingU3Ed__23_t2291090002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
