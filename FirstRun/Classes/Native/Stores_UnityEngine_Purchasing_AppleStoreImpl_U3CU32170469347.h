﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.Extension.ProductDescription
struct ProductDescription_t3318267523;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_t2170469347  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.Extension.ProductDescription UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass19_0::productDescription
	ProductDescription_t3318267523 * ___productDescription_0;

public:
	inline static int32_t get_offset_of_productDescription_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t2170469347, ___productDescription_0)); }
	inline ProductDescription_t3318267523 * get_productDescription_0() const { return ___productDescription_0; }
	inline ProductDescription_t3318267523 ** get_address_of_productDescription_0() { return &___productDescription_0; }
	inline void set_productDescription_0(ProductDescription_t3318267523 * value)
	{
		___productDescription_0 = value;
		Il2CppCodeGenWriteBarrier(&___productDescription_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
