﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.LocalizedProductDescription/<>c
struct U3CU3Ec_t3617267505;
// System.String
struct String_t;
// System.Text.RegularExpressions.Match
struct Match_t3164245899;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Text_RegularExpressions_Match3164245899.h"

// System.Void UnityEngine.Purchasing.LocalizedProductDescription/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m4243717079 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.LocalizedProductDescription/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m1799456440 (U3CU3Ec_t3617267505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.LocalizedProductDescription/<>c::<DecodeNonLatinCharacters>b__11_0(System.Text.RegularExpressions.Match)
extern "C"  String_t* U3CU3Ec_U3CDecodeNonLatinCharactersU3Eb__11_0_m2273619111 (U3CU3Ec_t3617267505 * __this, Match_t3164245899 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
