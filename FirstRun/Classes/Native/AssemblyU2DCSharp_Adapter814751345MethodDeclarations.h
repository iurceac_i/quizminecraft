﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Adapter
struct Adapter_t814751345;
// Levels[]
struct LevelsU5BU5D_t2050899114;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Adapter/OnItemClickListener
struct OnItemClickListener_t418219798;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void Adapter::.ctor()
extern "C"  void Adapter__ctor_m1806919526 (Adapter_t814751345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Adapter::setAdapter(Levels[])
extern "C"  void Adapter_setAdapter_m3137295986 (Adapter_t814751345 * __this, LevelsU5BU5D_t2050899114* ___levels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Adapter::UpdateAdapter(Levels[])
extern "C"  void Adapter_UpdateAdapter_m349117033 (Adapter_t814751345 * __this, LevelsU5BU5D_t2050899114* ___levels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Adapter::OnItemClick(UnityEngine.GameObject)
extern "C"  void Adapter_OnItemClick_m3700375114 (Adapter_t814751345 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Adapter::registerListener(Adapter/OnItemClickListener)
extern "C"  void Adapter_registerListener_m1706720079 (Adapter_t814751345 * __this, Il2CppObject * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
