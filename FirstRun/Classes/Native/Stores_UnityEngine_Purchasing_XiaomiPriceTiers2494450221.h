﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.XiaomiPriceTiers
struct  XiaomiPriceTiers_t2494450221  : public Il2CppObject
{
public:

public:
};

struct XiaomiPriceTiers_t2494450221_StaticFields
{
public:
	// System.Int32[] UnityEngine.Purchasing.XiaomiPriceTiers::XiaomiPriceTierPrices
	Int32U5BU5D_t3030399641* ___XiaomiPriceTierPrices_0;

public:
	inline static int32_t get_offset_of_XiaomiPriceTierPrices_0() { return static_cast<int32_t>(offsetof(XiaomiPriceTiers_t2494450221_StaticFields, ___XiaomiPriceTierPrices_0)); }
	inline Int32U5BU5D_t3030399641* get_XiaomiPriceTierPrices_0() const { return ___XiaomiPriceTierPrices_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_XiaomiPriceTierPrices_0() { return &___XiaomiPriceTierPrices_0; }
	inline void set_XiaomiPriceTierPrices_0(Int32U5BU5D_t3030399641* value)
	{
		___XiaomiPriceTierPrices_0 = value;
		Il2CppCodeGenWriteBarrier(&___XiaomiPriceTierPrices_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
