﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main
struct  Main_t2809994845  : public Il2CppObject
{
public:
	// System.String Main::product_id
	String_t* ___product_id_0;
	// System.String Main::store_id
	String_t* ___store_id_1;
	// System.String Main::store_App_url
	String_t* ___store_App_url_2;

public:
	inline static int32_t get_offset_of_product_id_0() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___product_id_0)); }
	inline String_t* get_product_id_0() const { return ___product_id_0; }
	inline String_t** get_address_of_product_id_0() { return &___product_id_0; }
	inline void set_product_id_0(String_t* value)
	{
		___product_id_0 = value;
		Il2CppCodeGenWriteBarrier(&___product_id_0, value);
	}

	inline static int32_t get_offset_of_store_id_1() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___store_id_1)); }
	inline String_t* get_store_id_1() const { return ___store_id_1; }
	inline String_t** get_address_of_store_id_1() { return &___store_id_1; }
	inline void set_store_id_1(String_t* value)
	{
		___store_id_1 = value;
		Il2CppCodeGenWriteBarrier(&___store_id_1, value);
	}

	inline static int32_t get_offset_of_store_App_url_2() { return static_cast<int32_t>(offsetof(Main_t2809994845, ___store_App_url_2)); }
	inline String_t* get_store_App_url_2() const { return ___store_App_url_2; }
	inline String_t** get_address_of_store_App_url_2() { return &___store_App_url_2; }
	inline void set_store_App_url_2(String_t* value)
	{
		___store_App_url_2 = value;
		Il2CppCodeGenWriteBarrier(&___store_App_url_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
