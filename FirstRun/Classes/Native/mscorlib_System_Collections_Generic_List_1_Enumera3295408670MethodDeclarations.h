﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.ProductCatalogPayout>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1788585414(__this, ___l0, method) ((  void (*) (Enumerator_t3295408670 *, List_1_t3760678996 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m554123172(__this, method) ((  void (*) (Enumerator_t3295408670 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.ProductCatalogPayout>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2626153072(__this, method) ((  Il2CppObject * (*) (Enumerator_t3295408670 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.ProductCatalogPayout>::Dispose()
#define Enumerator_Dispose_m3499486387(__this, method) ((  void (*) (Enumerator_t3295408670 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.ProductCatalogPayout>::VerifyState()
#define Enumerator_VerifyState_m2581772856(__this, method) ((  void (*) (Enumerator_t3295408670 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.ProductCatalogPayout>::MoveNext()
#define Enumerator_MoveNext_m1881927196(__this, method) ((  bool (*) (Enumerator_t3295408670 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.ProductCatalogPayout>::get_Current()
#define Enumerator_get_Current_m949551691(__this, method) ((  ProductCatalogPayout_t96590568 * (*) (Enumerator_t3295408670 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
