﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Util
struct Util_t4006552276;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"

// System.Void Util::.ctor()
extern "C"  void Util__ctor_m4057728895 (Util_t4006552276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::reshuffle(System.String[])
extern "C"  void Util_reshuffle_m3930049179 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___texts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
