﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.FakeStore/<>c__DisplayClass13_0
struct U3CU3Ec__DisplayClass13_0_t2968753776;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"

// System.Void UnityEngine.Purchasing.FakeStore/<>c__DisplayClass13_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass13_0__ctor_m4104139561 (U3CU3Ec__DisplayClass13_0_t2968753776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.FakeStore/<>c__DisplayClass13_0::<Purchase>b__0(System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason)
extern "C"  void U3CU3Ec__DisplayClass13_0_U3CPurchaseU3Eb__0_m468577857 (U3CU3Ec__DisplayClass13_0_t2968753776 * __this, bool ___allow0, int32_t ___failureReason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
