﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// QuestionController/UpdateListener
struct UpdateListener_t3189012468;
// System.String
struct String_t;
// Levels[]
struct LevelsU5BU5D_t2050899114;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestionController
struct  QuestionController_t445239244  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text QuestionController::question
	Text_t356221433 * ___question_2;
	// UnityEngine.UI.Text QuestionController::answer_0
	Text_t356221433 * ___answer_0_3;
	// UnityEngine.UI.Text QuestionController::answer_1
	Text_t356221433 * ___answer_1_4;
	// UnityEngine.UI.Text QuestionController::answer_2
	Text_t356221433 * ___answer_2_5;
	// UnityEngine.UI.Text QuestionController::answer_3
	Text_t356221433 * ___answer_3_6;
	// UnityEngine.GameObject QuestionController::star1
	GameObject_t1756533147 * ___star1_7;
	// UnityEngine.GameObject QuestionController::star2
	GameObject_t1756533147 * ___star2_8;
	// UnityEngine.GameObject QuestionController::star3
	GameObject_t1756533147 * ___star3_9;
	// QuestionController/UpdateListener QuestionController::callBack
	Il2CppObject * ___callBack_10;
	// System.String QuestionController::correctAns
	String_t* ___correctAns_11;
	// UnityEngine.GameObject QuestionController::oneButton
	GameObject_t1756533147 * ___oneButton_12;
	// UnityEngine.GameObject QuestionController::twoButtons
	GameObject_t1756533147 * ___twoButtons_13;
	// UnityEngine.GameObject QuestionController::oneFinButton
	GameObject_t1756533147 * ___oneFinButton_14;
	// UnityEngine.GameObject QuestionController::singleButtonPanel
	GameObject_t1756533147 * ___singleButtonPanel_15;
	// UnityEngine.GameObject QuestionController::finalAlertPanel
	GameObject_t1756533147 * ___finalAlertPanel_16;
	// UnityEngine.GameObject QuestionController::levelPanel
	GameObject_t1756533147 * ___levelPanel_17;
	// UnityEngine.UI.Text QuestionController::numb_of_compl_mission
	Text_t356221433 * ___numb_of_compl_mission_18;
	// UnityEngine.UI.Text QuestionController::numb_of_compl_mission_2
	Text_t356221433 * ___numb_of_compl_mission_2_19;
	// System.Int32 QuestionController::numQuest
	int32_t ___numQuest_20;
	// Levels[] QuestionController::levelGlobal
	LevelsU5BU5D_t2050899114* ___levelGlobal_21;
	// System.Int32 QuestionController::positionLevel
	int32_t ___positionLevel_22;
	// System.Int32 QuestionController::correctAnswer
	int32_t ___correctAnswer_23;
	// System.Boolean QuestionController::isPurchase
	bool ___isPurchase_24;

public:
	inline static int32_t get_offset_of_question_2() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___question_2)); }
	inline Text_t356221433 * get_question_2() const { return ___question_2; }
	inline Text_t356221433 ** get_address_of_question_2() { return &___question_2; }
	inline void set_question_2(Text_t356221433 * value)
	{
		___question_2 = value;
		Il2CppCodeGenWriteBarrier(&___question_2, value);
	}

	inline static int32_t get_offset_of_answer_0_3() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___answer_0_3)); }
	inline Text_t356221433 * get_answer_0_3() const { return ___answer_0_3; }
	inline Text_t356221433 ** get_address_of_answer_0_3() { return &___answer_0_3; }
	inline void set_answer_0_3(Text_t356221433 * value)
	{
		___answer_0_3 = value;
		Il2CppCodeGenWriteBarrier(&___answer_0_3, value);
	}

	inline static int32_t get_offset_of_answer_1_4() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___answer_1_4)); }
	inline Text_t356221433 * get_answer_1_4() const { return ___answer_1_4; }
	inline Text_t356221433 ** get_address_of_answer_1_4() { return &___answer_1_4; }
	inline void set_answer_1_4(Text_t356221433 * value)
	{
		___answer_1_4 = value;
		Il2CppCodeGenWriteBarrier(&___answer_1_4, value);
	}

	inline static int32_t get_offset_of_answer_2_5() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___answer_2_5)); }
	inline Text_t356221433 * get_answer_2_5() const { return ___answer_2_5; }
	inline Text_t356221433 ** get_address_of_answer_2_5() { return &___answer_2_5; }
	inline void set_answer_2_5(Text_t356221433 * value)
	{
		___answer_2_5 = value;
		Il2CppCodeGenWriteBarrier(&___answer_2_5, value);
	}

	inline static int32_t get_offset_of_answer_3_6() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___answer_3_6)); }
	inline Text_t356221433 * get_answer_3_6() const { return ___answer_3_6; }
	inline Text_t356221433 ** get_address_of_answer_3_6() { return &___answer_3_6; }
	inline void set_answer_3_6(Text_t356221433 * value)
	{
		___answer_3_6 = value;
		Il2CppCodeGenWriteBarrier(&___answer_3_6, value);
	}

	inline static int32_t get_offset_of_star1_7() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___star1_7)); }
	inline GameObject_t1756533147 * get_star1_7() const { return ___star1_7; }
	inline GameObject_t1756533147 ** get_address_of_star1_7() { return &___star1_7; }
	inline void set_star1_7(GameObject_t1756533147 * value)
	{
		___star1_7 = value;
		Il2CppCodeGenWriteBarrier(&___star1_7, value);
	}

	inline static int32_t get_offset_of_star2_8() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___star2_8)); }
	inline GameObject_t1756533147 * get_star2_8() const { return ___star2_8; }
	inline GameObject_t1756533147 ** get_address_of_star2_8() { return &___star2_8; }
	inline void set_star2_8(GameObject_t1756533147 * value)
	{
		___star2_8 = value;
		Il2CppCodeGenWriteBarrier(&___star2_8, value);
	}

	inline static int32_t get_offset_of_star3_9() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___star3_9)); }
	inline GameObject_t1756533147 * get_star3_9() const { return ___star3_9; }
	inline GameObject_t1756533147 ** get_address_of_star3_9() { return &___star3_9; }
	inline void set_star3_9(GameObject_t1756533147 * value)
	{
		___star3_9 = value;
		Il2CppCodeGenWriteBarrier(&___star3_9, value);
	}

	inline static int32_t get_offset_of_callBack_10() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___callBack_10)); }
	inline Il2CppObject * get_callBack_10() const { return ___callBack_10; }
	inline Il2CppObject ** get_address_of_callBack_10() { return &___callBack_10; }
	inline void set_callBack_10(Il2CppObject * value)
	{
		___callBack_10 = value;
		Il2CppCodeGenWriteBarrier(&___callBack_10, value);
	}

	inline static int32_t get_offset_of_correctAns_11() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___correctAns_11)); }
	inline String_t* get_correctAns_11() const { return ___correctAns_11; }
	inline String_t** get_address_of_correctAns_11() { return &___correctAns_11; }
	inline void set_correctAns_11(String_t* value)
	{
		___correctAns_11 = value;
		Il2CppCodeGenWriteBarrier(&___correctAns_11, value);
	}

	inline static int32_t get_offset_of_oneButton_12() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___oneButton_12)); }
	inline GameObject_t1756533147 * get_oneButton_12() const { return ___oneButton_12; }
	inline GameObject_t1756533147 ** get_address_of_oneButton_12() { return &___oneButton_12; }
	inline void set_oneButton_12(GameObject_t1756533147 * value)
	{
		___oneButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___oneButton_12, value);
	}

	inline static int32_t get_offset_of_twoButtons_13() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___twoButtons_13)); }
	inline GameObject_t1756533147 * get_twoButtons_13() const { return ___twoButtons_13; }
	inline GameObject_t1756533147 ** get_address_of_twoButtons_13() { return &___twoButtons_13; }
	inline void set_twoButtons_13(GameObject_t1756533147 * value)
	{
		___twoButtons_13 = value;
		Il2CppCodeGenWriteBarrier(&___twoButtons_13, value);
	}

	inline static int32_t get_offset_of_oneFinButton_14() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___oneFinButton_14)); }
	inline GameObject_t1756533147 * get_oneFinButton_14() const { return ___oneFinButton_14; }
	inline GameObject_t1756533147 ** get_address_of_oneFinButton_14() { return &___oneFinButton_14; }
	inline void set_oneFinButton_14(GameObject_t1756533147 * value)
	{
		___oneFinButton_14 = value;
		Il2CppCodeGenWriteBarrier(&___oneFinButton_14, value);
	}

	inline static int32_t get_offset_of_singleButtonPanel_15() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___singleButtonPanel_15)); }
	inline GameObject_t1756533147 * get_singleButtonPanel_15() const { return ___singleButtonPanel_15; }
	inline GameObject_t1756533147 ** get_address_of_singleButtonPanel_15() { return &___singleButtonPanel_15; }
	inline void set_singleButtonPanel_15(GameObject_t1756533147 * value)
	{
		___singleButtonPanel_15 = value;
		Il2CppCodeGenWriteBarrier(&___singleButtonPanel_15, value);
	}

	inline static int32_t get_offset_of_finalAlertPanel_16() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___finalAlertPanel_16)); }
	inline GameObject_t1756533147 * get_finalAlertPanel_16() const { return ___finalAlertPanel_16; }
	inline GameObject_t1756533147 ** get_address_of_finalAlertPanel_16() { return &___finalAlertPanel_16; }
	inline void set_finalAlertPanel_16(GameObject_t1756533147 * value)
	{
		___finalAlertPanel_16 = value;
		Il2CppCodeGenWriteBarrier(&___finalAlertPanel_16, value);
	}

	inline static int32_t get_offset_of_levelPanel_17() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___levelPanel_17)); }
	inline GameObject_t1756533147 * get_levelPanel_17() const { return ___levelPanel_17; }
	inline GameObject_t1756533147 ** get_address_of_levelPanel_17() { return &___levelPanel_17; }
	inline void set_levelPanel_17(GameObject_t1756533147 * value)
	{
		___levelPanel_17 = value;
		Il2CppCodeGenWriteBarrier(&___levelPanel_17, value);
	}

	inline static int32_t get_offset_of_numb_of_compl_mission_18() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___numb_of_compl_mission_18)); }
	inline Text_t356221433 * get_numb_of_compl_mission_18() const { return ___numb_of_compl_mission_18; }
	inline Text_t356221433 ** get_address_of_numb_of_compl_mission_18() { return &___numb_of_compl_mission_18; }
	inline void set_numb_of_compl_mission_18(Text_t356221433 * value)
	{
		___numb_of_compl_mission_18 = value;
		Il2CppCodeGenWriteBarrier(&___numb_of_compl_mission_18, value);
	}

	inline static int32_t get_offset_of_numb_of_compl_mission_2_19() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___numb_of_compl_mission_2_19)); }
	inline Text_t356221433 * get_numb_of_compl_mission_2_19() const { return ___numb_of_compl_mission_2_19; }
	inline Text_t356221433 ** get_address_of_numb_of_compl_mission_2_19() { return &___numb_of_compl_mission_2_19; }
	inline void set_numb_of_compl_mission_2_19(Text_t356221433 * value)
	{
		___numb_of_compl_mission_2_19 = value;
		Il2CppCodeGenWriteBarrier(&___numb_of_compl_mission_2_19, value);
	}

	inline static int32_t get_offset_of_numQuest_20() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___numQuest_20)); }
	inline int32_t get_numQuest_20() const { return ___numQuest_20; }
	inline int32_t* get_address_of_numQuest_20() { return &___numQuest_20; }
	inline void set_numQuest_20(int32_t value)
	{
		___numQuest_20 = value;
	}

	inline static int32_t get_offset_of_levelGlobal_21() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___levelGlobal_21)); }
	inline LevelsU5BU5D_t2050899114* get_levelGlobal_21() const { return ___levelGlobal_21; }
	inline LevelsU5BU5D_t2050899114** get_address_of_levelGlobal_21() { return &___levelGlobal_21; }
	inline void set_levelGlobal_21(LevelsU5BU5D_t2050899114* value)
	{
		___levelGlobal_21 = value;
		Il2CppCodeGenWriteBarrier(&___levelGlobal_21, value);
	}

	inline static int32_t get_offset_of_positionLevel_22() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___positionLevel_22)); }
	inline int32_t get_positionLevel_22() const { return ___positionLevel_22; }
	inline int32_t* get_address_of_positionLevel_22() { return &___positionLevel_22; }
	inline void set_positionLevel_22(int32_t value)
	{
		___positionLevel_22 = value;
	}

	inline static int32_t get_offset_of_correctAnswer_23() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___correctAnswer_23)); }
	inline int32_t get_correctAnswer_23() const { return ___correctAnswer_23; }
	inline int32_t* get_address_of_correctAnswer_23() { return &___correctAnswer_23; }
	inline void set_correctAnswer_23(int32_t value)
	{
		___correctAnswer_23 = value;
	}

	inline static int32_t get_offset_of_isPurchase_24() { return static_cast<int32_t>(offsetof(QuestionController_t445239244, ___isPurchase_24)); }
	inline bool get_isPurchase_24() const { return ___isPurchase_24; }
	inline bool* get_address_of_isPurchase_24() { return &___isPurchase_24; }
	inline void set_isPurchase_24(bool value)
	{
		___isPurchase_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
