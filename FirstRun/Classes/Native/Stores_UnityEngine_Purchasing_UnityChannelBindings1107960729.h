﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Purchasing.UnityChannelBindings
struct UnityChannelBindings_t2880355556;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t1107960729  : public Il2CppObject
{
public:
	// System.String UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0::transactionId
	String_t* ___transactionId_0;
	// UnityEngine.Purchasing.UnityChannelBindings UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0::<>4__this
	UnityChannelBindings_t2880355556 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_transactionId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t1107960729, ___transactionId_0)); }
	inline String_t* get_transactionId_0() const { return ___transactionId_0; }
	inline String_t** get_address_of_transactionId_0() { return &___transactionId_0; }
	inline void set_transactionId_0(String_t* value)
	{
		___transactionId_0 = value;
		Il2CppCodeGenWriteBarrier(&___transactionId_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t1107960729, ___U3CU3E4__this_1)); }
	inline UnityChannelBindings_t2880355556 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline UnityChannelBindings_t2880355556 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(UnityChannelBindings_t2880355556 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
