﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stats
struct  Stats_t967880071  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Stats::statsPanel
	GameObject_t1756533147 * ___statsPanel_2;
	// UnityEngine.GameObject Stats::backButton
	GameObject_t1756533147 * ___backButton_3;
	// UnityEngine.UI.Text Stats::totalLikes
	Text_t356221433 * ___totalLikes_4;
	// UnityEngine.UI.Text Stats::totalFollowers
	Text_t356221433 * ___totalFollowers_5;
	// UnityEngine.GameObject Stats::alertFailPanel
	GameObject_t1756533147 * ___alertFailPanel_6;
	// UnityEngine.GameObject Stats::alertCorrectPanel
	GameObject_t1756533147 * ___alertCorrectPanel_7;

public:
	inline static int32_t get_offset_of_statsPanel_2() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___statsPanel_2)); }
	inline GameObject_t1756533147 * get_statsPanel_2() const { return ___statsPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_statsPanel_2() { return &___statsPanel_2; }
	inline void set_statsPanel_2(GameObject_t1756533147 * value)
	{
		___statsPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___statsPanel_2, value);
	}

	inline static int32_t get_offset_of_backButton_3() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___backButton_3)); }
	inline GameObject_t1756533147 * get_backButton_3() const { return ___backButton_3; }
	inline GameObject_t1756533147 ** get_address_of_backButton_3() { return &___backButton_3; }
	inline void set_backButton_3(GameObject_t1756533147 * value)
	{
		___backButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___backButton_3, value);
	}

	inline static int32_t get_offset_of_totalLikes_4() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___totalLikes_4)); }
	inline Text_t356221433 * get_totalLikes_4() const { return ___totalLikes_4; }
	inline Text_t356221433 ** get_address_of_totalLikes_4() { return &___totalLikes_4; }
	inline void set_totalLikes_4(Text_t356221433 * value)
	{
		___totalLikes_4 = value;
		Il2CppCodeGenWriteBarrier(&___totalLikes_4, value);
	}

	inline static int32_t get_offset_of_totalFollowers_5() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___totalFollowers_5)); }
	inline Text_t356221433 * get_totalFollowers_5() const { return ___totalFollowers_5; }
	inline Text_t356221433 ** get_address_of_totalFollowers_5() { return &___totalFollowers_5; }
	inline void set_totalFollowers_5(Text_t356221433 * value)
	{
		___totalFollowers_5 = value;
		Il2CppCodeGenWriteBarrier(&___totalFollowers_5, value);
	}

	inline static int32_t get_offset_of_alertFailPanel_6() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___alertFailPanel_6)); }
	inline GameObject_t1756533147 * get_alertFailPanel_6() const { return ___alertFailPanel_6; }
	inline GameObject_t1756533147 ** get_address_of_alertFailPanel_6() { return &___alertFailPanel_6; }
	inline void set_alertFailPanel_6(GameObject_t1756533147 * value)
	{
		___alertFailPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___alertFailPanel_6, value);
	}

	inline static int32_t get_offset_of_alertCorrectPanel_7() { return static_cast<int32_t>(offsetof(Stats_t967880071, ___alertCorrectPanel_7)); }
	inline GameObject_t1756533147 * get_alertCorrectPanel_7() const { return ___alertCorrectPanel_7; }
	inline GameObject_t1756533147 ** get_address_of_alertCorrectPanel_7() { return &___alertCorrectPanel_7; }
	inline void set_alertCorrectPanel_7(GameObject_t1756533147 * value)
	{
		___alertCorrectPanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___alertCorrectPanel_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
