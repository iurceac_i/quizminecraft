﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// readHTML
struct  readHTML_t503794377  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text readHTML::contentURL
	Text_t356221433 * ___contentURL_2;
	// UnityEngine.GameObject readHTML::backButton
	GameObject_t1756533147 * ___backButton_3;

public:
	inline static int32_t get_offset_of_contentURL_2() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___contentURL_2)); }
	inline Text_t356221433 * get_contentURL_2() const { return ___contentURL_2; }
	inline Text_t356221433 ** get_address_of_contentURL_2() { return &___contentURL_2; }
	inline void set_contentURL_2(Text_t356221433 * value)
	{
		___contentURL_2 = value;
		Il2CppCodeGenWriteBarrier(&___contentURL_2, value);
	}

	inline static int32_t get_offset_of_backButton_3() { return static_cast<int32_t>(offsetof(readHTML_t503794377, ___backButton_3)); }
	inline GameObject_t1756533147 * get_backButton_3() const { return ___backButton_3; }
	inline GameObject_t1756533147 ** get_address_of_backButton_3() { return &___backButton_3; }
	inline void set_backButton_3(GameObject_t1756533147 * value)
	{
		___backButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___backButton_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
