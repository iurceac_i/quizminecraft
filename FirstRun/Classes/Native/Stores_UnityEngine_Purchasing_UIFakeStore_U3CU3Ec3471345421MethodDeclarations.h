﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.UIFakeStore/<>c
struct U3CU3Ec_t3471345421;
// System.String
struct String_t;
// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t1942475268;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"

// System.Void UnityEngine.Purchasing.UIFakeStore/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m70001523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UIFakeStore/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m2060140322 (U3CU3Ec_t3471345421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.UIFakeStore/<>c::<CreateRetrieveProductsQuestion>b__18_0(UnityEngine.Purchasing.ProductDefinition)
extern "C"  String_t* U3CU3Ec_U3CCreateRetrieveProductsQuestionU3Eb__18_0_m980068044 (U3CU3Ec_t3471345421 * __this, ProductDefinition_t1942475268 * ___pid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
