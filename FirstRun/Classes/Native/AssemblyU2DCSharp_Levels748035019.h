﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Question>
struct List_1_t2297069972;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Levels
struct  Levels_t748035019  : public Il2CppObject
{
public:
	// System.Int32 Levels::nrLevel
	int32_t ___nrLevel_0;
	// System.Int32 Levels::corectAnswers
	int32_t ___corectAnswers_1;
	// System.Collections.Generic.List`1<Question> Levels::quests
	List_1_t2297069972 * ___quests_2;
	// System.Boolean Levels::isLocked
	bool ___isLocked_3;

public:
	inline static int32_t get_offset_of_nrLevel_0() { return static_cast<int32_t>(offsetof(Levels_t748035019, ___nrLevel_0)); }
	inline int32_t get_nrLevel_0() const { return ___nrLevel_0; }
	inline int32_t* get_address_of_nrLevel_0() { return &___nrLevel_0; }
	inline void set_nrLevel_0(int32_t value)
	{
		___nrLevel_0 = value;
	}

	inline static int32_t get_offset_of_corectAnswers_1() { return static_cast<int32_t>(offsetof(Levels_t748035019, ___corectAnswers_1)); }
	inline int32_t get_corectAnswers_1() const { return ___corectAnswers_1; }
	inline int32_t* get_address_of_corectAnswers_1() { return &___corectAnswers_1; }
	inline void set_corectAnswers_1(int32_t value)
	{
		___corectAnswers_1 = value;
	}

	inline static int32_t get_offset_of_quests_2() { return static_cast<int32_t>(offsetof(Levels_t748035019, ___quests_2)); }
	inline List_1_t2297069972 * get_quests_2() const { return ___quests_2; }
	inline List_1_t2297069972 ** get_address_of_quests_2() { return &___quests_2; }
	inline void set_quests_2(List_1_t2297069972 * value)
	{
		___quests_2 = value;
		Il2CppCodeGenWriteBarrier(&___quests_2, value);
	}

	inline static int32_t get_offset_of_isLocked_3() { return static_cast<int32_t>(offsetof(Levels_t748035019, ___isLocked_3)); }
	inline bool get_isLocked_3() const { return ___isLocked_3; }
	inline bool* get_address_of_isLocked_3() { return &___isLocked_3; }
	inline void set_isLocked_3(bool value)
	{
		___isLocked_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
