﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannelP594865545.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannelP855507751.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_UnityChannel2253884029.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_U3CAwakeU3Ec2163001806.h"
#include "AssemblyU2DCSharpU2Dfirstpass_IAPDemo_U3CInitUIU3Ec996539604.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3789552708.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3384326983.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Purchasi3694879381.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1486305137.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa1500295812.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_LevelController2717835266.h"
#include "AssemblyU2DCSharp_Levels748035019.h"
#include "AssemblyU2DCSharp_Main2809994845.h"
#include "AssemblyU2DCSharp_MainGame3800664731.h"
#include "AssemblyU2DCSharp_Adapter814751345.h"
#include "AssemblyU2DCSharp_Adapter_U3CsetAdapterU3Ec__AnonS1050088512.h"
#include "AssemblyU2DCSharp_IAP_InAppManager657850267.h"
#include "AssemblyU2DCSharp_InappPurchase3461117763.h"
#include "AssemblyU2DCSharp_PlayerPrefsX1687815431.h"
#include "AssemblyU2DCSharp_PlayerPrefsX_ArrayType77146353.h"
#include "AssemblyU2DCSharp_Question2927948840.h"
#include "AssemblyU2DCSharp_QuestionController445239244.h"
#include "AssemblyU2DCSharp_Root2702590648.h"
#include "AssemblyU2DCSharp_Stats967880071.h"
#include "AssemblyU2DCSharp_Util4006552276.h"
#include "AssemblyU2DCSharp_readHTML503794377.h"
#include "AssemblyU2DCSharp_Kakera_PickerController3670494704.h"
#include "AssemblyU2DCSharp_Kakera_PickerController_U3CLoadI2450652310.h"
#include "AssemblyU2DCSharp_Kakera_Rotator3287300421.h"
#include "AssemblyU2DCSharp_Kakera_PickerUnsupported1738158813.h"
#include "AssemblyU2DCSharp_Kakera_PickeriOS3652150543.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker2332148304.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker_ImageDelegate3548206662.h"
#include "AssemblyU2DCSharp_Kakera_Unimgpicker_ErrorDelegate402150177.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (UnityChannelPurchaseError_t594865545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[2] = 
{
	UnityChannelPurchaseError_t594865545::get_offset_of_error_0(),
	UnityChannelPurchaseError_t594865545::get_offset_of_purchaseInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (UnityChannelPurchaseInfo_t855507751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[3] = 
{
	UnityChannelPurchaseInfo_t855507751::get_offset_of_productCode_0(),
	UnityChannelPurchaseInfo_t855507751::get_offset_of_gameOrderId_1(),
	UnityChannelPurchaseInfo_t855507751::get_offset_of_orderQueryToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (UnityChannelLoginHandler_t2253884029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[4] = 
{
	UnityChannelLoginHandler_t2253884029::get_offset_of_initializeSucceededAction_0(),
	UnityChannelLoginHandler_t2253884029::get_offset_of_initializeFailedAction_1(),
	UnityChannelLoginHandler_t2253884029::get_offset_of_loginSucceededAction_2(),
	UnityChannelLoginHandler_t2253884029::get_offset_of_loginFailedAction_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (U3CAwakeU3Ec__AnonStorey0_t2163001806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[3] = 
{
	U3CAwakeU3Ec__AnonStorey0_t2163001806::get_offset_of_builder_0(),
	U3CAwakeU3Ec__AnonStorey0_t2163001806::get_offset_of_initializeUnityIap_1(),
	U3CAwakeU3Ec__AnonStorey0_t2163001806::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (U3CInitUIU3Ec__AnonStorey1_t996539604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[1] = 
{
	U3CInitUIU3Ec__AnonStorey1_t996539604::get_offset_of_txId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (IAPListener_t3789552708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[4] = 
{
	IAPListener_t3789552708::get_offset_of_consumePurchase_2(),
	IAPListener_t3789552708::get_offset_of_dontDestroyOnLoad_3(),
	IAPListener_t3789552708::get_offset_of_onPurchaseComplete_4(),
	IAPListener_t3789552708::get_offset_of_onPurchaseFailed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (OnPurchaseCompletedEvent_t3384326983), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (OnPurchaseFailedEvent_t3694879381), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305144), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2008[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2DFB84E27F4158F1A2913CBAA0F2AC05A4208348AE_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (U24ArrayTypeU3D244_t1500295812)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D244_t1500295812 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (U3CModuleU3E_t3783534232), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (LevelController_t2717835266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[3] = 
{
	LevelController_t2717835266::get_offset_of_adapter_2(),
	LevelController_t2717835266::get_offset_of_levels_3(),
	LevelController_t2717835266::get_offset_of_questController_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (Levels_t748035019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[4] = 
{
	Levels_t748035019::get_offset_of_nrLevel_0(),
	Levels_t748035019::get_offset_of_corectAnswers_1(),
	Levels_t748035019::get_offset_of_quests_2(),
	Levels_t748035019::get_offset_of_isLocked_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (Main_t2809994845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[3] = 
{
	Main_t2809994845::get_offset_of_product_id_0(),
	Main_t2809994845::get_offset_of_store_id_1(),
	Main_t2809994845::get_offset_of_store_App_url_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (MainGame_t3800664731), -1, sizeof(MainGame_t3800664731_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2014[8] = 
{
	MainGame_t3800664731_StaticFields::get_offset_of_questions_2(),
	MainGame_t3800664731::get_offset_of_LegasyInfo_3(),
	MainGame_t3800664731::get_offset_of_levelPanel_4(),
	MainGame_t3800664731_StaticFields::get_offset_of_createdSub_5(),
	MainGame_t3800664731_StaticFields::get_offset_of_expiredSub_6(),
	MainGame_t3800664731::get_offset_of_debugText_7(),
	MainGame_t3800664731::get_offset_of_isSubscribed_8(),
	MainGame_t3800664731_StaticFields::get_offset_of_verfiSubscribe_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (Adapter_t814751345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[2] = 
{
	Adapter_t814751345::get_offset_of_gameObjects_2(),
	Adapter_t814751345::get_offset_of_adapterCallBack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (U3CsetAdapterU3Ec__AnonStorey0_t1050088512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[2] = 
{
	U3CsetAdapterU3Ec__AnonStorey0_t1050088512::get_offset_of_finalObj_0(),
	U3CsetAdapterU3Ec__AnonStorey0_t1050088512::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (InAppManager_t657850267), -1, sizeof(InAppManager_t657850267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2018[5] = 
{
	InAppManager_t657850267_StaticFields::get_offset_of_m_StoreController_2(),
	InAppManager_t657850267_StaticFields::get_offset_of_m_StoreExtensionProvider_3(),
	InAppManager_t657850267::get_offset_of_productID_4(),
	InAppManager_t657850267::get_offset_of_itemsContent_5(),
	InAppManager_t657850267_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (InappPurchase_t3461117763), -1, sizeof(InappPurchase_t3461117763_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2019[8] = 
{
	InappPurchase_t3461117763_StaticFields::get_offset_of_isPurchased_2(),
	InappPurchase_t3461117763_StaticFields::get_offset_of_m_StoreController_3(),
	InappPurchase_t3461117763_StaticFields::get_offset_of_m_StoreExtensionProvider_4(),
	InappPurchase_t3461117763::get_offset_of_m_AppleExtensions_5(),
	InappPurchase_t3461117763_StaticFields::get_offset_of_kProductIDConsumable_6(),
	InappPurchase_t3461117763_StaticFields::get_offset_of_kProductIDNonConsumable_7(),
	InappPurchase_t3461117763_StaticFields::get_offset_of_kProductIDSubscription_8(),
	InappPurchase_t3461117763_StaticFields::get_offset_of_kProductNameAppleSubscription_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (PlayerPrefsX_t1687815431), -1, sizeof(PlayerPrefsX_t1687815431_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2020[16] = 
{
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_endianDiff1_0(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_endianDiff2_1(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_idx_2(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_byteBlock_3(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_6(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_7(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_8(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_9(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_10(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_11(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_12(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_13(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_14(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (ArrayType_t77146353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2021[9] = 
{
	ArrayType_t77146353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (Question_t2927948840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[3] = 
{
	Question_t2927948840::get_offset_of_answer_0(),
	Question_t2927948840::get_offset_of_choices_1(),
	Question_t2927948840::get_offset_of_question_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (QuestionController_t445239244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[23] = 
{
	QuestionController_t445239244::get_offset_of_question_2(),
	QuestionController_t445239244::get_offset_of_answer_0_3(),
	QuestionController_t445239244::get_offset_of_answer_1_4(),
	QuestionController_t445239244::get_offset_of_answer_2_5(),
	QuestionController_t445239244::get_offset_of_answer_3_6(),
	QuestionController_t445239244::get_offset_of_star1_7(),
	QuestionController_t445239244::get_offset_of_star2_8(),
	QuestionController_t445239244::get_offset_of_star3_9(),
	QuestionController_t445239244::get_offset_of_callBack_10(),
	QuestionController_t445239244::get_offset_of_correctAns_11(),
	QuestionController_t445239244::get_offset_of_oneButton_12(),
	QuestionController_t445239244::get_offset_of_twoButtons_13(),
	QuestionController_t445239244::get_offset_of_oneFinButton_14(),
	QuestionController_t445239244::get_offset_of_singleButtonPanel_15(),
	QuestionController_t445239244::get_offset_of_finalAlertPanel_16(),
	QuestionController_t445239244::get_offset_of_levelPanel_17(),
	QuestionController_t445239244::get_offset_of_numb_of_compl_mission_18(),
	QuestionController_t445239244::get_offset_of_numb_of_compl_mission_2_19(),
	QuestionController_t445239244::get_offset_of_numQuest_20(),
	QuestionController_t445239244::get_offset_of_levelGlobal_21(),
	QuestionController_t445239244::get_offset_of_positionLevel_22(),
	QuestionController_t445239244::get_offset_of_correctAnswer_23(),
	QuestionController_t445239244::get_offset_of_isPurchase_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (Root_t2702590648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[1] = 
{
	Root_t2702590648::get_offset_of_questions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (Stats_t967880071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[6] = 
{
	Stats_t967880071::get_offset_of_statsPanel_2(),
	Stats_t967880071::get_offset_of_backButton_3(),
	Stats_t967880071::get_offset_of_totalLikes_4(),
	Stats_t967880071::get_offset_of_totalFollowers_5(),
	Stats_t967880071::get_offset_of_alertFailPanel_6(),
	Stats_t967880071::get_offset_of_alertCorrectPanel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (Util_t4006552276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (readHTML_t503794377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[2] = 
{
	readHTML_t503794377::get_offset_of_contentURL_2(),
	readHTML_t503794377::get_offset_of_backButton_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (PickerController_t3670494704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[2] = 
{
	PickerController_t3670494704::get_offset_of_imagePicker_2(),
	PickerController_t3670494704::get_offset_of_imageRenderer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (U3CLoadImageU3Ec__Iterator0_t2450652310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[8] = 
{
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_path_0(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U3CurlU3E__0_1(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U3CwwwU3E__1_2(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U3CtextureU3E__2_3(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_output_4(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U24current_5(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U24disposing_6(),
	U3CLoadImageU3Ec__Iterator0_t2450652310::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (Rotator_t3287300421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[1] = 
{
	Rotator_t3287300421::get_offset_of_rotationVector_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (PickerUnsupported_t1738158813), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (PickeriOS_t3652150543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (Unimgpicker_t2332148304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[3] = 
{
	Unimgpicker_t2332148304::get_offset_of_Completed_2(),
	Unimgpicker_t2332148304::get_offset_of_Failed_3(),
	Unimgpicker_t2332148304::get_offset_of_picker_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (ImageDelegate_t3548206662), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (ErrorDelegate_t402150177), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
