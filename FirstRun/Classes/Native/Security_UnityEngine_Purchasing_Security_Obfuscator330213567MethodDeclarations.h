﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.Obfuscator/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_t330213567;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.Obfuscator/<>c__DisplayClass1_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass1_0__ctor_m570357806 (U3CU3Ec__DisplayClass1_0_t330213567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte UnityEngine.Purchasing.Security.Obfuscator/<>c__DisplayClass1_0::<DeObfuscate>b__0(System.Byte)
extern "C"  uint8_t U3CU3Ec__DisplayClass1_0_U3CDeObfuscateU3Eb__0_m3248548264 (U3CU3Ec__DisplayClass1_0_t330213567 * __this, uint8_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
