﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Kakera.PickeriOS
struct PickeriOS_t3652150543;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Kakera.PickeriOS::.ctor()
extern "C"  void PickeriOS__ctor_m2158368285 (PickeriOS_t3652150543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.PickeriOS::Unimgpicker_show(System.String,System.String,System.Int32)
extern "C"  void PickeriOS_Unimgpicker_show_m114728490 (Il2CppObject * __this /* static, unused */, String_t* ___title0, String_t* ___outputFileName1, int32_t ___maxSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.PickeriOS::Show(System.String,System.String,System.Int32)
extern "C"  void PickeriOS_Show_m4287527539 (PickeriOS_t3652150543 * __this, String_t* ___title0, String_t* ___outputFileName1, int32_t ___maxSize2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
