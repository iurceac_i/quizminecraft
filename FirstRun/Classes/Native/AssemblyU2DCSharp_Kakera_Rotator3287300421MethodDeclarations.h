﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Kakera.Rotator
struct Rotator_t3287300421;

#include "codegen/il2cpp-codegen.h"

// System.Void Kakera.Rotator::.ctor()
extern "C"  void Rotator__ctor_m1196604511 (Rotator_t3287300421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.Rotator::Update()
extern "C"  void Rotator_Update_m3475716304 (Rotator_t3287300421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
