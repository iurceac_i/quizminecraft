﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t2096495139;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass10_0__ctor_m2850924464 (U3CU3Ec__DisplayClass10_0_t2096495139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::<FetchProducts>b__0(System.String)
extern "C"  void U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__0_m460871864 (U3CU3Ec__DisplayClass10_0_t2096495139 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::<FetchProducts>b__1(System.String)
extern "C"  void U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__1_m3375103129 (U3CU3Ec__DisplayClass10_0_t2096495139 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::<FetchProducts>b__2()
extern "C"  void U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__2_m3465341168 (U3CU3Ec__DisplayClass10_0_t2096495139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
