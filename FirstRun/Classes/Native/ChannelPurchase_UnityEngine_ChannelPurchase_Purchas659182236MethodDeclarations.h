﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// UnityEngine.ChannelPurchase.IPurchaseListener
struct IPurchaseListener_t2367674166;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.ChannelPurchase.PurchaseService::Purchase(System.String,System.String,UnityEngine.ChannelPurchase.IPurchaseListener)
extern "C"  void PurchaseService_Purchase_m977826592 (Il2CppObject * __this /* static, unused */, String_t* ___productCode0, String_t* ___gameOrderId1, Il2CppObject * ___listener2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ChannelPurchase.PurchaseService::ValidateReceipt(System.String,UnityEngine.ChannelPurchase.IPurchaseListener)
extern "C"  void PurchaseService_ValidateReceipt_m2412817733 (Il2CppObject * __this /* static, unused */, String_t* ___gameOrderId0, Il2CppObject * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ChannelPurchase.PurchaseService::.cctor()
extern "C"  void PurchaseService__cctor_m504943159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
