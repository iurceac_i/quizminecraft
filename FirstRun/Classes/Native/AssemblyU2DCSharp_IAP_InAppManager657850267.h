﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// System.String
struct String_t;
// Main
struct Main_t2809994845;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAP.InAppManager
struct  InAppManager_t657850267  : public MonoBehaviour_t1158329972
{
public:
	// System.String IAP.InAppManager::productID
	String_t* ___productID_4;
	// Main IAP.InAppManager::itemsContent
	Main_t2809994845 * ___itemsContent_5;

public:
	inline static int32_t get_offset_of_productID_4() { return static_cast<int32_t>(offsetof(InAppManager_t657850267, ___productID_4)); }
	inline String_t* get_productID_4() const { return ___productID_4; }
	inline String_t** get_address_of_productID_4() { return &___productID_4; }
	inline void set_productID_4(String_t* value)
	{
		___productID_4 = value;
		Il2CppCodeGenWriteBarrier(&___productID_4, value);
	}

	inline static int32_t get_offset_of_itemsContent_5() { return static_cast<int32_t>(offsetof(InAppManager_t657850267, ___itemsContent_5)); }
	inline Main_t2809994845 * get_itemsContent_5() const { return ___itemsContent_5; }
	inline Main_t2809994845 ** get_address_of_itemsContent_5() { return &___itemsContent_5; }
	inline void set_itemsContent_5(Main_t2809994845 * value)
	{
		___itemsContent_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemsContent_5, value);
	}
};

struct InAppManager_t657850267_StaticFields
{
public:
	// UnityEngine.Purchasing.IStoreController IAP.InAppManager::m_StoreController
	Il2CppObject * ___m_StoreController_2;
	// UnityEngine.Purchasing.IExtensionProvider IAP.InAppManager::m_StoreExtensionProvider
	Il2CppObject * ___m_StoreExtensionProvider_3;
	// System.Action`1<System.Boolean> IAP.InAppManager::<>f__am$cache0
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_m_StoreController_2() { return static_cast<int32_t>(offsetof(InAppManager_t657850267_StaticFields, ___m_StoreController_2)); }
	inline Il2CppObject * get_m_StoreController_2() const { return ___m_StoreController_2; }
	inline Il2CppObject ** get_address_of_m_StoreController_2() { return &___m_StoreController_2; }
	inline void set_m_StoreController_2(Il2CppObject * value)
	{
		___m_StoreController_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreController_2, value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_3() { return static_cast<int32_t>(offsetof(InAppManager_t657850267_StaticFields, ___m_StoreExtensionProvider_3)); }
	inline Il2CppObject * get_m_StoreExtensionProvider_3() const { return ___m_StoreExtensionProvider_3; }
	inline Il2CppObject ** get_address_of_m_StoreExtensionProvider_3() { return &___m_StoreExtensionProvider_3; }
	inline void set_m_StoreExtensionProvider_3(Il2CppObject * value)
	{
		___m_StoreExtensionProvider_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreExtensionProvider_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(InAppManager_t657850267_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
