﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Adapter/OnItemClickListener
struct OnItemClickListener_t418219798;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Adapter
struct  Adapter_t814751345  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Adapter::gameObjects
	GameObject_t1756533147 * ___gameObjects_2;
	// Adapter/OnItemClickListener Adapter::adapterCallBack
	Il2CppObject * ___adapterCallBack_3;

public:
	inline static int32_t get_offset_of_gameObjects_2() { return static_cast<int32_t>(offsetof(Adapter_t814751345, ___gameObjects_2)); }
	inline GameObject_t1756533147 * get_gameObjects_2() const { return ___gameObjects_2; }
	inline GameObject_t1756533147 ** get_address_of_gameObjects_2() { return &___gameObjects_2; }
	inline void set_gameObjects_2(GameObject_t1756533147 * value)
	{
		___gameObjects_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjects_2, value);
	}

	inline static int32_t get_offset_of_adapterCallBack_3() { return static_cast<int32_t>(offsetof(Adapter_t814751345, ___adapterCallBack_3)); }
	inline Il2CppObject * get_adapterCallBack_3() const { return ___adapterCallBack_3; }
	inline Il2CppObject ** get_address_of_adapterCallBack_3() { return &___adapterCallBack_3; }
	inline void set_adapterCallBack_3(Il2CppObject * value)
	{
		___adapterCallBack_3 = value;
		Il2CppCodeGenWriteBarrier(&___adapterCallBack_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
