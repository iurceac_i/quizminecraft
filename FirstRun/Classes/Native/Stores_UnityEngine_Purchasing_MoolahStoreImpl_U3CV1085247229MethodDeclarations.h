﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47
struct U3CValidateReceiptProcessU3Ed__47_t1085247229;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::.ctor(System.Int32)
extern "C"  void U3CValidateReceiptProcessU3Ed__47__ctor_m1578864149 (U3CValidateReceiptProcessU3Ed__47_t1085247229 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::System.IDisposable.Dispose()
extern "C"  void U3CValidateReceiptProcessU3Ed__47_System_IDisposable_Dispose_m4082377503 (U3CValidateReceiptProcessU3Ed__47_t1085247229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::MoveNext()
extern "C"  bool U3CValidateReceiptProcessU3Ed__47_MoveNext_m2236268810 (U3CValidateReceiptProcessU3Ed__47_t1085247229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  Il2CppObject * U3CValidateReceiptProcessU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1310788337 (U3CValidateReceiptProcessU3Ed__47_t1085247229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::System.Collections.IEnumerator.Reset()
extern "C"  void U3CValidateReceiptProcessU3Ed__47_System_Collections_IEnumerator_Reset_m1512142248 (U3CValidateReceiptProcessU3Ed__47_t1085247229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CValidateReceiptProcessU3Ed__47_System_Collections_IEnumerator_get_Current_m226447310 (U3CValidateReceiptProcessU3Ed__47_t1085247229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
