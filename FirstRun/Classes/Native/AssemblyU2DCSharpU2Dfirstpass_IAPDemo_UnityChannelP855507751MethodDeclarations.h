﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IAPDemo/UnityChannelPurchaseInfo
struct UnityChannelPurchaseInfo_t855507751;

#include "codegen/il2cpp-codegen.h"

// System.Void IAPDemo/UnityChannelPurchaseInfo::.ctor()
extern "C"  void UnityChannelPurchaseInfo__ctor_m932370414 (UnityChannelPurchaseInfo_t855507751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
