﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.FakeGooglePlayConfiguration
struct FakeGooglePlayConfiguration_t737012266;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.FakeGooglePlayConfiguration::.ctor()
extern "C"  void FakeGooglePlayConfiguration__ctor_m1997483180 (FakeGooglePlayConfiguration_t737012266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
