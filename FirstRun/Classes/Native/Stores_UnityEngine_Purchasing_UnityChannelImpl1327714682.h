﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.INativeUnityChannelStore
struct INativeUnityChannelStore_t670691399;
// System.String
struct String_t;

#include "Stores_UnityEngine_Purchasing_JSONStore1890359403.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelImpl
struct  UnityChannelImpl_t1327714682  : public JSONStore_t1890359403
{
public:
	// UnityEngine.Purchasing.INativeUnityChannelStore UnityEngine.Purchasing.UnityChannelImpl::m_Bindings
	Il2CppObject * ___m_Bindings_2;
	// System.String UnityEngine.Purchasing.UnityChannelImpl::m_LastPurchaseError
	String_t* ___m_LastPurchaseError_3;
	// System.Boolean UnityEngine.Purchasing.UnityChannelImpl::<fetchReceiptPayloadOnPurchase>k__BackingField
	bool ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_m_Bindings_2() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t1327714682, ___m_Bindings_2)); }
	inline Il2CppObject * get_m_Bindings_2() const { return ___m_Bindings_2; }
	inline Il2CppObject ** get_address_of_m_Bindings_2() { return &___m_Bindings_2; }
	inline void set_m_Bindings_2(Il2CppObject * value)
	{
		___m_Bindings_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Bindings_2, value);
	}

	inline static int32_t get_offset_of_m_LastPurchaseError_3() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t1327714682, ___m_LastPurchaseError_3)); }
	inline String_t* get_m_LastPurchaseError_3() const { return ___m_LastPurchaseError_3; }
	inline String_t** get_address_of_m_LastPurchaseError_3() { return &___m_LastPurchaseError_3; }
	inline void set_m_LastPurchaseError_3(String_t* value)
	{
		___m_LastPurchaseError_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_LastPurchaseError_3, value);
	}

	inline static int32_t get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t1327714682, ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_4)); }
	inline bool get_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_4() const { return ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_4() { return &___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_4; }
	inline void set_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_4(bool value)
	{
		___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
