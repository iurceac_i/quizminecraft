﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.IAsyncWebUtil
struct IAsyncWebUtil_t364059421;
// System.String
struct String_t;
// UnityEngine.ILogger
struct ILogger_t1425954571;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudCatalogImpl
struct  CloudCatalogImpl_t569898932  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.CloudCatalogImpl::m_AsyncUtil
	Il2CppObject * ___m_AsyncUtil_0;
	// System.String UnityEngine.Purchasing.CloudCatalogImpl::m_CacheFileName
	String_t* ___m_CacheFileName_1;
	// UnityEngine.ILogger UnityEngine.Purchasing.CloudCatalogImpl::m_Logger
	Il2CppObject * ___m_Logger_2;
	// System.String UnityEngine.Purchasing.CloudCatalogImpl::m_CatalogURL
	String_t* ___m_CatalogURL_3;
	// System.String UnityEngine.Purchasing.CloudCatalogImpl::m_StoreName
	String_t* ___m_StoreName_4;

public:
	inline static int32_t get_offset_of_m_AsyncUtil_0() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t569898932, ___m_AsyncUtil_0)); }
	inline Il2CppObject * get_m_AsyncUtil_0() const { return ___m_AsyncUtil_0; }
	inline Il2CppObject ** get_address_of_m_AsyncUtil_0() { return &___m_AsyncUtil_0; }
	inline void set_m_AsyncUtil_0(Il2CppObject * value)
	{
		___m_AsyncUtil_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_AsyncUtil_0, value);
	}

	inline static int32_t get_offset_of_m_CacheFileName_1() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t569898932, ___m_CacheFileName_1)); }
	inline String_t* get_m_CacheFileName_1() const { return ___m_CacheFileName_1; }
	inline String_t** get_address_of_m_CacheFileName_1() { return &___m_CacheFileName_1; }
	inline void set_m_CacheFileName_1(String_t* value)
	{
		___m_CacheFileName_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_CacheFileName_1, value);
	}

	inline static int32_t get_offset_of_m_Logger_2() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t569898932, ___m_Logger_2)); }
	inline Il2CppObject * get_m_Logger_2() const { return ___m_Logger_2; }
	inline Il2CppObject ** get_address_of_m_Logger_2() { return &___m_Logger_2; }
	inline void set_m_Logger_2(Il2CppObject * value)
	{
		___m_Logger_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Logger_2, value);
	}

	inline static int32_t get_offset_of_m_CatalogURL_3() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t569898932, ___m_CatalogURL_3)); }
	inline String_t* get_m_CatalogURL_3() const { return ___m_CatalogURL_3; }
	inline String_t** get_address_of_m_CatalogURL_3() { return &___m_CatalogURL_3; }
	inline void set_m_CatalogURL_3(String_t* value)
	{
		___m_CatalogURL_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_CatalogURL_3, value);
	}

	inline static int32_t get_offset_of_m_StoreName_4() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t569898932, ___m_StoreName_4)); }
	inline String_t* get_m_StoreName_4() const { return ___m_StoreName_4; }
	inline String_t** get_address_of_m_StoreName_4() { return &___m_StoreName_4; }
	inline void set_m_StoreName_4(String_t* value)
	{
		___m_StoreName_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreName_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
