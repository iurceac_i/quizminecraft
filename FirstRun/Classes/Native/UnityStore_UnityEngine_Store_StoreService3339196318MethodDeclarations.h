﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Store.AppInfo
struct AppInfo_t2080248435;
// UnityEngine.Store.ILoginListener
struct ILoginListener_t3164042774;

#include "codegen/il2cpp-codegen.h"
#include "UnityStore_UnityEngine_Store_AppInfo2080248435.h"

// System.Void UnityEngine.Store.StoreService::Initialize(UnityEngine.Store.AppInfo,UnityEngine.Store.ILoginListener)
extern "C"  void StoreService_Initialize_m2334922442 (Il2CppObject * __this /* static, unused */, AppInfo_t2080248435 * ___appInfo0, Il2CppObject * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Store.StoreService::Login(UnityEngine.Store.ILoginListener)
extern "C"  void StoreService_Login_m2535849060 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___listener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Store.StoreService::.cctor()
extern "C"  void StoreService__cctor_m2623941302 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
