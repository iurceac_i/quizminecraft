﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.ProductCatalogItem
struct ProductCatalogItem_t977711995;
// System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.StoreID>
struct ICollection_1_t1423527629;
// UnityEngine.Purchasing.LocalizedProductDescription
struct LocalizedProductDescription_t1525635964;

#include "codegen/il2cpp-codegen.h"
#include "Stores_UnityEngine_Purchasing_TranslationLocale3543162129.h"

// System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.StoreID> UnityEngine.Purchasing.ProductCatalogItem::get_allStoreIDs()
extern "C"  Il2CppObject* ProductCatalogItem_get_allStoreIDs_m2785218113 (ProductCatalogItem_t977711995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.LocalizedProductDescription UnityEngine.Purchasing.ProductCatalogItem::GetDescription(UnityEngine.Purchasing.TranslationLocale)
extern "C"  LocalizedProductDescription_t1525635964 * ProductCatalogItem_GetDescription_m2643762087 (ProductCatalogItem_t977711995 * __this, int32_t ___locale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.ProductCatalogItem::.ctor()
extern "C"  void ProductCatalogItem__ctor_m1993484609 (ProductCatalogItem_t977711995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
