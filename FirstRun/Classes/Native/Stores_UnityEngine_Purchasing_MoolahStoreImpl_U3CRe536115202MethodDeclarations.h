﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45
struct U3CRestoreTransactionIDProcessU3Ed__45_t536115202;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::.ctor(System.Int32)
extern "C"  void U3CRestoreTransactionIDProcessU3Ed__45__ctor_m4030535954 (U3CRestoreTransactionIDProcessU3Ed__45_t536115202 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::System.IDisposable.Dispose()
extern "C"  void U3CRestoreTransactionIDProcessU3Ed__45_System_IDisposable_Dispose_m1412002182 (U3CRestoreTransactionIDProcessU3Ed__45_t536115202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::MoveNext()
extern "C"  bool U3CRestoreTransactionIDProcessU3Ed__45_MoveNext_m3790462923 (U3CRestoreTransactionIDProcessU3Ed__45_t536115202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  Il2CppObject * U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1401262930 (U3CRestoreTransactionIDProcessU3Ed__45_t536115202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::System.Collections.IEnumerator.Reset()
extern "C"  void U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_IEnumerator_Reset_m2042622883 (U3CRestoreTransactionIDProcessU3Ed__45_t536115202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_IEnumerator_get_Current_m592103015 (U3CRestoreTransactionIDProcessU3Ed__45_t536115202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
