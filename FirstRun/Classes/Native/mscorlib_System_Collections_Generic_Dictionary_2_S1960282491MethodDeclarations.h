﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AppStore,System.Object>
struct ShimEnumerator_t1960282491;
// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>
struct Dictionary_2_t1855157670;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2545106771_gshared (ShimEnumerator_t1960282491 * __this, Dictionary_2_t1855157670 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2545106771(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1960282491 *, Dictionary_2_t1855157670 *, const MethodInfo*))ShimEnumerator__ctor_m2545106771_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AppStore,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2812408896_gshared (ShimEnumerator_t1960282491 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2812408896(__this, method) ((  bool (*) (ShimEnumerator_t1960282491 *, const MethodInfo*))ShimEnumerator_MoveNext_m2812408896_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AppStore,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3672505408_gshared (ShimEnumerator_t1960282491 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3672505408(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1960282491 *, const MethodInfo*))ShimEnumerator_get_Entry_m3672505408_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AppStore,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m122221899_gshared (ShimEnumerator_t1960282491 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m122221899(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1960282491 *, const MethodInfo*))ShimEnumerator_get_Key_m122221899_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AppStore,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2521857323_gshared (ShimEnumerator_t1960282491 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2521857323(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1960282491 *, const MethodInfo*))ShimEnumerator_get_Value_m2521857323_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AppStore,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m347317541_gshared (ShimEnumerator_t1960282491 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m347317541(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1960282491 *, const MethodInfo*))ShimEnumerator_get_Current_m347317541_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Purchasing.AppStore,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m46729113_gshared (ShimEnumerator_t1960282491 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m46729113(__this, method) ((  void (*) (ShimEnumerator_t1960282491 *, const MethodInfo*))ShimEnumerator_Reset_m46729113_gshared)(__this, method)
