﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>
struct Action_1_t2289103034;
// UnityEngine.Purchasing.MoolahStoreImpl
struct MoolahStoreImpl_t4206626141;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45
struct  U3CRestoreTransactionIDProcessU3Ed__45_t536115202  : public Il2CppObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>2__current
	Il2CppObject * ___U3CU3E2__current_1;
	// System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::result
	Action_1_t2289103034 * ___result_2;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>4__this
	MoolahStoreImpl_t4206626141 * ___U3CU3E4__this_3;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<customID>5__1
	String_t* ___U3CcustomIDU3E5__1_4;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<wf>5__2
	WWWForm_t3950226929 * ___U3CwfU3E5__2_5;
	// System.DateTime UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<now>5__3
	DateTime_t693205669  ___U3CnowU3E5__3_6;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<endDate>5__4
	String_t* ___U3CendDateU3E5__4_7;
	// System.DateTime UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<upperWeek>5__5
	DateTime_t693205669  ___U3CupperWeekU3E5__5_8;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<startDate>5__6
	String_t* ___U3CstartDateU3E5__6_9;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<sign>5__7
	String_t* ___U3CsignU3E5__7_10;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<w>5__8
	WWW_t2919945039 * ___U3CwU3E5__8_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<restoreObjects>5__9
	Dictionary_2_t309261261 * ___U3CrestoreObjectsU3E5__9_12;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<code>5__10
	String_t* ___U3CcodeU3E5__10_13;
	// System.Collections.Generic.List`1<System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<restoreValues>5__11
	List_1_t2058570427 * ___U3CrestoreValuesU3E5__11_14;
	// System.Collections.Generic.List`1/Enumerator<System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>s__12
	Enumerator_t1593300101  ___U3CU3Es__12_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<restoreObjectElem>5__13
	Dictionary_2_t309261261 * ___U3CrestoreObjectElemU3E5__13_16;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<productId>5__14
	String_t* ___U3CproductIdU3E5__14_17;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<tradeSeq>5__15
	String_t* ___U3CtradeSeqU3E5__15_18;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<receipt>5__16
	String_t* ___U3CreceiptU3E5__16_19;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CU3E2__current_1)); }
	inline Il2CppObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Il2CppObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Il2CppObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_1, value);
	}

	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___result_2)); }
	inline Action_1_t2289103034 * get_result_2() const { return ___result_2; }
	inline Action_1_t2289103034 ** get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(Action_1_t2289103034 * value)
	{
		___result_2 = value;
		Il2CppCodeGenWriteBarrier(&___result_2, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CU3E4__this_3)); }
	inline MoolahStoreImpl_t4206626141 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MoolahStoreImpl_t4206626141 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MoolahStoreImpl_t4206626141 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_3, value);
	}

	inline static int32_t get_offset_of_U3CcustomIDU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CcustomIDU3E5__1_4)); }
	inline String_t* get_U3CcustomIDU3E5__1_4() const { return ___U3CcustomIDU3E5__1_4; }
	inline String_t** get_address_of_U3CcustomIDU3E5__1_4() { return &___U3CcustomIDU3E5__1_4; }
	inline void set_U3CcustomIDU3E5__1_4(String_t* value)
	{
		___U3CcustomIDU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcustomIDU3E5__1_4, value);
	}

	inline static int32_t get_offset_of_U3CwfU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CwfU3E5__2_5)); }
	inline WWWForm_t3950226929 * get_U3CwfU3E5__2_5() const { return ___U3CwfU3E5__2_5; }
	inline WWWForm_t3950226929 ** get_address_of_U3CwfU3E5__2_5() { return &___U3CwfU3E5__2_5; }
	inline void set_U3CwfU3E5__2_5(WWWForm_t3950226929 * value)
	{
		___U3CwfU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwfU3E5__2_5, value);
	}

	inline static int32_t get_offset_of_U3CnowU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CnowU3E5__3_6)); }
	inline DateTime_t693205669  get_U3CnowU3E5__3_6() const { return ___U3CnowU3E5__3_6; }
	inline DateTime_t693205669 * get_address_of_U3CnowU3E5__3_6() { return &___U3CnowU3E5__3_6; }
	inline void set_U3CnowU3E5__3_6(DateTime_t693205669  value)
	{
		___U3CnowU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CendDateU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CendDateU3E5__4_7)); }
	inline String_t* get_U3CendDateU3E5__4_7() const { return ___U3CendDateU3E5__4_7; }
	inline String_t** get_address_of_U3CendDateU3E5__4_7() { return &___U3CendDateU3E5__4_7; }
	inline void set_U3CendDateU3E5__4_7(String_t* value)
	{
		___U3CendDateU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CendDateU3E5__4_7, value);
	}

	inline static int32_t get_offset_of_U3CupperWeekU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CupperWeekU3E5__5_8)); }
	inline DateTime_t693205669  get_U3CupperWeekU3E5__5_8() const { return ___U3CupperWeekU3E5__5_8; }
	inline DateTime_t693205669 * get_address_of_U3CupperWeekU3E5__5_8() { return &___U3CupperWeekU3E5__5_8; }
	inline void set_U3CupperWeekU3E5__5_8(DateTime_t693205669  value)
	{
		___U3CupperWeekU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CstartDateU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CstartDateU3E5__6_9)); }
	inline String_t* get_U3CstartDateU3E5__6_9() const { return ___U3CstartDateU3E5__6_9; }
	inline String_t** get_address_of_U3CstartDateU3E5__6_9() { return &___U3CstartDateU3E5__6_9; }
	inline void set_U3CstartDateU3E5__6_9(String_t* value)
	{
		___U3CstartDateU3E5__6_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstartDateU3E5__6_9, value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CsignU3E5__7_10)); }
	inline String_t* get_U3CsignU3E5__7_10() const { return ___U3CsignU3E5__7_10; }
	inline String_t** get_address_of_U3CsignU3E5__7_10() { return &___U3CsignU3E5__7_10; }
	inline void set_U3CsignU3E5__7_10(String_t* value)
	{
		___U3CsignU3E5__7_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsignU3E5__7_10, value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__8_11() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CwU3E5__8_11)); }
	inline WWW_t2919945039 * get_U3CwU3E5__8_11() const { return ___U3CwU3E5__8_11; }
	inline WWW_t2919945039 ** get_address_of_U3CwU3E5__8_11() { return &___U3CwU3E5__8_11; }
	inline void set_U3CwU3E5__8_11(WWW_t2919945039 * value)
	{
		___U3CwU3E5__8_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwU3E5__8_11, value);
	}

	inline static int32_t get_offset_of_U3CrestoreObjectsU3E5__9_12() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CrestoreObjectsU3E5__9_12)); }
	inline Dictionary_2_t309261261 * get_U3CrestoreObjectsU3E5__9_12() const { return ___U3CrestoreObjectsU3E5__9_12; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CrestoreObjectsU3E5__9_12() { return &___U3CrestoreObjectsU3E5__9_12; }
	inline void set_U3CrestoreObjectsU3E5__9_12(Dictionary_2_t309261261 * value)
	{
		___U3CrestoreObjectsU3E5__9_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrestoreObjectsU3E5__9_12, value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E5__10_13() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CcodeU3E5__10_13)); }
	inline String_t* get_U3CcodeU3E5__10_13() const { return ___U3CcodeU3E5__10_13; }
	inline String_t** get_address_of_U3CcodeU3E5__10_13() { return &___U3CcodeU3E5__10_13; }
	inline void set_U3CcodeU3E5__10_13(String_t* value)
	{
		___U3CcodeU3E5__10_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcodeU3E5__10_13, value);
	}

	inline static int32_t get_offset_of_U3CrestoreValuesU3E5__11_14() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CrestoreValuesU3E5__11_14)); }
	inline List_1_t2058570427 * get_U3CrestoreValuesU3E5__11_14() const { return ___U3CrestoreValuesU3E5__11_14; }
	inline List_1_t2058570427 ** get_address_of_U3CrestoreValuesU3E5__11_14() { return &___U3CrestoreValuesU3E5__11_14; }
	inline void set_U3CrestoreValuesU3E5__11_14(List_1_t2058570427 * value)
	{
		___U3CrestoreValuesU3E5__11_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrestoreValuesU3E5__11_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Es__12_15() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CU3Es__12_15)); }
	inline Enumerator_t1593300101  get_U3CU3Es__12_15() const { return ___U3CU3Es__12_15; }
	inline Enumerator_t1593300101 * get_address_of_U3CU3Es__12_15() { return &___U3CU3Es__12_15; }
	inline void set_U3CU3Es__12_15(Enumerator_t1593300101  value)
	{
		___U3CU3Es__12_15 = value;
	}

	inline static int32_t get_offset_of_U3CrestoreObjectElemU3E5__13_16() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CrestoreObjectElemU3E5__13_16)); }
	inline Dictionary_2_t309261261 * get_U3CrestoreObjectElemU3E5__13_16() const { return ___U3CrestoreObjectElemU3E5__13_16; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CrestoreObjectElemU3E5__13_16() { return &___U3CrestoreObjectElemU3E5__13_16; }
	inline void set_U3CrestoreObjectElemU3E5__13_16(Dictionary_2_t309261261 * value)
	{
		___U3CrestoreObjectElemU3E5__13_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrestoreObjectElemU3E5__13_16, value);
	}

	inline static int32_t get_offset_of_U3CproductIdU3E5__14_17() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CproductIdU3E5__14_17)); }
	inline String_t* get_U3CproductIdU3E5__14_17() const { return ___U3CproductIdU3E5__14_17; }
	inline String_t** get_address_of_U3CproductIdU3E5__14_17() { return &___U3CproductIdU3E5__14_17; }
	inline void set_U3CproductIdU3E5__14_17(String_t* value)
	{
		___U3CproductIdU3E5__14_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CproductIdU3E5__14_17, value);
	}

	inline static int32_t get_offset_of_U3CtradeSeqU3E5__15_18() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CtradeSeqU3E5__15_18)); }
	inline String_t* get_U3CtradeSeqU3E5__15_18() const { return ___U3CtradeSeqU3E5__15_18; }
	inline String_t** get_address_of_U3CtradeSeqU3E5__15_18() { return &___U3CtradeSeqU3E5__15_18; }
	inline void set_U3CtradeSeqU3E5__15_18(String_t* value)
	{
		___U3CtradeSeqU3E5__15_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtradeSeqU3E5__15_18, value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3E5__16_19() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t536115202, ___U3CreceiptU3E5__16_19)); }
	inline String_t* get_U3CreceiptU3E5__16_19() const { return ___U3CreceiptU3E5__16_19; }
	inline String_t** get_address_of_U3CreceiptU3E5__16_19() { return &___U3CreceiptU3E5__16_19; }
	inline void set_U3CreceiptU3E5__16_19(String_t* value)
	{
		___U3CreceiptU3E5__16_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CreceiptU3E5__16_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
