﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.FakeUnityChannelConfiguration
struct FakeUnityChannelConfiguration_t4230617165;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.FakeUnityChannelConfiguration::set_fetchReceiptPayloadOnPurchase(System.Boolean)
extern "C"  void FakeUnityChannelConfiguration_set_fetchReceiptPayloadOnPurchase_m171696075 (FakeUnityChannelConfiguration_t4230617165 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.FakeUnityChannelConfiguration::.ctor()
extern "C"  void FakeUnityChannelConfiguration__ctor_m1954393649 (FakeUnityChannelConfiguration_t4230617165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
