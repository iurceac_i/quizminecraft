﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stats
struct Stats_t967880071;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void Stats::.ctor()
extern "C"  void Stats__ctor_m3441957910 (Stats_t967880071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::StoreMenu(UnityEngine.GameObject)
extern "C"  void Stats_StoreMenu_m4176885938 (Stats_t967880071 * __this, GameObject_t1756533147 * ___storePanel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::activeAlert(UnityEngine.GameObject)
extern "C"  void Stats_activeAlert_m406229684 (Stats_t967880071 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stats::CloseAlert(UnityEngine.GameObject)
extern "C"  void Stats_CloseAlert_m3751967524 (Stats_t967880071 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
