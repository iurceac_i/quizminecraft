﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.AsyncWebUtil
struct AsyncWebUtil_t1370427196;
// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action
struct Action_t3226471752;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.WWW
struct WWW_t2919945039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Core_System_Action3226471752.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"

// System.Void UnityEngine.Purchasing.AsyncWebUtil::Get(System.String,System.Action`1<System.String>,System.Action`1<System.String>)
extern "C"  void AsyncWebUtil_Get_m834855952 (AsyncWebUtil_t1370427196 * __this, String_t* ___url0, Action_1_t1831019615 * ___responseHandler1, Action_1_t1831019615 * ___errorHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.AsyncWebUtil::Schedule(System.Action,System.Int32)
extern "C"  void AsyncWebUtil_Schedule_m1974772343 (AsyncWebUtil_t1370427196 * __this, Action_t3226471752 * ___a0, int32_t ___delayInSeconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.Purchasing.AsyncWebUtil::DoInvoke(System.Action,System.Int32)
extern "C"  Il2CppObject * AsyncWebUtil_DoInvoke_m2352559439 (AsyncWebUtil_t1370427196 * __this, Action_t3226471752 * ___a0, int32_t ___delayInSeconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.Purchasing.AsyncWebUtil::Process(UnityEngine.WWW,System.Action`1<System.String>,System.Action`1<System.String>)
extern "C"  Il2CppObject * AsyncWebUtil_Process_m1692060475 (AsyncWebUtil_t1370427196 * __this, WWW_t2919945039 * ___request0, Action_1_t1831019615 * ___responseHandler1, Action_1_t1831019615 * ___errorHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.AsyncWebUtil::.ctor()
extern "C"  void AsyncWebUtil__ctor_m4145569614 (AsyncWebUtil_t1370427196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
