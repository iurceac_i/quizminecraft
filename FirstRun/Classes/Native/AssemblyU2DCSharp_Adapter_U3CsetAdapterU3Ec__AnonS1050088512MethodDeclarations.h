﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Adapter/<setAdapter>c__AnonStorey0
struct U3CsetAdapterU3Ec__AnonStorey0_t1050088512;

#include "codegen/il2cpp-codegen.h"

// System.Void Adapter/<setAdapter>c__AnonStorey0::.ctor()
extern "C"  void U3CsetAdapterU3Ec__AnonStorey0__ctor_m2813505567 (U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Adapter/<setAdapter>c__AnonStorey0::<>m__0()
extern "C"  void U3CsetAdapterU3Ec__AnonStorey0_U3CU3Em__0_m3611385842 (U3CsetAdapterU3Ec__AnonStorey0_t1050088512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
