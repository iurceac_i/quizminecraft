﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Question[]
struct QuestionU5BU5D_t1028533817;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainGame
struct  MainGame_t3800664731  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MainGame::LegasyInfo
	GameObject_t1756533147 * ___LegasyInfo_3;
	// UnityEngine.GameObject MainGame::levelPanel
	GameObject_t1756533147 * ___levelPanel_4;
	// UnityEngine.UI.Text MainGame::debugText
	Text_t356221433 * ___debugText_7;
	// System.String MainGame::isSubscribed
	String_t* ___isSubscribed_8;

public:
	inline static int32_t get_offset_of_LegasyInfo_3() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___LegasyInfo_3)); }
	inline GameObject_t1756533147 * get_LegasyInfo_3() const { return ___LegasyInfo_3; }
	inline GameObject_t1756533147 ** get_address_of_LegasyInfo_3() { return &___LegasyInfo_3; }
	inline void set_LegasyInfo_3(GameObject_t1756533147 * value)
	{
		___LegasyInfo_3 = value;
		Il2CppCodeGenWriteBarrier(&___LegasyInfo_3, value);
	}

	inline static int32_t get_offset_of_levelPanel_4() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___levelPanel_4)); }
	inline GameObject_t1756533147 * get_levelPanel_4() const { return ___levelPanel_4; }
	inline GameObject_t1756533147 ** get_address_of_levelPanel_4() { return &___levelPanel_4; }
	inline void set_levelPanel_4(GameObject_t1756533147 * value)
	{
		___levelPanel_4 = value;
		Il2CppCodeGenWriteBarrier(&___levelPanel_4, value);
	}

	inline static int32_t get_offset_of_debugText_7() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___debugText_7)); }
	inline Text_t356221433 * get_debugText_7() const { return ___debugText_7; }
	inline Text_t356221433 ** get_address_of_debugText_7() { return &___debugText_7; }
	inline void set_debugText_7(Text_t356221433 * value)
	{
		___debugText_7 = value;
		Il2CppCodeGenWriteBarrier(&___debugText_7, value);
	}

	inline static int32_t get_offset_of_isSubscribed_8() { return static_cast<int32_t>(offsetof(MainGame_t3800664731, ___isSubscribed_8)); }
	inline String_t* get_isSubscribed_8() const { return ___isSubscribed_8; }
	inline String_t** get_address_of_isSubscribed_8() { return &___isSubscribed_8; }
	inline void set_isSubscribed_8(String_t* value)
	{
		___isSubscribed_8 = value;
		Il2CppCodeGenWriteBarrier(&___isSubscribed_8, value);
	}
};

struct MainGame_t3800664731_StaticFields
{
public:
	// Question[] MainGame::questions
	QuestionU5BU5D_t1028533817* ___questions_2;
	// System.String MainGame::createdSub
	String_t* ___createdSub_5;
	// System.String MainGame::expiredSub
	String_t* ___expiredSub_6;
	// System.Boolean MainGame::verfiSubscribe
	bool ___verfiSubscribe_9;

public:
	inline static int32_t get_offset_of_questions_2() { return static_cast<int32_t>(offsetof(MainGame_t3800664731_StaticFields, ___questions_2)); }
	inline QuestionU5BU5D_t1028533817* get_questions_2() const { return ___questions_2; }
	inline QuestionU5BU5D_t1028533817** get_address_of_questions_2() { return &___questions_2; }
	inline void set_questions_2(QuestionU5BU5D_t1028533817* value)
	{
		___questions_2 = value;
		Il2CppCodeGenWriteBarrier(&___questions_2, value);
	}

	inline static int32_t get_offset_of_createdSub_5() { return static_cast<int32_t>(offsetof(MainGame_t3800664731_StaticFields, ___createdSub_5)); }
	inline String_t* get_createdSub_5() const { return ___createdSub_5; }
	inline String_t** get_address_of_createdSub_5() { return &___createdSub_5; }
	inline void set_createdSub_5(String_t* value)
	{
		___createdSub_5 = value;
		Il2CppCodeGenWriteBarrier(&___createdSub_5, value);
	}

	inline static int32_t get_offset_of_expiredSub_6() { return static_cast<int32_t>(offsetof(MainGame_t3800664731_StaticFields, ___expiredSub_6)); }
	inline String_t* get_expiredSub_6() const { return ___expiredSub_6; }
	inline String_t** get_address_of_expiredSub_6() { return &___expiredSub_6; }
	inline void set_expiredSub_6(String_t* value)
	{
		___expiredSub_6 = value;
		Il2CppCodeGenWriteBarrier(&___expiredSub_6, value);
	}

	inline static int32_t get_offset_of_verfiSubscribe_9() { return static_cast<int32_t>(offsetof(MainGame_t3800664731_StaticFields, ___verfiSubscribe_9)); }
	inline bool get_verfiSubscribe_9() const { return ___verfiSubscribe_9; }
	inline bool* get_address_of_verfiSubscribe_9() { return &___verfiSubscribe_9; }
	inline void set_verfiSubscribe_9(bool value)
	{
		___verfiSubscribe_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
