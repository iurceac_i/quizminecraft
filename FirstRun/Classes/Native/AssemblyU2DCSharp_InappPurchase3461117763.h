﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.IAppleExtensions
struct IAppleExtensions_t1627764765;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InappPurchase
struct  InappPurchase_t3461117763  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Purchasing.IAppleExtensions InappPurchase::m_AppleExtensions
	Il2CppObject * ___m_AppleExtensions_5;

public:
	inline static int32_t get_offset_of_m_AppleExtensions_5() { return static_cast<int32_t>(offsetof(InappPurchase_t3461117763, ___m_AppleExtensions_5)); }
	inline Il2CppObject * get_m_AppleExtensions_5() const { return ___m_AppleExtensions_5; }
	inline Il2CppObject ** get_address_of_m_AppleExtensions_5() { return &___m_AppleExtensions_5; }
	inline void set_m_AppleExtensions_5(Il2CppObject * value)
	{
		___m_AppleExtensions_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_AppleExtensions_5, value);
	}
};

struct InappPurchase_t3461117763_StaticFields
{
public:
	// System.Boolean InappPurchase::isPurchased
	bool ___isPurchased_2;
	// UnityEngine.Purchasing.IStoreController InappPurchase::m_StoreController
	Il2CppObject * ___m_StoreController_3;
	// UnityEngine.Purchasing.IExtensionProvider InappPurchase::m_StoreExtensionProvider
	Il2CppObject * ___m_StoreExtensionProvider_4;
	// System.String InappPurchase::kProductIDConsumable
	String_t* ___kProductIDConsumable_6;
	// System.String InappPurchase::kProductIDNonConsumable
	String_t* ___kProductIDNonConsumable_7;
	// System.String InappPurchase::kProductIDSubscription
	String_t* ___kProductIDSubscription_8;
	// System.String InappPurchase::kProductNameAppleSubscription
	String_t* ___kProductNameAppleSubscription_9;

public:
	inline static int32_t get_offset_of_isPurchased_2() { return static_cast<int32_t>(offsetof(InappPurchase_t3461117763_StaticFields, ___isPurchased_2)); }
	inline bool get_isPurchased_2() const { return ___isPurchased_2; }
	inline bool* get_address_of_isPurchased_2() { return &___isPurchased_2; }
	inline void set_isPurchased_2(bool value)
	{
		___isPurchased_2 = value;
	}

	inline static int32_t get_offset_of_m_StoreController_3() { return static_cast<int32_t>(offsetof(InappPurchase_t3461117763_StaticFields, ___m_StoreController_3)); }
	inline Il2CppObject * get_m_StoreController_3() const { return ___m_StoreController_3; }
	inline Il2CppObject ** get_address_of_m_StoreController_3() { return &___m_StoreController_3; }
	inline void set_m_StoreController_3(Il2CppObject * value)
	{
		___m_StoreController_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreController_3, value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_4() { return static_cast<int32_t>(offsetof(InappPurchase_t3461117763_StaticFields, ___m_StoreExtensionProvider_4)); }
	inline Il2CppObject * get_m_StoreExtensionProvider_4() const { return ___m_StoreExtensionProvider_4; }
	inline Il2CppObject ** get_address_of_m_StoreExtensionProvider_4() { return &___m_StoreExtensionProvider_4; }
	inline void set_m_StoreExtensionProvider_4(Il2CppObject * value)
	{
		___m_StoreExtensionProvider_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreExtensionProvider_4, value);
	}

	inline static int32_t get_offset_of_kProductIDConsumable_6() { return static_cast<int32_t>(offsetof(InappPurchase_t3461117763_StaticFields, ___kProductIDConsumable_6)); }
	inline String_t* get_kProductIDConsumable_6() const { return ___kProductIDConsumable_6; }
	inline String_t** get_address_of_kProductIDConsumable_6() { return &___kProductIDConsumable_6; }
	inline void set_kProductIDConsumable_6(String_t* value)
	{
		___kProductIDConsumable_6 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDConsumable_6, value);
	}

	inline static int32_t get_offset_of_kProductIDNonConsumable_7() { return static_cast<int32_t>(offsetof(InappPurchase_t3461117763_StaticFields, ___kProductIDNonConsumable_7)); }
	inline String_t* get_kProductIDNonConsumable_7() const { return ___kProductIDNonConsumable_7; }
	inline String_t** get_address_of_kProductIDNonConsumable_7() { return &___kProductIDNonConsumable_7; }
	inline void set_kProductIDNonConsumable_7(String_t* value)
	{
		___kProductIDNonConsumable_7 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDNonConsumable_7, value);
	}

	inline static int32_t get_offset_of_kProductIDSubscription_8() { return static_cast<int32_t>(offsetof(InappPurchase_t3461117763_StaticFields, ___kProductIDSubscription_8)); }
	inline String_t* get_kProductIDSubscription_8() const { return ___kProductIDSubscription_8; }
	inline String_t** get_address_of_kProductIDSubscription_8() { return &___kProductIDSubscription_8; }
	inline void set_kProductIDSubscription_8(String_t* value)
	{
		___kProductIDSubscription_8 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDSubscription_8, value);
	}

	inline static int32_t get_offset_of_kProductNameAppleSubscription_9() { return static_cast<int32_t>(offsetof(InappPurchase_t3461117763_StaticFields, ___kProductNameAppleSubscription_9)); }
	inline String_t* get_kProductNameAppleSubscription_9() const { return ___kProductNameAppleSubscription_9; }
	inline String_t** get_address_of_kProductNameAppleSubscription_9() { return &___kProductNameAppleSubscription_9; }
	inline void set_kProductNameAppleSubscription_9(String_t* value)
	{
		___kProductNameAppleSubscription_9 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameAppleSubscription_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
