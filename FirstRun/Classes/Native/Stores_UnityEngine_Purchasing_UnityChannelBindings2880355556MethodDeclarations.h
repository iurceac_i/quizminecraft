﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.UnityChannelBindings
struct UnityChannelBindings_t2880355556;
// System.String
struct String_t;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t1865222972;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>
struct ReadOnlyCollection_1_t2128260960;
// System.Action`3<System.Boolean,System.String,System.String>
struct Action_3_t3864773326;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>
struct Dictionary_2_t853706424;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Core_System_Action3226471752.h"

// System.Void UnityEngine.Purchasing.UnityChannelBindings::Purchase(System.String,System.Action`2<System.Boolean,System.String>)
extern "C"  void UnityChannelBindings_Purchase_m624945367 (UnityChannelBindings_t2880355556 * __this, String_t* ___productId0, Action_2_t1865222972 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelBindings::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>,System.Action`2<System.Boolean,System.String>)
extern "C"  void UnityChannelBindings_RetrieveProducts_m1095305944 (UnityChannelBindings_t2880355556 * __this, ReadOnlyCollection_1_t2128260960 * ___products0, Action_2_t1865222972 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelBindings::ValidateReceipt(System.String,System.Action`3<System.Boolean,System.String,System.String>)
extern "C"  void UnityChannelBindings_ValidateReceipt_m3411439973 (UnityChannelBindings_t2880355556 * __this, String_t* ___transactionId0, Action_3_t3864773326 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelBindings::RequestUniquely(System.String,System.Action`3<System.Boolean,System.String,System.String>,System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>,System.Action)
extern "C"  void UnityChannelBindings_RequestUniquely_m1439297133 (UnityChannelBindings_t2880355556 * __this, String_t* ___transactionId0, Action_3_t3864773326 * ___callback1, Dictionary_2_t853706424 * ___callbackDictionary2, Action_t3226471752 * ___requestAction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelBindings::RetrieveProducts(System.String)
extern "C"  void UnityChannelBindings_RetrieveProducts_m2334981462 (UnityChannelBindings_t2880355556 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelBindings::Purchase(System.String,System.String)
extern "C"  void UnityChannelBindings_Purchase_m2162196425 (UnityChannelBindings_t2880355556 * __this, String_t* ___productJSON0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelBindings::FinishTransaction(System.String,System.String)
extern "C"  void UnityChannelBindings_FinishTransaction_m1363685441 (UnityChannelBindings_t2880355556 * __this, String_t* ___productJSON0, String_t* ___transactionID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelBindings::.ctor()
extern "C"  void UnityChannelBindings__ctor_m3449949178 (UnityChannelBindings_t2880355556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
