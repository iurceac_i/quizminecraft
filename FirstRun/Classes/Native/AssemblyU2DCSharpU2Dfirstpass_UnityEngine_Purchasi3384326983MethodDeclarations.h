﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent
struct OnPurchaseCompletedEvent_t3384326983;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent::.ctor()
extern "C"  void OnPurchaseCompletedEvent__ctor_m2960164866 (OnPurchaseCompletedEvent_t3384326983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
