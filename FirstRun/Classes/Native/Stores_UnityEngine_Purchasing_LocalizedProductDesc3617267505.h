﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.LocalizedProductDescription/<>c
struct U3CU3Ec_t3617267505;
// System.Text.RegularExpressions.MatchEvaluator
struct MatchEvaluator_t710107290;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.LocalizedProductDescription/<>c
struct  U3CU3Ec_t3617267505  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t3617267505_StaticFields
{
public:
	// UnityEngine.Purchasing.LocalizedProductDescription/<>c UnityEngine.Purchasing.LocalizedProductDescription/<>c::<>9
	U3CU3Ec_t3617267505 * ___U3CU3E9_0;
	// System.Text.RegularExpressions.MatchEvaluator UnityEngine.Purchasing.LocalizedProductDescription/<>c::<>9__11_0
	MatchEvaluator_t710107290 * ___U3CU3E9__11_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3617267505_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3617267505 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3617267505 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3617267505 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3617267505_StaticFields, ___U3CU3E9__11_0_1)); }
	inline MatchEvaluator_t710107290 * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline MatchEvaluator_t710107290 ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(MatchEvaluator_t710107290 * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__11_0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
