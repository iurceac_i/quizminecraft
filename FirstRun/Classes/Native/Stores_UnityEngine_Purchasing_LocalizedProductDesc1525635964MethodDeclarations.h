﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.LocalizedProductDescription
struct LocalizedProductDescription_t1525635964;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String UnityEngine.Purchasing.LocalizedProductDescription::get_Title()
extern "C"  String_t* LocalizedProductDescription_get_Title_m3689615546 (LocalizedProductDescription_t1525635964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.LocalizedProductDescription::get_Description()
extern "C"  String_t* LocalizedProductDescription_get_Description_m943727562 (LocalizedProductDescription_t1525635964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.LocalizedProductDescription::DecodeNonLatinCharacters(System.String)
extern "C"  String_t* LocalizedProductDescription_DecodeNonLatinCharacters_m2452865504 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.LocalizedProductDescription::.ctor()
extern "C"  void LocalizedProductDescription__ctor_m112948676 (LocalizedProductDescription_t1525635964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
