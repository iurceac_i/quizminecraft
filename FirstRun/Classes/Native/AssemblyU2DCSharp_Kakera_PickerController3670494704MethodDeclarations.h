﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Kakera.PickerController
struct PickerController_t3670494704;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"

// System.Void Kakera.PickerController::.ctor()
extern "C"  void PickerController__ctor_m2804163018 (PickerController_t3670494704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.PickerController::Awake()
extern "C"  void PickerController_Awake_m238727193 (PickerController_t3670494704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.PickerController::OnPressShowPicker()
extern "C"  void PickerController_OnPressShowPicker_m1265903567 (PickerController_t3670494704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Kakera.PickerController::LoadImage(System.String,UnityEngine.UI.Image)
extern "C"  Il2CppObject * PickerController_LoadImage_m2557733243 (PickerController_t3670494704 * __this, String_t* ___path0, Image_t2042527209 * ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Kakera.PickerController::<Awake>m__0(System.String)
extern "C"  void PickerController_U3CAwakeU3Em__0_m1932660688 (PickerController_t3670494704 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
