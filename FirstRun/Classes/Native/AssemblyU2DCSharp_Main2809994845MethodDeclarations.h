﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Main
struct Main_t2809994845;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Main::.ctor()
extern "C"  void Main__ctor_m325986520 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Main::get_store_id()
extern "C"  String_t* Main_get_store_id_m2078185357 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Main::get_store_App_url()
extern "C"  String_t* Main_get_store_App_url_m1213734165 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Main::get_product_id()
extern "C"  String_t* Main_get_product_id_m3829812457 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
