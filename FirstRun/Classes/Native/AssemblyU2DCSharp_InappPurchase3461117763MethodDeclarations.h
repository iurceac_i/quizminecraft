﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InappPurchase
struct InappPurchase_t3461117763;
// System.String
struct String_t;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_t547992434;
// UnityEngine.Purchasing.Product
struct Product_t1203687971;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "mscorlib_System_Nullable_1_gen3251239280.h"

// System.Void InappPurchase::.ctor()
extern "C"  void InappPurchase__ctor_m3079851024 (InappPurchase_t3461117763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::Start()
extern "C"  void InappPurchase_Start_m2598178612 (InappPurchase_t3461117763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::InitializePurchasing()
extern "C"  void InappPurchase_InitializePurchasing_m2295598604 (InappPurchase_t3461117763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InappPurchase::IsInitialized()
extern "C"  bool InappPurchase_IsInitialized_m2868860492 (InappPurchase_t3461117763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::BuySubscription()
extern "C"  void InappPurchase_BuySubscription_m2702495767 (InappPurchase_t3461117763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::BuyProductID(System.String)
extern "C"  void InappPurchase_BuyProductID_m1572290882 (InappPurchase_t3461117763 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::RestorePurchases()
extern "C"  void InappPurchase_RestorePurchases_m3286949500 (InappPurchase_t3461117763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern "C"  void InappPurchase_OnInitialized_m3876423718 (InappPurchase_t3461117763 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern "C"  void InappPurchase_OnInitializeFailed_m3778064935 (InappPurchase_t3461117763 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.PurchaseProcessingResult InappPurchase::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern "C"  int32_t InappPurchase_ProcessPurchase_m3658573002 (InappPurchase_t3461117763 * __this, PurchaseEventArgs_t547992434 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern "C"  void InappPurchase_OnPurchaseFailed_m3469875515 (InappPurchase_t3461117763 * __this, Product_t1203687971 * ___product0, int32_t ___failureReason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::OnTransactionsRestored(System.Boolean)
extern "C"  void InappPurchase_OnTransactionsRestored_m902677147 (InappPurchase_t3461117763 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::OnDeferred(UnityEngine.Purchasing.Product)
extern "C"  void InappPurchase_OnDeferred_m3930573962 (InappPurchase_t3461117763 * __this, Product_t1203687971 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> InappPurchase::validate(System.String,System.String)
extern "C"  Nullable_1_t3251239280  InappPurchase_validate_m2993361072 (InappPurchase_t3461117763 * __this, String_t* ___receiptStr0, String_t* ___myProduct1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::verifyOnStart()
extern "C"  void InappPurchase_verifyOnStart_m3766824720 (InappPurchase_t3461117763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::.cctor()
extern "C"  void InappPurchase__cctor_m1040427937 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InappPurchase::<RestorePurchases>m__0(System.Boolean)
extern "C"  void InappPurchase_U3CRestorePurchasesU3Em__0_m2079244172 (InappPurchase_t3461117763 * __this, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
