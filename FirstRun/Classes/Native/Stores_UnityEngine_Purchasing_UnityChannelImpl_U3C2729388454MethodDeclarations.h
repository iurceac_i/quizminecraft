﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1
struct U3CU3Ec__DisplayClass7_1_t2729388454;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_1__ctor_m2463119227 (U3CU3Ec__DisplayClass7_1_t2729388454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1::<Purchase>b__1(System.Boolean,System.String,System.String)
extern "C"  void U3CU3Ec__DisplayClass7_1_U3CPurchaseU3Eb__1_m1821532482 (U3CU3Ec__DisplayClass7_1_t2729388454 * __this, bool ___success0, String_t* ___signData1, String_t* ___signature2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
