﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainGame
struct MainGame_t3800664731;
// Question[]
struct QuestionU5BU5D_t1028533817;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MainGame::.ctor()
extern "C"  void MainGame__ctor_m2380176706 (MainGame_t3800664731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::Start()
extern "C"  void MainGame_Start_m520251270 (MainGame_t3800664731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Question[] MainGame::ReadJson()
extern "C"  QuestionU5BU5D_t1028533817* MainGame_ReadJson_m2628908067 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Question[] MainGame::getQuestions()
extern "C"  QuestionU5BU5D_t1028533817* MainGame_getQuestions_m2295499002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::ActivePanel(UnityEngine.GameObject)
extern "C"  void MainGame_ActivePanel_m1042157020 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::BackButton(UnityEngine.GameObject)
extern "C"  void MainGame_BackButton_m1576195507 (MainGame_t3800664731 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::SendDataTime(System.String,System.String)
extern "C"  void MainGame_SendDataTime_m3143383431 (MainGame_t3800664731 * __this, String_t* ___createSubscription0, String_t* ___expireSubscription1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainGame::VerifySubscribe()
extern "C"  void MainGame_VerifySubscribe_m3291854269 (MainGame_t3800664731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
