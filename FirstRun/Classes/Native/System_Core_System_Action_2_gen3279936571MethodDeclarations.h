﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"

// System.Void System.Action`2<System.Object,System.Byte[]>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2344398591(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3279936571 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3362391082_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Object,System.Byte[]>::Invoke(T1,T2)
#define Action_2_Invoke_m2316020026(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3279936571 *, Il2CppObject *, ByteU5BU5D_t3397334013*, const MethodInfo*))Action_2_Invoke_m1501152969_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Object,System.Byte[]>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m2467335355(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3279936571 *, Il2CppObject *, ByteU5BU5D_t3397334013*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1914861552_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Object,System.Byte[]>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1320346973(__this, ___result0, method) ((  void (*) (Action_2_t3279936571 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3956733788_gshared)(__this, ___result0, method)
