﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.Boolean,System.String>
struct Action_2_t1865222972;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>
struct Dictionary_2_t853706424;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelBindings
struct  UnityChannelBindings_t2880355556  : public Il2CppObject
{
public:
	// System.Action`2<System.Boolean,System.String> UnityEngine.Purchasing.UnityChannelBindings::m_PurchaseCallback
	Action_2_t1865222972 * ___m_PurchaseCallback_0;
	// System.String UnityEngine.Purchasing.UnityChannelBindings::m_PurchaseGuid
	String_t* ___m_PurchaseGuid_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>> UnityEngine.Purchasing.UnityChannelBindings::m_ValidateCallbacks
	Dictionary_2_t853706424 * ___m_ValidateCallbacks_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>> UnityEngine.Purchasing.UnityChannelBindings::m_PurchaseConfirmCallbacks
	Dictionary_2_t853706424 * ___m_PurchaseConfirmCallbacks_3;

public:
	inline static int32_t get_offset_of_m_PurchaseCallback_0() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t2880355556, ___m_PurchaseCallback_0)); }
	inline Action_2_t1865222972 * get_m_PurchaseCallback_0() const { return ___m_PurchaseCallback_0; }
	inline Action_2_t1865222972 ** get_address_of_m_PurchaseCallback_0() { return &___m_PurchaseCallback_0; }
	inline void set_m_PurchaseCallback_0(Action_2_t1865222972 * value)
	{
		___m_PurchaseCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_PurchaseCallback_0, value);
	}

	inline static int32_t get_offset_of_m_PurchaseGuid_1() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t2880355556, ___m_PurchaseGuid_1)); }
	inline String_t* get_m_PurchaseGuid_1() const { return ___m_PurchaseGuid_1; }
	inline String_t** get_address_of_m_PurchaseGuid_1() { return &___m_PurchaseGuid_1; }
	inline void set_m_PurchaseGuid_1(String_t* value)
	{
		___m_PurchaseGuid_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_PurchaseGuid_1, value);
	}

	inline static int32_t get_offset_of_m_ValidateCallbacks_2() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t2880355556, ___m_ValidateCallbacks_2)); }
	inline Dictionary_2_t853706424 * get_m_ValidateCallbacks_2() const { return ___m_ValidateCallbacks_2; }
	inline Dictionary_2_t853706424 ** get_address_of_m_ValidateCallbacks_2() { return &___m_ValidateCallbacks_2; }
	inline void set_m_ValidateCallbacks_2(Dictionary_2_t853706424 * value)
	{
		___m_ValidateCallbacks_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_ValidateCallbacks_2, value);
	}

	inline static int32_t get_offset_of_m_PurchaseConfirmCallbacks_3() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t2880355556, ___m_PurchaseConfirmCallbacks_3)); }
	inline Dictionary_2_t853706424 * get_m_PurchaseConfirmCallbacks_3() const { return ___m_PurchaseConfirmCallbacks_3; }
	inline Dictionary_2_t853706424 ** get_address_of_m_PurchaseConfirmCallbacks_3() { return &___m_PurchaseConfirmCallbacks_3; }
	inline void set_m_PurchaseConfirmCallbacks_3(Dictionary_2_t853706424 * value)
	{
		___m_PurchaseConfirmCallbacks_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_PurchaseConfirmCallbacks_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
