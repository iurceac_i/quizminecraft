﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen498085336MethodDeclarations.h"

// System.Void System.Action`3<UnityEngine.Color[],System.Byte[],System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m313799129(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t2179559061 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m368200038_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<UnityEngine.Color[],System.Byte[],System.Int32>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m742589245(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t2179559061 *, ColorU5BU5D_t672350442*, ByteU5BU5D_t3397334013*, int32_t, const MethodInfo*))Action_3_Invoke_m3918104480_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<UnityEngine.Color[],System.Byte[],System.Int32>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m2888274350(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t2179559061 *, ColorU5BU5D_t672350442*, ByteU5BU5D_t3397334013*, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m2980569513_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<UnityEngine.Color[],System.Byte[],System.Int32>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m485527855(__this, ___result0, method) ((  void (*) (Action_3_t2179559061 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m3099811844_gshared)(__this, ___result0, method)
