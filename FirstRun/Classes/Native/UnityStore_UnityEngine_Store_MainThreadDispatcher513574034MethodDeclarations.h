﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Store.MainThreadDispatcher
struct MainThreadDispatcher_t513574034;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"

// System.Void UnityEngine.Store.MainThreadDispatcher::.ctor()
extern "C"  void MainThreadDispatcher__ctor_m292601619 (MainThreadDispatcher_t513574034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Store.MainThreadDispatcher::RunOnMainThread(System.Action)
extern "C"  void MainThreadDispatcher_RunOnMainThread_m3425248423 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___runnable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Store.MainThreadDispatcher::Start()
extern "C"  void MainThreadDispatcher_Start_m3725736351 (MainThreadDispatcher_t513574034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Store.MainThreadDispatcher::Update()
extern "C"  void MainThreadDispatcher_Update_m546764710 (MainThreadDispatcher_t513574034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Store.MainThreadDispatcher::.cctor()
extern "C"  void MainThreadDispatcher__cctor_m2497885682 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
