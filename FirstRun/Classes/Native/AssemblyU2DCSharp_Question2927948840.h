﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Question
struct  Question_t2927948840  : public Il2CppObject
{
public:
	// System.String Question::answer
	String_t* ___answer_0;
	// System.String[] Question::choices
	StringU5BU5D_t1642385972* ___choices_1;
	// System.String Question::question
	String_t* ___question_2;

public:
	inline static int32_t get_offset_of_answer_0() { return static_cast<int32_t>(offsetof(Question_t2927948840, ___answer_0)); }
	inline String_t* get_answer_0() const { return ___answer_0; }
	inline String_t** get_address_of_answer_0() { return &___answer_0; }
	inline void set_answer_0(String_t* value)
	{
		___answer_0 = value;
		Il2CppCodeGenWriteBarrier(&___answer_0, value);
	}

	inline static int32_t get_offset_of_choices_1() { return static_cast<int32_t>(offsetof(Question_t2927948840, ___choices_1)); }
	inline StringU5BU5D_t1642385972* get_choices_1() const { return ___choices_1; }
	inline StringU5BU5D_t1642385972** get_address_of_choices_1() { return &___choices_1; }
	inline void set_choices_1(StringU5BU5D_t1642385972* value)
	{
		___choices_1 = value;
		Il2CppCodeGenWriteBarrier(&___choices_1, value);
	}

	inline static int32_t get_offset_of_question_2() { return static_cast<int32_t>(offsetof(Question_t2927948840, ___question_2)); }
	inline String_t* get_question_2() const { return ___question_2; }
	inline String_t** get_address_of_question_2() { return &___question_2; }
	inline void set_question_2(String_t* value)
	{
		___question_2 = value;
		Il2CppCodeGenWriteBarrier(&___question_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
