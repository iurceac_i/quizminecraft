﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>
struct Dictionary_2_t1855157670;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Purchasing.AppStore>
struct IEqualityComparer_1_t3886704302;
// System.Collections.Generic.IDictionary`2<UnityEngine.Purchasing.AppStore,System.Object>
struct IDictionary_2_t4149208387;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.AppStore>
struct ICollection_1_t1331179533;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>[]
struct KeyValuePair_2U5BU5D_t1039730021;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>>
struct IEnumerator_1_t1382994015;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Purchasing.AppStore,System.Object>
struct KeyCollection_t43688145;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.Object>
struct ValueCollection_t558217513;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23907470188.h"
#include "mscorlib_System_Array3829468939.h"
#include "Stores_UnityEngine_Purchasing_AppStore379104228.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3175182372.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3024962984_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3024962984(__this, method) ((  void (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2__ctor_m3024962984_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2722323875_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2722323875(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1855157670 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2722323875_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3860002482_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3860002482(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1855157670 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3860002482_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2937622197_gshared (Dictionary_2_t1855157670 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2937622197(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1855157670 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2937622197_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2900147973_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2900147973(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1855157670 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2900147973_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m766724579_gshared (Dictionary_2_t1855157670 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m766724579(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1855157670 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m766724579_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1715566750_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1715566750(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1715566750_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1813012598_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1813012598(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1813012598_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m3624529434_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3624529434(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m3624529434_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m384793648_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m384793648(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1855157670 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m384793648_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1802009163_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1802009163(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1855157670 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1802009163_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2788471002_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2788471002(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1855157670 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2788471002_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2546880570_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2546880570(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1855157670 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2546880570_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1835079463_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1835079463(__this, ___key0, method) ((  void (*) (Dictionary_2_t1855157670 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1835079463_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2511753984_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2511753984(__this, method) ((  bool (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2511753984_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2012905232_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2012905232(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2012905232_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m490137618_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m490137618(__this, method) ((  bool (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m490137618_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1730924155_gshared (Dictionary_2_t1855157670 * __this, KeyValuePair_2_t3907470188  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1730924155(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1855157670 *, KeyValuePair_2_t3907470188 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1730924155_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1560822153_gshared (Dictionary_2_t1855157670 * __this, KeyValuePair_2_t3907470188  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1560822153(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1855157670 *, KeyValuePair_2_t3907470188 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1560822153_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2172333663_gshared (Dictionary_2_t1855157670 * __this, KeyValuePair_2U5BU5D_t1039730021* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2172333663(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1855157670 *, KeyValuePair_2U5BU5D_t1039730021*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2172333663_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2631931420_gshared (Dictionary_2_t1855157670 * __this, KeyValuePair_2_t3907470188  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2631931420(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1855157670 *, KeyValuePair_2_t3907470188 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2631931420_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m554141264_gshared (Dictionary_2_t1855157670 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m554141264(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1855157670 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m554141264_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3042607357_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3042607357(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3042607357_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2767671946_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2767671946(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2767671946_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1319233599_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1319233599(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1319233599_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m102785280_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m102785280(__this, method) ((  int32_t (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_get_Count_m102785280_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m789705671_gshared (Dictionary_2_t1855157670 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m789705671(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1855157670 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m789705671_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3077432618_gshared (Dictionary_2_t1855157670 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3077432618(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1855157670 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m3077432618_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m753564156_gshared (Dictionary_2_t1855157670 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m753564156(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1855157670 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m753564156_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m1483463677_gshared (Dictionary_2_t1855157670 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1483463677(__this, ___size0, method) ((  void (*) (Dictionary_2_t1855157670 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1483463677_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2876750639_gshared (Dictionary_2_t1855157670 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2876750639(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1855157670 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2876750639_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3907470188  Dictionary_2_make_pair_m712958813_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m712958813(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3907470188  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m712958813_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m3156138461_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3156138461(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m3156138461_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3797801357_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3797801357(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3797801357_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2273796686_gshared (Dictionary_2_t1855157670 * __this, KeyValuePair_2U5BU5D_t1039730021* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2273796686(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1855157670 *, KeyValuePair_2U5BU5D_t1039730021*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2273796686_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m708153138_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m708153138(__this, method) ((  void (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_Resize_m708153138_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2610246905_gshared (Dictionary_2_t1855157670 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2610246905(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1855157670 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m2610246905_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m1050298221_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1050298221(__this, method) ((  void (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_Clear_m1050298221_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1085928479_gshared (Dictionary_2_t1855157670 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1085928479(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1855157670 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1085928479_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3712557903_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3712557903(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1855157670 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3712557903_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1564145572_gshared (Dictionary_2_t1855157670 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1564145572(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1855157670 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m1564145572_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2785278122_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2785278122(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1855157670 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2785278122_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1812135761_gshared (Dictionary_2_t1855157670 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1812135761(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1855157670 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m1812135761_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1691672222_gshared (Dictionary_2_t1855157670 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1691672222(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1855157670 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m1691672222_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::get_Keys()
extern "C"  KeyCollection_t43688145 * Dictionary_2_get_Keys_m1416373403_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1416373403(__this, method) ((  KeyCollection_t43688145 * (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_get_Keys_m1416373403_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::get_Values()
extern "C"  ValueCollection_t558217513 * Dictionary_2_get_Values_m3401451059_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3401451059(__this, method) ((  ValueCollection_t558217513 * (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_get_Values_m3401451059_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1254663640_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1254663640(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1855157670 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1254663640_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2140622672_gshared (Dictionary_2_t1855157670 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2140622672(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1855157670 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2140622672_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m275170438_gshared (Dictionary_2_t1855157670 * __this, KeyValuePair_2_t3907470188  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m275170438(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1855157670 *, KeyValuePair_2_t3907470188 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m275170438_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3175182372  Dictionary_2_GetEnumerator_m1636019681_gshared (Dictionary_2_t1855157670 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1636019681(__this, method) ((  Enumerator_t3175182372  (*) (Dictionary_2_t1855157670 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1636019681_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m1065149304_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1065149304(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1065149304_gshared)(__this /* static, unused */, ___key0, ___value1, method)
