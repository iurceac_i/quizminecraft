﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String>
struct Action_3_t681716397;
// UnityEngine.Purchasing.MoolahStoreImpl
struct MoolahStoreImpl_t4206626141;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// UnityEngine.WWW
struct WWW_t2919945039;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47
struct  U3CValidateReceiptProcessU3Ed__47_t1085247229  : public Il2CppObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<>2__current
	Il2CppObject * ___U3CU3E2__current_1;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::transactionId
	String_t* ___transactionId_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::receipt
	String_t* ___receipt_3;
	// System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::result
	Action_3_t681716397 * ___result_4;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<>4__this
	MoolahStoreImpl_t4206626141 * ___U3CU3E4__this_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<tempJson>5__1
	Dictionary_2_t309261261 * ___U3CtempJsonU3E5__1_6;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<wf>5__2
	WWWForm_t3950226929 * ___U3CwfU3E5__2_7;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<sign>5__3
	String_t* ___U3CsignU3E5__3_8;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<w>5__4
	WWW_t2919945039 * ___U3CwU3E5__4_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<jsonObjects>5__5
	Dictionary_2_t309261261 * ___U3CjsonObjectsU3E5__5_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<code>5__6
	String_t* ___U3CcodeU3E5__6_11;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<msg>5__7
	String_t* ___U3CmsgU3E5__7_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___U3CU3E2__current_1)); }
	inline Il2CppObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Il2CppObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Il2CppObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_1, value);
	}

	inline static int32_t get_offset_of_transactionId_2() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___transactionId_2)); }
	inline String_t* get_transactionId_2() const { return ___transactionId_2; }
	inline String_t** get_address_of_transactionId_2() { return &___transactionId_2; }
	inline void set_transactionId_2(String_t* value)
	{
		___transactionId_2 = value;
		Il2CppCodeGenWriteBarrier(&___transactionId_2, value);
	}

	inline static int32_t get_offset_of_receipt_3() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___receipt_3)); }
	inline String_t* get_receipt_3() const { return ___receipt_3; }
	inline String_t** get_address_of_receipt_3() { return &___receipt_3; }
	inline void set_receipt_3(String_t* value)
	{
		___receipt_3 = value;
		Il2CppCodeGenWriteBarrier(&___receipt_3, value);
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___result_4)); }
	inline Action_3_t681716397 * get_result_4() const { return ___result_4; }
	inline Action_3_t681716397 ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Action_3_t681716397 * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier(&___result_4, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___U3CU3E4__this_5)); }
	inline MoolahStoreImpl_t4206626141 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline MoolahStoreImpl_t4206626141 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(MoolahStoreImpl_t4206626141 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_5, value);
	}

	inline static int32_t get_offset_of_U3CtempJsonU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___U3CtempJsonU3E5__1_6)); }
	inline Dictionary_2_t309261261 * get_U3CtempJsonU3E5__1_6() const { return ___U3CtempJsonU3E5__1_6; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CtempJsonU3E5__1_6() { return &___U3CtempJsonU3E5__1_6; }
	inline void set_U3CtempJsonU3E5__1_6(Dictionary_2_t309261261 * value)
	{
		___U3CtempJsonU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempJsonU3E5__1_6, value);
	}

	inline static int32_t get_offset_of_U3CwfU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___U3CwfU3E5__2_7)); }
	inline WWWForm_t3950226929 * get_U3CwfU3E5__2_7() const { return ___U3CwfU3E5__2_7; }
	inline WWWForm_t3950226929 ** get_address_of_U3CwfU3E5__2_7() { return &___U3CwfU3E5__2_7; }
	inline void set_U3CwfU3E5__2_7(WWWForm_t3950226929 * value)
	{
		___U3CwfU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwfU3E5__2_7, value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___U3CsignU3E5__3_8)); }
	inline String_t* get_U3CsignU3E5__3_8() const { return ___U3CsignU3E5__3_8; }
	inline String_t** get_address_of_U3CsignU3E5__3_8() { return &___U3CsignU3E5__3_8; }
	inline void set_U3CsignU3E5__3_8(String_t* value)
	{
		___U3CsignU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsignU3E5__3_8, value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__4_9() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___U3CwU3E5__4_9)); }
	inline WWW_t2919945039 * get_U3CwU3E5__4_9() const { return ___U3CwU3E5__4_9; }
	inline WWW_t2919945039 ** get_address_of_U3CwU3E5__4_9() { return &___U3CwU3E5__4_9; }
	inline void set_U3CwU3E5__4_9(WWW_t2919945039 * value)
	{
		___U3CwU3E5__4_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwU3E5__4_9, value);
	}

	inline static int32_t get_offset_of_U3CjsonObjectsU3E5__5_10() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___U3CjsonObjectsU3E5__5_10)); }
	inline Dictionary_2_t309261261 * get_U3CjsonObjectsU3E5__5_10() const { return ___U3CjsonObjectsU3E5__5_10; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CjsonObjectsU3E5__5_10() { return &___U3CjsonObjectsU3E5__5_10; }
	inline void set_U3CjsonObjectsU3E5__5_10(Dictionary_2_t309261261 * value)
	{
		___U3CjsonObjectsU3E5__5_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonObjectsU3E5__5_10, value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E5__6_11() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___U3CcodeU3E5__6_11)); }
	inline String_t* get_U3CcodeU3E5__6_11() const { return ___U3CcodeU3E5__6_11; }
	inline String_t** get_address_of_U3CcodeU3E5__6_11() { return &___U3CcodeU3E5__6_11; }
	inline void set_U3CcodeU3E5__6_11(String_t* value)
	{
		___U3CcodeU3E5__6_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcodeU3E5__6_11, value);
	}

	inline static int32_t get_offset_of_U3CmsgU3E5__7_12() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t1085247229, ___U3CmsgU3E5__7_12)); }
	inline String_t* get_U3CmsgU3E5__7_12() const { return ___U3CmsgU3E5__7_12; }
	inline String_t** get_address_of_U3CmsgU3E5__7_12() { return &___U3CmsgU3E5__7_12; }
	inline void set_U3CmsgU3E5__7_12(String_t* value)
	{
		___U3CmsgU3E5__7_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmsgU3E5__7_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
