﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QuestionController
struct QuestionController_t445239244;
// UnityEngine.UI.Text
struct Text_t356221433;
// Levels[]
struct LevelsU5BU5D_t2050899114;
// Question
struct Question_t2927948840;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// QuestionController/UpdateListener
struct UpdateListener_t3189012468;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "AssemblyU2DCSharp_Question2927948840.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void QuestionController::.ctor()
extern "C"  void QuestionController__ctor_m3917901329 (QuestionController_t445239244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::Start()
extern "C"  void QuestionController_Start_m3168895129 (QuestionController_t445239244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::VerifyQuestion(UnityEngine.UI.Text)
extern "C"  void QuestionController_VerifyQuestion_m118869514 (QuestionController_t445239244 * __this, Text_t356221433 * ___ans0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::SetLevels(Levels[],System.Int32)
extern "C"  void QuestionController_SetLevels_m2193348496 (QuestionController_t445239244 * __this, LevelsU5BU5D_t2050899114* ___level0, int32_t ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::SetQuestion(Question)
extern "C"  void QuestionController_SetQuestion_m3640094679 (QuestionController_t445239244 * __this, Question_t2927948840 * ___quest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::CounterQuestion()
extern "C"  void QuestionController_CounterQuestion_m1292623087 (QuestionController_t445239244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::NextLevel()
extern "C"  void QuestionController_NextLevel_m2486282466 (QuestionController_t445239244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::RetryLevel()
extern "C"  void QuestionController_RetryLevel_m2186892727 (QuestionController_t445239244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::LastMissionComplete()
extern "C"  void QuestionController_LastMissionComplete_m1554503374 (QuestionController_t445239244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::BackButton(UnityEngine.GameObject)
extern "C"  void QuestionController_BackButton_m2933698246 (QuestionController_t445239244 * __this, GameObject_t1756533147 * ___gamePanel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionController::registerListener(QuestionController/UpdateListener)
extern "C"  void QuestionController_registerListener_m712129072 (QuestionController_t445239244 * __this, Il2CppObject * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
