﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13
struct U3CVaildateProductU3Ed__13_t631815662;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::.ctor(System.Int32)
extern "C"  void U3CVaildateProductU3Ed__13__ctor_m2143600610 (U3CVaildateProductU3Ed__13_t631815662 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::System.IDisposable.Dispose()
extern "C"  void U3CVaildateProductU3Ed__13_System_IDisposable_Dispose_m2806579618 (U3CVaildateProductU3Ed__13_t631815662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::MoveNext()
extern "C"  bool U3CVaildateProductU3Ed__13_MoveNext_m1000592967 (U3CVaildateProductU3Ed__13_t631815662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  Il2CppObject * U3CVaildateProductU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3044365410 (U3CVaildateProductU3Ed__13_t631815662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::System.Collections.IEnumerator.Reset()
extern "C"  void U3CVaildateProductU3Ed__13_System_Collections_IEnumerator_Reset_m1095190151 (U3CVaildateProductU3Ed__13_t631815662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CVaildateProductU3Ed__13_System_Collections_IEnumerator_get_Current_m247248667 (U3CVaildateProductU3Ed__13_t631815662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
