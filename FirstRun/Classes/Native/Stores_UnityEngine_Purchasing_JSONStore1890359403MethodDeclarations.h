﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.JSONStore
struct JSONStore_t1890359403;
// UnityEngine.Purchasing.INativeStore
struct INativeStore_t3203646079;
// UnityEngine.Purchasing.Extension.IStoreCallback
struct IStoreCallback_t2691517565;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>
struct ReadOnlyCollection_1_t2128260960;
// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t1942475268;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.JSONStore::.ctor()
extern "C"  void JSONStore__ctor_m1861746843 (JSONStore_t1890359403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.JSONStore::SetNativeStore(UnityEngine.Purchasing.INativeStore)
extern "C"  void JSONStore_SetNativeStore_m574687663 (JSONStore_t1890359403 * __this, Il2CppObject * ___native0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.JSONStore::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern "C"  void JSONStore_Initialize_m1081052102 (JSONStore_t1890359403 * __this, Il2CppObject * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.JSONStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern "C"  void JSONStore_RetrieveProducts_m294379807 (JSONStore_t1890359403 * __this, ReadOnlyCollection_1_t2128260960 * ___products0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.JSONStore::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern "C"  void JSONStore_Purchase_m3988104895 (JSONStore_t1890359403 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.JSONStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern "C"  void JSONStore_FinishTransaction_m1364679771 (JSONStore_t1890359403 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___transactionId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.JSONStore::OnSetupFailed(System.String)
extern "C"  void JSONStore_OnSetupFailed_m2981678534 (JSONStore_t1890359403 * __this, String_t* ___reason0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.JSONStore::OnProductsRetrieved(System.String)
extern "C"  void JSONStore_OnProductsRetrieved_m4004189930 (JSONStore_t1890359403 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.JSONStore::OnPurchaseSucceeded(System.String,System.String,System.String)
extern "C"  void JSONStore_OnPurchaseSucceeded_m1392069030 (JSONStore_t1890359403 * __this, String_t* ___id0, String_t* ___receipt1, String_t* ___transactionID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.JSONStore::OnPurchaseFailed(System.String)
extern "C"  void JSONStore_OnPurchaseFailed_m759651174 (JSONStore_t1890359403 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
