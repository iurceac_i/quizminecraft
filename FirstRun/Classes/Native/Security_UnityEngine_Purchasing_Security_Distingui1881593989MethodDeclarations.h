﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.DistinguishedName
struct DistinguishedName_t1881593989;
// System.String
struct String_t;
// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "Security_UnityEngine_Purchasing_Security_Distingui1881593989.h"

// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_Country()
extern "C"  String_t* DistinguishedName_get_Country_m4278360821 (DistinguishedName_t1881593989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_Country(System.String)
extern "C"  void DistinguishedName_set_Country_m2844947450 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_Organization()
extern "C"  String_t* DistinguishedName_get_Organization_m3989970552 (DistinguishedName_t1881593989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_Organization(System.String)
extern "C"  void DistinguishedName_set_Organization_m2141035885 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_OrganizationalUnit()
extern "C"  String_t* DistinguishedName_get_OrganizationalUnit_m3911770541 (DistinguishedName_t1881593989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_OrganizationalUnit(System.String)
extern "C"  void DistinguishedName_set_OrganizationalUnit_m1833607010 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_Dnq()
extern "C"  String_t* DistinguishedName_get_Dnq_m1662125674 (DistinguishedName_t1881593989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_Dnq(System.String)
extern "C"  void DistinguishedName_set_Dnq_m3889139193 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_State()
extern "C"  String_t* DistinguishedName_get_State_m1071035862 (DistinguishedName_t1881593989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_State(System.String)
extern "C"  void DistinguishedName_set_State_m1863823769 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_CommonName()
extern "C"  String_t* DistinguishedName_get_CommonName_m481243823 (DistinguishedName_t1881593989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_CommonName(System.String)
extern "C"  void DistinguishedName_set_CommonName_m2980787556 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_SerialNumber(System.String)
extern "C"  void DistinguishedName_set_SerialNumber_m3659779817 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void DistinguishedName__ctor_m3658482605 (DistinguishedName_t1881593989 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.Security.DistinguishedName::Equals(UnityEngine.Purchasing.Security.DistinguishedName)
extern "C"  bool DistinguishedName_Equals_m575298400 (DistinguishedName_t1881593989 * __this, DistinguishedName_t1881593989 * ___n20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.DistinguishedName::ToString()
extern "C"  String_t* DistinguishedName_ToString_m3805516334 (DistinguishedName_t1881593989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
