﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IAP.InAppManager
struct InAppManager_t657850267;
// System.String
struct String_t;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_t547992434;
// UnityEngine.Purchasing.Product
struct Product_t1203687971;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"

// System.Void IAP.InAppManager::.ctor()
extern "C"  void InAppManager__ctor_m2644185824 (InAppManager_t657850267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP.InAppManager::Start()
extern "C"  void InAppManager_Start_m3800438144 (InAppManager_t657850267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP.InAppManager::ReadJson()
extern "C"  void InAppManager_ReadJson_m3571219960 (InAppManager_t657850267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP.InAppManager::InitializePurchasing()
extern "C"  void InAppManager_InitializePurchasing_m2523278908 (InAppManager_t657850267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IAP.InAppManager::IsInitialized()
extern "C"  bool InAppManager_IsInitialized_m703064724 (InAppManager_t657850267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP.InAppManager::BuyProductID(System.String)
extern "C"  void InAppManager_BuyProductID_m3335019766 (InAppManager_t657850267 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP.InAppManager::RestorePurchases()
extern "C"  void InAppManager_RestorePurchases_m2961281220 (InAppManager_t657850267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP.InAppManager::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern "C"  void InAppManager_OnInitialized_m3787121530 (InAppManager_t657850267 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP.InAppManager::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern "C"  void InAppManager_OnInitializeFailed_m1478023637 (InAppManager_t657850267 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.PurchaseProcessingResult IAP.InAppManager::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern "C"  int32_t InAppManager_ProcessPurchase_m3115926812 (InAppManager_t657850267 * __this, PurchaseEventArgs_t547992434 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP.InAppManager::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern "C"  void InAppManager_OnPurchaseFailed_m1662743473 (InAppManager_t657850267 * __this, Product_t1203687971 * ___product0, int32_t ___failureReason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IAP.InAppManager::<RestorePurchases>m__0(System.Boolean)
extern "C"  void InAppManager_U3CRestorePurchasesU3Em__0_m3527601028 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
