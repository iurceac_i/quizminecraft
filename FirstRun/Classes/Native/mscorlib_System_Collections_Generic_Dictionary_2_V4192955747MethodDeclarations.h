﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va558217513MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1681088185(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4192955747 *, Dictionary_2_t1194928608 *, const MethodInfo*))ValueCollection__ctor_m265178557_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1053567287(__this, ___item0, method) ((  void (*) (ValueCollection_t4192955747 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3521129867_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1683130604(__this, method) ((  void (*) (ValueCollection_t4192955747 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4117182274_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1425882049(__this, ___item0, method) ((  bool (*) (ValueCollection_t4192955747 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3510268949_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m393216462(__this, ___item0, method) ((  bool (*) (ValueCollection_t4192955747 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1408463800_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3543462496(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4192955747 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2310517290_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1802746024(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4192955747 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2627478578_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2370185063(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4192955747 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m204113011_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1640890482(__this, method) ((  bool (*) (ValueCollection_t4192955747 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m184767916_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3348910252(__this, method) ((  bool (*) (ValueCollection_t4192955747 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4268585782_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1715395592(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4192955747 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m4134424594_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m4119883264(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4192955747 *, StringU5BU5D_t1642385972*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m552790214_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::GetEnumerator()
#define ValueCollection_GetEnumerator_m538624421(__this, method) ((  Enumerator_t2881461372  (*) (ValueCollection_t4192955747 *, const MethodInfo*))ValueCollection_GetEnumerator_m827968417_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Purchasing.AppStore,System.String>::get_Count()
#define ValueCollection_get_Count_m1513640716(__this, method) ((  int32_t (*) (ValueCollection_t4192955747 *, const MethodInfo*))ValueCollection_get_Count_m2125382038_gshared)(__this, method)
