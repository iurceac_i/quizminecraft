﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.ChannelPurchase.PurchaseForwardCallback
struct PurchaseForwardCallback_t144617755;
// UnityEngine.ChannelPurchase.IPurchaseListener
struct IPurchaseListener_t2367674166;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "ChannelPurchase_U3CModuleU3E3783534214.h"
#include "ChannelPurchase_U3CModuleU3E3783534214MethodDeclarations.h"
#include "ChannelPurchase_UnityEngine_ChannelPurchase_Purchas144617755.h"
#include "ChannelPurchase_UnityEngine_ChannelPurchase_Purchas144617755MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_AndroidJavaProxy4274989947MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "ChannelPurchase_UnityEngine_ChannelPurchase_Purchas659182236.h"
#include "ChannelPurchase_UnityEngine_ChannelPurchase_Purchas659182236MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583MethodDeclarations.h"

// System.Void UnityEngine.AndroidJavaObject::Set<System.Object>(System.String,!!0)
extern "C"  void AndroidJavaObject_Set_TisIl2CppObject_m1125093376_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, Il2CppObject * p1, const MethodInfo* method);
#define AndroidJavaObject_Set_TisIl2CppObject_m1125093376(__this, p0, p1, method) ((  void (*) (AndroidJavaObject_t4251328308 *, String_t*, Il2CppObject *, const MethodInfo*))AndroidJavaObject_Set_TisIl2CppObject_m1125093376_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.AndroidJavaObject::Set<System.String>(System.String,!!0)
#define AndroidJavaObject_Set_TisString_t_m551399930(__this, p0, p1, method) ((  void (*) (AndroidJavaObject_t4251328308 *, String_t*, String_t*, const MethodInfo*))AndroidJavaObject_Set_TisIl2CppObject_m1125093376_gshared)(__this, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.ChannelPurchase.PurchaseForwardCallback::.ctor(UnityEngine.ChannelPurchase.IPurchaseListener)
extern Il2CppCodeGenString* _stringLiteral2389614283;
extern const uint32_t PurchaseForwardCallback__ctor_m859758458_MetadataUsageId;
extern "C"  void PurchaseForwardCallback__ctor_m859758458 (PurchaseForwardCallback_t144617755 * __this, Il2CppObject * ___purchaseListener0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PurchaseForwardCallback__ctor_m859758458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaProxy__ctor_m4016180768(__this, _stringLiteral2389614283, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___purchaseListener0;
		__this->set_purchaseListener_1(L_0);
		return;
	}
}
// System.Void UnityEngine.ChannelPurchase.PurchaseService::Purchase(System.String,System.String,UnityEngine.ChannelPurchase.IPurchaseListener)
extern Il2CppClass* PurchaseForwardCallback_t144617755_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaObject_t4251328308_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseService_t659182236_il2cpp_TypeInfo_var;
extern const MethodInfo* AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral145236180;
extern Il2CppCodeGenString* _stringLiteral2651994212;
extern Il2CppCodeGenString* _stringLiteral3112617933;
extern Il2CppCodeGenString* _stringLiteral4092008431;
extern const uint32_t PurchaseService_Purchase_m977826592_MetadataUsageId;
extern "C"  void PurchaseService_Purchase_m977826592 (Il2CppObject * __this /* static, unused */, String_t* ___productCode0, String_t* ___gameOrderId1, Il2CppObject * ___listener2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PurchaseService_Purchase_m977826592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PurchaseForwardCallback_t144617755 * V_0 = NULL;
	AndroidJavaObject_t4251328308 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___listener2;
		PurchaseForwardCallback_t144617755 * L_1 = (PurchaseForwardCallback_t144617755 *)il2cpp_codegen_object_new(PurchaseForwardCallback_t144617755_il2cpp_TypeInfo_var);
		PurchaseForwardCallback__ctor_m859758458(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		AndroidJavaObject_t4251328308 * L_2 = (AndroidJavaObject_t4251328308 *)il2cpp_codegen_object_new(AndroidJavaObject_t4251328308_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m1076535321(L_2, _stringLiteral145236180, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_1 = L_2;
		AndroidJavaObject_t4251328308 * L_3 = V_1;
		String_t* L_4 = ___productCode0;
		NullCheck(L_3);
		AndroidJavaObject_Set_TisString_t_m551399930(L_3, _stringLiteral2651994212, L_4, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var);
		AndroidJavaObject_t4251328308 * L_5 = V_1;
		String_t* L_6 = ___gameOrderId1;
		NullCheck(L_5);
		AndroidJavaObject_Set_TisString_t_m551399930(L_5, _stringLiteral3112617933, L_6, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PurchaseService_t659182236_il2cpp_TypeInfo_var);
		AndroidJavaClass_t2973420583 * L_7 = ((PurchaseService_t659182236_StaticFields*)PurchaseService_t659182236_il2cpp_TypeInfo_var->static_fields)->get_serviceClass_0();
		ObjectU5BU5D_t3614634134* L_8 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		AndroidJavaObject_t4251328308 * L_9 = V_1;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_8;
		PurchaseForwardCallback_t144617755 * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		NullCheck(L_7);
		AndroidJavaObject_CallStatic_m1227537731(L_7, _stringLiteral4092008431, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ChannelPurchase.PurchaseService::ValidateReceipt(System.String,UnityEngine.ChannelPurchase.IPurchaseListener)
extern Il2CppClass* PurchaseForwardCallback_t144617755_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseService_t659182236_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1786369152;
extern const uint32_t PurchaseService_ValidateReceipt_m2412817733_MetadataUsageId;
extern "C"  void PurchaseService_ValidateReceipt_m2412817733 (Il2CppObject * __this /* static, unused */, String_t* ___gameOrderId0, Il2CppObject * ___listener1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PurchaseService_ValidateReceipt_m2412817733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PurchaseForwardCallback_t144617755 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___listener1;
		PurchaseForwardCallback_t144617755 * L_1 = (PurchaseForwardCallback_t144617755 *)il2cpp_codegen_object_new(PurchaseForwardCallback_t144617755_il2cpp_TypeInfo_var);
		PurchaseForwardCallback__ctor_m859758458(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(PurchaseService_t659182236_il2cpp_TypeInfo_var);
		AndroidJavaClass_t2973420583 * L_2 = ((PurchaseService_t659182236_StaticFields*)PurchaseService_t659182236_il2cpp_TypeInfo_var->static_fields)->get_serviceClass_0();
		ObjectU5BU5D_t3614634134* L_3 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		String_t* L_4 = ___gameOrderId0;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		PurchaseForwardCallback_t144617755 * L_6 = V_0;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_6);
		NullCheck(L_2);
		AndroidJavaObject_CallStatic_m1227537731(L_2, _stringLiteral1786369152, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ChannelPurchase.PurchaseService::.cctor()
extern Il2CppClass* AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseService_t659182236_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2225618755;
extern const uint32_t PurchaseService__cctor_m504943159_MetadataUsageId;
extern "C"  void PurchaseService__cctor_m504943159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PurchaseService__cctor_m504943159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaClass_t2973420583 * L_0 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_0, _stringLiteral2225618755, /*hidden argument*/NULL);
		((PurchaseService_t659182236_StaticFields*)PurchaseService_t659182236_il2cpp_TypeInfo_var->static_fields)->set_serviceClass_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
