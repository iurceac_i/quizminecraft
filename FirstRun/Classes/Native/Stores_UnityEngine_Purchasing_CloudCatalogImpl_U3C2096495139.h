﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>
struct Action_1_t77735504;
// UnityEngine.Purchasing.CloudCatalogImpl
struct CloudCatalogImpl_t569898932;
// System.Action
struct Action_t3226471752;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t2096495139  : public Il2CppObject
{
public:
	// System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>> UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::callback
	Action_1_t77735504 * ___callback_0;
	// System.Int32 UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::delayInSeconds
	int32_t ___delayInSeconds_1;
	// UnityEngine.Purchasing.CloudCatalogImpl UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::<>4__this
	CloudCatalogImpl_t569898932 * ___U3CU3E4__this_2;
	// System.Action UnityEngine.Purchasing.CloudCatalogImpl/<>c__DisplayClass10_0::<>9__2
	Action_t3226471752 * ___U3CU3E9__2_3;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t2096495139, ___callback_0)); }
	inline Action_1_t77735504 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t77735504 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t77735504 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}

	inline static int32_t get_offset_of_delayInSeconds_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t2096495139, ___delayInSeconds_1)); }
	inline int32_t get_delayInSeconds_1() const { return ___delayInSeconds_1; }
	inline int32_t* get_address_of_delayInSeconds_1() { return &___delayInSeconds_1; }
	inline void set_delayInSeconds_1(int32_t value)
	{
		___delayInSeconds_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t2096495139, ___U3CU3E4__this_2)); }
	inline CloudCatalogImpl_t569898932 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CloudCatalogImpl_t569898932 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CloudCatalogImpl_t569898932 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_2, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t2096495139, ___U3CU3E9__2_3)); }
	inline Action_t3226471752 * get_U3CU3E9__2_3() const { return ___U3CU3E9__2_3; }
	inline Action_t3226471752 ** get_address_of_U3CU3E9__2_3() { return &___U3CU3E9__2_3; }
	inline void set_U3CU3E9__2_3(Action_t3226471752 * value)
	{
		___U3CU3E9__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__2_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
