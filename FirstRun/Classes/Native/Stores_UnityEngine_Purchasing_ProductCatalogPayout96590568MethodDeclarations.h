﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.ProductCatalogPayout
struct ProductCatalogPayout_t96590568;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.ProductCatalogPayout::.ctor()
extern "C"  void ProductCatalogPayout__ctor_m3688661088 (ProductCatalogPayout_t96590568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
