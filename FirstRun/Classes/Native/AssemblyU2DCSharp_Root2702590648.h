﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Question[]
struct QuestionU5BU5D_t1028533817;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Root
struct  Root_t2702590648  : public Il2CppObject
{
public:
	// Question[] Root::questions
	QuestionU5BU5D_t1028533817* ___questions_0;

public:
	inline static int32_t get_offset_of_questions_0() { return static_cast<int32_t>(offsetof(Root_t2702590648, ___questions_0)); }
	inline QuestionU5BU5D_t1028533817* get_questions_0() const { return ___questions_0; }
	inline QuestionU5BU5D_t1028533817** get_address_of_questions_0() { return &___questions_0; }
	inline void set_questions_0(QuestionU5BU5D_t1028533817* value)
	{
		___questions_0 = value;
		Il2CppCodeGenWriteBarrier(&___questions_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
