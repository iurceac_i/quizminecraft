﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.CloudCatalogImpl/<>c
struct U3CU3Ec_t4238608033;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.CloudCatalogImpl/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m1588861783 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.CloudCatalogImpl/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m580385434 (U3CU3Ec_t4238608033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.CloudCatalogImpl/<>c::<CamelCaseToSnakeCase>b__12_0(System.Char,System.Int32)
extern "C"  String_t* U3CU3Ec_U3CCamelCaseToSnakeCaseU3Eb__12_0_m2288225120 (U3CU3Ec_t4238608033 * __this, Il2CppChar ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.CloudCatalogImpl/<>c::<CamelCaseToSnakeCase>b__12_1(System.String,System.String)
extern "C"  String_t* U3CU3Ec_U3CCamelCaseToSnakeCaseU3Eb__12_1_m1404194035 (U3CU3Ec_t4238608033 * __this, String_t* ___a0, String_t* ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
