﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.FakeStore/<>c__DisplayClass12_0
struct U3CU3Ec__DisplayClass12_0_t1664791861;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"

// System.Void UnityEngine.Purchasing.FakeStore/<>c__DisplayClass12_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass12_0__ctor_m4140970632 (U3CU3Ec__DisplayClass12_0_t1664791861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.FakeStore/<>c__DisplayClass12_0::<RetrieveProducts>b__0(System.Boolean,UnityEngine.Purchasing.InitializationFailureReason)
extern "C"  void U3CU3Ec__DisplayClass12_0_U3CRetrieveProductsU3Eb__0_m3778970204 (U3CU3Ec__DisplayClass12_0_t1664791861 * __this, bool ___allow0, int32_t ___failureReason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
