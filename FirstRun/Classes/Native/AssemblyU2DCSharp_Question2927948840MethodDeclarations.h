﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Question
struct Question_t2927948840;

#include "codegen/il2cpp-codegen.h"

// System.Void Question::.ctor()
extern "C"  void Question__ctor_m1719258817 (Question_t2927948840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
