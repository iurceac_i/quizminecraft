﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Store.AppInfo
struct AppInfo_t2080248435;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Store.AppInfo::.ctor()
extern "C"  void AppInfo__ctor_m2870035846 (AppInfo_t2080248435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Store.AppInfo::get_appId()
extern "C"  String_t* AppInfo_get_appId_m3632567790 (AppInfo_t2080248435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Store.AppInfo::set_appId(System.String)
extern "C"  void AppInfo_set_appId_m1112756485 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Store.AppInfo::get_appKey()
extern "C"  String_t* AppInfo_get_appKey_m3678981840 (AppInfo_t2080248435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Store.AppInfo::set_appKey(System.String)
extern "C"  void AppInfo_set_appKey_m3344021451 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Store.AppInfo::get_clientId()
extern "C"  String_t* AppInfo_get_clientId_m1686889884 (AppInfo_t2080248435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Store.AppInfo::set_clientId(System.String)
extern "C"  void AppInfo_set_clientId_m1298558813 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Store.AppInfo::get_clientKey()
extern "C"  String_t* AppInfo_get_clientKey_m385900370 (AppInfo_t2080248435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Store.AppInfo::set_clientKey(System.String)
extern "C"  void AppInfo_set_clientKey_m1161742527 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Store.AppInfo::get_debug()
extern "C"  bool AppInfo_get_debug_m3181976920 (AppInfo_t2080248435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Store.AppInfo::set_debug(System.Boolean)
extern "C"  void AppInfo_set_debug_m4042117453 (AppInfo_t2080248435 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
