﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2595592884;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_Boolean3825574718.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Store.MainThreadDispatcher
struct  MainThreadDispatcher_t513574034  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct MainThreadDispatcher_t513574034_StaticFields
{
public:
	// System.String UnityEngine.Store.MainThreadDispatcher::OBJECT_NAME
	String_t* ___OBJECT_NAME_2;
	// System.Collections.Generic.List`1<System.Action> UnityEngine.Store.MainThreadDispatcher::s_Callbacks
	List_1_t2595592884 * ___s_Callbacks_3;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngine.Store.MainThreadDispatcher::s_CallbacksPending
	bool ___s_CallbacksPending_4;

public:
	inline static int32_t get_offset_of_OBJECT_NAME_2() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t513574034_StaticFields, ___OBJECT_NAME_2)); }
	inline String_t* get_OBJECT_NAME_2() const { return ___OBJECT_NAME_2; }
	inline String_t** get_address_of_OBJECT_NAME_2() { return &___OBJECT_NAME_2; }
	inline void set_OBJECT_NAME_2(String_t* value)
	{
		___OBJECT_NAME_2 = value;
		Il2CppCodeGenWriteBarrier(&___OBJECT_NAME_2, value);
	}

	inline static int32_t get_offset_of_s_Callbacks_3() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t513574034_StaticFields, ___s_Callbacks_3)); }
	inline List_1_t2595592884 * get_s_Callbacks_3() const { return ___s_Callbacks_3; }
	inline List_1_t2595592884 ** get_address_of_s_Callbacks_3() { return &___s_Callbacks_3; }
	inline void set_s_Callbacks_3(List_1_t2595592884 * value)
	{
		___s_Callbacks_3 = value;
		Il2CppCodeGenWriteBarrier(&___s_Callbacks_3, value);
	}

	inline static int32_t get_offset_of_s_CallbacksPending_4() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t513574034_StaticFields, ___s_CallbacksPending_4)); }
	inline bool get_s_CallbacksPending_4() const { return ___s_CallbacksPending_4; }
	inline bool* get_address_of_s_CallbacksPending_4() { return &___s_CallbacksPending_4; }
	inline void set_s_CallbacksPending_4(bool value)
	{
		___s_CallbacksPending_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
