﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Store.UserInfo
struct UserInfo_t741955747;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.String UnityEngine.Store.UserInfo::get_channel()
extern "C"  String_t* UserInfo_get_channel_m1234361509 (UserInfo_t741955747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Store.UserInfo::get_userId()
extern "C"  String_t* UserInfo_get_userId_m3160010982 (UserInfo_t741955747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Store.UserInfo::get_userLoginToken()
extern "C"  String_t* UserInfo_get_userLoginToken_m706751899 (UserInfo_t741955747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
