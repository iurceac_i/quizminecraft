﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass28_0
struct U3CU3Ec__DisplayClass28_0_t3474431165;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass28_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass28_0__ctor_m2915432770 (U3CU3Ec__DisplayClass28_0_t3474431165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.AppleStoreImpl/<>c__DisplayClass28_0::<MessageCallback>b__0()
extern "C"  void U3CU3Ec__DisplayClass28_0_U3CMessageCallbackU3Eb__0_m3975300192 (U3CU3Ec__DisplayClass28_0_t3474431165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
