﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.UIFakeStore/<>c__DisplayClass14_0`1<UnityEngine.Purchasing.PurchaseFailureReason>
struct U3CU3Ec__DisplayClass14_0_1_t175275736;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.UIFakeStore/<>c__DisplayClass14_0`1<UnityEngine.Purchasing.PurchaseFailureReason>::.ctor()
extern "C"  void U3CU3Ec__DisplayClass14_0_1__ctor_m1658999797_gshared (U3CU3Ec__DisplayClass14_0_1_t175275736 * __this, const MethodInfo* method);
#define U3CU3Ec__DisplayClass14_0_1__ctor_m1658999797(__this, method) ((  void (*) (U3CU3Ec__DisplayClass14_0_1_t175275736 *, const MethodInfo*))U3CU3Ec__DisplayClass14_0_1__ctor_m1658999797_gshared)(__this, method)
// System.Void UnityEngine.Purchasing.UIFakeStore/<>c__DisplayClass14_0`1<UnityEngine.Purchasing.PurchaseFailureReason>::<StartUI>b__0(System.Boolean,System.Int32)
extern "C"  void U3CU3Ec__DisplayClass14_0_1_U3CStartUIU3Eb__0_m1003245755_gshared (U3CU3Ec__DisplayClass14_0_1_t175275736 * __this, bool ___result0, int32_t ___codeValue1, const MethodInfo* method);
#define U3CU3Ec__DisplayClass14_0_1_U3CStartUIU3Eb__0_m1003245755(__this, ___result0, ___codeValue1, method) ((  void (*) (U3CU3Ec__DisplayClass14_0_1_t175275736 *, bool, int32_t, const MethodInfo*))U3CU3Ec__DisplayClass14_0_1_U3CStartUIU3Eb__0_m1003245755_gshared)(__this, ___result0, ___codeValue1, method)
