﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1237856490.h"
#include "mscorlib_System_Array3829468939.h"
#include "Stores_UnityEngine_Purchasing_AppStore379104228.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Purchasing.AppStore>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m308447404_gshared (InternalEnumerator_1_t1237856490 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m308447404(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1237856490 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m308447404_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Purchasing.AppStore>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2125719244_gshared (InternalEnumerator_1_t1237856490 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2125719244(__this, method) ((  void (*) (InternalEnumerator_1_t1237856490 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2125719244_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Purchasing.AppStore>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m39858482_gshared (InternalEnumerator_1_t1237856490 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m39858482(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1237856490 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m39858482_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Purchasing.AppStore>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m987608855_gshared (InternalEnumerator_1_t1237856490 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m987608855(__this, method) ((  void (*) (InternalEnumerator_1_t1237856490 *, const MethodInfo*))InternalEnumerator_1_Dispose_m987608855_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Purchasing.AppStore>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m792945412_gshared (InternalEnumerator_1_t1237856490 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m792945412(__this, method) ((  bool (*) (InternalEnumerator_1_t1237856490 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m792945412_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Purchasing.AppStore>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1369737755_gshared (InternalEnumerator_1_t1237856490 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1369737755(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1237856490 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1369737755_gshared)(__this, method)
