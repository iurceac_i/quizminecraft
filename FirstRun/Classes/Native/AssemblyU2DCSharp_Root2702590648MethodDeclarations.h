﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Root
struct Root_t2702590648;
// Question[]
struct QuestionU5BU5D_t1028533817;

#include "codegen/il2cpp-codegen.h"

// System.Void Root::.ctor()
extern "C"  void Root__ctor_m3913752709 (Root_t2702590648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Question[] Root::get_questions()
extern "C"  QuestionU5BU5D_t1028533817* Root_get_questions_m2291853548 (Root_t2702590648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Root::set_question(Question[])
extern "C"  void Root_set_question_m1612369518 (Root_t2702590648 * __this, QuestionU5BU5D_t1028533817* ___items0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
