﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3
struct U3CProcessU3Ed__3_t1496112696;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::.ctor(System.Int32)
extern "C"  void U3CProcessU3Ed__3__ctor_m2781900868 (U3CProcessU3Ed__3_t1496112696 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::System.IDisposable.Dispose()
extern "C"  void U3CProcessU3Ed__3_System_IDisposable_Dispose_m2723470584 (U3CProcessU3Ed__3_t1496112696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::MoveNext()
extern "C"  bool U3CProcessU3Ed__3_MoveNext_m2080984541 (U3CProcessU3Ed__3_t1496112696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  Il2CppObject * U3CProcessU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1316407736 (U3CProcessU3Ed__3_t1496112696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::System.Collections.IEnumerator.Reset()
extern "C"  void U3CProcessU3Ed__3_System_Collections_IEnumerator_Reset_m2096882437 (U3CProcessU3Ed__3_t1496112696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.AsyncWebUtil/<Process>d__3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CProcessU3Ed__3_System_Collections_IEnumerator_get_Current_m424598917 (U3CProcessU3Ed__3_t1496112696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
