﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::.ctor()
#define HashSet_1__ctor_m738744506(__this, method) ((  void (*) (HashSet_1_t1651728377 *, const MethodInfo*))HashSet_1__ctor_m2858247305_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1__ctor_m855985141(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t1651728377 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1__ctor_m3582855242_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1475143808(__this, method) ((  Il2CppObject* (*) (HashSet_1_t1651728377 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m788997721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3434600627(__this, method) ((  bool (*) (HashSet_1_t1651728377 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2633171492_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m4003643307(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t1651728377 *, ProductDescriptionU5BU5D_t2604130322*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m1933244740_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::System.Collections.Generic.ICollection<T>.Add(T)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1514034807(__this, ___item0, method) ((  void (*) (HashSet_1_t1651728377 *, ProductDescription_t3318267523 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3632050820_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::System.Collections.IEnumerable.GetEnumerator()
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2235920527(__this, method) ((  Il2CppObject * (*) (HashSet_1_t1651728377 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2498631708_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::get_Count()
#define HashSet_1_get_Count_m1412075628(__this, method) ((  int32_t (*) (HashSet_1_t1651728377 *, const MethodInfo*))HashSet_1_get_Count_m4103055329_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1_Init_m1341645881(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t1651728377 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m1258286688_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::InitArrays(System.Int32)
#define HashSet_1_InitArrays_m717193579(__this, ___size0, method) ((  void (*) (HashSet_1_t1651728377 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m1536879844_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::SlotsContainsAt(System.Int32,System.Int32,T)
#define HashSet_1_SlotsContainsAt_m1383838223(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t1651728377 *, int32_t, int32_t, ProductDescription_t3318267523 *, const MethodInfo*))HashSet_1_SlotsContainsAt_m219342270_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::CopyTo(T[],System.Int32)
#define HashSet_1_CopyTo_m1669994083(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t1651728377 *, ProductDescriptionU5BU5D_t2604130322*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m1750586488_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::CopyTo(T[],System.Int32,System.Int32)
#define HashSet_1_CopyTo_m1206877380(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t1651728377 *, ProductDescriptionU5BU5D_t2604130322*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m4175866709_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::Resize()
#define HashSet_1_Resize_m1789158692(__this, method) ((  void (*) (HashSet_1_t1651728377 *, const MethodInfo*))HashSet_1_Resize_m1435308491_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::GetLinkHashCode(System.Int32)
#define HashSet_1_GetLinkHashCode_m3921464324(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t1651728377 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m3972670595_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::GetItemHashCode(T)
#define HashSet_1_GetItemHashCode_m3296734060(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t1651728377 *, ProductDescription_t3318267523 *, const MethodInfo*))HashSet_1_GetItemHashCode_m433445195_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::Add(T)
#define HashSet_1_Add_m1190086556(__this, ___item0, method) ((  bool (*) (HashSet_1_t1651728377 *, ProductDescription_t3318267523 *, const MethodInfo*))HashSet_1_Add_m2918921714_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::Clear()
#define HashSet_1_Clear_m3898910715(__this, method) ((  void (*) (HashSet_1_t1651728377 *, const MethodInfo*))HashSet_1_Clear_m350367572_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::Contains(T)
#define HashSet_1_Contains_m127297685(__this, ___item0, method) ((  bool (*) (HashSet_1_t1651728377 *, ProductDescription_t3318267523 *, const MethodInfo*))HashSet_1_Contains_m1075264948_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::Remove(T)
#define HashSet_1_Remove_m955013860(__this, ___item0, method) ((  bool (*) (HashSet_1_t1651728377 *, ProductDescription_t3318267523 *, const MethodInfo*))HashSet_1_Remove_m4157587527_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
#define HashSet_1_UnionWith_m594433906(__this, ___other0, method) ((  void (*) (HashSet_1_t1651728377 *, Il2CppObject*, const MethodInfo*))HashSet_1_UnionWith_m1038905987_gshared)(__this, ___other0, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1_GetObjectData_m2990875078(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t1651728377 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1_GetObjectData_m2935317189_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::OnDeserialization(System.Object)
#define HashSet_1_OnDeserialization_m3546026552(__this, ___sender0, method) ((  void (*) (HashSet_1_t1651728377 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m1222146673_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::GetEnumerator()
#define HashSet_1_GetEnumerator_m2208413758(__this, method) ((  Enumerator_t140044219  (*) (HashSet_1_t1651728377 *, const MethodInfo*))HashSet_1_GetEnumerator_m623886159_gshared)(__this, method)
