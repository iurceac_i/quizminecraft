﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2
struct U3CDoInvokeU3Ed__2_t21677283;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2::.ctor(System.Int32)
extern "C"  void U3CDoInvokeU3Ed__2__ctor_m1239921587 (U3CDoInvokeU3Ed__2_t21677283 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2::System.IDisposable.Dispose()
extern "C"  void U3CDoInvokeU3Ed__2_System_IDisposable_Dispose_m2419147253 (U3CDoInvokeU3Ed__2_t21677283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2::MoveNext()
extern "C"  bool U3CDoInvokeU3Ed__2_MoveNext_m886695784 (U3CDoInvokeU3Ed__2_t21677283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  Il2CppObject * U3CDoInvokeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3973713491 (U3CDoInvokeU3Ed__2_t21677283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2::System.Collections.IEnumerator.Reset()
extern "C"  void U3CDoInvokeU3Ed__2_System_Collections_IEnumerator_Reset_m433183878 (U3CDoInvokeU3Ed__2_t21677283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoInvokeU3Ed__2_System_Collections_IEnumerator_get_Current_m960237592 (U3CDoInvokeU3Ed__2_t21677283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
