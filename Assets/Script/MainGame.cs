﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;

public class MainGame : MonoBehaviour
{
	private static Question[] questions;
	public GameObject LegasyInfo;
	public GameObject levelPanel;
	public Button play_btn;
	public Button store_btn;
	public Button stats_btn;
	public Button back_1;
	public Button back_2;
	public Button back_3;
	public Button back_4;
	public Text titleGame;
	public Text gameDescription;
	string valueDescription;
	string valueTitle;
	// Use this for initialization
	string isSubscribed;
	public static bool verfiSubscribe;
	public SoundController soundController;
	Main itemsContent;


	void Awake ()
	{
		QualitySettings.vSyncCount = 1;  // VSync must be disabled
		Application.targetFrameRate = 45;
	}

	void Start ()
	{
		GameDescription ();
		questions = ReadJson ();
		ActiveButtons ();
	}

	private static Question[] ReadJson ()
	{
		string path = "/Raw/data.json";
		#if UNITY_EDITOR
		path = "/StreamingAssets/data.json";
		#endif
		string json = File.ReadAllText (Application.dataPath + path);
		return JsonUtility.FromJson<Root> (json).get_questions ();
	}
	public void GameDescription(){

		string path = "/Raw/description.json";
		#if UNITY_EDITOR
			path = "/StreamingAssets/description.json";
		#endif
		string json = File.ReadAllText(Application.dataPath + path);
		itemsContent = JsonUtility.FromJson<Main> (json); 
		valueTitle = itemsContent.get_game_title ();	
		valueDescription = itemsContent.get_game_description ();
		titleGame.text = valueTitle;
		gameDescription.text = valueDescription;

	}

	public static Question[] getQuestions ()
	{
		if (questions == null) {
			questions = ReadJson ();
			return questions;
		} else {
			return questions;
		}
	}

	public void ActivePanel (GameObject gamePanel)
	{
		Handler.StartDelayed (0.5f, soundController.OpenWindow, this);
		Handler.StartDelayed (0.3f, EnablePanel, gamePanel, this);
		InactiveButtons ();
	}

	public void EnablePanel (GameObject go)
	{
		Animator anim = go.GetComponent <Animator> ();
		anim.SetTrigger ("Show");
		go.SetActive (true);
	}

	public void BackButton (GameObject gamePanel){
		ActiveButtons ();
		Animator anim = gamePanel.GetComponent <Animator> ();
		anim.SetTrigger ("Hide");
		Handler.StartDelayed (0.4f, soundController.CloseWindow, this);
		Handler.StartDelayed (0.8f, DisablePan, gamePanel, this);
		Handler.StartDelayed (0.1f, BackOffBtn, this);
		Handler.StartDelayed (0.8f, BackOnBtn, this);
	}

	public void DisablePan (GameObject go)
	{
		go.SetActive (false);
	}

	public void LegasiInfoBack (GameObject gamePanel)
	{
		BackOffBtn ();
		//is enabled on the button script
		Handler.StartDelayed (0.4f, soundController.CloseWindow, this);
		Animator anim = gamePanel.GetComponent <Animator> ();
		anim.SetTrigger ("Hide");
		Handler.StartDelayed (1.2f, DisablePan, gamePanel, this);

		Handler.StartDelayed (1.0f, BackOnBtn, this);
	}

	public void InactiveButtons ()
	{
		play_btn.interactable = false;
		store_btn.interactable = false;
		stats_btn.interactable = false;
	}

	public void ActiveButtons ()
	{
		play_btn.interactable = true;
		store_btn.interactable = true;
		stats_btn.interactable = true;
	
	}

	public void BackOnBtn ()
	{
		back_1.interactable = true;
		back_2.interactable = true;
		back_3.interactable = true;
		back_4.interactable = true;
	}

	public void BackOffBtn ()
	{
		back_1.interactable = false;
		back_2.interactable = false;
		back_3.interactable = false;
		back_4.interactable = false;
	}

}
