﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using AssemblyCSharp;

public class Adapter : MonoBehaviour
{
	public GameObject gameObjects;
	bool isPurchase;
	private OnItemClickListener adapterCallBack;


	public void setAdapter (Levels[] levels)
	{

		for (int i = 0; i < levels.Length; i++) {
			GameObject mission = gameObjects.transform.GetChild (0).gameObject;
			Text nrOfMission = mission.GetComponent<Text> ();
			nrOfMission.text = levels [i].NrLevel.ToString ();
			GameObject star1 = gameObjects.transform.GetChild (1).GetChild (3).gameObject;
			GameObject star2 = gameObjects.transform.GetChild (1).GetChild (4).gameObject;
			GameObject star3 = gameObjects.transform.GetChild (1).GetChild (5).gameObject;
			GameObject locked = gameObjects.transform.GetChild (3).gameObject;
			locked.SetActive (levels [i].IsLocked);
			GameObject stars = gameObjects.transform.GetChild (1).gameObject;
			stars.SetActive (!(levels [i].IsLocked));
			int nr = levels [i].getStarNumber ();
			star3.SetActive (nr == 3);
			star2.SetActive (nr > 1);
			star1.SetActive (nr > 0);

			GameObject finalObj = Instantiate (gameObjects, transform);
			finalObj.SetActive (true);
			finalObj.name = i.ToString ();			
			GameObject playButton = finalObj.transform.GetChild (2).gameObject;
			Button playGame = playButton.GetComponent<Button> ();
			playGame.onClick.AddListener (() => OnItemClick (finalObj));
			playGame.interactable = (!levels [i].IsLocked);
		} 
	}

	public void UpdateAdapter (Levels[] levels)
	{
		for (int i = 0; i < levels.Length; i++) {
			GameObject star1 = transform.GetChild (i).GetChild (1).GetChild (3).gameObject;
			GameObject star2 = transform.GetChild (i).GetChild (1).GetChild (4).gameObject;
			GameObject star3 = transform.GetChild (i).GetChild (1).GetChild (5).gameObject;

			int nr = levels [i].getStarNumber ();
			star3.SetActive (nr == 3);
			star2.SetActive (nr > 1);
			star1.SetActive (nr > 0);

			GameObject locked = transform.GetChild (i).GetChild (3).gameObject;
			locked.SetActive (levels [i].IsLocked);

			GameObject playGame = transform.GetChild (i).GetChild (2).gameObject;
			Button playNow = playGame.GetComponent <Button> ();
			playNow.interactable = !levels [i].IsLocked;

			GameObject rateGO = transform.GetChild (i).GetChild (1).gameObject;
			rateGO.SetActive (!levels [i].IsLocked);
		}
	}

	public void OnItemClick (GameObject go)
	{
		int position = Int32.Parse (go.name);

//		PlayerPrefs.SetInt ("currentQuestion", 0);
		if (adapterCallBack != null) {
			adapterCallBack.onItemClicked (position);
		}
	}

	public interface OnItemClickListener
	{
		void onItemClicked (int position);
	}

	public void registerListener (OnItemClickListener callback)
	{
		this.adapterCallBack = callback;
	}
}
