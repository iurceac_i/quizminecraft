﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using System.Collections;
using UnityEngine.UI;
using System.IO;

namespace AssemblyCSharp
{
	public class Purchase : MonoBehaviour, IStoreListener
	{

		public static bool isPurchased = false;
		public GameObject alertPurchase;
		public GameObject gamePanel;
		private static IStoreController m_StoreController;
		// The Unity Purchasing system.
		private static IExtensionProvider m_StoreExtensionProvider;
		// The store-specific Purchasing subsystems.

		// Product identifiers for all products capable of being purchased:
		// "convenience" general identifiers for use with Purchasing, and their store-specific identifier
		// counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers
		// also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

		// General product identifiers for the consumable, non-consumable, and subscription products.
		// Use these handles in the code to reference which product to purchase. Also use these values
		// when defining the Product Identifiers on the store. Except, for illustration purposes, the
		// kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
		// specific mapping to Unity Purchasing's AddProduct, below.
		public static string kProductIDConsumable = "consumable";
		public static string kProductIDNonConsumable = "nonconsumable";
		public static string kProductIDSubscription;
		Main itemsContent;
		// Apple App Store-specific product identifier for the subscription product.
//		private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

		// Google Play Store-specific product identifier subscription product.
//		private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

		void Start ()
		{
			readPurchaseJson ();
			Debug.Log (kProductIDSubscription);
			// If we haven't set up the Unity Purchasing reference
			if (m_StoreController == null) {
				// Begin to configure our connection to Purchasing
				if (Application.internetReachability == NetworkReachability.NotReachable) {
					Debug.Log ("Nu exista conexiunea cu internetul");
					//Ori aplicatia necesita restore Purchases ori nu este conexiune cu reteaua
					string dateStr = PlayerPrefs.GetString ("date");
					Debug.Log ("Isdatanull: " + (dateStr == null).ToString ());
					Debug.Log ("Data curenta: " + dateStr);
					if (dateStr == null || dateStr == "")
						return;
					DateTime? savedTime = DateTime.Parse (dateStr);
					isPurchased = savedTime > DateTime.UtcNow;
					Debug.Log ("Is Purchased: " + isPurchased);
				} else {
					InitializePurchasing ();
				}
			}

		}

		public void readPurchaseJson(){
		
			string path = "/Raw/description.json";
			#if UNITY_EDITOR
			path = "/StreamingAssets/description.json";
			#endif
			string json = File.ReadAllText(Application.dataPath + path);
			itemsContent = JsonUtility.FromJson<Main> (json); 
			kProductIDSubscription = itemsContent.product_id;

		}


		public void InitializePurchasing ()
		{
			// If we have already connected to Purchasing ...
			if (IsInitialized ()) {
				// ... we are done here.
				return;
			}

			// Create a builder, first passing in a suite of Unity provided stores.
			var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());

			// Add a product to sell / restore by way of its identifier, associating the general identifier
			// with its store-specific identifiers.
//			builder.AddProduct (kProductIDConsumable, ProductType.Consumable);
			// Continue adding the non-consumable product.
//			builder.AddProduct (kProductIDNonConsumable, ProductType.NonConsumable);
			// And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
			// if the Product ID was configured differently between Apple and Google stores. Also note that
			// one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
			// must only be referenced here. 
			builder.AddProduct (kProductIDSubscription, ProductType.Subscription);

			// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
			// and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
			UnityPurchasing.Initialize (this, builder);
		}


		private bool IsInitialized ()
		{
			// Only say we are initialized if both the Purchasing references are set.
			return m_StoreController != null && m_StoreExtensionProvider != null;
		}


		public void BuyConsumable ()
		{
			
			// Buy the consumable product using its general identifier. Expect a response either 
			// through ProcessPurchase or OnPurchaseFailed asynchronously.
			BuyProductID (kProductIDConsumable);
		}


		public void BuyNonConsumable ()
		{
			// Buy the non-consumable product using its general identifier. Expect a response either 
			// through ProcessPurchase or OnPurchaseFailed asynchronously.
			BuyProductID (kProductIDNonConsumable);
		}


		public void BuySubscription (GameObject button)
		{
			BuyProductID (kProductIDSubscription);
			Button btn = button.GetComponent <Button> ();
			btn.interactable = false;
			Handler.StartDelayed (7f,BlockInteractableButton,btn,this);

		}
		public void BlockInteractableButton(Button btn){
			btn.interactable = true;
		}



		void BuyProductID (string productId)
		{
			// If Purchasing has been initialized ...
			if (IsInitialized ()) {
				// ... look up the Product reference with the general product identifier and the Purchasing 
				// system's products collection.
				Product product = m_StoreController.products.WithID (productId);

				// If the look up found a product for this device's store and that product is ready to be sold ... 
				if (product != null && product.availableToPurchase) {
					Debug.Log (string.Format ("Purchasing product asychronously: '{0}'", product.definition.id));
					// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
					// asynchronously.
					m_StoreController.InitiatePurchase (product);
				}
				// Otherwise ...
				else {
					// ... report the product look-up failure situation  
					Debug.Log ("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
				}
			}
			// Otherwise ...
			else {
				// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
				// retrying initiailization.
				InitializePurchasing ();
				Debug.Log ("BuyProductID FAIL. Not initialized.");
			}
		}


		// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google.
		// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
		public void RestorePurchases (GameObject button)
		{
			Button btn = button.GetComponent <Button> ();
			btn.interactable = false;
			Handler.StartDelayed (7f,BlockInteractableButton,btn,this);
			// If Purchasing has not yet been set up ...
			if (!IsInitialized ()) {
				// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
				InitializePurchasing ();
				Debug.Log ("RestorePurchases FAIL. Not initialized.");
				return;
			}

			// If we are running on an Apple device ... 
			if (Application.platform == RuntimePlatform.IPhonePlayer ||
			    Application.platform == RuntimePlatform.OSXPlayer) {
				// ... begin restoring purchases
				Debug.Log ("RestorePurchases started ...");

				// Fetch the Apple store-specific subsystem.
				var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions> ();
				// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
				// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
				apple.RestoreTransactions ((result) => {
					// The first phase of restoration. If no more responses are received on ProcessPurchase then 
					// no purchases are available to be restored.
					if (verifyOnStart ()) {
						//Afiseaza alert
						Debug.Log ("Restore succesfull");
					}
					Debug.Log ("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
				});
			}
			// Otherwise ...
			else {
				// We are not running on an Apple device. No work is necessary to restore purchases.
				Debug.Log ("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
			}
		
		}


		//
		// --- IStoreListener
		//

		public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
		{
			// Purchasing has succeeded initializing. Collect our Purchasing references.
			Debug.Log ("OnInitialized: PASS");

			// Overall Purchasing system, configured with products for this application.
			m_StoreController = controller;
			// Store specific subsystem, for accessing device-specific store features.
			m_StoreExtensionProvider = extensions;

			verifyOnStart ();
		}


		public void OnInitializeFailed (InitializationFailureReason error)
		{
			// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
			Debug.Log ("OnInitializeFailed InitializationFailureReason:" + error);
		}


		public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args)
		{
			Debug.Log ("A venit produsul: " + args.purchasedProduct.definition.id);
			if (String.Equals (args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal)) {
//				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				// TODO: The subscription item has been successfully purchased, grant this to the player.
				#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX // TODO: The subscription item has been successfully purchased, grant this to the player.
				var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());
				var appleConfig = builder.Configure<IAppleConfiguration> ();// Get a reference to IAppleConfiguration during IAP initialization.
				DateTime? data = validate (appleConfig.appReceipt, kProductIDSubscription);
				if (data == null) {
					Debug.Log ("valuer blea null");
				} else {
					Debug.Log ("Date: " + data);
				}
				if (data != null) {
				alertPurchase.SetActive (false);
				gamePanel.SetActive (false);
					Debug.Log ("Data a fost salvata in preferences: " + data.ToString ());
					PlayerPrefs.SetString ("date", data.ToString ());
					isPurchased = true;
				}
				#endif
			}	
			// Or ... an unknown product has been purchased by this user. Fill in additional products here....
			else {
				Debug.Log (string.Format ("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
			}

			// Return a flag indicating whether this product has completely been received, or if the application needs 
			// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
			// saving purchased products to the cloud, and when that save is delayed. 
			return PurchaseProcessingResult.Complete;
		}


		public void OnPurchaseFailed (Product product, PurchaseFailureReason failureReason)
		{
			// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
			// this reason with the user to guide their troubleshooting actions.
			Debug.Log (string.Format ("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
		}


		private DateTime? validate (string receiptStr, string myProduct)
		{
			DateTime? result = null;
			var receiptData = System.Convert.FromBase64String (receiptStr);
			AppleReceipt receipt = new AppleValidator (AppleTangle.Data ()).Validate (receiptData);
			if (receipt != null) {
				foreach (AppleInAppPurchaseReceipt productReceipt in receipt.inAppPurchaseReceipts) {
					DateTime expireDate = productReceipt.subscriptionExpirationDate;
					string productId = productReceipt.productID;
					if (String.Equals (myProduct, productId, StringComparison.Ordinal) && expireDate > DateTime.UtcNow) {
						result = expireDate;
						Debug.Log ("ExpireData in validate Method " + expireDate);
					}
				}
			}
			return result;
		}

		public bool verifyOnStart ()
		{
			#if UNITY_ANDROID || UNITY_IOS
			var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());
			// Get a reference to IAppleConfiguration during IAP initialization.
			var appleConfig = builder.Configure<IAppleConfiguration> ();
			//var receiptData = System.Convert.FromBase64String (appleConfig.appReceipt);
//			Debug.Log (appleConfig.appReceipt);
			if (appleConfig.appReceipt == null || appleConfig.appReceipt == "") {
				Debug.Log ("Receipt failed to receive");
				//Ori aplicatia necesita restore Purchases ori nu este conexiune cu reteaua
				string dateStr = PlayerPrefs.GetString ("date");
				if (dateStr == null || dateStr == "")
					return false;
				DateTime? savedTime = DateTime.Parse (dateStr);
				isPurchased = savedTime > DateTime.UtcNow;
				Debug.Log ("Is Purchased: " + isPurchased);
				//Descarci data din preferences.
				//In caz ca este null - userul nu a salvat niciodata datele acolo, logic, nu a facut cumparatura
				//In caz ca data exista - verifici daca data salvata depaseste data curenta/ daca da inseamna ca subscription este valida, daca nu - inseamna ca a expirat subscription
			} else {
				DateTime? date = validate (appleConfig.appReceipt, kProductIDSubscription);
				if (date == null) {
					Debug.Log ("Am validat rezultatele si nu a fost nici un produs cu data disponibila");
					isPurchased = false;
					PlayerPrefs.SetString ("date", "");
				} else {
					Debug.Log ("Exista un produs cu data buna, am salvato in prefrences, data este: " + date.ToString ());
					isPurchased = true;
					PlayerPrefs.SetString ("date", date.ToString ());
					Debug.Log ("Data limita activa pentru subscription " + date);
				}
				Debug.Log ("Is Purchased: " + isPurchased);
				return true;
			}
			#endif
			return false;
		}


	}
}

