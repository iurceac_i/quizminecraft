﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenOrientation : MonoBehaviour {
	public GameObject landscapeStatsPanel;
	public GameObject PortraitStatsPanel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if ((Input.deviceOrientation == DeviceOrientation.Portrait))
		{
			landscapeStatsPanel.SetActive (false);
			PortraitStatsPanel.SetActive (true);

		}else if ((Input.deviceOrientation == DeviceOrientation.LandscapeLeft) || (Input.deviceOrientation == DeviceOrientation.LandscapeRight))
		{
			PortraitStatsPanel.SetActive (false);
			landscapeStatsPanel.SetActive (true);
		}

//		if ((Input.deviceOrientation == DeviceOrientation.FaceUp)&&(Screen.width > Screen.height)){
			if (Screen.width > Screen.height) {
				PortraitStatsPanel.SetActive (false);
				landscapeStatsPanel.SetActive (true);
			} else {
				landscapeStatsPanel.SetActive (false);
				PortraitStatsPanel.SetActive (true);
			}
//		}

	}
}
