﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp;

public class LevelController : MonoBehaviour,Adapter.OnItemClickListener, QuestionController.UpdateListener
{
	public Adapter adapter;
	private Levels[] levels;
	public QuestionController questController;
	public GameObject gamePanel;
	public GameObject buyPanel;
	// Use this for initialization
	void Start ()
	{
		levels = GetLevels (MainGame.getQuestions ());
		SetUpAnswers (levels);
		DetectOnEnableLevel (levels);
		adapter.setAdapter (levels);
		adapter.registerListener (this);
		questController.registerListener (this);
	}

	private Levels[] GetLevels (Question[] quests)
	{
		Levels[] createLevels = new Levels[quests.Length / 11];
		int j = 0;
		Levels level = new Levels ();
		level.IsLocked = false;
		level.NrLevel = 1;
		createLevels [j] = level;
		for (int i = 0; i < quests.Length; i++) {
			if (i == 11 * (j + 1)) {
				j++;
				level = new Levels ();
				level.NrLevel = j + 1;
				createLevels [j] = level;
			}
			createLevels [j].addElement (quests [i]);
		}
		return createLevels;
	}

	private void SetUpAnswers (Levels[] levels)
	{
		for (int i = 0; i < levels.Length; i++) {
			int correctAnswers = PlayerPrefs.GetInt ((levels [i].NrLevel).ToString ());
			levels [i].CorectAnswers = correctAnswers;
		}
	}

	public void DetectOnEnableLevel (Levels[] levels)
	{
		for (int i = 0; i < levels.Length - 1; i++) {
			if (levels [i].IsLocked == false && levels [i].getStarNumber () > 0) {
				levels [i + 1].IsLocked = false;
			} else
				break;
		}
	}

	#region OnItemClickListener implementation

	public void onItemClicked (int position)
	{
		if (position <= 5) {
			if ((levels [position].IsLocked) == false) {
				questController.SetLevels (levels, position);
				gamePanel.SetActive (true);
			}
		} else if (Purchase.isPurchased) {
			if ((levels [position].IsLocked) == false) {
				questController.SetLevels (levels, position);
				gamePanel.SetActive (true);
			}
		} else {
			gamePanel.SetActive (false);
			buyPanel.SetActive (true);
		}
	}

	#endregion

	#region UpdateListener implementation

	public void onUpdateAdapter ()
	{
		if (adapter != null && levels != null) {
			SetUpAnswers (levels);
			DetectOnEnableLevel (levels);
			adapter.UpdateAdapter (levels);
		}
	}

	#endregion



}
