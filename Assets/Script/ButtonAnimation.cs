﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonAnimation : MonoBehaviour {
//	public Animator buttonAnimator;

	// Use this for initialization
	void Start () {
		Button btn = GetComponent<Button>();
		btn.onClick.AddListener(hideButton);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void hideButton(){
//		buttonAnimator.SetTrigger ("Disappear");


	}

	public void showButton(){
//		buttonAnimator.SetTrigger ("Appear");

	}
}
