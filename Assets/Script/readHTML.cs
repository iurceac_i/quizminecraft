﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net;
using System.IO;
using System;


public class readHTML : MonoBehaviour {
	public Text contentURL;
	public GameObject backButton;
	string path_terms = "/Raw/terms.txt";
	string path_subscription = "/Raw/subscription.txt";
	string path_privacy = "/Raw/privacy.txt";
	public Animator anim;
	string readTextFileSubscription;
	string readTextFileTerms;
	string readTextFilePrivacy;
	public Button SubscriptionBtn;
	public Button privacyBtn;
	public Button termsBtn;
	public Button btn_1;
	public SoundController soundControll;
	public void Start(){

		#if UNITY_EDITOR
		path_terms = "/StreamingAssets/terms.txt";
		path_subscription = "/StreamingAssets/subscription.txt";
		path_privacy = "/StreamingAssets/privacy.txt";
		#endif

		readTextFileSubscription = File.ReadAllText(Application.dataPath + path_subscription);
		readTextFileTerms = File.ReadAllText(Application.dataPath + path_terms);
		readTextFilePrivacy = File.ReadAllText(Application.dataPath + path_privacy);
		GUIStyle style = new GUIStyle ();
		style.richText = true;
		contentURL.text = readTextFileSubscription;

	}

	public void EnableLegasyInfo(GameObject go){
		btn_1.interactable = false;
		soundControll.OpenWindow ();
		go.SetActive (true);
		backButton.SetActive (true);
		Handler.StartDelayed (1.5f,ActiveBtn,this);
	}


	public void StartAnimation(int i){
	
		if(i == 1){
			SubscriptionBtn.interactable = false;
			anim.SetTrigger ("StartEffect");
			Handler.StartDelayed (0.3f, SelectSubscription, this);
		}else if(i == 2){
			privacyBtn.interactable = false;
			anim.SetTrigger ("StartEffect");
			Handler.StartDelayed (0.3f, SelectPrivacy, this);
		}else if(i == 3){
			termsBtn.interactable = false;
			anim.SetTrigger ("StartEffect");
			Handler.StartDelayed (0.3f, SelectTerms, this);
		}
	
	}


	public void SelectSubscription(){
		contentURL.text = readTextFileSubscription;
		SubscriptionBtn.interactable = true;
	}
	public void SelectPrivacy(){
		privacyBtn.interactable = true;
		contentURL.text = readTextFilePrivacy;
	}
	public void SelectTerms(){
		termsBtn.interactable = true;
		contentURL.text = readTextFileTerms;
	}
	public void ActiveBtn(){
		btn_1.interactable = true;
	}

// Start code - Read from hosting
//			string fileContent = new WebClient().DownloadString("http://callrecorder.feedbackapps.xyz/privat.txt");
//	
//			GUIStyle style = new GUIStyle ();
//			style.richText = true;
//			contentURL.text = fileContent;
	// end code
}
