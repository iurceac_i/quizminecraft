﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using AssemblyCSharp;

public class QuestionController : MonoBehaviour
{

	public Text question;
	public Text answer_0;
	public Text answer_1;
	public Text answer_2;
	public Text answer_3;
	public Button btn_1;
	public Button btn_2;
	public Button btn_3;
	public Button btn_4;
	public GameObject star1;
	public GameObject star2;
	public GameObject star3;
	UpdateListener callBack;
	string correctAns;
	public GameObject buyPanel;
	public GameObject GamePanel;
	public GameObject twoButtons;
	public GameObject singleButtonPanel;
	public GameObject finalAlertPanel;
	public GameObject levelPanel;
	public GameObject notEnought;
	public Text numb_of_compl_mission;
	int numQuest = 0;
	Levels[] levelGlobal;
	int positionLevel;
	int correctAnswer;
	public Purchase purch;
	public SoundController soundControll;
	// Use this for initialization

	public void VerifyQuestion (GameObject obj)
	{
		correctAns = (levelGlobal [positionLevel].Quests [numQuest].answer).ToString ();
		Debug.Log (correctAns);
		GameObject GO	= obj.gameObject.transform.GetChild (0).gameObject;
		Text ans = GO.GetComponent <Text> ();
		Image img = obj.GetComponent <Image> ();
		if (ans.text == correctAns) {
			correctAnswer++;
			soundControll.OnSound ();
			img.color = Color.green;
			int	correctAns = PlayerPrefs.GetInt ("CorrectAnswer", 0) + 1;
			PlayerPrefs.SetInt ("CorrectAnswer", correctAns);
			Handler.StartDelayed (1f, CounterQuestion, obj, this);		
		} else if (ans.text != correctAns.ToString ()) {
			soundControll.OffSound ();
			img.color = Color.red;
			int	failAns = PlayerPrefs.GetInt ("FailAnswer", 0) + 1;
			PlayerPrefs.SetInt ("FailAnswer", failAns);
			Handler.StartDelayed (1f, CounterQuestion, obj, this);		
		}
		btn_1.interactable = false;
		btn_2.interactable = false;
		btn_3.interactable = false;
		btn_4.interactable = false;
	}

	public void SetLevels (Levels[] level, int position)
	{
		//Detect selected level from list
		numQuest = 0;
		levelGlobal = level;
		positionLevel = position;
//		Debug.Log (levelGlobal[positionLevel].NrLevel);
		SetQuestion (levelGlobal [positionLevel].Quests [numQuest]);
	}

	public void SetQuestion (Question quest)
	{
		Util.reshuffle (quest.choices);
		question.text = quest.question;
		answer_0.text = quest.choices [0];
		answer_1.text = quest.choices [1];
		answer_2.text = quest.choices [2];
		answer_3.text = quest.choices [3];
	}

	public void CounterQuestion (GameObject obj)
	{
		Image img = obj.GetComponent <Image> ();
		img.color = Color.white;
		btn_1.interactable = true;
		btn_2.interactable = true;
		btn_3.interactable = true;
		btn_4.interactable = true;
		if (numQuest <= 9) {
			numQuest++;
			SetQuestion (levelGlobal [positionLevel].Quests [numQuest]);
		} else {
			levelGlobal [positionLevel].CorectAnswers = correctAnswer;
			int nr = levelGlobal [positionLevel].getStarNumber ();
			star3.SetActive (nr == 3);
			star2.SetActive (nr > 1);
			star1.SetActive (nr > 0);

			numb_of_compl_mission.text = (positionLevel + 1).ToString ();
			this.gameObject.SetActive (false);

			if (positionLevel <= 38) {
				if (levelGlobal [positionLevel].getStarNumber () > 0) {
					singleButtonPanel.SetActive (true);
					twoButtons.SetActive (true);
					soundControll.SuccesMission ();
					notEnought.SetActive (false);
				} else {
					soundControll.FailMission ();
					twoButtons.SetActive (false);
					notEnought.SetActive (true);
					singleButtonPanel.SetActive (true);
				}
			} else {
				soundControll.SuccesMission ();
				twoButtons.SetActive (false);
				notEnought.SetActive (false);
				finalAlertPanel.SetActive (true);
				singleButtonPanel.SetActive (true);
			}
		}
	}

	public void NextLevel ()
	{
		if (levelGlobal [positionLevel].NrLevel <= 5) {
			if (correctAnswer > PlayerPrefs.GetInt ((levelGlobal [positionLevel].NrLevel).ToString ())) { 
				PlayerPrefs.SetInt ((levelGlobal [positionLevel].NrLevel).ToString (), correctAnswer);
				if (callBack != null) {
					callBack.onUpdateAdapter ();
				}
			}
			correctAnswer = 0;
			numQuest = 0;

			if (levelGlobal [positionLevel].IsLocked == false && positionLevel <= 39) {
				positionLevel++;

				SetQuestion (levelGlobal [positionLevel].Quests [numQuest]);
				singleButtonPanel.SetActive (false);
				this.gameObject.SetActive (true);
			}
		} else {
			if (Purchase.isPurchased) {
				if (correctAnswer > PlayerPrefs.GetInt ((levelGlobal [positionLevel].NrLevel).ToString ())) { 
					PlayerPrefs.SetInt ((levelGlobal [positionLevel].NrLevel).ToString (), correctAnswer);
					if (callBack != null) {
						callBack.onUpdateAdapter ();
					}
				}
				correctAnswer = 0;
				numQuest = 0;
				if (levelGlobal [positionLevel].IsLocked == false && positionLevel <= 39) {
					positionLevel++;
					SetQuestion (levelGlobal [positionLevel].Quests [numQuest]);
					singleButtonPanel.SetActive (false);
					this.gameObject.SetActive (true);
				}
			} else {
				Debug.Log ("NeedSubscribe");
				buyPanel.SetActive (true);
				// activam panelul try triall;
			}
		}
	}

	public void RetryLevel (GameObject gamePanel)
	{
		if (correctAnswer > PlayerPrefs.GetInt ((levelGlobal [positionLevel].NrLevel).ToString ())) { 
			PlayerPrefs.SetInt ((levelGlobal [positionLevel].NrLevel).ToString (), correctAnswer);
			if (callBack != null) {
				callBack.onUpdateAdapter ();
			}
		}
		gamePanel.SetActive (false);
		correctAnswer = 0;
		numQuest = 0;
		if (levelGlobal [positionLevel].IsLocked == false && positionLevel <= 39) {
			SetQuestion (levelGlobal [positionLevel].Quests [numQuest]);
			singleButtonPanel.SetActive (false);
			this.gameObject.SetActive (true);
		} 
		finalAlertPanel.SetActive (false);
	}

	//	public void LastMissionComplete(GameObject gamePanel){
	//		finalAlertPanel.SetActive (false);
	//		this.gameObject.SetActive (false);
	//		levelPanel.SetActive (false);
	//	}

	public void BackButton (GameObject gamePanel)
	{
		if (correctAnswer > PlayerPrefs.GetInt ((levelGlobal [positionLevel].NrLevel).ToString ())) { 
			PlayerPrefs.SetInt ((levelGlobal [positionLevel].NrLevel).ToString (), correctAnswer);
			if (callBack != null) {
				callBack.onUpdateAdapter ();
			}
			gamePanel.SetActive (false);
		}
		correctAnswer = 0;
		gamePanel.SetActive (false);
	}

	public void ClosePopup (GameObject gamePanel)
	{
		soundControll.CloseWindow ();
		GamePanel.SetActive (false);
		gamePanel.SetActive (false);
	}

	public void GoHome (GameObject go)
	{
		if (correctAnswer > PlayerPrefs.GetInt ((levelGlobal [positionLevel].NrLevel).ToString ())) { 
			PlayerPrefs.SetInt ((levelGlobal [positionLevel].NrLevel).ToString (), correctAnswer);
			if (callBack != null) {
				callBack.onUpdateAdapter ();
			}
		}
		go.SetActive (false);
		GamePanel.SetActive (false);
	}

	public interface UpdateListener
	{
		void onUpdateAdapter ();
	}

	public void registerListener (UpdateListener callback)
	{
		this.callBack = callback;
	}

	public void RefreshQuestionController ()
	{
		if (correctAnswer > PlayerPrefs.GetInt ((levelGlobal [positionLevel].NrLevel).ToString ())) { 
			PlayerPrefs.SetInt ((levelGlobal [positionLevel].NrLevel).ToString (), correctAnswer);
			if (callBack != null) {
				callBack.onUpdateAdapter ();
			}
		}
	}

}
