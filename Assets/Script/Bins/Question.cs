﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class Question {
	public string answer;  //raspuns corect 
	public string[] choices; // variante de raspuns
	public string question; // intrebarea


}
