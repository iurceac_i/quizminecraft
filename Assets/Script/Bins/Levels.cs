﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Levels
{

	private int nrLevel;
	private int corectAnswers;
	private List<Question> quests;
	private bool isLocked;

	public int NrLevel {
		get {
			return this.nrLevel;
		}
		set {
			nrLevel = value;
		}
	}

	public bool IsLocked {
		get {
			return this.isLocked;
		}
		set {
			isLocked = value;
		}
	}

	public int CorectAnswers {
		get {
			return this.corectAnswers;
		}
		set {
			corectAnswers = value;
		}
	}

	public List<Question> Quests {
		get {
			return this.quests;
		}
		set {
			quests = value;
		}
	}

		
	public void addElement (Question question)
	{
		if (quests == null)
			quests = new List<Question> ();
		quests.Add (question);
	}

	public int getStarNumber ()
	{
		if (corectAnswers > 8)
			return 3;
		else if (corectAnswers < 9 && corectAnswers > 6)
			return 2;
		else if (corectAnswers < 7 && corectAnswers > 3)
			return 1;
		else
			return 0;
	}

	public Levels ()
	{
		isLocked = true;
	}

}
