﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class Root
{

	public Question[] questions;

	public Question[] get_questions ()
	{
		return questions;
	}

	public void set_question (Question[] items)
	{
		questions = items;
	}

}
