﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Main {
	public String product_id;
	public String store_id;
	public String game_title;
	public String game_description;

	public string get_store_id(){
		return store_id;
	}


	public string get_product_id (){
			return product_id;
	}
	public string get_game_title(){
		return game_title;
	}
	public string get_game_description(){
		return game_description;
	}
}
