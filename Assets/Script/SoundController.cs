﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundController : MonoBehaviour {

	public AudioClip correctSound;
	public AudioClip incorrectSound;
	public AudioClip closeWindow;
	public AudioClip failMission;
	public AudioClip menuSong;
	public AudioClip succesMission;
	public GameObject settingsMenu;
	public AudioSource soundControll;
	public AudioSource backgroundSound;
	public Sprite onButton;
	public Sprite offButton;
	// Use this for initialization
	void Start () {
		backgroundSound.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnSound(){
		soundControll.clip = correctSound;
		soundControll.loop = false;
		soundControll.Play ();

	}
	public void OffSound(){
		soundControll.clip = incorrectSound;
		soundControll.loop = false;
		soundControll.Play ();
	}
	public void FailMission(){
		soundControll.clip = failMission;
		soundControll.loop = false;
		soundControll.Play ();
	}
	public void CloseWindow(){
		soundControll.clip = closeWindow;
		soundControll.loop = false;
		soundControll.Play ();
	}
	public void OpenWindow(){
		soundControll.clip = menuSong;
		soundControll.loop = false;
		soundControll.Play ();
	}
	public void SuccesMission(){
		soundControll.clip = succesMission;
		soundControll.loop = false;
		soundControll.Play ();
	}
	public void soundSettings(){
		OpenWindow ();
		settingsMenu.SetActive (true);
	}
	public void DisableEffectSound(GameObject go){
		soundControll.enabled = !soundControll.enabled;
		CloseWindow ();
		Image img = go.GetComponent <Image> ();
		if(soundControll.enabled){
			img.sprite = onButton;
		}else
			img.sprite = offButton;
	}
	public void DisableBackgroundSound(GameObject go){
		backgroundSound.enabled = !backgroundSound.enabled;
		CloseWindow ();
		Image img = go.GetComponent <Image> ();
		if(backgroundSound.enabled){
			img.sprite = onButton;
		}else
			img.sprite = offButton;
	}

}
