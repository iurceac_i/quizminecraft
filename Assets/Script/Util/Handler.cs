﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Handler
{
	public delegate void Execute ();

	public delegate void ExecuteWithGameObjcet (GameObject go);
	public delegate void ExecuteWithButton (Button btns);

	public static Coroutine StartDelayed (float waitSeconds, bool isValable, MonoBehaviour behaviour)
	{
		return behaviour.StartCoroutine (WaitForSeconds (waitSeconds, isValable));
	}

	public static Coroutine StartDelayed (float waitSeconds, Execute execute, MonoBehaviour behaviour){
		return behaviour.StartCoroutine (WaitForSeconds(waitSeconds, execute));
	}

	public static Coroutine StartDelayed (float waitSeconds, ExecuteWithGameObjcet execute,GameObject GO, MonoBehaviour behaviour){
		return behaviour.StartCoroutine (WaitForSeconds(waitSeconds, execute, GO));
	}
	public static Coroutine StartDelayed (float waitSeconds, ExecuteWithButton execute,Button btn, MonoBehaviour behaviour){
		return behaviour.StartCoroutine (WaitForSeconds(waitSeconds, execute, btn));
	}

	public static void StopDelayed (Coroutine coroutine, MonoBehaviour behaviour)
	{
		if (coroutine != null)
			behaviour.StopCoroutine (coroutine);
	}

	private static IEnumerator WaitForSeconds (float time, bool isValable)
	{
		yield return new WaitForSeconds (time);
		isValable = true;
	}


	private static IEnumerator WaitForSeconds (float time, Execute method)
	{
		yield return new WaitForSeconds (time);
		method ();
	}
	private static IEnumerator WaitForSeconds (float time, ExecuteWithGameObjcet method, GameObject GO)
	{
		yield return new WaitForSeconds (time);
		method (GO);
	}
	private static IEnumerator WaitForSeconds (float time, ExecuteWithButton method, Button btn)
	{
		yield return new WaitForSeconds (time);
		method (btn);
	}
}

