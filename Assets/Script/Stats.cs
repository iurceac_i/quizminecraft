﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stats : MonoBehaviour
{
	public GameObject statsPanel;
	public GameObject backButton;
	public Text totalLikes;
	public Text totalFollowers;
	public Text totalLikesPortret;
	public Text totalFollowersPortret;
	public GameObject alertFailPanel;
	public GameObject alertCorrectPanel;
	public GameObject landscapeStatsPanel;
	public GameObject PortraitStatsPanel;
	public SoundController soundController;
	public MainGame mainGame;
	public Button btn_back_1;
	public Button btn_back_2;
	string pathImageStats;
	public Image imageStats;
	public Image imagePortraitStats;
	public Sprite defaultImage;

	public void StoreMenu ()
{
		soundController.OpenWindow ();
		pathImageStats = PlayerPrefs.GetString ("pathImageStats");
		if (pathImageStats == "" || pathImageStats.Length == 0) {	
			imageStats.sprite = defaultImage;
			imagePortraitStats.sprite = defaultImage;
		} else {
			StartCoroutine (LoadImageStats (pathImageStats, imageStats));
			StartCoroutine (LoadImageStats (pathImageStats, imagePortraitStats));
		}
		if ((Input.deviceOrientation == DeviceOrientation.Portrait)) {
			landscapeStatsPanel.SetActive (false);
			Handler.StartDelayed (1f, PortretEnable, this);
		} else {
			PortraitStatsPanel.SetActive (false);
			Handler.StartDelayed (1f, LanscapeEnable, this);	
		}
		if (Screen.width > Screen.height) {
			PortraitStatsPanel.SetActive (false);
			Handler.StartDelayed (1f, LanscapeEnable, this);	
		} else {
			landscapeStatsPanel.SetActive (false);
			Handler.StartDelayed (1f, PortretEnable, this);
		}

		mainGame.InactiveButtons ();
		backButton.SetActive (true);
		statsPanel.SetActive (true);
		totalFollowers.text = (PlayerPrefs.GetInt ("CorrectAnswer", 0).ToString ());
		totalLikes.text = (PlayerPrefs.GetInt ("FailAnswer", 0).ToString ());
		totalLikesPortret.text = (PlayerPrefs.GetInt ("CorrectAnswer", 0).ToString ());
		totalFollowersPortret.text = (PlayerPrefs.GetInt ("FailAnswer", 0).ToString ());
	}

	public void activeAlert (GameObject go)
	{
		go.SetActive (true);
		soundController.OpenWindow ();
	}

	public void CloseAlert (GameObject go)
	{
		go.SetActive (false);
		soundController.CloseWindow ();
	}

	public void BackButtStats (GameObject gamePanel)
	{
		offBackButt ();
		Handler.StartDelayed (1f, onBackButt, this);
		Animator anim = gamePanel.GetComponent <Animator> ();
		anim.SetTrigger ("Hide");
		Handler.StartDelayed (0.1f, mainGame.ActiveButtons, this);
		Handler.StartDelayed (0.4f, soundController.CloseWindow, this);
		Handler.StartDelayed (1f, DisablePanel, gamePanel, this);
	}

	public void DisablePanel (GameObject go)
	{
		go.SetActive (false);
	}

	public void LanscapeEnable ()
	{
//		Handler.StartDelayed (0.5f,soundController.OpenWindow,this);
		landscapeStatsPanel.SetActive (true);
	}

	public void PortretEnable ()
	{
		PortraitStatsPanel.SetActive (true);
	}

	public void onBackButt ()
	{
		btn_back_2.interactable = true;
		btn_back_1.interactable = true;
	}

	public void offBackButt ()
	{
		btn_back_2.interactable = false;
		btn_back_1.interactable = false;
	}

	public void ActiveMyStatsSong ()
	{
		Handler.StartDelayed (0.6f, soundController.OpenWindow, this);
	}

	private IEnumerator LoadImageStats (string path, Image output)
	{

		var url = "file://" + path;
		var www = new WWW (url);
		yield return www;
		var texture = www.texture;
		if (texture == null) {
			Debug.LogError ("Failed to load texture url:" + url);
		}

		output.sprite = Sprite.Create (www.texture, new Rect (0, 0, www.texture.width, www.texture.height), new Vector2 (0, 0));
	}
}
