#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("RUpFT01YRUNCDG1ZWERDXkVYVR0criiXHK4vj4wvLi0uLi0uHCEqJac1pfLVZ0DZK4cOHC7ENBLUfCX/9RpT7at59Yu1lR5u1/T5XbJSjX6uLSwqJQaqZKrbT0gpLRyt3hwGKuU1XtlxIvlTc7feCS+WeaNhcSHdI7ER3wdlBDbk0uKZlSL1cjD65xERCksMph9G2yGu4/LHjwPVf0Z3SCksL64tIywcri0mLq4tLSzIvYUl7E8fW9sWKwB6x/YjDSL2ll81Y5koKj8ueX8dPxw9Ki95KCY/Jm1cXGX0WrMfOEmNW7jlAS4vLSwtj64tJHIcri09Ki95MQwori0kHK4tKBxcQEkMfkNDWAxvbRwyOyEcGhwYHoePXb5rf3ntgwNtn9TXz1zhyo9gubJWIIhrp3f4Ohsf5+gjYeI4Rf0kByotKSkrLi06MkRYWFxfFgMDWwxvbRyuLQ4cISolBqpkqtshLS0tGrVgAVSbwaC38N9bt95a/lscY+0DHK3vKiQHKi0pKSsuLhytmjatnyEqJQaqZKrbIS0tKSksL64tLSxwWERDXkVYVR06HDgqL3koLz8hbVwMTUJIDE9JXlhFSkVPTVhFQ0IMXFUMTV9fWUFJXwxNT09JXFhNQk9Jo1+tTOo3dyUDvp7UaGTcTBSyOdkMQ0oMWERJDFhESUIMTVxcQEVPTTO99zJrfMcpwXJVqAHHGo57YHnAU22EtNX95kqwCEc9/I+XyDcG7zMzqa+pN7URaxvehbdsogD4nbw+9DocOCoveSgvPyFtXFxASQx+Q0NYmRaB2CMiLL4nnQ06Alj5ECH3TjoCbIrba2FTJHIcMyoveTEPKDQcOpLYX7fC/kgj51VjGPSOEtVU00fknRx0wHYoHqBEn6Mx8klf00tySZB1iyklUDtsej0yWP+bpw8Xa4/5QwjOx/2bXPMjac0L5t1BVMHLmTs7HD0qL3koJj8mbVxcQEkMZUJPAh1+SUBFTUJPSQxDQgxYREVfDE9JXlxASQxvSV5YRUpFT01YRUNCDG1ZXk1PWEVPSQxfWE1YSUFJQlhfAhxWHK4tWhwiKi95MSMtLdMoKC8uLYTwUg4Z5gn59SP6R/iOCA89242AS6MkmAzb54AADENcmhMtHKCbb+MZHh0YHB8adjshHxkcHhwVHh0YHKw4B/xFa7haJdLYR6ECbIrba2FTHxp2HE4dJxwlKi95KCo/Lnl/HT8rwFEVr6d/DP8U6J2TtmMmR9MH0FhFSkVPTVhJDE5VDE1CVQxcTV5YaVIzYEd8um2l6FhOJzyvbasfpq1IGQ85Zzl1MZ+429qwsuN8lu10fEJIDE9DQkhFWEVDQl8MQ0oMWV9JQEkMZUJPAh0KHAgqL3koJz8xbVwqHCMqL3kxPy0t0ygpHC8tLdMcMQAMT0leWEVKRU9NWEkMXENARU9VKi95MSIoOig4B/xFa7haJdLYR6EGqmSq2yEtLSkpLBxOHSccJSoveVtbAk1cXEBJAk9DQQNNXFxASU9NmzeRv24IPgbrIzGaYbByT+RnrDtOQEkMX1hNQkhNXkgMWEleQV8MTQocCCoveSgnPzFtXFxASQxvSV5YfIam+fbI0PwlKxucWVkN");
        private static int[] order = new int[] { 11,11,16,58,34,19,22,56,14,57,16,27,33,22,24,18,35,22,48,37,47,37,45,40,55,41,48,45,49,33,40,31,50,48,52,53,57,38,52,46,49,54,52,56,50,55,51,51,54,51,56,58,54,56,58,55,57,59,58,59,60 };
        private static int key = 44;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
