﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Kakera
{
    public class PickerController : MonoBehaviour
    {
        [SerializeField]
        private Unimgpicker imagePicker;

        [SerializeField]
		private Image imageRenderer;
		[SerializeField]
		private Image imagePortretRenderer;
        void Awake()
        {
            imagePicker.Completed += (string path) =>
            {
                StartCoroutine(LoadImage(path, imageRenderer));
				StartCoroutine(LoadImage(path, imagePortretRenderer));
            };
        }

        public void OnPressShowPicker()
        {
            imagePicker.Show("Select Image", "unimgpicker", 1024);
        }

		private IEnumerator LoadImage(string path, Image output)
        {
			PlayerPrefs.SetString ("pathImageStats",path);
			Debug.Log ("path " + path);
            var url = "file://" + path;
            var www = new WWW(url);
            yield return www;
			Debug.Log ("www " + www);
            var texture = www.texture;
            if (texture == null)
            {
                Debug.LogError("Failed to load texture url:" + url);
            }

			output.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
    }
}